<?php

use App\Notifications\UserHasNotPostedInAWhile;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/testnotify', function () {
	$users = App\User::get();
	
	foreach ($users as $user) {
    		if(($user->verified == 1) && ($user->posts->count() != 0)) {

                $last_post = $user->posts()->orderBy('created_at', 'desc')->first();
                $last_post_date = $last_post->created_at;
                $last_post_date->addWeek();
                $now = Carbon::now();

                if($last_post_date->lte($now)) {
                	$user->notify(new UserHasNotPostedInAWhile($user, $last_post));
                }
    		}
    	} 
});

Route::get('/testnexmo', function () {
	$nexmo = app('Nexmo\Client');
	$nexmo->message()->send([
	    'to' => '2348062680306',
	    'from' => '2348062680306',
	    'text' => 'Welcome to Prtals, start enlightening others with insightful posts in your best subject or field of study'
	]); 
});
// main
Route::get('/', 'MainController@index');
Route::get('/guide', 'MainController@guide');
Route::get('/faq', 'MainController@faq');

// school
Route::post('/school_cover_image', 'SchoolController@uploadCoverImage');
Route::post('/school_logo', 'SchoolController@uploadLogo');
Route::get('/schools', 'SchoolController@allSchools');
Route::get('/choose_school', 'SchoolController@chooseSchool');
Route::post('/attach_school', 'SchoolController@attachSchool');
Route::post('/choose_school_by_state', 'SchoolController@chooseByState');
Route::post('/schools_by_state', 'SchoolController@filterAllByState');
Route::get('/new_school_request', 'SchoolController@newSchoolRequest');
Route::post('/new_school_request', 'SchoolController@submitNewSchoolRequest');
Route::get('/add_new_school', 'SchoolController@adminNewSchoolForm');
Route::post('/add_new_school', 'SchoolController@adminAddNewSchool');
Route::get('/schools/{slug}', 'SchoolController@show')->name('schools.show');;
Route::get('/schools/details/{slug}/edit', 'SchoolController@editDetails');
Route::post('/schools/details/{slug}/edit', 'SchoolController@updateDetails');
Route::get('/schools/{slug}/members', 'SchoolController@members');
Route::get('/schools/{slug}/posts', 'SchoolController@posts');
Route::get('/schools/{slug}/posts/mine', 'SchoolController@mySchoolPosts');
Route::get('/schools/{slug}/events', 'SchoolController@events');
Route::get('/schools/{slug}/events/mine', 'SchoolController@mySchoolEvents');
Route::get('/schools/{slug}/news', 'SchoolController@news');
Route::get('/schools/{slug}/news/mine', 'SchoolController@mySchoolNews');

// university
Route::post('/university_cover_image', 'UniversityController@uploadCoverImage');
Route::post('/university_logo', 'UniversityController@uploadLogo');
Route::get('/universities', 'UniversityController@allUniversities');
Route::get('/choose_university', 'UniversityController@chooseUniversity');
Route::post('/attach_university', 'UniversityController@attachUniversity');
Route::post('/choose_university_by_state', 'UniversityController@chooseByState');
Route::post('/universities_by_state', 'UniversityController@filterAllByState');
Route::get('/new_university_request', 'UniversityController@newUniversityRequest');
Route::post('/new_university_request', 'UniversityController@submitNewUniversityRequest');
Route::get('/add_new_university', 'UniversityController@adminNewUniversityForm');
Route::post('/add_new_university', 'UniversityController@adminAddNewUniversity');
Route::get('/universities/{slug}', 'UniversityController@show')->name('universities.show');;
Route::get('/universities/details/{slug}/edit', 'UniversityController@editDetails');
Route::post('/universities/details/{slug}/edit', 'UniversityController@updateDetails');
Route::get('/universities/{slug}/members', 'UniversityController@members');
Route::get('/universities/{slug}/posts', 'UniversityController@posts');
Route::get('/universities/{slug}/posts/mine', 'UniversityController@myUniversityPosts');
Route::get('/universities/{slug}/events', 'UniversityController@events');
Route::get('/universities/{slug}/events/mine', 'UniversityController@myUniversityEvents');
Route::get('/universities/{slug}/news', 'UniversityController@news');
Route::get('/universities/{slug}/announcements', 'UniversityController@announcements');
Route::get('/universities/{slug}/news/mine', 'UniversityController@myUniversityNews');

// subject
Route::get('/subjects', 'SubjectController@index');
Route::get('/add_new_subject', 'SubjectController@adminNewSubjectForm');
Route::post('/add_new_subject', 'SubjectController@adminAddNewSubject');

// category
Route::get('/categories', 'CategoryController@index');

// field
Route::get('/fields', 'FieldController@index');
Route::get('/add_new_field', 'FieldController@adminNewFieldForm');
Route::post('/add_new_field', 'FieldController@adminAddNewField');

// role 
Route::get('/choose_role', 'RoleController@chooseRole');
Route::post('/attach_role', 'RoleController@attachRole');
Route::get('/dashboard', 'RoleController@dashboard');
Route::get('/home', 'RoleController@dashboard');

// user profile
Route::get('/profile', 'UserController@profile');
Route::post('/profile/details/{username}/edit', 'UserController@updateDetails');
Route::get('/profile/details/{username}/edit', 'UserController@editDetails');
Route::post('/profile/bio/{user_id}/edit', 'UserController@updateBio');
Route::post('/profile/socials/{user_id}/edit', 'UserController@updateSocials');
Route::get('/profile/username/{username}/check', 'UserController@checkUsername');
Route::post('/profile/image_upload', 'UserController@uploadProfileImage');
Route::get('/profile/{username}', 'UserController@viewUser');

//news
Route::post('/news/{slug}', 'NewsController@update');
Route::resource('/news', 'NewsController');
Route::get('/news/school/{slug}', 'NewsController@school');
Route::get('/news/university/{slug}', 'NewsController@university');
Route::get('/news/user/{username}', 'NewsController@user');

//announcements
Route::post('/announcements/{slug}', 'AnnouncementController@update');
Route::resource('/announcements', 'AnnouncementController');
Route::get('/announcements/school/{slug}', 'AnnouncementController@school');
Route::get('/announcements/university/{slug}', 'AnnouncementController@university');
Route::get('/announcements/user/{username}', 'AnnouncementController@user');

//events
Route::post('/events/{slug}', 'EventController@update');
Route::post('/events/{slug}/add_photos', 'EventController@addPhotos');
Route::get('/events/{slug}/add_photos', 'EventController@photos');
Route::resource('/events', 'EventController');
Route::get('/events/school/{slug}', 'EventController@school');
Route::get('/events/university/{slug}', 'EventController@university');
Route::get('/events/user/{username}', 'EventController@user');

//student
Route::get('/choose_student_type', 'StudentController@chooseType');
Route::resource('/student', 'StudentController');

//undergraduate
Route::resource('/undergraduate', 'UndergraduateController');

// teacher
Route::get('/teacher/manage_subjects', 'TeacherController@viewSubjects');
Route::post('/teacher/add_subject', 'TeacherController@addSubject');
Route::post('/teacher/remove_duty', 'TeacherController@removeDuty');
Route::resource('/teacher', 'TeacherController');

// authority
Route::resource('/authority', 'AuthorityController');
Route::get('/add_new_member', 'AuthorityController@newMemberRequest');
Route::post('/add_new_member', 'AuthorityController@addNewMember');

// Posts
Route::post('/posts/{slug}/update', 'PostController@update');
Route::resource('/posts', 'PostController');
Route::get('/posts/like/{id}', 'LikeController@likePost');
Route::get('/posts/school/{slug}', 'PostController@school');
Route::get('/posts/university/{slug}', 'PostController@university');
Route::get('/posts/user/{username}', 'PostController@user');
Route::get('/posts/category/{category}', 'PostController@category');
Route::get('/posts/subject/{category}', 'PostController@subject');
Route::get('/posts/field/{category}', 'PostController@field');

// Messages
Route::post('/messages/{id}/add', 'MessageController@update');
Route::resource('/messages', 'MessageController');

// Comments
Route::resource('/comments', 'CommentController');
Route::get('/comments/like/{post_id}/{comment_id}', 'LikeController@likeComment');


// Authentication Routes...
Route::get('signin', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('signin', 'Auth\LoginController@login');
//Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('signup', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('signup', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('/logout', function(){
	Auth::logout();
	return redirect('/');
});



// notifications
Route::delete('users/{user}/notifications', 'NotificationController@markAsRead');
Route::resource('/notifications', 'NotificationController');
Route::post('clear_notifications', 'NotificationController@deleteAll');

// search
Route::get('/search', 'SearchController@all');
Route::get('/search/schools_only', 'SearchController@schoolsOnly');
Route::get('/search/universities_only', 'SearchController@universitiesOnly');
Route::get('/search/{type}', 'SearchController@typeAndRequest');
Route::get('/search/{type}/{query}', 'SearchController@typeAndQuery');

// Api
Route::group(['prefix' => 'api/v1', 'namespace' => 'Api\V1', 'middleware' => 'cors'], function () {
	// posts
	Route::get('/posts', 'PostController@index');
	Route::get('/posts/{id}', 'PostController@show');
	Route::post('/posts/store', 'PostController@store');
	Route::get('/posts/user/{user_id}', 'PostController@user');

	// comments
	Route::post('/comments/store', 'CommentController@store');

	// likes
	Route::post('/likes/store', 'LikeController@store');

	// goodybag
	Route::get('/goodybag/posts-subjects', 'GoodyBagController@getPostsSubjects');
	Route::get('/goodybag/posts-fields', 'GoodyBagController@getPostsFields');
	Route::get('/goodybag/posts-categories', 'GoodyBagController@getPostsCategories');

	// user/login/signin
	Route::resource('/users', 'UserController');
	Route::post('/signin', 'LoginController@login');
});
