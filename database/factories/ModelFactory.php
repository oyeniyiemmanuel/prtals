<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});*/

$factory->define(App\School::class, function (Faker\Generator $faker){

	return [
		'name' => $faker->name,
		'founded' => $faker->date,
		'state' => $faker->state,
		'address' => $faker->streetAddress,
		'uniform' => $faker->safeColorName,
		'no_of_pupils' => $faker->numberBetween($min = 10, $max = 90),
		'no_of_teachers' => $faker->numberBetween($min = 10, $max = 90),
		'no_of_other_staffs' => $faker->numberBetween($min = 10, $max = 90),
		'no_of_classes' => $faker->numberBetween($min = 10, $max = 90),
		'history' => $faker->text($maxNbChars = 100),
		'tuition' => $faker->text($maxNbChars = 100),
		'rules' => $faker->text($maxNbChars = 100),
		'anthem' => $faker->text($maxNbChars = 100),
		'accomplishments' => $faker->text($maxNbChars = 100),
	];
});

$factory->define(App\Authority::class, function (Faker\Generator $faker){

	return [
		'reg_no' => $faker->numberBetween($min = 1, $max = 200),
		'school_id' => $faker->numberBetween($min = 0, $max = 10),
	];
});

$factory->define(App\Subject::class, function (Faker\Generator $faker){

	return [
		'name' => $faker->state,
	];
});

$factory->define(App\Grade::class, function (Faker\Generator $faker){

	return [
		'name' => $faker->state,
	];
});

$factory->define(App\Department::class, function (Faker\Generator $faker){

	return [
		'name' => $faker->state,
	];
});
