<?php

use Illuminate\Database\Seeder;

class AuthoritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Authority::class)->create([
        	'reg_no' => '123'
        	,'school_id' => '1'
        ]);
    }
}
