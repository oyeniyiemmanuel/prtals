<?php

use Illuminate\Database\Seeder;

class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Grade::class)->create([
        	'name' => 'jss1'
        ]);

        factory(App\Grade::class)->create([
        	'name' => 'jss2'
        ]);

        factory(App\Grade::class)->create([
        	'name' => 'jss3'
        ]);

        factory(App\Grade::class)->create([
        	'name' => 'sss1'
        ]);

        factory(App\Grade::class)->create([
        	'name' => 'sss2'
        ]);

        factory(App\Grade::class)->create([
        	'name' => 'sss3'
        ]);
    }
}
