<?php

use Illuminate\Database\Seeder;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\School::class, 15)->create([
        	'state' => 'lagos'
        ]);

        factory(App\School::class, 15)->create([
        	'state' => 'abia'
        ]);
    }
}
