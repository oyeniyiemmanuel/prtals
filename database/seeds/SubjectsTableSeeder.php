<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Subject::class)->create([
        	'name' => 'mathematics'
        ]);

        factory(App\Subject::class)->create([
        	'name' => 'physics'
        ]);

        factory(App\Subject::class)->create([
        	'name' => 'biology'
        ]);

        factory(App\Subject::class)->create([
        	'name' => 'english'
        ]);

        factory(App\Subject::class)->create([
        	'name' => 'chemistry'
        ]);

        factory(App\Subject::class)->create([
        	'name' => 'government'
        ]);

        factory(App\Subject::class)->create([
        	'name' => 'general'
        ]);
    }
}
