<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Department::class)->create([
        	'name' => 'art'
        ]);

        factory(App\Department::class)->create([
        	'name' => 'commercial'
        ]);

        factory(App\Department::class)->create([
            'name' => 'science'
        ]);

        factory(App\Department::class)->create([
        	'name' => 'junior-high-school'
        ]);
    }
}
