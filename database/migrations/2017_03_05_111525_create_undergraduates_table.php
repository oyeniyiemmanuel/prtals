<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUndergraduatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('undergraduates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_no')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('university_id')->nullable();
            $table->string('gender')->nullable();
            $table->string('best_course')->nullable();
            $table->date('d_o_b')->nullable();
            $table->string('state_of_origin')->nullable();
            $table->string('local_gov')->nullable();
            $table->text('address')->nullable();
            $table->text('residential_state')->nullable();
            $table->string('phone')->nullable();
            $table->text('bio')->nullable();
            $table->string('g_plus')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('undergraduates');
    }
}
