<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePrincipalFieldsFromNewSchoolRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_school_requests', function (Blueprint $table) {

            $table->dropColumn('principal_name');
            $table->dropColumn('principal_phone');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->string('principal_name')->nullable();
        $table->string('principal_phone')->nullable();
    }
}
