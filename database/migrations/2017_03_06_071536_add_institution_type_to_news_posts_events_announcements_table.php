<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInstitutionTypeToNewsPostsEventsAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {

            $table->string('institution_type')->default('school')->nullable();
            $table->string('institution_type_plural')->default('schools')->nullable();

        });
        Schema::table('posts', function (Blueprint $table) {

            $table->string('institution_type')->default('school')->nullable();
            $table->string('institution_type_plural')->default('schools')->nullable();

        });
        Schema::table('events', function (Blueprint $table) {

            $table->string('institution_type')->default('school')->nullable();
            $table->string('institution_type_plural')->default('schools')->nullable();

        });
        Schema::table('announcements', function (Blueprint $table) {

            $table->string('institution_type')->default('school')->nullable();
            $table->string('institution_type_plural')->default('schools')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {

            $table->dropColumn('institution_type');
            $table->dropColumn('institution_type_plural');
            
        });
        Schema::table('posts', function (Blueprint $table) {

            $table->dropColumn('institution_type');
            $table->dropColumn('institution_type_plural');
            
        });
        Schema::table('events', function (Blueprint $table) {

            $table->dropColumn('institution_type');
            $table->dropColumn('institution_type_plural');
            
        });
        Schema::table('announcements', function (Blueprint $table) {

            $table->dropColumn('institution_type');
            $table->dropColumn('institution_type_plural');
            
        });
    }
}
