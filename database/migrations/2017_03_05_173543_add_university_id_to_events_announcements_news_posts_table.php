<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniversityIdToEventsAnnouncementsNewsPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {

            $table->integer('university_id')->nullable();

        });
        Schema::table('announcements', function (Blueprint $table) {

            $table->integer('university_id')->nullable();

        });
        Schema::table('news', function (Blueprint $table) {

            $table->integer('university_id')->nullable();

        });
        Schema::table('posts', function (Blueprint $table) {

            $table->integer('university_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {

            $table->dropColumn('university_id');
            
        });
        Schema::table('announcements', function (Blueprint $table) {

            $table->dropColumn('university_id');
            
        });
        Schema::table('news', function (Blueprint $table) {

            $table->dropColumn('university_id');
            
        });
        Schema::table('posts', function (Blueprint $table) {

            $table->dropColumn('university_id');
            
        });
    }
}
