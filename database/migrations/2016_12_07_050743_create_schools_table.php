<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->unique()->nullable();
            $table->date('founded')->nullable();
            $table->string('state')->nullable();
            $table->string('address')->nullable();
            $table->string('uniform')->nullable();
            $table->integer('no_of_pupils')->nullable();
            $table->integer('no_of_teachers')->nullable();
            $table->integer('no_of_other_staffs')->nullable();
            $table->integer('no_of_classes')->nullable();
            $table->text('history')->nullable();
            $table->text('tuition')->nullable();
            $table->text('rules')->nullable();
            $table->text('anthem')->nullable();
            $table->text('accomplishments')->nullable();
            $table->string('last_edited_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
