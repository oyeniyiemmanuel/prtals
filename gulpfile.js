const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    
    mix.styles([
    	'/semantic/dist/semantic.min.css'
    	,'/css/semantic-custom.css'
    	,'/css/sweetalert.css'
    	,'/css/dropzone.css'
    	,'/css/photoswipe.css'
    	,'/css/photoswipe-default-skin.css'
    	,'/css/prtals-style.css'
    ], 'public/load/prtals-main.css', 'public');

    mix.scripts([
    	'/js/dropzone'
    	,'/js/jquery-3.1.1.min.js'
    	,'/js/imagesloaded.pkgd.min.js'
    	,'/js/masonry.pkgd.min.js'
    	,'/js/jquery-input-file-text.js'
    	,'/semantic/dist/semantic.min.js'
    	,'/js/semantic-custom.js'
    	,'/js/sweetalert-dev.js'
    	,'/js/jquery.photoswipe-global.js'
    	,'/js/jquery-ias.min.js'
    	,'/js/prtals-custom.js'
    ], 'public/load/prtals-main.js', 'public');

    mix.version(['public/load/prtals-main.css', 'public/load/prtals-main.js']);

});
