<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Event extends Model
{
    use Searchable;
    
    protected $fillable = [
    	'user_id'
    	,'slug'
        ,'school_id'
    	,'university_id'
    	,'title'
    	,'message'
        ,'institution_type'
        ,'institution_type_plural'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function school(){
        return $this->belongsTo('App\School');
    }

    public function university(){
        return $this->belongsTo('App\University');
    }

    public function pictures(){
        return $this->hasMany('App\Event_picture');
    }
}
