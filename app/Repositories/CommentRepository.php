<?php
namespace App\Repositories;

use App\Repositories\Interfaces\CommentInterface;
use App\Notifications\NewCommentOnPost;
use Illuminate\Http\Request;
use App\Comment;
use App\Post;
use App\User;
use Auth;
use DB;

class CommentRepository implements CommentInterface
{
	public function store(Request $request)
	{
		$comment = Comment::create([
				'message' => $request->get('message')
			]);

		return $comment;
	}

	public function syncWithUser($comment)
	{
		$comment->user()->associate(Auth::user())->save();
	}

	public function apiSyncWithUser($comment, Request $request)
	{
		$comment->user()->associate(User::findOrFail($request->get('user_id')))->save();
	}

	public function syncWithPost($comment, Request $request)
	{
		$this->notifyPostOwner($comment, $request);
		
		$comment->post()->associate($request->get('post_id'))->save();
	}

	public function apiSyncWithPost($comment, Request $request)
	{
		$this->apiNotifyPostOwner($comment, $request);
		
		$comment->post()->associate($request->get('post_id'))->save();
	}

	public function notifyPostOwner($comment, Request $request)
	{
		$post = Post::findOrFail($request->get('post_id'));

		$owner = $post->user;

		$commenter = Auth::user();

		// notify only if the logged in user is not the owner
		if(Auth::id() !== $owner->id){
			$owner->notify(new NewCommentOnPost($post, $commenter, $comment));
		}
	}

	public function apiNotifyPostOwner($comment, Request $request)
	{
		$post = Post::findOrFail($request->get('post_id'));

		$owner = $post->user;

		$commenter = User::findOrFail($request->get('user_id'));

		// notify only if the logged in user is not the owner
		if($commenter->id !== $owner->id){
			$owner->notify(new NewCommentOnPost($post, $commenter, $comment));
		}
	}

	public function getPost($comment)
	{
		return $comment->post()->get()->first();
	}

	public function getComment($comment_id)
	{
		return $comment = Comment::findOrFail($comment_id);
	}

	public function delete(Request $request)
	{
		Comment::findOrFail($request->get('comment_id'))->delete();

		DB::table('likeables')->where([
				['likeable_id', '=', $request->get('comment_id')]
				,['likeable_type', '=', 'App\Comment']
			])
			->delete();
	}
}