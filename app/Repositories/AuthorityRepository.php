<?php
namespace App\Repositories;

use App\Repositories\Interfaces\AuthorityInterface;
use Illuminate\Http\Request;
use App\Authority;
use Auth;

class AuthorityRepository implements AuthorityInterface
{
	public function getMySchool()
	{
		return Auth::user()->school;
	}
	public function addNewMember(Request $request)
	{
		$result = array();

		$user = Auth::user();

		if($user->hasRole('authority') || $user->hasRole('school-admin')){
			
			// get the user model e.g "App\Student" or "App\Teacher"
			$model = "App\\".ucfirst($request->get('role'));

			$reg_no = $request->get('reg_no');

			$school_id = $request->get('school_id');
			// check if the user has been given a reg no. that matches the school id
			$check = $model::where([
								['reg_no', '=', $reg_no]
								,['school_id', '=', $school_id]
							])->get()->first();

			if(!$check){
				$new_member = $model::firstOrCreate([
					'reg_no' => $reg_no
					,'school_id' => $school_id
				]);

				$new_member->added_by = Auth::id();
				$new_member->save();

				$result["message"] = "New ".$request->get('role')." member added";	
				$result["status"] = true;	

				return $result;
			}

			$result["message"] = $request->get('role')." with the reg no exists";	
			$result["status"] = false;	

			return $result;
		}

		$result["message"] = "you don not have permission";	
		$result["status"] = false;	

		return $result;	

	}
}