<?php
namespace App\Repositories;

use App\Repositories\Interfaces\SubjectInterface;
use Illuminate\Http\Request;
use App\Subject;
use App\Department;
use App\Grade;

class SubjectRepository implements SubjectInterface
{
	public function adminAddNewSubject(Request $request)
	{

		$subject = Subject::firstOrCreate([
					'name' => str_replace(' ', '-', strtolower($request->get('subject')))
				]);

		if($request->get('department') == 'art_and_commercial'){

			$departments = Department::whereIn('name',['art', 'commercial'])->get();

			$grades = Grade::whereIn('name',['sss1', 'sss2', 'sss3'])->get();

			foreach($departments as $department){

				$subject->departments()->syncWithoutDetaching([$department->id]);
			}

			foreach($grades as $grade){

				$subject->grades()->syncWithoutDetaching([$grade->id]);
			}

		}elseif($request->get('department') == 'all_senior_high'){

			$departments = Department::whereIn('name',['art', 'commercial', 'science'])->get();

			$grades = Grade::whereIn('name',['sss1', 'sss2', 'sss3'])->get();

			foreach($departments as $department){

				$subject->departments()->syncWithoutDetaching([$department->id]);
			}

			foreach($grades as $grade){

				$subject->grades()->syncWithoutDetaching([$grade->id]);
			}

		}elseif($request->get('department') == 'general'){

			$departments = Department::get();

			$grades = Grade::get();

			foreach($departments as $department){

				$subject->departments()->syncWithoutDetaching([$department->id]);
			}

			foreach($grades as $grade){

				$subject->grades()->syncWithoutDetaching([$grade->id]);
			}

		}else{

			$department = Department::where('name', '=', $request->get('department'))->get()->first();

			$subject->departments()->sync([$department->id]);

				if($request->get('department') == 'junior-high-school'){

					$grades = Grade::whereIn('name',['jss1', 'jss2', 'jss3'])->get();

					foreach($grades as $grade){

						$subject->grades()->syncWithoutDetaching([$grade->id]);
					}
				}else{
					$grades = Grade::whereIn('name', ['sss1', 'sss2', 'sss3'])->get();

					foreach($grades as $grade){

						$subject->grades()->syncWithoutDetaching([$grade->id]);
					}
				}

		}

	}
}