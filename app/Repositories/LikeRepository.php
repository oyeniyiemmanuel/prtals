<?php
namespace App\Repositories;

use App\Repositories\Interfaces\LikeInterface;
use App\Notifications\NewLikeOnPost;
use App\Notifications\NewLikeOnComment;
use Illuminate\Http\Request;
use App\Like;
use App\Post;
use App\User;
use App\Comment;
use Auth;

class LikeRepository implements LikeInterface
{
    public function getUser($user_id)
    {
        return $user = User::findOrFail($user_id);
    }

	public function getPost($post_id)
	{
        return $post = Post::findOrFail($post_id);
	}

    public function likePost($id)
    {
        $this->handlePostLike('App\Post', $id);

        return $post = Post::findOrFail($id);
    }

	public function apiLikePost($post_id, $user_id)
	{
		$this->apiHandlePostLike('App\Post', $post_id, $user_id);

        return $post = Post::with('user.picture', 'school', 'university', 'subject', 'field', 'comments', 'likes')->findOrFail($post_id);
	}

	public function likeComment($comment_id, $post_id)
	{
		$this->handleCommentLike('App\Comment', $comment_id, $post_id);

        return $comment = Comment::findOrFail($comment_id);
	}

    public function handlePostLike($type, $id)
    {
        $existing_like = Like::withTrashed()->whereLikeableType($type)->whereLikeableId($id)->whereUserId(Auth::id())->first();

        if (is_null($existing_like)) {
            Like::create([
                'user_id'       => Auth::id(),
                'likeable_id'   => $id,
                'likeable_type' => $type,
            ]);

            //send notifications
            $this->notifyPostOwner($id);

        } else {
            if (is_null($existing_like->deleted_at)) {
                $existing_like->delete();
            } else {
                $existing_like->restore();
            }
        }
    }

	public function apiHandlePostLike($type, $post_id, $user_id)
    {
        $existing_like = Like::withTrashed()->whereLikeableType($type)->whereLikeableId($post_id)->whereUserId($user_id)->first();

        if (is_null($existing_like)) {
            Like::create([
                'user_id'       => $user_id,
                'likeable_id'   => $post_id,
                'likeable_type' => $type,
            ]);

            //send notifications
        	$this->apiNotifyPostOwner($user_id);

        } else {
            if (is_null($existing_like->deleted_at)) {
                $existing_like->delete();
            } else {
                $existing_like->restore();
            }
        }
    }

	public function handleCommentLike($type, $comment_id, $post_id)
    {
        $existing_like = Like::withTrashed()->whereLikeableType($type)->whereLikeableId($comment_id)->whereUserId(Auth::id())->first();

        if (is_null($existing_like)) {
            Like::create([
                'user_id'       => Auth::id(),
                'likeable_id'   => $comment_id,
                'likeable_type' => $type,
            ]);

            //send notifications
        	$this->notifyCommentOwner($comment_id, $post_id);

        } else {
            if (is_null($existing_like->deleted_at)) {
                $existing_like->delete();
            } else {
                $existing_like->restore();
            }
        }
    }

    public function notifyPostOwner($id)
    {
        $post = Post::findOrFail($id);

        $owner = $post->user;

        $liker = Auth::user();

        // notify only if the logged in user is not the owner
        if(Auth::id() !== $owner->id){
            $owner->notify(new NewLikeOnPost($post, $liker));
        }
    }

    public function apiNotifyPostOwner($user_id)
    {
    	$post = Post::findOrFail($user_id);

    	$owner = $post->user;

    	$liker = User::findOrFail($user_id);

    	// notify only if the logged in user is not the owner
    	if($liker->id !== $owner->id){
    		$owner->notify(new NewLikeOnPost($post, $liker));
    	}
    }

    public function notifyCommentOwner($comment_id, $post_id)
    {
    	$comment = Comment::findOrFail($comment_id);

    	$post = Post::findOrFail($post_id);

    	$owner = $comment->user;

    	$liker = Auth::user();

    	// notify only if the logged in user is not the owner
    	if(Auth::id() !== $owner->id){
    		$owner->notify(new NewLikeOnComment($post, $comment, $liker));
    	}
    }
}