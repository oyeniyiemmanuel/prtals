<?php
namespace App\Repositories;

use App\Repositories\Interfaces\UserInterface;
use Illuminate\Http\Request;
use App\User;
use App\Student;
use App\Undergraduate;
use App\Teacher;
use App\Authority;
use App\Grade;
use App\Department;
use App\School;
use App\Picture;
use App\Wing;
use Auth;
use Cloudder;

class UserRepository implements UserInterface
{
    public function viewUser($username)
    {
        return $user = $this->findByUsername($username);
    }
    public function findByUsername($username)
    {
        return User::where('username', '=', $username)->get()->first();
    }
	public function findById($id)
	{
		return User::with('picture', 'student', 'undergraduate', 'school', 'university')->findOrFail($id);
	}

	public function updateDetails(Request $request)
	{
		$user = Auth::user();

        $username = str_replace(' ', '_', $request->get('username'));

		User::findOrFail(Auth::id())->update(['name' => $request->get('name'),'username' => $username]);

        $role = ucfirst($user->roles->first()->name);

        $method = 'update'.$role.'Details';

		return $this->$method($request);

	}

    protected function updateStudentDetails(Request $request)
    {
    	$student = Student::where([
    			['user_id', '=', Auth::id()]
    			,['school_id', '=', Auth::user()->school_id]
    		])->get()->first();

    	$student->update([
    			'gender' => $request->get('gender')
    			,'phone' => $request->get('phone')
    			,'residential_state' => $request->get('residential_state')
    			,'state_of_origin' => $request->get('state_of_origin')
    			,'d_o_b' => $request->get('date_of_birth')
    			,'local_gov' => $request->get('local_gov')
    			,'best_subject' => $request->get('best_subject')
    		]);

    	$grade = Grade::where('name', '=', $request->get('grade'))->get()->first();
        $department = Department::where('name', '=', $request->get('department'))->get()->first();
        $school = School::where('id', '=', Auth::user()->school_id)->get()->first();
    	$wing = Wing::firstOrCreate(['name' => $request->get('wing')]);

        // if user is in junior high
        if($request->get('grade') == 'jss1' || $request->get('grade') == 'jss2' || $request->get('grade') == 'jss3'){

            $junior_department = Department::where('name', '=', 'junior-high-school')->get()->first();
            // attach user to department
            Auth::user()->department()->associate($junior_department)->save();

        }else{
            // attach user to department
            Auth::user()->department()->associate($department)->save();
        }

        // attach user to grade
        Auth::user()->grade()->associate($grade)->save();
        // attach student to department
        $student->department()->associate($department)->save();
        // attach student to grade
        $student->grade()->associate($grade)->save();
        // attach student to wing
        $student->wing()->associate($wing)->save();
        // sync school with grade
        $grade->schools()->syncWithoutDetaching([$school->id]);
        // sync school with department
        $department->schools()->syncWithoutDetaching([$school->id]);
    	
    	return Auth::user();
    }

    protected function updateUndergraduateDetails(Request $request)
    {
        $undergraduate = Undergraduate::where([
                ['user_id', '=', Auth::id()]
                ,['university_id', '=', Auth::user()->university_id]
            ])->get()->first();

        $undergraduate->update([
                'gender' => $request->get('gender')
                ,'phone' => $request->get('phone')
                ,'residential_state' => $request->get('residential_state')
                ,'state_of_origin' => $request->get('state_of_origin')
                ,'d_o_b' => $request->get('date_of_birth')
                ,'local_gov' => $request->get('local_gov')
                ,'best_subject' => $request->get('best_subject')
            ]);

        // $school = School::where('id', '=', Auth::user()->school_id)->get()->first();
        
        // $department->schools()->syncWithoutDetaching([$school->id]);
        
        return Auth::user();
    }

    public function updateTeacherDetails(Request $request)
    {
        $teacher = Teacher::where([
                ['user_id', '=', Auth::id()]
                ,['school_id', '=', Auth::user()->school_id]
            ])->get()->first();

        $teacher->update([
                'gender' => $request->get('gender')
                ,'phone' => $request->get('phone')
                ,'residential_state' => $request->get('residential_state')
                ,'state_of_origin' => $request->get('state_of_origin')
                ,'d_o_b' => $request->get('date_of_birth')
                ,'local_gov' => $request->get('local_gov')
            ]);

        // attach teacher to user
        Auth::user()->teacher()->save($teacher);
    }

    public function updateAuthorityDetails(Request $request)
    {
        $authority = Authority::where([
                ['user_id', '=', Auth::id()]
                ,['school_id', '=', Auth::user()->school_id]
            ])->get()->first();

        $authority->update([
                'gender' => $request->get('gender')
                ,'phone' => $request->get('phone')
                ,'residential_state' => $request->get('residential_state')
                ,'state_of_origin' => $request->get('state_of_origin')
                ,'d_o_b' => $request->get('date_of_birth')
                ,'local_gov' => $request->get('local_gov')
            ]);

        // attach authority to user
        Auth::user()->authority()->save($authority);
    }

    public function updateBio(Request $request)
    {
        $user = Auth::user();

        $institution_type = $user->institution_type;

        $role = $user->roles->first()->name;

        $model = "App\\".ucfirst($role);

        $pupil = $model::where([
                ['user_id', '=', Auth::id()]
                ,[$user->institution_type.'_id', '=', $user->$institution_type->id]
            ])->get()->first();

        $pupil->update(['bio' => $request->get('bio')]);

        return $role;

    }

    public function updateSocials(Request $request)
    {
        $user = Auth::user();

        $institution_type = $user->institution_type;

        $role = $user->roles->first()->name;

        $model = "App\\".ucfirst($role);

        $pupil =  $model::where([
                ['user_id', '=', Auth::id()]
                ,[$user->institution_type.'_id', '=', $user->$institution_type->id]
            ])->get()->first();

        $pupil->update([
            'g_plus' => $request->get('g_plus')
            ,'facebook' => $request->get('facebook')
            ,'twitter' => $request->get('twitter')
            ]);

        return $role;

    }

    public function checkUsername($username)
    {
        $user = User::where('username', '=', $username)->get()->first();

        if($user){

            if($user->id == Auth::id()){

                return 'you';

            }else{
                return 'taken';
            }
            
        }else{
            return 'okay';
        }
    }

    public function uploadProfileImage(Request $request)
    {        
        $file = $request->image;

        list($width, $height, $type, $attr) = getimagesize($file);

        $dimensions = $width . 'x' . $height;
        // get name without extension
        $name_without_extension = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // append current time, prepend dimension and extension to photo name (i.e 1479022764photoname-1280x960.jpeg)
        $name = time() . $name_without_extension . '-' . $dimensions . '.' . $file->extension();
        // upload photo 
        $path = $file->getPathname();

        $options = array(
                       "public_id" => $name,
                       );

        $tags = array( "user", "profile_image" );

        // delete user prev picture on cloud
        if( Auth::user()->picture || !empty(Auth::user()->picture->public_id)){

            Cloudder::destroyImage(Auth::user()->picture->public_id);

        }

        // upload new user profile pic to cloud
        $cloudder = Cloudder::upload($path, $name, $options, $tags);

        $image_result = Cloudder::getResult();

        //check if user has picture
        $picture = Auth::user()->picture;

        if($picture == null){
            // create new user picture
            $new = Picture::create([
                'url' => str_replace('http://', 'https://', $image_result['url'])
                ,'format' => $image_result['format']
                ,'public_id' => $image_result['public_id']
            ]);

        }else{
            // update picture details to db
            $picture->update([
                'url' => str_replace('http://', 'https://', $image_result['url'])
                ,'format' => $image_result['format']
                ,'public_id' => $image_result['public_id']
            ]);

            $new = $picture;
        }

        // attach new picture to user
        Auth::user()->picture()->save($new);
    }
}