<?php
namespace App\Repositories;

use App\Repositories\Interfaces\NewsInterface;
use App\Notifications\NewNewsInMySchool;
use Illuminate\Http\Request;
use App\School;
use App\University;
use App\News;
use App\User;
use Auth;

class NewsRepository implements NewsInterface
{
	public function getAllNews(){

		$news = News::latest()->paginate(10);

		return $news;

	}

	public function getBySlug($slug){

		$news = News::latest()->where('slug', '=', $slug)->get()->first();

		return $news;

	}

	public function findUser($username)
	{
		return User::where('username', '=', $username)->get()->first();
	}

	public function findSchool($slug)
	{
		return School::where('slug', '=', $slug)->get()->first();
	}

	public function findUniversity($slug)
	{
		return University::where('slug', '=', $slug)->get()->first();
	}

	public function getSchoolNews($slug)
	{
		$school_id = School::where('slug', '=', $slug)->get()->first()->id;
		
		$news = News::latest()->where('school_id', '=', $school_id)->latest()->paginate(10);

		return $news;
	}

	public function getUniversityNews($slug)
	{
		$university_id = University::where('slug', '=', $slug)->get()->first()->id;
		
		$news = News::latest()->where('university_id', '=', $university_id)->latest()->paginate(10);

		return $news;
	}

	public function getUserNews($username)
	{
		$user = User::where('username', '=', $username)->get()->first();

		$news = News::latest()->where('user_id', '=', $user->id)->latest()->paginate(10);

		return $news;
	}

	public function store(Request $request)
	{
		$user = Auth::user();
		// create slug
		$slug = $this->makeSlug($request->get('title'));

		// store
		$news = News::firstOrCreate([
				'title' => substr($request->get('title'), 0, 200 )
				,'slug' => $slug
				,'message' => $request->get('message')
			]);

		if ($user->roles()->first()->name == 'undergraduate') {
			$news->institution_type = 'university';
			$news->institution_type_plural = 'universities';
			$news->save();
		}

		$news = $this->getBySlug($news->slug);

		$institution_type = $news->institution_type;
		//sync with user, sync with school
		$news->user()->associate($user)->save();
		$news->$institution_type()->associate($user->$institution_type)->save();
		// notify school members
		$this->notifySchoolMembers($news);

	}

	public function notifySchoolMembers($news)
	{
		$institution_type = $news->institution_type;
		// get all school members
		$members = Auth::user()->$institution_type->users;

		foreach( $members as $member ){
			// notify all members except the logged in user
			if( Auth::id() !== $member->id ){

				$member->notify(new NewNewsInMySchool( $news, $news->user ));

			}
		}
	}

	public function update(Request $request)
	{
		$user = Auth::user();

		$news = News::findOrFail($request->get('news_id'));
		// create slug
		$slug = $this->makeEditSlug($request->get('title'), $news);

		// store
		$update = $news->update([
				'title' => substr($request->get('title'), 0, 200 )
				,'slug' => $slug
				,'message' => $request->get('message')
			]);
		return $news->slug;
	}

	public function makeEditSlug($title, $news)
	{
		$slug = str_slug( substr($title, 0, 150) );

        $latestSlug = 
            News::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
        	if($news->slug == $latestSlug){
        		return $news->slug;
        	}else{
        		$pieces = explode('-', $latestSlug);
	            $number = intval(end($pieces));
	            $slug.='-'.($number + 1);
        	}
        }  
        
        return $slug;
	}

	public function makeSlug($title)
	{
		$slug = str_slug( substr($title, 0, 150) );

        $latestSlug = 
            News::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
    		$pieces = explode('-', $latestSlug);
            $number = intval(end($pieces));
            $slug.='-'.($number + 1);
        }  
        
        return $slug;
	}

	public function delete(Request $request)
	{
		$news = News::findOrFail($request->get('news_id'));

		$news->delete();
	}
}