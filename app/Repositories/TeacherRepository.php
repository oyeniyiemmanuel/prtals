<?php
namespace App\Repositories;

use App\Repositories\Interfaces\TeacherInterface;
use Illuminate\Http\Request;
use App\Teacher;
use App\Subject;
use App\Grade;
use App\Wing;
use App\Department;
use App\Duty;
use Auth;

class TeacherRepository implements TeacherInterface
{
	public function validateSubjectGradeAndDepartment(Request $request)
	{
		$result = array();

		$subject = Subject::where('name', $request->get('subject'))->get()->first();
		$grade = Grade::where('name', $request->get('grade'))->get()->first();
		$departments = $subject->departments->pluck('name')->all();
		$grades = $subject->grades->pluck('name')->all();

		if(!in_array($request->get('department'), $departments)){
			$result["message"] = $request->get('department')." is not a valid department for ".$request->get('subject');	
			$result["status"] = false;
		}else{
			if(!in_array($request->get('grade'), $grades)){
				$result["message"] = $request->get('subject')." is not a valid subject for ".$request->get('grade');	
				$result["status"] = false;	
			}else{
				$result["message"] = "request is valid";	
				$result["status"] = true;
			}
		}

		return $result;

	}

	public function addSubject(Request $request)
	{
		$wing = Wing::firstOrCreate(['name' => $request->get('wing')]);

		$teacher = Teacher::where([
                ['reg_no', '=', Auth::user()->reg_no]
                ,['school_id', '=', Auth::user()->school_id]
            ])->get()->first();

		$subject = Subject::where('name', $request->get('subject'))->get()->first();
		$grade = Grade::where('name', $request->get('grade'))->get()->first();
		$duty_dept = Department::where('name', $request->get('department'))->get()->first();
		$duty_grd = Grade::where('name', $request->get('grade'))->get()->first();
		$duty = Duty::firstOrCreate([
				'name' => $subject->name
				,'subject_id' => $subject->id
				,'teacher_id' => $teacher->id
				,'department_id' => $duty_dept->id
				,'grade_id' => $duty_grd->id
				,'wing_id' => $wing->id
				,'school_id' => Auth::user()->school->id
			]);

		$duty->teacher()->associate($teacher)->save();

		$departments = $subject->departments;

		foreach($departments as $department){
			if($department->name == $request->get('department')){
				// sync department with teacher
	        	$teacher->departments()->syncWithoutDetaching([$department->id]);
	        	// sync department with grade
	        	$grade->departments()->syncWithoutDetaching([$department->id]);
	        	// sync school with department
	        	$department->schools()->syncWithoutDetaching([Auth::user()->school_id]);
	        	$department->wings()->syncWithoutDetaching([$wing->id]);
			}
		}
		// sync teacher pivots
        $teacher->subjects()->syncWithoutDetaching([$subject->id]);
        $teacher->wings()->syncWithoutDetaching([$wing->id]);
        $teacher->grades()->syncWithoutDetaching([$grade->id]);
        // sync grade pivots
        $grade->schools()->syncWithoutDetaching([Auth::user()->school_id]);
        $grade->wings()->syncWithoutDetaching([$wing->id]);
        // sync subject pivots
        $subject->wings()->syncWithoutDetaching([$wing->id]);
		

        //dd($teacher);
	}

	public function removeDuty(Request $request)
	{
		$duty = Duty::findOrFail($request->get('duty_id'));
		$subject = Subject::findOrFail($duty->subject_id);
		$teacher = Teacher::findOrFail(Auth::user()->teacher->id);
		// delete duty
		$duty->delete();
		// get remaining duties of the teacher
		$duties = Duty::where([
				['teacher_id', Auth::user()->teacher->id]
				,['subject_id', $subject->id]
			])->pluck('subject_id')->all();

		// if no duty for the current subject anymore, detach subject
		if(empty($duties)){
			$teacher->subjects()->detach([$subject->id]);
		}
	}
}