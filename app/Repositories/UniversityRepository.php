<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\UniversityInterface;
use App\Http\Requests\PleaToAddNewUniversity;
use App\Http\Requests\AdminAddNewUniversity;
use App\Notifications\UserIsVerified;
use App\NewUniversityRequest;
use App\University;
use App\Post;
use App\Event;
use App\News;
use App\Picture;
use App\Cover;
use App\Logo;
use App\User;
use App\Role;
use Auth;
use Cloudder;

class UniversityRepository implements UniversityInterface
{
	private $university;

	public function __construct(University $university)
	{
		$this->university = $university;
	}

	public function find($id){

		return $this->university::findOrFail($id);
	
	}

	public function allMembers($slug)
	{
		return $this->getUniversityBySlug($slug)->users->where('verified', '=', 1)->sortBy('name')->all();

	}

	public function allUniversities(){

		return $this->university::orderBy('name')->get();
	
	}

	public function filterByState($state){

		return $this->university::where('state', '=', $state)->get();
	
	}

	public function syncUniversityWithUser( $university_id ){

		$university = $this->find($university_id);

		Auth::user()->university()->associate($university)->save();

		$user = Auth::user();
		$user->institution_type = 'university';
		$user->institution_type_plural = 'universities';
		$user->save();

		return $user;
	}

	public function syncRoleWithUser(Request $request){
		$new_role = $request->get('role');
		$university_id = Auth::user()->university->id;

		// get all role names in the db
		$roles_in_db = Role::pluck('name')->toArray();
		// get that matches $new_role
		$role = Role::where('name', '=', $new_role)->get()->first();

		$model = "App\\".ucfirst($request->get('role'));

		$user_obj = $model::Create([
					'reg_no' => null
					,'university_id' => $university_id
					,'institution_type' => 'university'
					,'institution_type_plural' => 'universities'
				]);

		// check if $new_role exists in db
		if(!in_array($new_role, $roles_in_db)){
			$role = new Role();
			$role->name = $new_role;
			$role->save();
		}
		// if user does not have role, attach one
		if(Auth::user()->roles()->pluck('name')->isEmpty()){
			
			Auth::user()->attachRole($role);
		}
		// connect the eloquent relation i.e user->student, user->teacher
		Auth::user()->$new_role()->save($user_obj);

		// return role of user
		return Auth::user()->roles()->pluck('name')->toArray();
	}

	public function verifyUser(Request $request)
	{
		
		//Auth::user()->reg_no = $request->get('reg_no');
		
		Auth::user()->verified = true;

		Auth::user()->save();

		$user = Auth::user();
		
		$institution_type = $user->institution_type;

		$user->notify(new UserIsVerified($user, $user->$institution_type));

	}

	public function setAsAdminIfUniversityFirstUser(Request $request)
	{
		// get university's first user
		$first_user = User::where('university_id', '=', Auth::user()->university->id)->get()->first();

		// if user is the first user of the schol
		if($first_user->id == Auth::id()){

			$new_role = 'university-admin';

			// get that matches $new_role
			$universityAdmin = Role::where('name', '=', $new_role)->get()->first();

			Auth::user()->attachRole($universityAdmin);

		}
		
	}

	public function getUniversityPosts($slug)
	{
		$university_id = University::where('slug', '=', $slug)->get()->first()->id;
		
		$posts = Post::where('university_id', '=', $university_id)->paginate(10);

		return $posts;
	}

	public function getMyUniversityPosts($slug)
	{
		$university_id = University::where('slug', '=', $slug)->get()->first()->id;
		
		$posts = Post::where('user_id', '=', Auth::user()->id)->paginate(10);

		return $posts;
	}

	public function getUniversityEvents($slug)
	{
		$university_id = University::where('slug', '=', $slug)->get()->first()->id;
		
		$events = Event::where('university_id', '=', $university_id)->paginate(10);

		return $events;
	}

	public function getMyUniversityEvents($slug)
	{
		$university_id = University::where('slug', '=', $slug)->get()->first()->id;
		
		$events = Event::where('user_id', '=', Auth::user()->id)->paginate(10);

		return $events;
	}

	public function getUniversityNews($slug)
	{
		$university_id = University::where('slug', '=', $slug)->get()->first()->id;
		
		$news = News::where('university_id', '=', $university_id)->paginate(10);

		return $news;
	}

	public function getMyUniversityNews($slug)
	{
		$university_id = University::where('slug', '=', $slug)->get()->first()->id;
		
		$news = News::where('user_id', '=', Auth::user()->id)->paginate(10);

		return $news;
	}

	public function submitNewUniversityRequest(PleaToAddNewUniversity $request){

		 NewUniversityRequest::create($request->all());

		 flash()->overlay("Request submitted","<div class='ui segment'>If it is not already in our records, we will verify and add your school within 24hrs. Thanks</div>");

	}

	public function makeSlug($university_name)
	{
		$slug = str_slug( substr($university_name, 0, 150) );

        $latestSlug = 
            University::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
        	if(Auth::user()->university && Auth::user()->university->slug == $latestSlug){
        		return Auth::user()->university->slug;
        	}else{
        		$pieces = explode('-', $latestSlug);
	            $number = intval(end($pieces));
	            $slug.='-'.($number + 1);
        	}
            
        }  
        
        return $slug;
	}

	public function addFirstUserRegistrationNumber(AdminAddNewSchool $request, $school_id )
	{
		$result = array();

		// get the user model e.g "App\Student" or "App\Teacher"
		$model = "App\\".ucfirst($request->get('role'));

		$reg_no = $request->get('reg_no');

		// check if the user has been given a reg no. that matches the school id
		$check = $model::where([
							['reg_no', '=', $reg_no]
							,['school_id', '=', $school_id]
						])->get()->first();

		if(!$check){
			$new_member = $model::firstOrCreate([
				'reg_no' => $reg_no
				,'school_id' => $school_id
			]);

			$new_member->added_by = Auth::id();
			$new_member->save();

			$result["message"] = "New ".$request->get('role')." member added";	
			$result["status"] = true;	

			return $result;
		}

		$result["message"] = $request->get('role')." with the reg no exists";	
		$result["status"] = false;	

		return $result;
	}

	public function adminAddNewUniversity(AdminAddNewUniversity $request)
	{
		$result = array();

		$slug = $this->makeSlug($request->get('name'));

		$new_university = $this->university::create([
				'name' => $request->get('name')
				,'slug' => $slug
				,'address' => $request->get('address')
				,'state' => $request->get('state')
			]);

		/*$addUser = $this->addFirstUserRegistrationNumber($request, $new_school->id);

		if($addUser["status"] == false){

			$result["school"] = $addUser["message"];

			return $result;

		}else{

			$result["school"] = $slug;

			$result["user_reg_no"] = $request->get('reg_no');

			return $result;
		}*/
		$result["university"] = $slug;

		return $result;
	}

	public function getUniversityBySlug($slug)
	{
		return University::where('slug', '=', $slug)->get()->first();
	}

	public function updateUniversityDetails(Request $request)
	{
		$university = University::findOrFail(Auth::user()->university_id);

		$slug = $this->makeSlug($request->get('name'));

		$university->update([
				'name' => $request->get('name')
				,'slug' => $slug
				,'founded' => $request->get('founded')
				,'state' => $request->get('state')
				,'no_of_pupils' => $request->get('no_of_pupils')
				,'address' => $request->get('address')
				,'history' => $request->get('history')
				,'tuition' => $request->get('tuition')
				,'accomplishments' => $request->get('accomplishments')
				,'last_edited_by' => Auth::user()->id
				]);
		
		return $university->slug;
	}

	public function uploadCoverImage(Request $request)
	{
		$file = $request->image;

        list($width, $height, $type, $attr) = getimagesize($file);

        $dimensions = $width . 'x' . $height;
        // get name without extension
        $name_without_extension = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // append current time, prepend dimension and extension to photo name (i.e 1479022764photoname-1280x960.jpeg)
        $name = time() . $name_without_extension . '-' . $dimensions . '.' . $file->extension();
        // upload photo 
        $path = $file->getPathname();

        $options = array(
                       "public_id" => $name,
                       );

        $tags = array( "university", "university_cover_image" );

        // delete university prev cover on cloud
        if( Auth::user()->university->cover || !empty(Auth::user()->university->cover->public_id)){

            Cloudder::destroyImage(Auth::user()->university->cover->public_id);

        }

        // upload new university cover to cloud
        $cloudder = Cloudder::upload($path, $name, $options, $tags);

        $image_result = Cloudder::getResult();

        //check if university has cover
        $cover = Auth::user()->university->cover;

        if($cover == null){
        	// create new university cover
        	$image = Cover::create([
                'url' => str_replace('http://', 'https://', $image_result['url'])
                ,'format' => $image_result['format']
                ,'public_id' => $image_result['public_id']
                ,'user_id' => Auth::id()
            ]);
            // attach new cover to university
        	Auth::user()->university->cover()->save($image);

        }else{
        	// update cover details in db
        	$cover->update([
                'url' => str_replace('http://', 'https://', $image_result['url'])
                ,'format' => $image_result['format']
                ,'public_id' => $image_result['public_id']
                ,'user_id' => Auth::id()
            ]);

            $image = $cover->toArray();
            // attach new cover to university
        	$cover->save($image);
        }
        
	}

	public function uploadLogo(Request $request)
	{
		$file = $request->image;

        list($width, $height, $type, $attr) = getimagesize($file);

        $dimensions = $width . 'x' . $height;
        // get name without extension
        $name_without_extension = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // append current time, prepend dimension and extension to photo name (i.e 1479022764photoname-1280x960.jpeg)
        $name = time() . $name_without_extension . '-' . $dimensions . '.' . $file->extension();
        // upload photo 
        $path = $file->getPathname();

        $options = array(
                       "public_id" => $name,
                       );

        $tags = array( "university", "university_cover_image" );

        // delete university prev logo on cloud
        if( Auth::user()->university->logo || !empty(Auth::user()->university->logo->public_id)){

            Cloudder::destroyImage(Auth::user()->university->logo->public_id);

        }

        // upload new university logo to cloud
        $cloudder = Cloudder::upload($path, $name, $options, $tags);

        $image_result = Cloudder::getResult();

        //check if university has logo
        $logo = Auth::user()->university->logo;

        if($logo == null){
        	// create new university logo
        	$image = Logo::create([
                'url' => str_replace('http://', 'https://', $image_result['url'])
                ,'format' => $image_result['format']
                ,'public_id' => $image_result['public_id']
                ,'user_id' => Auth::id()
            ]);
            // attach new logo to university
        	Auth::user()->university->logo()->save($image);

        }else{
        	// update logo details in db
        	$logo->update([
                'url' => str_replace('http://', 'https://', $image_result['url'])
                ,'format' => $image_result['format']
                ,'public_id' => $image_result['public_id']
                ,'user_id' => Auth::id()
            ]);

            $image = $logo->toArray();
            // attach new logo to university
        	$logo->save($image);
        }
        
	}
}