<?php
namespace App\Repositories;

use App\Repositories\Interfaces\RoleInterface;
use Illuminate\Http\Request;
use App\Role;
use App\Permission;
use App\Student;
use App\Teacher;
use App\Authority;
use App\User;
use App\School;
use Auth;

class RoleRepository implements RoleInterface
{
	public function syncRoleWithUser(Request $request){
		$new_role = $request->get('role');

		// get all role names in the db
		$roles_in_db = Role::pluck('name')->toArray();
		// get that matches $new_role
		$role = Role::where('name', '=', $new_role)->get()->first();

		$model = "App\\".ucfirst($request->get('role'));

		$user_obj = $model::where([
				['reg_no', '=', $request->get('reg_no')]
				,['school_id', '=', $request->get('school_id')]
			])->get()->first();

		// check if $new_role exists in db
		if(!in_array($new_role, $roles_in_db)){
			$role = new Role();
			$role->name = $new_role;
			$role->save();
		}
		// if user does not have role, attach one
		if(Auth::user()->roles()->pluck('name')->isEmpty()){
			
			Auth::user()->attachRole($role);
		}
		// connect the eloquent relation i.e user->student, user->teacher
		Auth::user()->$new_role()->save($user_obj);

		// return role of user
		return Auth::user()->roles()->pluck('name')->toArray();
	}

	public function determineRoleView(){
		$user = Auth::user();

		if(!$user->roles->isEmpty()){
			//return user's role i.e "student", "teacher", or "authority"
			return $user->roles->first()->name;

		}else{

			return 'choose_role';

		}
	}

	public function checkRegNo(Request $request)
	{
		$result = array();
		// get the user model e.g "App\Student" or "App\Teacher"
		$model = "App\\".ucfirst($request->get('role'));

		$reg_no = $request->get('reg_no');

		$school_id = $request->get('school_id');
		// check if the user has been given a reg no. that matches the school id
		$check = $model::where([
				['reg_no', '=', $reg_no]
				,['school_id', '=', $school_id]
			])->get()->first();
		// check if there is a registered user with the reg no
		$regNoIsUsed = $model::where([
				['reg_no', '=', $reg_no]
				,['school_id', '=', $school_id]
				,['user_id', '!=', null]
			])->get()->first();

		if(!$check){

			$result["message"] = $reg_no." is not a valid registration no.";	
			$result["status"] = false;	

			return $result;	
		}
		elseif($regNoIsUsed){

			$result["message"] = " The reg no has been used by another ".$request->get('role');	
			$result["status"] = false;	

			return $result;	
		}else{

			$result["message"] = " reg no is okay";	
			$result["status"] = true;	

			return $result;
		}

	}

	public function verifyUser(Request $request)
	{
		
		Auth::user()->reg_no = $request->get('reg_no');
		
		Auth::user()->verified = true;

		Auth::user()->save();

	}

	public function setAsAdminIfSchoolFirstUser(Request $request)
	{
		// get school's first user
		$first_user = User::where('school_id', '=', $request->get('school_id'))->get()->first();

		// if user's reg no is the first of its school on the users table, set admin role 
		if($first_user->reg_no == $request->get('reg_no')){

			$new_role = 'school-admin';

			// get all role names in the db
			$roles_in_db = Role::pluck('name')->toArray();

			// get that matches $new_role
			$schoolAdmin = Role::where('name', '=', $new_role)->get()->first();

			// check if $new_role exists in db
			if(!in_array($new_role, $roles_in_db)){
				$schoolAdmin = new Role();
				$schoolAdmin->name = $new_role;
				$schoolAdmin->save();
			}

			Auth::user()->attachRole($schoolAdmin);
		}
		
	}
}