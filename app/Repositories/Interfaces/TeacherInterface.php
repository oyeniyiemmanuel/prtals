<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Teacher;

interface TeacherInterface{

	function addSubject(Request $request);

	function removeDuty(Request $request);
	
	function validateSubjectGradeAndDepartment(Request $request);
}