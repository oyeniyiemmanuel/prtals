<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\PleaToAddNewSchool;
use App\Http\Requests\AdminAddNewSchool;
use App\School;

interface SchoolInterface{

	function allSchools();

	function find($id);

	function updateSchoolDetails(Request $request);

	function uploadCoverImage(Request $request);

	function uploadLogo(Request $request);

	function syncSchoolWithUser($school_id);

	function filterByState($state);

	function submitNewSchoolRequest(PleaToAddNewSchool $state);

	function adminAddNewSchool(AdminAddNewSchool $request);

	function getSchoolBySlug($slug);


}