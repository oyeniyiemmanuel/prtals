<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Announcement;

interface AnnouncementInterface{

	function store(Request $request);

	function update(Request $request);

	function getSchoolAnnouncements($slug);
}