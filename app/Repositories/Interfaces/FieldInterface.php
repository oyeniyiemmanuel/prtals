<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Field;

interface FieldInterface{

	function adminAddNewField(Request $request);
}