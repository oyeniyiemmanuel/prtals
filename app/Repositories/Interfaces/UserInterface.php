<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\User;

interface UserInterface{

	function updateDetails(Request $request);

	function updateBio(Request $request);

	function updateSocials(Request $request);

	function checkUsername($username);

	function viewUser($username);

	function uploadProfileImage(Request $request);

}