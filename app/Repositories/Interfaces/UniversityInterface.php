<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\PleaToAddNewUniversity;
use App\Http\Requests\AdminAddNewUniversity;
use App\University;

interface UniversityInterface{

	function allUniversities();

	function find($id);

	function updateUniversityDetails(Request $request);

	function uploadCoverImage(Request $request);

	function uploadLogo(Request $request);

	function syncUniversityWithUser($university_id);

	function filterByState($state);

	function submitNewUniversityRequest(PleaToAddNewUniversity $state);

	function adminAddNewUniversity(AdminAddNewUniversity $request);

	function getUniversityBySlug($slug);


}