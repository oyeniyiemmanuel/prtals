<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Event;

interface EventInterface{

	function store(Request $request);

	function update(Request $request);

	function getSchoolEvents($slug);
}