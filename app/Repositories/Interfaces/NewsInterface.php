<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\News;

interface NewsInterface{

	function store(Request $request);

	function update(Request $request);

	function getSchoolNews($slug);
}