<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Subject;

interface SubjectInterface{

	function adminAddNewSubject(Request $request);
}