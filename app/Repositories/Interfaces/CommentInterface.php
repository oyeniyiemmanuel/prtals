<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Comment;

interface CommentInterface{

	function store(Request $request);

	function syncWithUser($comment);

	function syncWithPost($comment, Request $request);

	function getPost($comment);

	function getComment($comment_id);

	function delete(Request $request);

}