<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Like;

interface LikeInterface{

	function getPost($post_id);

	function likePost($id);

	function likeComment($comment_id, $post_id);

	function handlePostLike($type, $id);

	function handleCommentLike($type, $comment_id, $post_id);

}