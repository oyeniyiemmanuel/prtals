<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Authority;

interface AuthorityInterface{

	function addNewMember(Request $request);

}