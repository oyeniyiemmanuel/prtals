<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\School;

interface RoleInterface{

	function syncRoleWithUser(Request $request);

	function determineRoleView();

	function checkRegNo(Request $request);

	function verifyUser(Request $request);

	function setAsAdminIfSchoolFirstUser(Request $request);

}