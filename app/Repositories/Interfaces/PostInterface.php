<?php
namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Post;

interface PostInterface{

	function getAllPosts();

	function makeSlug($title);

	function getBySlug($slug);

	function getSchoolPosts($slug);

	function getUserPosts($username);

	function category($category);

	function subject($subject);

	function store($slug, Request $request);

	function syncWithUser($post);

	function syncWithSchool(Request $request, $post);

	function delete(Request $request);
}