<?php
namespace App\Repositories;

use App\Repositories\Interfaces\PostInterface;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\School;
use App\University;
use App\Subject;
use App\Field;
use Auth;
use DB;

class PostRepository implements PostInterface
{
	public function findUser($username)
	{
		return User::where('username', '=', $username)->get()->first();
	}
	
	public function makeSlug($title)
	{
		$slug = str_slug( substr($title, 0, 150) );

        $latestSlug = 
            Post::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
            $pieces = explode('-', $latestSlug);
            $number = intval(end($pieces));
            $slug.='-'.($number + 1);
        }  
        
        return $slug;
	}

	public function makeEditSlug($title, $post)
	{
		$slug = str_slug( substr($title, 0, 150) );

        $latestSlug = 
            Post::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
        	if($post->slug == $latestSlug){
        		return $post->slug;
        	}else{
        		$pieces = explode('-', $latestSlug);
	            $number = intval(end($pieces));
	            $slug.='-'.($number + 1);
        	}
        }  
        
        return $slug;
	}

	public function store($slug, Request $request){

		$post = Post::firstOrCreate([
				'title' => substr($request->get('title'), 0, 150 )
				,'slug' => $slug
				,'message' => $request->get('message')
				,'category' => $request->get('category')
			]);

		if (Auth::user()->roles()->first()->name == 'undergraduate') {
			$post->institution_type = 'university';
			$post->institution_type_plural = 'universities';
			$post->save();
		}

		$post = $this->getBySlug($post->slug);

		return $post;
	}

	public function update($slug, $prev_post, Request $request){

		$prev_post->update([
				'title' => substr($request->get('title'), 0, 150 )
				,'slug' => $slug
				,'message' => $request->get('message')
				,'category' => $request->get('category')
			]);

		$post = $this->getBySlug($slug);

		return $post;
	}

	public function findSchool($slug)
	{
		return School::where('slug', '=', $slug)->get()->first();
	}

	public function findUniversity($slug)
	{
		return University::where('slug', '=', $slug)->get()->first();
	}

	public function syncWithUser($post){

		$user = Auth::user();

		$post->user()->associate($user)->save();

	}

	public function syncWithSchool(Request $request, $post){
		$user = Auth::user();

		$institution_type = $post->institution_type;

		//$role = $user->roles->first()->name;

        $model = "App\\".ucfirst($institution_type);

		$school = $model::findOrFail(Auth::user()->$institution_type->id);

		$post->$institution_type()->associate($school)->save();

		if(Auth::user()->roles()->first()->name == 'student') {

			$this->syncWithSubject($request->get('subject_name'), $post);

		}

		if(Auth::user()->roles()->first()->name == 'undergraduate') {

			$this->syncWithField($request->get('field_name'), $post);

		}
 
	}

	public function syncWithSubject($subject_name, $post){

		$subject = Subject::where('name', '=', $subject_name)->get()->first();

		$post->subject()->associate($subject)->save();

	}

	public function syncWithField($field_name, $post){

		$field = Field::where('name', '=', $field_name)->get()->first();

		$post->field()->associate($field)->save();

	}

	public function getAllPosts(){

		$posts = Post::latest()->paginate(10);

		return $posts;

	}

	public function apiGetAllPosts(){

		$posts = Post::with('user.picture', 'school', 'university', 'subject', 'field', 'comments', 'likes')->latest()->paginate(10);

		return $posts;

	} public function apiShow($id){

		$post = Post::with('user.picture', 'school', 'university', 'subject', 'field', 'comments', 'likes')->findOrFail($id);

		return $post;

	}

	public function apiStore(Request $request)
	{
		// get user data
		$user = User::with('posts', 'news', 'announcements', 'events', 'student', 'picture', 'school.logo')->where('id', $request->get('user_id'))->get()->first();

		//dd($user->roles()->first()->name);

		$slug = $this->makeSlug($request->get('title'));

		$post = Post::firstOrCreate([
				'title' => substr($request->get('title'), 0, 150 )
				,'slug' => $slug
				,'message' => $request->get('message')
				,'category' => $request->get('category')
			]);

		if ($user->roles()->first()->name == 'undergraduate') {
			$post->institution_type = 'university';
			$post->institution_type_plural = 'universities';
			$post->save();
		}
		// sync with user
		$post->user()->associate($user)->save();

		//sync with school
		$post = Post::where('slug', '=', $post->slug)->get()->first();
		$institution_type = $post->institution_type;
        $model = "App\\".ucfirst($institution_type);
		$school = $model::findOrFail($user->$institution_type->id);
		$post->$institution_type()->associate($school)->save();

		if($user->roles()->first()->name == 'student') {
			$this->syncWithSubject($request->get('subject'), $post);
		}

		if($user->roles()->first()->name == 'undergraduate') {
			$this->syncWithField($request->get('field'), $post);
		}

        return $user->toJSON();

	}

	public function apiGetUserPosts($user_id){

		$posts = Post::with('user.picture', 'school', 'university', 'subject', 'field', 'comments', 'likes')
					->latest()
					->where('user_id', '=', $user_id)
					->paginate(10);

		return $posts;

	}

	public function getBySlug($slug){

		$posts = Post::where('slug', '=', $slug)->get()->first();

		return $posts;

	}

	public function getSchoolPosts($slug)
	{
		$school_id = School::where('slug', '=', $slug)->get()->first()->id;
		
		$posts = Post::latest()->where('school_id', '=', $school_id)->paginate(10);

		return $posts;
	}

	public function getUniversityPosts($slug)
	{
		$university_id = University::where('slug', '=', $slug)->get()->first()->id;
		
		$posts = Post::latest()->where('university_id', '=', $university_id)->paginate(10);

		return $posts;
	}

	public function getUserPosts($username)
	{
		$user_id = User::where('username', '=', $username)->get()->first()->id;

		$posts = Post::latest()->where('user_id', '=', $user_id)->paginate(10);

		return $posts;
	}

	public function category($category)
	{
		$posts = Post::latest()->where('category', '=', $category)->paginate(10);

		return $posts;
	}

	public function subject($subject)
	{
		$subject = Subject::where('name', '=', $subject)->get()->first();

		$posts = $subject->posts()->latest()->paginate(10);

		return $posts;
	}


	public function field($field)
	{
		$field = Field::where('name', '=', $field)->get()->first();

		$posts = $field->posts()->latest()->paginate(10);

		return $posts;
	}

	public function delete(Request $request)
	{
		$post = Post::findOrFail($request->get('post_id'));

		$post->comments()->delete();

		DB::table('likeables')->where([
				['likeable_id', '=', $request->get('post_id')]
				,['likeable_type', '=', 'App\Post']
			])
			->delete();

		$post->delete();
	}
}