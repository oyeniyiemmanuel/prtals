<?php
namespace App\Repositories;

use App\Repositories\Interfaces\EventInterface;
use App\Notifications\NewEventInMySchool;
use Illuminate\Http\Request;
use App\University;
use App\School;
use App\Event_picture;
use App\Event;
use App\User;
use Auth;
use Cloudder;

class EventRepository implements EventInterface
{
	public function getAllEvents(){

		$events = Event::latest()->paginate(10);

		return $events;

	}

	public function getBySlug($slug){

		$event = Event::where('slug', '=', $slug)->get()->first();

		return $event;

	}

	public function findSchool($slug)
	{
		return School::where('slug', '=', $slug)->get()->first();
	}

	public function findUniversity($slug)
	{
		return University::where('slug', '=', $slug)->get()->first();
	}

	public function findUser($username)
	{
		return User::where('username', '=', $username)->get()->first();
	}

	public function getSchoolEvents($slug)
	{
		$school_id = School::where('slug', '=', $slug)->get()->first()->id;
		
		$events = Event::latest()->where('school_id', '=', $school_id)->paginate(10);

		return $events;
	}

	public function getUniversityEvents($slug)
	{
		$university_id = University::where('slug', '=', $slug)->get()->first()->id;
		
		$events = Event::latest()->where('university_id', '=', $university_id)->paginate(10);

		return $events;
	}

	public function getUserEvents($username)
	{
		$user = User::where('username', '=', $username)->get()->first();

		$events = Event::latest()->where('user_id', '=', $user->id)->paginate(10);

		return $events;
	}

	public function store(Request $request)
	{
		$user = Auth::user();
		// create slug
		$slug = $this->makeSlug($request->get('title'));

		// store
		$event = Event::firstOrCreate([
				'title' => substr($request->get('title'), 0, 200 )
				,'slug' => $slug
				,'message' => $request->get('message')
			]);

		if ($user->roles()->first()->name == 'undergraduate') {
			$event->institution_type = 'university';
			$event->institution_type_plural = 'universities';
			$event->save();
		}

		$event = $this->getBySlug($event->slug);

		$institution_type = $event->institution_type;
		//sync with user, sync with school
		$event->user()->associate($user)->save();
		$event->$institution_type()->associate($user->$institution_type)->save();
		// notify school members
		$this->notifySchoolMembers($event);

		return $event->slug;

	}

	public function notifySchoolMembers($event)
	{
		$institution_type = $event->institution_type;
		// get all school members
		$members = Auth::user()->$institution_type->users;

		foreach( $members as $member ){
			// notify all members except the logged in user
			if( Auth::id() !== $member->id ){

				$member->notify(new NewEventInMySchool( $event, $event->user ));

			}
		}
	}

	public function update(Request $request)
	{
		$user = Auth::user();

		$event = Event::findOrFail($request->get('event_id'));
		// create slug
		$slug = $this->makeEditSlug($request->get('title'), $event);

		// store
		$update = $event->update([
				'title' => substr($request->get('title'), 0, 200 )
				,'slug' => $slug
				,'message' => $request->get('message')
			]);
		return $event->slug;
	}

	public function uploadPhotos(Request $request)
	{
		$event = Event::findOrFail($request->get('event_id'));

		$file = $request->file;

        list($width, $height, $type, $attr) = getimagesize($file);

        $dimensions = $width . 'x' . $height;
        // get name without extension
        $name_without_extension = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // append current time, prepend dimension and extension to photo name (i.e 1479022764photoname-1280x960.jpeg)
        $name = time() . $name_without_extension . '-' . $dimensions . '.' . $file->extension();
        // upload photo 
        $path = $file->getPathname();

        $options = array(
                       "public_id" => $name,
                       );

        $tags = array( "event", $event->title );

        // delete event prev logo on cloud
        /*if( Auth::user()->school->logo || !empty(Auth::user()->school->logo->public_id)){

            Cloudder::destroyImage(Auth::user()->school->logo->public_id);

        }*/

        // upload new school logo to cloud
        $cloudder = Cloudder::upload($path, $name, $options, $tags);

        $image_result = Cloudder::getResult();

    	// create new event pictures
    	$image = Event_picture::create([
            'url' => str_replace('http://', 'https://', $image_result['url'])
            ,'format' => $image_result['format']
            ,'public_id' => $image_result['public_id']
            ,'user_id' => Auth::id()
        ]);
        // attach new logo to school
    	$image->event()->associate($event)->save();

    	return $event;
	}

	public function makeEditSlug($title, $event)
	{
		$slug = str_slug( substr($title, 0, 150) );

        $latestSlug = 
            Event::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
        	if($event->slug == $latestSlug){
        		return $event->slug;
        	}else{
        		$pieces = explode('-', $latestSlug);
	            $number = intval(end($pieces));
	            $slug.='-'.($number + 1);
        	}
        }  
        
        return $slug;
	}

	public function makeSlug($title)
	{
		$slug = str_slug( substr($title, 0, 150) );

        $latestSlug = 
            Event::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
    		$pieces = explode('-', $latestSlug);
            $number = intval(end($pieces));
            $slug.='-'.($number + 1);
        }  
        
        return $slug;
	}

	public function delete(Request $request)
	{
		$event = Event::findOrFail($request->get('event_id'));
		// clear event pictures on cloud and in db

		if($event->pictures && !empty($event->pictures->all())){
			foreach($event->pictures as $picture){
				Cloudder::destroyImage($picture->public_id);

				$picture->delete();
			}
		}
		// delete event
		$event->delete();
	}
}