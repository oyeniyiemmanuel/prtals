<?php
namespace App\Repositories;

use App\Repositories\Interfaces\AnnouncementInterface;
use App\Notifications\NewAnnouncementInMySchool;
use Illuminate\Http\Request;
use App\School;
use App\University;
use App\Announcement;
use App\User;
use Auth;

class AnnouncementRepository implements AnnouncementInterface
{
	public function getAllAnnouncements(){

		$announcements = Announcement::latest()->paginate(10);

		return $announcements;

	}

	public function getUser($username){

		$user = User::where('username', '=', $username)->get()->first();

		return $user;

	}

	public function getBySlug($slug){

		$announcement = Announcement::where('slug', '=', $slug)->get()->first();

		return $announcement;

	}

	public function getSlug()
	{
		if(Auth::user()->roles()->first()->name == 'student') {
			return Auth::user()->school->slug;
		}

		if(Auth::user()->roles()->first()->name == 'undergraduate') {
			return Auth::user()->university->slug;
		}
	}

	public function getSchoolBySlug($slug)
	{
		return School::where('slug', '=', $slug)->get()->first();
	}

	public function getUniversityBySlug($slug)
	{
		return University::where('slug', '=', $slug)->get()->first();
	}

	public function getSchoolAnnouncements($slug)
	{
		$school_id = School::where('slug', '=', $slug)->get()->first()->id;
		
		$announcements = Announcement::where('school_id', '=', $school_id)->latest()->paginate(10);

		return $announcements;
	}

	public function getUniversityAnnouncements($slug)
	{
		$university_id = University::where('slug', '=', $slug)->get()->first()->id;
		
		$announcements = Announcement::where('university_id', '=', $university_id)->latest()->paginate(10);

		return $announcements;
	}

	public function getUserAnnouncements($username)
	{
		$user = User::where('username', '=', $username)->get()->first();

		$announcements = Announcement::where('user_id', '=', $user->id)->latest()->paginate(10);

		return $announcements;
	}

	public function store(Request $request)
	{
		$user = Auth::user();
		// create slug
		$slug = $this->makeSlug($request->get('title'));

		// store
		$announcement = Announcement::firstOrCreate([
				'title' => substr($request->get('title'), 0, 200 )
				,'slug' => $slug
				,'message' => $request->get('message')
			]);

		if ($user->roles()->first()->name == 'undergraduate') {
			$announcement->institution_type = 'university';
			$announcement->institution_type_plural = 'universities';
			$announcement->save();
		}

		$announcement = $this->getBySlug($announcement->slug);

		$institution_type = $announcement->institution_type;

		//sync with user, sync with school
		$announcement->user()->associate($user)->save();
		$announcement->$institution_type()->associate($user->$institution_type)->save();
		// notify school members
		$this->notifySchoolMembers($announcement);

	}

	public function notifySchoolMembers($announcement)
	{
		$institution_type = $announcement->institution_type;
		// get all school members
		$members = Auth::user()->$institution_type->users;

		foreach( $members as $member ){
			// notify all members excepty the logged in user
			if( Auth::id() != $member->id ){

				$member->notify(new NewAnnouncementInMySchool( $announcement, $announcement->user ));

			}
		}
	}

	public function update(Request $request)
	{
		$user = Auth::user();

		$announcement = Announcement::findOrFail($request->get('announcement_id'));
		// create slug
		$slug = $this->makeEditSlug($request->get('title'), $announcement);

		// store
		$update = $announcement->update([
				'title' => substr($request->get('title'), 0, 200 )
				,'slug' => $slug
				,'message' => $request->get('message')
			]);
		return $announcement->slug;
	}

	public function makeEditSlug($title, $announcement)
	{
		$slug = str_slug( substr($title, 0, 150) );

        $latestSlug = 
            Announcement::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
        	if($announcement->slug == $latestSlug){
        		return $announcement->slug;
        	}else{
        		$pieces = explode('-', $latestSlug);
	            $number = intval(end($pieces));
	            $slug.='-'.($number + 1);
        	}
        }  
        
        return $slug;
	}

	public function makeSlug($title)
	{
		$slug = str_slug( substr($title, 0, 150) );

        $latestSlug = 
            Announcement::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
    		$pieces = explode('-', $latestSlug);
            $number = intval(end($pieces));
            $slug.='-'.($number + 1);
        }  
        
        return $slug;
	}

	public function delete(Request $request)
	{
		$announcement = Announcement::findOrFail($request->get('announcement_id'));

		$announcement->delete();
	}
}