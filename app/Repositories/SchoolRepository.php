<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\SchoolInterface;
use App\Http\Requests\PleaToAddNewSchool;
use App\Http\Requests\AdminAddNewSchool;
use App\Notifications\UserIsVerified;
use App\NewSchoolRequest;
use App\School;
use App\Post;
use App\Event;
use App\News;
use App\Picture;
use App\Cover;
use App\Logo;
use App\User;
use App\Role;
use Auth;
use Cloudder;

class SchoolRepository implements SchoolInterface
{
	private $school;

	public function __construct(School $school)
	{
		$this->school = $school;
	}

	public function find($id){

		return $this->school::findOrFail($id);
	
	}

	public function allMembers($slug)
	{
		return $this->getSchoolBySlug($slug)->users->where('verified', '=', 1)->sortBy('name')->all();

	}

	public function allSchools(){

		return $this->school::orderBy('name')->get();
	
	}

	public function filterByState($state){

		return $this->school::where('state', '=', $state)->get();
	
	}

	public function syncSchoolWithUser( $school_id ){

		$school = $this->find($school_id);

		$user = Auth::user()->school()->associate($school)->save();

		return $user;
	}

	public function syncRoleWithUser(Request $request){
		$new_role = $request->get('role');
		$school_id = Auth::user()->school->id;

		// get all role names in the db
		$roles_in_db = Role::pluck('name')->toArray();
		// get that matches $new_role
		$role = Role::where('name', '=', $new_role)->get()->first();

		$model = "App\\".ucfirst($request->get('role'));

		$user_obj = $model::Create([
					'reg_no' => null
					,'school_id' => $school_id
					,'institution_type' => 'school'
					,'institution_type_plural' => 'schools'
				]);

		// check if $new_role exists in db
		if(!in_array($new_role, $roles_in_db)){
			$role = new Role();
			$role->name = $new_role;
			$role->save();
		}
		// if user does not have role, attach one
		if(Auth::user()->roles()->pluck('name')->isEmpty()){
			
			Auth::user()->attachRole($role);
		}
		// connect the eloquent relation i.e user->student, user->teacher
		Auth::user()->$new_role()->save($user_obj);

		// return role of user
		return Auth::user()->roles()->pluck('name')->toArray();
	}

	public function verifyUser(Request $request)
	{
		
		//Auth::user()->reg_no = $request->get('reg_no');
		
		Auth::user()->verified = true;

		Auth::user()->save();

		$user = Auth::user();

		$institution_type = $user->institution_type;

		$user->notify(new UserIsVerified($user, $user->$institution_type));

	}

	public function setAsAdminIfSchoolFirstUser(Request $request)
	{
		// get school's first user
		$first_user = User::where('school_id', '=', Auth::user()->school->id)->get()->first();

		// if user is the first user of the schol
		if($first_user->id == Auth::id()){

			$new_role = 'school-admin';

			// get that matches $new_role
			$schoolAdmin = Role::where('name', '=', $new_role)->get()->first();

			Auth::user()->attachRole($schoolAdmin);

		}
		
	}

	public function getSchoolPosts($slug)
	{
		$school_id = School::where('slug', '=', $slug)->get()->first()->id;
		
		$posts = Post::where('school_id', '=', $school_id)->paginate(10);

		return $posts;
	}

	public function getMySchoolPosts($slug)
	{
		$school_id = School::where('slug', '=', $slug)->get()->first()->id;
		
		$posts = Post::where('user_id', '=', Auth::user()->id)->paginate(10);

		return $posts;
	}

	public function getSchoolEvents($slug)
	{
		$school_id = School::where('slug', '=', $slug)->get()->first()->id;
		
		$events = Event::where('school_id', '=', $school_id)->paginate(10);

		return $events;
	}

	public function getMySchoolEvents($slug)
	{
		$school_id = School::where('slug', '=', $slug)->get()->first()->id;
		
		$events = Event::where('user_id', '=', Auth::user()->id)->paginate(10);

		return $events;
	}

	public function getSchoolNews($slug)
	{
		$school_id = School::where('slug', '=', $slug)->get()->first()->id;
		
		$news = News::where('school_id', '=', $school_id)->paginate(10);

		return $news;
	}

	public function getMySchoolNews($slug)
	{
		$school_id = School::where('slug', '=', $slug)->get()->first()->id;
		
		$news = News::where('user_id', '=', Auth::user()->id)->paginate(10);

		return $news;
	}

	public function submitNewSchoolRequest(PleaToAddNewSchool $request){

		 NewSchoolRequest::create($request->all());

		 flash()->overlay("Request submitted","<div class='ui segment'>If it is not already in our records, we will verify and add your school within 24hrs. Thanks</div>");

	}

	public function makeSlug($school_name)
	{
		$slug = str_slug( substr($school_name, 0, 150) );

        $latestSlug = 
            School::whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug')->first();

        if( $latestSlug ){
        	if(Auth::user()->school && Auth::user()->school->slug == $latestSlug){
        		return Auth::user()->school->slug;
        	}else{
        		$pieces = explode('-', $latestSlug);
	            $number = intval(end($pieces));
	            $slug.='-'.($number + 1);
        	}
            
        }  
        
        return $slug;
	}

	public function addFirstUserRegistrationNumber(AdminAddNewSchool $request, $school_id )
	{
		$result = array();

		// get the user model e.g "App\Student" or "App\Teacher"
		$model = "App\\".ucfirst($request->get('role'));

		$reg_no = $request->get('reg_no');

		// check if the user has been given a reg no. that matches the school id
		$check = $model::where([
							['reg_no', '=', $reg_no]
							,['school_id', '=', $school_id]
						])->get()->first();

		if(!$check){
			$new_member = $model::firstOrCreate([
				'reg_no' => $reg_no
				,'school_id' => $school_id
			]);

			$new_member->added_by = Auth::id();
			$new_member->save();

			$result["message"] = "New ".$request->get('role')." member added";	
			$result["status"] = true;	

			return $result;
		}

		$result["message"] = $request->get('role')." with the reg no exists";	
		$result["status"] = false;	

		return $result;
	}

	public function adminAddNewSchool(AdminAddNewSchool $request)
	{
		$result = array();

		$slug = $this->makeSlug($request->get('name'));

		$new_school = $this->school::create([
				'name' => $request->get('name')
				,'slug' => $slug
				,'address' => $request->get('address')
				,'state' => $request->get('state')
			]);

		/*$addUser = $this->addFirstUserRegistrationNumber($request, $new_school->id);

		if($addUser["status"] == false){

			$result["school"] = $addUser["message"];

			return $result;

		}else{

			$result["school"] = $slug;

			$result["user_reg_no"] = $request->get('reg_no');

			return $result;
		}*/
		$result["school"] = $slug;

		return $result;
	}

	public function getSchoolBySlug($slug)
	{
		return School::where('slug', '=', $slug)->get()->first();
	}

	public function updateSchoolDetails(Request $request)
	{
		$school = School::findOrFail(Auth::user()->school_id);

		$slug = $this->makeSlug($request->get('name'));

		$school->update([
				'name' => $request->get('name')
				,'slug' => $slug
				,'founded' => $request->get('founded')
				,'state' => $request->get('state')
				,'uniform' => $request->get('uniform')
				,'no_of_pupils' => $request->get('no_of_pupils')
				,'no_of_teachers' => $request->get('no_of_teachers')
				,'no_of_other_staffs' => $request->get('no_of_other_staffs')
				,'no_of_classes' => $request->get('no_of_classes')
				,'address' => $request->get('address')
				,'history' => $request->get('history')
				,'tuition' => $request->get('tuition')
				,'rules' => $request->get('rules')
				,'anthem' => $request->get('anthem')
				,'accomplishments' => $request->get('accomplishments')
				,'last_edited_by' => Auth::user()->id
				]);
		
		return $school->slug;
	}

	public function uploadCoverImage(Request $request)
	{
		$file = $request->image;

        list($width, $height, $type, $attr) = getimagesize($file);

        $dimensions = $width . 'x' . $height;
        // get name without extension
        $name_without_extension = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // append current time, prepend dimension and extension to photo name (i.e 1479022764photoname-1280x960.jpeg)
        $name = time() . $name_without_extension . '-' . $dimensions . '.' . $file->extension();
        // upload photo 
        $path = $file->getPathname();

        $options = array(
                       "public_id" => $name,
                       );

        $tags = array( "school", "school_cover_image" );

        // delete school prev cover on cloud
        if( Auth::user()->school->cover || !empty(Auth::user()->school->cover->public_id)){

            Cloudder::destroyImage(Auth::user()->school->cover->public_id);

        }

        // upload new school cover to cloud
        $cloudder = Cloudder::upload($path, $name, $options, $tags);

        $image_result = Cloudder::getResult();

        //check if school has cover
        $cover = Auth::user()->school->cover;

        if($cover == null){
        	// create new school cover
        	$image = Cover::create([
                'url' => str_replace('http://', 'https://', $image_result['url'])
                ,'format' => $image_result['format']
                ,'public_id' => $image_result['public_id']
                ,'user_id' => Auth::id()
            ]);
            // attach new cover to school
        	Auth::user()->school->cover()->save($image);

        }else{
        	// update cover details in db
        	$cover->update([
                'url' => str_replace('http://', 'https://', $image_result['url'])
                ,'format' => $image_result['format']
                ,'public_id' => $image_result['public_id']
                ,'user_id' => Auth::id()
            ]);

            $image = $cover->toArray();
            // attach new cover to school
        	$cover->save($image);
        }
        
	}

	public function uploadLogo(Request $request)
	{
		$file = $request->image;

        list($width, $height, $type, $attr) = getimagesize($file);

        $dimensions = $width . 'x' . $height;
        // get name without extension
        $name_without_extension = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // append current time, prepend dimension and extension to photo name (i.e 1479022764photoname-1280x960.jpeg)
        $name = time() . $name_without_extension . '-' . $dimensions . '.' . $file->extension();
        // upload photo 
        $path = $file->getPathname();

        $options = array(
                       "public_id" => $name,
                       );

        $tags = array( "school", "school_cover_image" );

        // delete school prev logo on cloud
        if( Auth::user()->school->logo || !empty(Auth::user()->school->logo->public_id)){

            Cloudder::destroyImage(Auth::user()->school->logo->public_id);

        }

        // upload new school logo to cloud
        $cloudder = Cloudder::upload($path, $name, $options, $tags);

        $image_result = Cloudder::getResult();

        //check if school has logo
        $logo = Auth::user()->school->logo;

        if($logo == null){
        	// create new school logo
        	$image = Logo::create([
                'url' => str_replace('http://', 'https://', $image_result['url'])
                ,'format' => $image_result['format']
                ,'public_id' => $image_result['public_id']
                ,'user_id' => Auth::id()
            ]);
            // attach new logo to school
        	Auth::user()->school->logo()->save($image);

        }else{
        	// update logo details in db
        	$logo->update([
                'url' => str_replace('http://', 'https://', $image_result['url'])
                ,'format' => $image_result['format']
                ,'public_id' => $image_result['public_id']
                ,'user_id' => Auth::id()
            ]);

            $image = $logo->toArray();
            // attach new logo to school
        	$logo->save($image);
        }
        
	}
}