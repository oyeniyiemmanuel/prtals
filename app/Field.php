<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
    	'name'
    ];

    public function students(){
    	return $this->belongsToMany('App\Student');
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }
    
}
