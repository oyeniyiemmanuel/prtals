<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Cmgmyr\Messenger\Traits\Messagable;
use Laravel\Scout\Searchable;

class User extends Authenticatable
{
    use Searchable;
    use Notifiable;
    use EntrustUserTrait;
    use Messagable;

    protected $fillable = [
        'name'
        ,'username'
        ,'reg_no'
        ,'school_id'
        ,'university_id'
        ,'student_id'
        ,'teacher_id'
        ,'authority_id'
        ,'email'
        ,'verified'
        ,'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function student(){
        return $this->hasOne('App\Student');
    }

    public function undergraduate(){
        return $this->hasOne('App\Undergraduate');
    }

    public function teacher(){
        return $this->hasOne('App\Teacher');
    }

    public function authority(){
        return $this->hasOne('App\Authority');
    }

    public function school(){
        return $this->belongsTo('App\School');
    }

    public function university(){
        return $this->belongsTo('App\University');
    }

    public function grade(){
        return $this->belongsTo('App\Grade');
    }

    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function news(){
        return $this->hasMany('App\News');
    }

    public function announcements(){
        return $this->hasMany('App\Announcement');
    }

    public function events(){
        return $this->hasMany('App\Event');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function likedPosts()
    {
        return $this->morphedByMany('App\Post', 'likeable')->whereDeletedAt(null);
    }

    public function likedComments()
    {
        return $this->morphedByMany('App\Comment', 'likeable')->whereDeletedAt(null);
    }

    public function picture(){
        return $this->hasOne('App\Picture');
    }
}
