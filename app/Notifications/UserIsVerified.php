<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserIsVerified extends Notification
{
    use Queueable;

    private $user;
    private $institution;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $institution)
    {
        $this->user = $user;
        $this->institution = $institution;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message1 = 'Welcome to prtals, you are verified as a student of '
                    .ucwords($this->institution->name.'.');
        $message2 = 'Start enlightening other students about what you know in various fields, keep writing news about your school, and don\'t hesitate to test the knowledge of others with exciting questions';
        return (new MailMessage)
                    ->subject('Welcome to Prtals')
                    ->greeting('Hello '.ucwords($this->user->name))
                    ->line($message1)
                    ->line($message2)
                    ->action('continue to your dashboard', env('APP_URL').'/dashboard');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
