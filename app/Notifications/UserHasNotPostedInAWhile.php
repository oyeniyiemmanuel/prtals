<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserHasNotPostedInAWhile extends Notification
{
    use Queueable;

    private $user;
    private $last_post;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $last_post)
    {
        $this->user = $user;
        $this->last_post = $last_post;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('You Haven\'t Posted In A While')
                    ->greeting('Hello '.ucwords($this->user->name))
                    ->line('It has been a while since you posted on prtals, your last post was <b>"'.ucwords($this->last_post->title).'"</b> '.$this->last_post->created_at->diffForHumans())
                    ->action('Write a new post', env('APP_URL').'/posts')
                    ->line('Help others learn something cool in your field of study');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
