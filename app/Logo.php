<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logo extends Model
{
    protected $fillable = [
        'user_id'
        ,'school_id'
        ,'university_id'
        ,'url'
        ,'format'
        ,'public_id'
    ];

    public function school(){
        return $this->belongsTo('App\School');
    }

    public function university(){
        return $this->belongsTo('App\University');
    }
}
