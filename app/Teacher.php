<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'reg_no'
        ,'school_id'
        ,'department_id'
        ,'gender'
        ,'state_of_origin'
        ,'address'
        ,'residential_state'
        ,'d_o_b'
        ,'local_gov'
        ,'address'
        ,'phone'
        ,'bio'
        ,'g_plus'
        ,'facebook'
        ,'twitter'
        ,'added_by'
    ];


    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function departments(){
        return $this->belongsToMany('App\Department')->withTimestamps();
    }

    public function subjects(){
        return $this->belongsToMany('App\Subject')->withTimestamps();
    }

    public function duties(){
        return $this->hasMany('App\duty');
    }

    public function grades(){
    	return $this->belongsToMany('App\Grade')->withTimestamps();
    }

    public function wings(){
        return $this->belongsToMany('App\Wing')->withTimestamps();
    }
}
