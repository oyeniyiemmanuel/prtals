<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\SchoolRepository;
use App\Repositories\UniversityRepository;
use App\Repositories\RoleRepository;
use App\Repositories\AuthorityRepository;
use App\Repositories\TeacherRepository;
use App\Repositories\StudentRepository;
use App\Repositories\PostRepository;
use App\Repositories\CommentRepository;
use App\Repositories\LikeRepository;
use App\Repositories\UserRepository;
use App\Repositories\SubjectRepository;
use App\Repositories\FieldRepository;
use App\Repositories\NewsRepository;
use App\Repositories\AnnouncementRepository;
use App\Repositories\EventRepository;
use App\Repositories\Interfaces\UniversityInterface;
use App\Repositories\Interfaces\SchoolInterface;
use App\Repositories\Interfaces\RoleInterface;
use App\Repositories\Interfaces\AuthorityInterface;
use App\Repositories\Interfaces\TeacherInterface;
use App\Repositories\Interfaces\StudentInterface;
use App\Repositories\Interfaces\PostInterface;
use App\Repositories\Interfaces\CommentInterface;
use App\Repositories\Interfaces\LikeInterface;
use App\Repositories\Interfaces\UserInterface;
use App\Repositories\Interfaces\SubjectInterface;
use App\Repositories\Interfaces\FieldInterface;
use App\Repositories\Interfaces\NewsInterface;
use App\Repositories\Interfaces\AnnouncementInterface;
use App\Repositories\Interfaces\EventInterface;
use App\Post;
use App\Subject;
use App\Field;
use App\Grade;
use App\Department;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer([
                'layout.master'
                ,'school.choose_schoolV'
                ,'school.all_schoolsV'
                ,'school.new_school_requestV'
                ,'school.admin_add_new_school'
                ,'school.edit_details'
                ,'school.postsV'
                ,'school.mineV'
                ,'university.choose_universityV'
                ,'university.all_universitiesV'
                ,'university.new_university_requestV'
                ,'university.admin_add_new_university'
                ,'university.edit_details'
                ,'university.postsV'
                ,'university.mineV'
                ,'partial.post_dashmenu'
                ,'user.edit_detailsV'
                ,'category.indexV'
                ,'field.indexV'
                ,'subject.indexV'
                ,'subject.admin_add_new_subject'
                ,'teacher.view_subjects'
                ,'post.createV'
                ,'post.schoolV'
                ,'post.userV'
                ,'post.editV'
                ,'bots.post'
                ,'plain.homepageV'
            ], function($view){
                //get the list of all the Nigerian states
                $json_file = file_get_contents("js/states-list.js");
                $states = json_decode($json_file, true);
                asort($states);
                //get the letters a-z
                $json_file2 = file_get_contents("js/alphabets-list.js");
                $alphabets = json_decode($json_file2, true);
                asort($alphabets);

                //get all post categories and subjects
                $post_categories = Post::select('category')->groupBy('category')->get();
                $subject_categories = Subject::orderBy('name')->pluck('name');
                $field_categories = Field::orderBy('name')->pluck('name');
                $post_random = Post::inRandomOrder()->select('category')->groupBy('category')->get();
                $subject_random = Subject::inRandomOrder()->pluck('name');
                $field_random = Field::inRandomOrder()->pluck('name');

                $grades = Grade::orderBy('name')->pluck('name');
                $departments = Department::orderBy('name')->pluck('name');

                $view->with([
                        'states' => $states
                        ,'alphabets' => $alphabets
                        ,'post_categories' => $post_categories
                        ,'subject_categories' => $subject_categories
                        ,'field_categories' => $field_categories
                        ,'post_random' => $post_random
                        ,'subject_random' => $subject_random
                        ,'field_random' => $field_random
                        ,'grades' => $grades
                        ,'departments' => $departments
                    ]);
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SchoolInterface::class, SchoolRepository::class);
        $this->app->bind(UniversityInterface::class, UniversityRepository::class);
        $this->app->bind(RoleInterface::class, RoleRepository::class);
        $this->app->bind(AuthorityInterface::class, AuthorityRepository::class);
        $this->app->bind(TeacherInterface::class, TeacherRepository::class);
        $this->app->bind(StudentInterface::class, StudentRepository::class);
        $this->app->bind(PostInterface::class, PostRepository::class);
        $this->app->bind(CommentInterface::class, CommentRepository::class);
        $this->app->bind(LikeInterface::class, LikeRepository::class);
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(SubjectInterface::class, SubjectRepository::class);
        $this->app->bind(FieldInterface::class, FieldRepository::class);
        $this->app->bind(NewsInterface::class, NewsRepository::class);
        $this->app->bind(AnnouncementInterface::class, AnnouncementRepository::class);
        $this->app->bind(EventInterface::class, EventRepository::class);
    }
}
