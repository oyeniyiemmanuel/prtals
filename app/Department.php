<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
    	'name'
    ];

    public function users(){
    	return $this->hasMany('App\User');
    }

    public function students(){
    	return $this->hasMany('App\Student');
    }

    public function teachers(){
        return $this->belongsToMany('App\Teacher')->withTimestamps();
    }

    public function grades(){
    	return $this->belongsToMany('App\Grade')->withTimestamps();
    }

    public function subjects(){
        return $this->belongsToMany('App\Subject')->withTimeStamps();
    }

    public function schools(){
        return $this->belongsToMany('App\School')->withTimeStamps();
    }

    public function wings(){
        return $this->belongsToMany('App\Wing')->withTimestamps();
    }
}
