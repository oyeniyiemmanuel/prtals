<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Duty extends Model
{
    protected $fillable = [
        'name'
        ,'subject_id'
        ,'teacher_id'
        ,'department_id'
        ,'grade_id'
        ,'wing_id'
        ,'school_id'
    ];


    public function teacher(){
    	return $this->belongsTo('App\Teacher');
    }
}
