<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Tracker;

class University extends Model
{
    use Searchable;

    protected $fillable = [
    	'name'
        ,'slug'
        ,'founded'
    	,'state'
    	,'address'
    	,'no_of_pupils'
    	,'history'
    	,'tuition'
        ,'accomplishments'
    	,'last_edited_by'
    ];

    public function getRouteKeyName(){
        
        return 'slug';
        
    }

    public function users(){
        return $this->hasMany('App\User');
    }

    public function undergraduates(){
        return $this->hasMany('App\Undergraduate');
    }

    public function departments(){
        return $this->belongsToMany('App\Department')->withTimestamps();
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function news(){
        return $this->hasMany('App\News');
    }

    public function announcements(){
        return $this->hasMany('App\Announcement');
    }

    public function events(){
        return $this->hasMany('App\Event');
    }

    public function pictures(){
        return $this->hasMany('App\Picture');
    }

    public function cover(){
        return $this->hasOne('App\Cover');
    }

    public function logo(){
        return $this->hasOne('App\Logo');
    }

    public function views($slug){

         //return Tracker::logByRouteName('schools.show')
         //                   ->where(function($query) use ($slug)
         //                   {
         //                       $query
         //                           ->where('parameter', 'school')
         //                           ->where('value', $slug);
         //                   })
         //                   ->select('tracker_log.session_id')
         //                   ->groupBy('tracker_log.session_id')
         //                   ->distinct()
         //                   ->count('tracker_log.session_id');
        $result = Tracker::logByRouteName('universities.show')
                                ->where(function($query) use ($slug)
                                {
                                    $query
                                        ->where('parameter', 'slug')
                                        ->where('value', $slug);
                                })
                                ->count()/2;
        return round($result);

    }
}
