<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wing extends Model
{
    protected $fillable = [
    	'name'
    ];

    public function schools(){
    	return $this->belongsToMany('App\School')->withTimestamps();
    }

    public function teachers(){
    	return $this->belongsToMany('App\Teacher')->withTimestamps();
    }

    public function students(){
    	return $this->hasMany('App\Student');
    }

    public function grades(){
    	return $this->belongsToMany('App\Grade')->withTimestamps();
    }

    public function departments(){
    	return $this->belongsToMany('App\Department')->withTimestamps();
    }

    public function subjects(){
    	return $this->belongsToMany('App\Subject')->withTimestamps();
    }
}
