<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Undergraduate extends Model
{
    protected $fillable = [
    	'reg_no'
        ,'university_id'
        ,'gender'
        ,'best_course'
        ,'state_of_origin'
        ,'address'
    	,'residential_state'
    	,'d_o_b'
    	,'local_gov'
    	,'address'
        ,'phone'
        ,'bio'
        ,'g_plus'
        ,'facebook'
        ,'twitter'
    ];

    /*protected $dates = [
        'd_o_b'
    ];

    public function getDOBAttribute($dates){

        return Carbon::parse($dates);

    }*/

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function university(){
        return $this->belongsTo('App\University');
    }

}
