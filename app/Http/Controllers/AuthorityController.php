<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\AuthorityInterface;

class AuthorityController extends Controller
{

	private $repository;

    public function __construct(AuthorityInterface $repository)
	{
		$this->middleware(['auth', 'hasNoSchool' , 'hasNoRole', 'isNotAuthority']);

		$this->repository = $repository;

	}

    public function index(){

    	return view('authority.indexV');
    	
    }

    public function newMemberRequest(){

        $school = $this->repository->getMySchool();

    	return view('authority.add_new_memberV', compact('school'));

    }

    public function addNewMember(Request $request){

    	$add = $this->repository->addNewMember($request);

        $school = $this->repository->getMySchool();

    	if($add["status"] == true){

    		flash()->success($add["message"], " ");

    	}else{

    		flash()->info($add["message"], " ");

    	}

    	return view('authority.add_new_memberV', compact('school'));

    }
}
