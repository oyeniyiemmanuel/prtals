<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\EventInterface;
use Auth;

class EventController extends Controller
{
    private $repository;

    public function __construct(EventInterface $repository)
    {

    	$this->middleware(['auth', 'hasNoRole'])->except('index', 'show', 'school', 'user');

    	$this->repository = $repository;
    }

    public function create()
	{
		return view('event.createV');
	}

	public function store(Request $request)
	{
		$slug = $this->repository->store($request);

		return redirect('/events'.'/'.$slug.'/add_photos');
	}

	public function photos($slug)
	{
		$event = $this->repository->getBySlug($slug);

		return view('event.add_photosV', compact('event'));
	}

	public function addPhotos(Request $request)
	{
		$this->validate($request, ['file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

		$event = $this->repository->uploadPhotos($request);

		return redirect('/events');
	}

	public function index()
    {	
    	$events = $this->repository->getAllEvents();

    	return view('event.indexV', compact('events'));

    }

    public function edit($slug)
    {
        $event = $this->repository->getBySlug($slug);

        return view('event.editV', compact('event'));
    }

    public function show($slug)
     {
     	$event_thread = $this->repository->getBySlug($slug);

     	return view('event.showV', compact('event_thread'));
     } 

    public function update(Request $request)
    {
        $slug = $this->repository->update($request);

        flash()->success('Updated', '');

        return redirect('/events'.'/'.$slug.'/add_photos');
    } 

    public function school($slug)
    {
        $events = $this->repository->getSchoolEvents($slug);

        $focus_school = $this->repository->findSchool($slug);

        return view('event.schoolV', compact('events', 'focus_school'));
    }  

    public function university($slug)
    {
        $events = $this->repository->getUniversityEvents($slug);

        $focus_university = $this->repository->findUniversity($slug);

        return view('event.universityV', compact('events', 'focus_university'));
    } 

    public function user($username)
    {
        $events = $this->repository->getUserEvents($username);

        $owner = $this->repository->findUser($username);

        return view('event.userV', compact('events', 'owner'));
    }

    public function destroy(Request $request)
    {
        $action = $this->repository->delete($request);

        return 'deleted';
    }
}
