<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\PostInterface;
use App\Http\Requests\StoreNewPost;
use Auth;

class PostController extends Controller
{
    private $repository;

    public function __construct(PostInterface $repository)
    {

    	$this->middleware(['auth', 'hasNoRole'])->except('index', 'show', 'subject', 'category', 'field', 'user');

    	$this->repository = $repository;
    }

    public function index()
    {	
    	
    	$posts = $this->repository->getAllPosts();

    	return view('post.indexV', compact('posts'));

    }

    public function create()
    {       
        return view('post.createV');
    }

    public function show($slug)
    {
        $post = $this->repository->getBySlug($slug);

        if($post){

            return view('post.showV', compact('post'));

        }else{

            flash()->info('cant find this post, owner may have deleted it', '');

            return redirect()->back();
        }    	
    }

    public function edit($slug)
    {
        $post = $this->repository->getBySlug($slug);

        if(Auth::id() == $post->user->id){

            return view('post.editV', compact('post'));

        }else{
            flash()->error('unauthorised access', '');

            return redirect()->back();
        }
    }

    public function store(StoreNewPost $request)
    {   
        //dd($request->all());

        $slug = $this->repository->makeSlug($request->get('title'));

        $post = $this->repository->store($slug, $request);

        $this->repository->syncWithUser($post);

        $this->repository->syncWithSchool($request, $post);

        flash()->success("Post Submitted", " ");

        return redirect('/posts');
    }

    public function update($slug, StoreNewPost $request)
    {
        $prev_post = $this->repository->getBySlug($slug);	

    	$slug = $this->repository->makeEditSlug($request->get('title'), $prev_post);

    	$post = $this->repository->update($slug, $prev_post, $request);

    	$this->repository->syncWithUser($post);

    	$this->repository->syncWithSchool($request, $post);


    	flash()->success("Post Edited", " ");

    	return redirect('/posts'.'/'.$slug);
    }

    public function school($slug)
    {
        $posts = $this->repository->getSchoolPosts($slug);

        $focus_school = $this->repository->findSchool($slug);

        return view('post.schoolV', compact('posts', 'focus_school'));
    }

    public function university($slug)
    {
        $posts = $this->repository->getUniversityPosts($slug);

        $focus_university = $this->repository->findUniversity($slug);

        return view('post.universityV', compact('posts', 'focus_university'));
    }

    public function user($username)
    {
        $posts = $this->repository->getUserPosts($username);

        $owner = $this->repository->findUser($username);

        return view('post.userV', compact('posts', 'owner'));
    }

    public function category($category)
    {
        $posts = $this->repository->category($category);

        return view('post.categoryV', compact('posts', 'category'));
    }

    public function subject($subject)
    {
        $posts = $this->repository->subject($subject);

        return view('post.subjectV', compact('posts', 'subject'));
    }

    public function field($field)
    {
        $posts = $this->repository->field($field);

        return view('post.fieldV', compact('posts', 'field'));
    }

    public function destroy(Request $request)
    {
        $action = $this->repository->delete($request);

        return 'deleted';
    }
}
