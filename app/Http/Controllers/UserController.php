<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserDetails;
use App\Repositories\Interfaces\UserInterface;
use Auth;

class UserController extends Controller
{
	private $repository;

	public function __construct(UserInterface $repository){

		$this->middleware(['auth', 'hasNoSchool'])->except('viewUser');

		$this->repository = $repository;

	}

    public function profile()
	{
		if(Auth::user()->roles->pluck('name')->isEmpty()){

            return redirect('/dashboard');

        }

		$user = Auth::user();

		return view('user.profileV', compact('user'));
	}

    public function viewUser($username)
	{
		if(Auth::user() && Auth::user()->username == $username ){
			return redirect('/profile');
		}
		
		$user = $this->repository->viewUser($username);

		return view('user.profileV', compact('user'));
	}

    public function editDetails($username)
	{
		$user = $this->repository->findByUsername($username);

		return view('user.edit_detailsV', compact('user'));
	}

    public function updateDetails(UpdateUserDetails $request)
	{
		$updatedUser = $this->repository->updateDetails($request);

		flash()->success('profile details updated', '');

		return redirect('/profile');

	}

	public function updateBio(Request $request)
	{
		$user_role = $this->repository->updateBio($request);

		$user = Auth::user();

        $returnHTML = view('partial.user_bio', compact('user_role', 'user'));

        return response($returnHTML)->getOriginalContent();
	}

	public function updateSocials(Request $request)
	{
		$user_role = $this->repository->updateSocials($request);

		$user = Auth::user();

        $returnHTML = view('partial.user_socials', compact('user_role', 'user'));

        return response($returnHTML)->getOriginalContent();
	}

	public function checkUsername($username)
	{
		$result = $this->repository->checkUsername($username);

        return $result;
	}

	public function uploadProfileImage(Request $request)
	{
		$this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1048']);

		$this->repository->uploadProfileImage($request);

		return 'done';
	}
}
