<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\TeacherInterface;
use Auth;

class TeacherController extends Controller
{
    private $repository;

    public function __construct(TeacherInterface $repository)
	{
		$this->middleware(['auth', 'hasNoSchool', 'hasNoRole', 'isNotTeacher']);

        $this->repository = $repository;

	}

    public function index(){

    	return view('teacher.indexV');
    	
    }

    public function viewSubjects()
    {
        $user = Auth::user();

        return view('teacher.view_subjects', compact('user'));
    }

    public function addSubject(Request $request)
    {
        $check = $this->repository->validateSubjectGradeAndDepartment($request);

        if($check["status"] == false){

            flash()->error($check["message"], " ");

            return redirect('/teacher/manage_subjects');
            
        }

    	$this->repository->addSubject($request);

        return redirect('/teacher/manage_subjects');
    }

    public function removeDuty(Request $request)
    {
        $this->repository->removeDuty($request);

        flash()->info('subject removed', '');

        return redirect()->back();
    }
}
