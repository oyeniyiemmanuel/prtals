<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\AnnouncementInterface;
use Auth;

class AnnouncementController extends Controller
{
    private $repository;

    public function __construct(AnnouncementInterface $repository)
    {

    	$this->middleware(['auth', 'hasNoRole']);

    	$this->repository = $repository;
    }

    public function create()
	{
        $school = Auth::user()->school;

		return view('announcement.createV', compact('school'));
	}

	public function store(Request $request)
	{
		$this->repository->store($request);

		return redirect('/announcements');
	}

	public function index()
    {	
    	//$announcements = $this->repository->getAllAnnouncements();

    	//return view('announcement.indexV', compact('announcements'));
    	
    	$slug = $this->repository->getSlug();

        if(Auth::user()->roles()->first()->name == 'student') {
            return redirect('/announcements'.'/school'.'/'.$slug);
        }

        if(Auth::user()->roles()->first()->name == 'undergraduate') {
            return redirect('/announcements'.'/university'.'/'.$slug);
        }

    }

    public function edit($slug)
    {
        $announcement = $this->repository->getBySlug($slug);

        if(Auth::user()->school_id != null) {
            $school = Auth::user()->school;
            return view('announcement.editV', compact('announcement', 'school'));
        }
        if(Auth::user()->university_id != null) {
            $university = Auth::user()->university;
            return view('announcement.editV', compact('announcement', 'university'));
        }
    }

    public function show($slug)
     {
     	$announcement_thread = $this->repository->getBySlug($slug);

        if(Auth::user()->school_id != null) {
            $school = Auth::user()->school;
            return view('announcement.showV', compact('announcement_thread', 'school'));
        }
        if(Auth::user()->university_id != null) {
            $university = Auth::user()->university;
            return view('announcement.showV', compact('announcement_thread', 'university'));
        }

     } 

    public function update(Request $request)
    {
        $slug = $this->repository->update($request);

        flash()->success('Updated', '');

        return redirect('/announcements'.'/'.$slug);
    } 

    public function school($slug)
    {
        $announcements = $this->repository->getSchoolAnnouncements($slug);

        $school = $this->repository->getSchoolBySlug($slug);

        return view('announcement.schoolV', compact('announcements', 'school'));
    } 

    public function university($slug)
    {
        $announcements = $this->repository->getUniversityAnnouncements($slug);

        $university = $this->repository->getUniversityBySlug($slug);

        return view('announcement.universityV', compact('announcements', 'university'));
    } 

    public function user($username)
    {
        $owner = $this->repository->getUser($username);

        $announcements = $this->repository->getUserAnnouncements($username);

        if($owner->roles()->first()->name == 'student') {
            $school = $this->repository->getSchoolBySlug($owner->school->slug);
            return view('announcement.userV', compact('announcements', 'school', 'owner'));
        }

        if($owner->roles()->first()->name == 'undergraduate') {
            $university = $this->repository->getUniversityBySlug($owner->university->slug);
            return view('announcement.userV', compact('announcements', 'university', 'owner'));
        }
        
    }

    public function destroy(Request $request)
    {
        $action = $this->repository->delete($request);

        return 'deleted';
    }
}
