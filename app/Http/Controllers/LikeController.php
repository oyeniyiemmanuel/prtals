<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\LikeInterface;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{

    private $repository;

    public function __construct(LikeInterface $repository)
    {
        $this->repository = $repository;
    }
    
    public function likePost($id)
    {
        $post = $this->repository->likePost($id);

        $returnHTML = view('partial.half_post_threadV', compact('post'));

        return response($returnHTML)->getOriginalContent();
    }

    public function likeComment($post_id, $comment_id)
    {
        $comment = $this->repository->likeComment($comment_id, $post_id);

        $post = $this->repository->getPost($post_id);        

        $returnHTML = view('partial.comment_threadV', compact('comment', 'post'));

        return response($returnHTML)->getOriginalContent();
    }

}
