<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\News;
use App\Event;
use App\User;
use App\School;
use App\University;

class SearchController extends Controller
{
    public function all(Request $request)
    {
    	$query = $request->get('query');

    	$v = array();

    	$posts = Post::search($query)->get();
    	$p = $posts->count();
    	$values = array_merge($v, ['Post'=>$p]);

    	$news = News::search($query)->get();
    	$n = $news->count();
    	$values = array_merge($values, ['News'=>$n]);

    	$events = Event::search($query)->get();
    	$e = $events->count();
    	$values = array_merge($values, ['Event'=>$e]);

    	$users = User::search($query)->get();
    	$u = $users->count();
    	$values = array_merge($values, ['User'=>$u]);

        $schools = School::search($query)->get();
        $s = $schools->count();
        $values = array_merge($values, ['School'=>$s]);

    	$universities = University::search($query)->get();
    	$s = $universities->count();
    	$values = array_merge($values, ['University'=>$s]);

    	$key = array_keys($values, max($values));

    	$model = "App\\".ucfirst($key[0]);

    	$results = $model::search($query)->paginate(10);

        if($key[0] == 'News'){
            return view('search.'.strtolower($key[0]), compact('results', 'query'));
        }
    	if($key[0] == 'University'){
    		return view('search.universities', compact('results', 'query'));
    	}
    	return view('search.'.strtolower($key[0]).'s', compact('results', 'query'));

    }
    public function schoolsOnly(Request $request)
    {
        $query = $request->get('query');

        $schools = School::search($query)->get();

        $filtered_state = 'search: '.$query;

        return view('school.all_schoolsV', compact('schools', 'filtered_state', 'query'));

    }
    public function universitiesOnly(Request $request)
    {
        $query = $request->get('query');

        $universities = University::search($query)->get();

        $filtered_state = 'search: '.$query;

        return view('university.all_universitiesV', compact('universities', 'filtered_state', 'query'));

    }
    public function typeAndRequest(Request $request, $type)
    {
    	$query = $request->get('query');

    	$model = "App\\".ucfirst($type);

    	$results = $model::search($query)->paginate(10);

        if($type == 'news'){
            return view('search.'.$type, compact('results', 'query'));
        }
    	if($type == 'university'){
    		return view('search.universities', compact('results', 'query'));
    	}
        return view('search.'.$type.'s', compact('results', 'query'));
    	

    }

    public function typeAndQuery($type, $query)
    {
    	$model = "App\\".ucfirst($type);

    	$results = $model::search($query)->paginate(10);

        if($type == 'news'){
            return view('search.'.$type, compact('results', 'query'));
        }
    	if($type == 'university'){
    		return view('search.universities', compact('results', 'query'));
    	}
    	return view('search.'.$type.'s', compact('results', 'query'));
    }
}
