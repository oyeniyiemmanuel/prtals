<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class NotificationController extends Controller
{
	private $repository;

    public function __construct()
    {
    	$this->middleware(['auth']);
    }

    public function markAsRead(User $user){

    	$user->notifications->map(function ($n){
    		$n->markAsRead();
    	});
    }

    public function index()
    {
    	return view('notifications.indexV');
    }

    public function deleteAll()
    {
    	Auth::user()->notifications()->delete();

    	flash()->info('Notifications cleared', '');

    	return view('notifications.indexV');
    }
}
