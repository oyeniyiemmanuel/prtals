<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(){

    	return view('plain.homepageV');

    }

    public function guide(){

    	return view('plain.guideV');
    	
    }

    public function faq(){

    	return view('plain.faqV');
    	
    }
}
