<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\FieldInterface;

class FieldController extends Controller
{
    private $repository;

    public function __construct(FieldInterface $repository)
    {
    	$this->repository = $repository;
    }

    public function index()
    {
        return view('field.indexV');
    }

    public function adminNewFieldForm()
    {
    	return view('field.admin_add_new_field');
    }

    public function adminAddNewField(Request $request)
    {
    	$this->repository->adminAddNewField($request);

        flash()->overlay(''.$request->get('field').' has been added to '.$request->get('department').' department', '');
    	
    	return view('field.admin_add_new_field');
    }
}
