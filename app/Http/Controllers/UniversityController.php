<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\UniversityInterface;
use App\Http\Requests\PleaToAddNewUniversity;
use App\Http\Requests\AdminAddNewUniversity;
use App\Http\Requests\UniversityDetailsRequest;
use App\University;
use Auth;

class UniversityController extends Controller
{
    private $repository;

	public function __construct(UniversityInterface $repository){

		$this->middleware('auth')->except('allUniversities'
                                            ,'show'
                                            ,'filterAllByState'
                                            ,'members'
                                            ,'posts'
                                            ,'events'
                                            ,'news'
                                            );

        $this->middleware('isVerified')->only('chooseUniversity'
                                                ,'attachUniversity'
                                                ,'chooseByState'
                                                ,'newUniversityRequest'
                                                ,'submitNewUniversityRequest'
                                                );

		$this->repository = $repository;
	}

	public function allUniversities(){

        $universities = $this->repository->allUniversities();

        $filtered_state = 'All';

        return view('university.all_universitiesV', compact('universities', 'filtered_state'));
    }

    public function chooseUniversity(){

    	$universities = $this->repository->allUniversities();

    	return view('university.choose_universityV', compact('universities'));
    }

    public function attachUniversity(Request $request){

        $this->repository->syncUniversityWithUser($request->get('university_name'));

        $this->repository->syncRoleWithUser($request);

        $this->repository->verifyUser($request);

        $this->repository->setAsAdminIfUniversityFirstUser($request);

        return redirect('/dashboard');
    }

    public function chooseByState(Request $request){

        $filtered_state = $request->get('state');

        $universities = $this->repository->filterByState($filtered_state);

        return view('university.choose_universityV', compact('universities', 'filtered_state'));
    }

    public function filterAllByState(Request $request){

        $filtered_state = $request->get('state');

        $universities = $this->repository->filterByState($filtered_state);

        return view('university.all_universitiesV', compact('universities', 'filtered_state'));
    }

    public function newUniversityRequest(){

        return view('university.new_university_requestV');
    }

    public function submitNewUniversityRequest(PleaToAddNewUniversity $request){

        $this->repository->submitNewUniversityRequest($request);

        return redirect('/');
    }

    public function adminNewUniversityForm(){

        return view('university.admin_add_new_university');
    }

    public function adminAddNewUniversity(AdminAddNewUniversity $request){

        $result = $this->repository->adminAddNewUniversity($request);

        flash()->overlay(''.ucwords($result["university"]).' was successfully added', '');

    	return view('university.admin_add_new_university');
    }

    public function show($slug)
    {
        $university = $this->repository->getUniversityBySlug($slug);

        return view('university.show', compact('university'));
    }

    public function editDetails($slug)
    {
        $university = $this->repository->getUniversityBySlug($slug);

        if((Auth::user()->hasRole('authority') || Auth::user()->hasRole('university-admin')) && ($university == Auth::user()->university)){

            $university = $this->repository->find(Auth::user()->university_id);

            return view('university.edit_details', compact('university'));
        }else{
            flash()->error("you are not authorised", "");

            return redirect()->back();
        }
    }

    public function updateDetails(UniversityDetailsRequest $request)
    {
        $slug = $this->repository->updateUniversityDetails($request);

        return redirect('/universities'.'/'.$slug);
    }

    public function uploadCoverImage(Request $request)
    {
        $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $this->repository->uploadCoverImage($request);

        return 'done';
    }

    public function uploadLogo(Request $request)
    {
        $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $this->repository->uploadLogo($request);

        return 'done';
    }
    public function members($slug)
    {
        $members = $this->repository->allMembers($slug);

        $university = $this->repository->getUniversityBySlug($slug);

        return view('university.membersV', compact('members', 'university'));
    }
    public function posts($slug)
    {
        $posts = $this->repository->getUniversityPosts($slug);

        $university = $this->repository->getUniversityBySlug($slug);

        return view('university.postsV', compact('posts', 'university'));
    }
    public function myUniversityPosts($slug)
    {
        $posts = $this->repository->getMyUniversityPosts($slug);

        $university = $this->repository->getUniversityBySlug($slug);

        return view('university.posts_by_meV', compact('posts', 'university'));
    }
    public function events($slug)
    {
        $events = $this->repository->getUniversityEvents($slug);

        $university = $this->repository->getUniversityBySlug($slug);

        return view('university.eventsV', compact('events', 'university'));
    }
    public function myUniversityEvents($slug)
    {
        $events = $this->repository->getMyUniversityEvents($slug);

        $university = $this->repository->getUniversityBySlug($slug);

        return view('university.events_by_meV', compact('events', 'university'));
    }
    public function news($slug)
    {
        $news = $this->repository->getUniversityNews($slug);

        $university = $this->repository->getUniversityBySlug($slug);

        return view('university.newsV', compact('news', 'university'));
    }
    public function myUniversitynews($slug)
    {
        $news = $this->repository->getMyUniversityNews($slug);

        $university = $this->repository->getUniversityBySlug($slug);

        return view('university.news_by_meV', compact('news', 'university'));
    }
    public function announcements($slug)
    {
        return redirect('announcements');
    }

}
