<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\CommentInterface;

class CommentController extends Controller
{
    private $repository;

    public function __construct(CommentInterface $repository)
    {

    	$this->middleware(['auth', 'hasNoRole']);

    	$this->repository = $repository;
    }

    public function store(Request $request)
    {
        $comment = $this->repository->store($request);

        $this->repository->syncWithUser($comment);

        $this->repository->syncWithPost($comment, $request);

        $post = $this->repository->getPost($comment);

        $returnHTML = view('partial.half_post_threadV', compact('post'));

        return response($returnHTML)->getOriginalContent();
    }

    public function destroy(Request $request)
    {
        $comment = $this->repository->getComment($request->get('comment_id'));

        $post = $this->repository->getPost($comment);

    	$this->repository->delete($request);

        $returnHTML = view('partial.half_post_threadV', compact('post'));

        return response($returnHTML)->getOriginalContent();

    }
}
