<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\SubjectInterface;

class SubjectController extends Controller
{
    private $repository;

    public function __construct(SubjectInterface $repository)
    {
    	$this->repository = $repository;
    }

    public function index()
    {
        return view('subject.indexV');
    }

    public function adminNewSubjectForm()
    {
    	return view('subject.admin_add_new_subject');
    }

    public function adminAddNewSubject(Request $request)
    {
    	$this->repository->adminAddNewSubject($request);

        flash()->overlay(''.$request->get('subject').' has been added to '.$request->get('department').' department', '');
    	
    	return view('subject.admin_add_new_subject');
    }
}
