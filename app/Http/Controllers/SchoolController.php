<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\SchoolInterface;
use App\Http\Requests\PleaToAddNewSchool;
use App\Http\Requests\AdminAddNewSchool;
use App\Http\Requests\SchoolDetailsRequest;
use App\School;
use Auth;

class SchoolController extends Controller
{
	private $repository;

	public function __construct(SchoolInterface $repository){

		$this->middleware('auth')->except('allSchools'
                                            ,'show'
                                            ,'filterAllByState'
                                            ,'members'
                                            ,'posts'
                                            ,'events'
                                            ,'news'
                                            );

        $this->middleware('isVerified')->only('chooseSchool'
                                                ,'attachSchool'
                                                ,'chooseByState'
                                                ,'newSchoolRequest'
                                                ,'submitNewSchoolRequest'
                                                );

		$this->repository = $repository;
	}

    public function allSchools(){

        $schools = $this->repository->allSchools();

        $filtered_state = 'All';

        return view('school.all_schoolsV', compact('schools', 'filtered_state'));
    }

    public function chooseSchool(){

    	$schools = $this->repository->allSchools();

    	return view('school.choose_schoolV', compact('schools'));
    }

    public function attachSchool(Request $request){

        $this->repository->syncSchoolWithUser($request->get('school_name'));

        $this->repository->syncRoleWithUser($request);

        $this->repository->verifyUser($request);

        $this->repository->setAsAdminIfSchoolFirstUser($request);

        return redirect('/dashboard');
    }

    public function chooseByState(Request $request){

        $filtered_state = $request->get('state');

        $schools = $this->repository->filterByState($filtered_state);

        return view('school.choose_schoolV', compact('schools', 'filtered_state'));
    }

    public function filterAllByState(Request $request){

        $filtered_state = $request->get('state');

        $schools = $this->repository->filterByState($filtered_state);

        return view('school.all_schoolsV', compact('schools', 'filtered_state'));
    }

    public function newSchoolRequest(){

        return view('school.new_school_requestV');
    }

    public function submitNewSchoolRequest(PleaToAddNewSchool $request){

        $this->repository->submitNewSchoolRequest($request);

        return redirect('/');
    }

    public function adminNewSchoolForm(){

        return view('school.admin_add_new_school');
    }

    public function adminAddNewSchool(AdminAddNewSchool $request){

        $result = $this->repository->adminAddNewSchool($request);

        flash()->overlay(''.ucwords($result["school"]).' was successfully added', '');

    	return view('school.admin_add_new_school');
    }

    public function show($slug)
    {
        $school = $this->repository->getSchoolBySlug($slug);

        return view('school.show', compact('school'));
    }

    public function editDetails($slug)
    {
        $school = $this->repository->getSchoolBySlug($slug);

        if((Auth::user()->hasRole('authority') || Auth::user()->hasRole('school-admin')) && ($school == Auth::user()->school)){

            $school = $this->repository->find(Auth::user()->school_id);

            return view('school.edit_details', compact('school'));
        }else{
            flash()->error("you are not authorised", "");

            return redirect()->back();
        }
    }

    public function updateDetails(SchoolDetailsRequest $request)
    {
        $slug = $this->repository->updateSchoolDetails($request);

        return redirect('/schools'.'/'.$slug);
    }

    public function uploadCoverImage(Request $request)
    {
        $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $this->repository->uploadCoverImage($request);

        return 'done';
    }

    public function uploadLogo(Request $request)
    {
        $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $this->repository->uploadLogo($request);

        return 'done';
    }
    public function members($slug)
    {
        $members = $this->repository->allMembers($slug);

        $school = $this->repository->getSchoolBySlug($slug);

        return view('school.membersV', compact('members', 'school'));
    }
    public function posts($slug)
    {
        $posts = $this->repository->getSchoolPosts($slug);

        $school = $this->repository->getSchoolBySlug($slug);

        return view('school.postsV', compact('posts', 'school'));
    }
    public function mySchoolPosts($slug)
    {
        $posts = $this->repository->getMySchoolPosts($slug);

        $school = $this->repository->getSchoolBySlug($slug);

        return view('school.posts_by_meV', compact('posts', 'school'));
    }
    public function events($slug)
    {
        $events = $this->repository->getSchoolEvents($slug);

        $school = $this->repository->getSchoolBySlug($slug);

        return view('school.eventsV', compact('events', 'school'));
    }
    public function mySchoolEvents($slug)
    {
        $events = $this->repository->getMySchoolEvents($slug);

        $school = $this->repository->getSchoolBySlug($slug);

        return view('school.events_by_meV', compact('events', 'school'));
    }
    public function news($slug)
    {
        $news = $this->repository->getSchoolNews($slug);

        $school = $this->repository->getSchoolBySlug($slug);

        return view('school.newsV', compact('news', 'school'));
    }
    public function mySchoolnews($slug)
    {
        $news = $this->repository->getMySchoolNews($slug);

        $school = $this->repository->getSchoolBySlug($slug);

        return view('school.news_by_meV', compact('news', 'school'));
    }
}
