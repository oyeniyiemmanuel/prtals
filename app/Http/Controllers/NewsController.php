<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\NewsInterface;

class NewsController extends Controller
{
	private $repository;

    public function __construct(NewsInterface $repository)
    {

    	$this->middleware(['auth', 'hasNoRole'])->except('index', 'show', 'school', 'university', 'user');

    	$this->repository = $repository;
    }

    public function create()
	{
		return view('news.createV');
	}

	public function store(Request $request)
	{
		$this->repository->store($request);

		return redirect('/news');
	}

	public function index()
    {	
    	$news = $this->repository->getAllNews();

    	return view('news.indexV', compact('news'));

    }

    public function edit($slug)
    {
        $news = $this->repository->getBySlug($slug);

        return view('news.editV', compact('news'));
    }

    public function show($slug)
     {
     	$news_thread = $this->repository->getBySlug($slug);

     	return view('news.showV', compact('news_thread'));
     } 

    public function update(Request $request)
    {
        $slug = $this->repository->update($request);

        flash()->success('Updated', '');

        return redirect('/news'.'/'.$slug);
    } 

    public function school($slug)
    {
        $news = $this->repository->getSchoolNews($slug);

        $focus_school = $this->repository->findSchool($slug);

        return view('news.schoolV', compact('news', 'focus_school'));
    }

    public function university($slug)
    {
        $news = $this->repository->getUniversityNews($slug);

        $focus_university = $this->repository->findUniversity($slug);

        return view('news.universityV', compact('news', 'focus_university'));
    } 

    public function user($username)
    {
        $news = $this->repository->getUserNews($username);

        $owner = $this->repository->findUser($username);

        return view('news.userV', compact('news', 'owner'));
    }

    public function destroy(Request $request)
    {
        $action = $this->repository->delete($request);

        return 'deleted';
    } 
}
