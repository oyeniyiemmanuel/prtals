<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\UserYetToMakeFirstPost;
use App\Notifications\UserNotYetVerified;
use App\Notifications\UserHasNotPostedInAWhile;
use App\User;
use Carbon\Carbon;

class ScheduleController extends Controller
{
	public $users;

	public function __construct()
	{
		$this->users = App\User::get();
	}

    public function checkIfUserIsVerified()
    {
    	foreach ($this->users as $user) {
    		if($user->verified == 0) {
    			$user->notify(new UserNotYetVerified($user));
    		}
    	} 
    }

    public function checkIfUserHasMadeAPost()
    {
        foreach ($this->users as $user) {
            if(($user->verified == 1) && ($user->posts->count() == 0)) {
                $user->notify(new UserYetToMakeFirstPost($user));
            }
        } 
    }

    public function checkIfUserHasRecentPost()
    {
    	foreach ($users as $user) {
            if(($user->verified == 1) && ($user->posts->count() != 0)) {

                $last_post = $user->posts()->orderBy('created_at', 'desc')->first();
                $last_post_date = $last_post->created_at;
                $last_post_date->addWeek();
                $now = Carbon::now();

                if($last_post_date->lte($now)) {
                    $user->notify(new UserHasNotPostedInAWhile($user, $last_post));
                }
            }
        }
    }
}
