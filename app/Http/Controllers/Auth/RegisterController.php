<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/choose_student_type';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $username = $this->makeUsername($data['name']);

        return User::create([
            'name' => $data['name'],
            'username' => $username,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function makeUsername($name)
    {
        $username = str_replace(' ', '_', substr(strtolower($name), 0, 150) );

        $latestUsername = 
            User::whereRaw("username RLIKE '^{$username}(_[0-9]*)?$'")
                ->latest('id')
                ->pluck('username')->first();

        if( $latestUsername ){
            $pieces = explode('_', $latestUsername);
            $number = intval(end($pieces));
            $username.='_'.($number + 1);
        }  
        
        return $username;
    }

}
