<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{

	public function __construct()
    {
    	$this->middleware(['auth', 'hasNoRole']);

    }

    public function index()
    {
    	$currentUserId = Auth::user()->id;
        // All threads, ignore deleted/archived participants
        // $threads = Thread::getAllLatest()->get();
        // All threads that user is participating in
        $threads = Thread::forUser($currentUserId)->latest('updated_at')->get();
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages($currentUserId)->latest('updated_at')->get();

        //dd($threads);

    	return view('message.indexV', compact('threads'));
    }

    public function show($id)
    {
    		try {

	            $thread = Thread::findOrFail($id);

	            if($thread->hasParticipant(Auth::id())){

	            	$thread->markAsRead(Auth::id());

	            	return view('message.showV', compact('thread', 'users'));

	            }else{
		    		flash()->error("Your aren't authorised to view this thread", " ");

			        return redirect('messages');
		    	}

	        } catch (ModelNotFoundException $e) {

	            flash()->info("Message Thread Not found", " ");

	            return redirect('messages');
	        }	        
    }

    public function create()
    {
        $users = User::where([
            ['id', '!=', Auth::id()]
            ,['verified', '=', 1]
            ])->get();
        
        return view('message.createV', compact('users'));
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $thread = Thread::create(
            [
                'subject' => $input['message'],
            ]
        );
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'body'      => $input['message'],
            ]
        );
        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'last_read' => new Carbon,
            ]
        );
        // Recipients
        if ($request->has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }
        return redirect('messages');
    }

    public function update($id, Request $request)
    {
    	$input = $request->all();

        try {

            $thread = Thread::findOrFail($id);

        } catch (ModelNotFoundException $e) {

            flash()->info("Message Thread Not found", " ");

            return redirect('messages');
        }

        $thread->activateAllParticipants();
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::id(),
                'body'      => $input['message'],
            ]
        );
        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if ($request->has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }
        return redirect('messages/' . $id);
    }

    public function destroy($id)
    {
    	$thread = Thread::findOrFail($id);

        $thread->markAsRead(Auth::id());

    	$thread->removeParticipant(Auth::id());

    	return 'deleted';
    }
}
