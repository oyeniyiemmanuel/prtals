<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\RoleInterface;
use App\School;
use Auth;

class RoleController extends Controller
{
    private $repository;

	public function __construct(RoleInterface $repository){

		$this->middleware(['auth', 'hasNoSchool', 'isVerified']);

		$this->repository = $repository;

	}

	public function chooseRole(){

		if(!Auth::user()->school || !Auth::user()->university){
            return redirect('/choose_student_type');
        }

		return view('role.choose_roleV');
		
	}

	public function attachRole(Request $request){

		/*$check = $this->repository->checkRegNo($request);
		
		if($check["status"] == false){

    		flash()->error($check["message"], " ");

    		return redirect('/choose_role');
    		
    	}
    	
		$this->repository->syncRoleWithUser($request);

		$this->repository->verifyUser($request);

		$this->repository->setAsAdminIfSchoolFirstUser($request);*/

		return redirect('/dashboard'); 

	}

	public function dashboard(Request $request){

		$role = $this->repository->determineRoleView();
		// redirects to either /student, /teacher /authority or /choose_role
		return redirect('/'.$role);

	}

}
