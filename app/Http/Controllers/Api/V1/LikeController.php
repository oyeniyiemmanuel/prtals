<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\LikeInterface;

class LikeController extends Controller
{
    private $repository;

    public function __construct(LikeInterface $repository)
    {
        $this->repository = $repository;
    }

    public function store(Request $request)
    {
    	return $this->repository->apiLikePost($request->get('post_id'), $request->get('user_id'));
    }
}
