<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use App\Field;
use App\Post;

class GoodyBagController extends Controller
{
    public function getPostsSubjects() {
    	return Subject::orderBy('name')->pluck('name');
    }

    public function getPostsFields() {
    	return Field::orderBy('name')->pluck('name');
    }

    public function getPostsCategories() {
    	return Post::select('category')->groupBy('category')->pluck('category');
    }
}
