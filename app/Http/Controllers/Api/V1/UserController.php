<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\UserInterface;

class UserController extends Controller
{
    private $repository;

	public function __construct(UserInterface $repository){

		//$this->middleware(['auth', 'hasNoSchool'])->except('viewUser');

		$this->repository = $repository;

	}

	public function show($id)
	{
		return $this->repository->findById($id)->toJson();
	}
}
