<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\CommentInterface;

class CommentController extends Controller
{
    private $repository;

    public function __construct(CommentInterface $repository)
    {
    	$this->repository = $repository;
    }

    public function store(Request $request)
    {
    	$comment = $this->repository->store($request);

        $this->repository->apiSyncWithUser($comment, $request);

        $this->repository->apiSyncWithPost($comment, $request);

        return $comment;
    }
}
