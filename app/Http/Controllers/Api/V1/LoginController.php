<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    private $email;
    private $password;

    public function __construct(Request $request)
    {
        $this->email = $request->email;
        $this->password = $request->password;
    }

    public function login()
    {
        //$this->middleware('guest', ['except' => 'logout']);
        if (Auth::attempt(['email' => $this->email, 'password' => $this->password])) {
            // Authentication passed...
            $data = User::with('posts', 'news', 'announcements', 'events', 'student', 'undergraduate', 'picture', 'school.logo', 'university.logo')->where('id', Auth::id())->get()->first();

            return $data->toJSON();
        } else {
            return json_decode('["fail"]', true);
        }
    }
}
