<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\PostInterface;

class PostController extends Controller
{

	private $repository;

    public function __construct(PostInterface $repository)
    {

    	//$this->middleware(['auth', 'hasNoRole']);

    	$this->repository = $repository;
    }

    public function index()
    {
        return $this->repository->apiGetAllPosts()->toJson();
    }

    public function store(Request $request)
    {
        return $this->repository->apiStore($request);
    }

    public function show($id)
    {
        return $this->repository->apiShow($id);
    }

    public function user($user_id)
    {
    	return $this->repository->apiGetUserPosts($user_id)->toJson();
    }
}
