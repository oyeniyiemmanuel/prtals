<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UniversityDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
            ,'founded' => 'required'
            ,'state' => 'required'
            ,'address' => 'required'
            ,'no_of_pupils' => 'required'
            ,'history' => 'required'
            ,'tuition' => 'required'
            ,'accomplishments' => 'required'
        ];
    }
}
