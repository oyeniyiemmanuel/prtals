<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserDetails extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
            ,'username' => 'required'
            ,'gender' => 'required'
            ,'phone' => 'required'
            ,'residential_state' => 'required'
            ,'state_of_origin' => 'required'
            ,'date_of_birth' => 'required'
            ,'local_gov' => 'required'
        ];
    }
}
