<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PleaToAddNewSchool extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
            ,'state' => 'required'
            ,'address' => 'required'
            ,'reporter_name' => 'required'
            ,'reporter_phone' => 'required'
            ,'reporter_role' => 'required'
        ];
   }
}
