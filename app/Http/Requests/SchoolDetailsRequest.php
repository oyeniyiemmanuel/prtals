<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchoolDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
            ,'founded' => 'required'
            ,'state' => 'required'
            ,'address' => 'required'
            ,'uniform' => 'required'
            ,'address' => 'required'
            ,'no_of_pupils' => 'required'
            ,'no_of_teachers' => 'required'
            ,'no_of_other_staffs' => 'required'
            ,'no_of_classes' => 'required'
            ,'history' => 'required'
            ,'tuition' => 'required'
            ,'rules' => 'required'
            ,'anthem' => 'required'
            ,'accomplishments' => 'required'
        ];
    }
}
