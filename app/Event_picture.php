<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_picture extends Model
{
    protected $fillable = [
        'user_id'
        ,'event_id'
        ,'url'
        ,'format'
        ,'public_id'
    ];

    public function event(){
        return $this->belongsTo('App\Event');
    }
}
