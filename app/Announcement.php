<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $fillable = [
    	'user_id'
    	,'slug'
        ,'school_id'
    	,'university_id'
    	,'title'
    	,'message'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function school(){
        return $this->belongsTo('App\School');
    }

    public function university(){
        return $this->belongsTo('App\University');
    }
}
