<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewUniversityRequest extends Model
{
    protected $fillable = [
    	'name'
    	,'state'
    	,'address'
    	,'reporter_name'
    	,'reporter_phone'
    	,'reporter_role'
    ];
}
