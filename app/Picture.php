<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $fillable = [
        'user_id'
        ,'post_id'
        ,'school_id'
        ,'url'
        ,'format'
        ,'public_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function post(){
        return $this->belongsTo('App\Post');
    }

    public function school(){
        return $this->belongsTo('App\School');
    }
}
