<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Student extends Model
{
    protected $fillable = [
    	'reg_no'
        ,'school_id'
        ,'department_id'
        ,'wing_id'
        ,'gender'
        ,'best_subject'
        ,'state_of_origin'
        ,'address'
    	,'residential_state'
    	,'d_o_b'
    	,'local_gov'
    	,'address'
        ,'phone'
        ,'bio'
        ,'g_plus'
        ,'facebook'
        ,'twitter'
    	,'added_by'
    ];

    /*protected $dates = [
        'd_o_b'
    ];

    public function getDOBAttribute($dates){

        return Carbon::parse($dates);

    }*/

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function school(){
        return $this->belongsTo('App\School');
    }

    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function grade(){
        return $this->belongsTo('App\Grade');
    }

    public function wing(){
        return $this->belongsTo('App\Wing');
    }

    public function subjects(){
        return $this->belongsToMany('App\Subject');
    }
    
    public function fields(){
    	return $this->belongsToMany('App\Field');
    }

    public function wings(){
        return $this->belongsToMany('App\Wing')->withTimestamps();
    }
}
