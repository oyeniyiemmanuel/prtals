<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
	protected $fillable = [
    	'name'
    ];

    public function students(){
    	return $this->belongsToMany('App\Student');
    }

    public function teachers(){
    	return $this->belongsToMany('App\Teacher')->withTimestamps();
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function departments(){
        return $this->belongsToMany('App\Department')->withTimestamps();
    }

    public function grades(){
        return $this->belongsToMany('App\Grade')->withTimestamps();
    }

    public function wings(){
        return $this->belongsToMany('App\Wing')->withTimestamps();
    }
}
