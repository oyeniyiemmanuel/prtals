<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authority extends Model
{
    protected $fillable = [
    	'reg_no'
        ,'school_id'
        ,'department_id'
        ,'gender'
        ,'state_of_origin'
        ,'address'
        ,'residential_state'
        ,'d_o_b'
        ,'local_gov'
        ,'address'
        ,'phone'
        ,'bio'
        ,'g_plus'
        ,'facebook'
        ,'twitter'
        ,'added_by'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function school(){
        return $this->belongsTo('App\School');
    }
}
