<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $fillable = [
    	'name'
    ];

    public function users(){
    	return $this->hasMany('App\User');
    }

    public function students(){
    	return $this->hasMany('App\Student');
    }

    public function teachers(){
    	return $this->hasMany('App\Teacher')->withTimestamps();
    }

    public function schools(){
        return $this->belongsToMany('App\School')->withTimestamps();
    }

    public function departments(){
        return $this->belongsToMany('App\Department')->withTimestamps();
    }

    public function subjects(){
        return $this->belongsToMany('App\Subject')->withTimestamps();
    }

    public function wings(){
        return $this->belongsToMany('App\Wing')->withTimestamps();
    }
}
