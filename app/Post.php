<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Auth;

class Post extends Model
{
    use Searchable;

    protected $fillable = [
        'title'
        ,'slug'
        ,'message'
        ,'user_id'
        ,'school_id'
        ,'university_id'
        ,'subject_id'
        ,'category'
        ,'institution_type'
        ,'institution_type_plural'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function school(){
        return $this->belongsTo('App\School');
    }

    public function university(){
        return $this->belongsTo('App\University');
    }

    public function subject(){
        return $this->belongsTo('App\Subject');
    }

    public function field(){
        return $this->belongsTo('App\Field');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function likes()
    {
        return $this->morphToMany('App\User', 'likeable')->whereDeletedAt(null);
    }

    public function getIsLikedAttribute()
    {
        $like = $this->likes()->whereUserId(Auth::id())->first();
        return (!is_null($like)) ? true : false;
    }

    public function pictures(){
        return $this->hasMany('App\Picture');
    }
}
