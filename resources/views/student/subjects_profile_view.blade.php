<div class="ui secondary segment">
	<div class="ui header">Subjects</div>
	<table class="ui celled striped table"><thead>
		<tr>
		  <th>Name</th>
		  <th>Teacher</th>
		</tr>
		</thead>
		<tbody>
		@if($user->student && !empty($user->department->subjects))
		  @foreach($user->department->subjects->sortBy('name') as $subject)
		    <tr>
		      <td>
		      	<a href="{{ url('/posts') }}/subject/{{ $subject->name }}">
		        <i class="circle {{ $subject->name }} icon"></i> {{ ucfirst($subject->name) }}
		        @if($user->student->best_subject == $subject->name)
			      	<div class="ui mini black left pointing label">Favorite</div>
			    @endif
			    </a>
		      </td>
		      <td>
				<?php
					$x = App\Duty::where([
							['subject_id', $subject->id]
							,['department_id', $user->student->department_id]
							,['grade_id', $user->student->grade_id]
							,['wing_id', $user->student->wing_id]
							,['school_id', $user->school_id]
						])->get();

					if(!empty($x->all())){
						foreach($x as $i){?>
							<span class="ui small label ">
      							<i class="user icon"></i>
      							 {{ucwords($i->teacher->user->name)}}
  							</span>
						<?php }
					}else{
						echo '<em>no teacher</em>';
					}
				?>
		      </td>
		    </tr>
		   @endforeach

		@else
			<tr>
		      <td>
		      @if(Auth::id() == $user->id)
		        <i class="circle info icon"></i> edit your profile and select department first
		      @else
		      	<i class="circle info icon"></i><?php $name = explode(' ', $user->name); echo ucfirst(end($name)); ?> has no subjects
		      @endif
		      </td>
		      <td>not set</td>
		    </tr>
		@endif
		</tbody>
	</table>
</div>