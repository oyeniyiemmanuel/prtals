@extends('layout.master')

@section('title', 'Choose Student Type | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => 'You\'re almost done'
			,'trend' =>  'Select Student Type'
		])
	<br>
	<div class="ui container">
		<div class="ui stackable grid">
			<div class="four wide column"></div>
			<div class="eight wide column">
				<div class="ui secondary segment">
					<a href="{{ url('/choose_school') }}">
                      <div class="ui stacked segment">
                        <button type="submit" class="ui fluid large teal submit button">I'm in High School</button>
                      </div>
                    </a>
					<a href="{{ url('/choose_university') }}">
                      <div class="ui stacked segment">
                        <button type="submit" class="ui fluid large teal submit button">I'm an Undergraduate</button>
                      </div>
                    </a>
				</div>
			</div>
			<div class="four wide column"></div>
		</div>
	</div>

@endsection

@section('footer')
	@parent
@endsection