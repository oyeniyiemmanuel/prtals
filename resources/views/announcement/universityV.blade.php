@extends('layout.master')

@section('title', 'Announcements: '.ucwords( Auth::user()->university->name ).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">

		@include('partial.university_page_header')

	</div>
	<br>

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="/announcements/university/{{ Auth::user()->university->slug }}" class="active item">
				    My University Announcements
				  </a>
				  <a href="/announcements/user/{{ Auth::user()->username }}" class="item">
				    Announcements Posted By Me
				  </a>
				</div>
				
			</div>
			<div class="eight wide column announcement_threads">
				@if(empty($announcements->all()))
					<div class="ui centered dividing header">
						<div class="content">
						  Announcements
						</div>
						<div class="sub header">0 in total</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No announcements to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
				
					<div class="ui fluid card">
						<div id="cover_wrapper" class="image">
							@if($announcements->first()->university->cover && !empty($announcements->first()->university->cover->url))
							<img id="cover" src="{{ $announcements->first()->university->cover->url }}">
							@else
							<img id="cover" src="/media/img/university_cover.jpg">
							@endif
						</div>
					</div>

					<div class="ui centered block header">
						@if($announcements->first()->university->logo && !empty($announcements->first()->university->logo->url))
						<img class="ui avatar image" src="{{ $announcements->first()->university->logo->url }}">
						@else
						<img class="ui avatar image" src="/media/img/university_logo.jpg">
						@endif
						<div class="content">
						  {{ ucwords($announcements->first()->university->name) }} Announcements
						</div>
						<div class="sub header"><em>visible only to University members</em></div>
					</div>

					@foreach($announcements as $announcement_thread)
					
						@include('partial.announcement_threadV')

					@endforeach
					{{ $announcements->links() }}
				@endif
					
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection