@extends('layout.master')

@section('title', 'Edit Announcement | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">
 	<?php
	    $institution_type = Auth::user()->institution_type;
	    $institution_type_plural = Auth::user()->institution_type_plural;
	?>
		@include('partial.'.$institution_type.'_page_header')

	</div>
	<br>

	<div class="ui container welcome">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="/announcements/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My {{ucfirst($institution_type)}} Announcements
				  </a>
				  <a href="/announcements/user/{{ Auth::user()->username }}" class="item">
				    Announcements Posted By Me
				  </a>
				</div>
			</div>
			<div class="eight wide column">
				<div class="ui secondary raised segment">
					<div class="ui dividing header">
						Edit Announcement: <em>{{ ucwords($announcement->title) }}</em>
					</div>
					@include('errors.form_valid')
					<form id="login" class="ui large form" method="POST" action="/announcements/{{ $announcement->slug }}">
			          {{ csrf_field() }}
			          <div class="ui stacked segment">
						<div class="field">
							<label>Title</label>
							<input type="text" name="title" placeholder="announcement title.." value="{{ $announcement->title }}" required>
						</div>

						<div class="field">
							<label>Message</label>
							<textarea rows="6" name="message" placeholder="announcement body.." required>{{ $announcement->message }}</textarea>
						</div>

						<input type="hidden" name="announcement_id" value="{{ $announcement->id }}">

			            <button type="submit" class="ui mini teal submit button">Update</button>
			          </div>

			          <div class="ui error message"></div>

			        </form>
		        </div>
			</div>
			<div class="five wide column"></div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection