@extends('layout.master')

@section('title', ucwords(Auth::user()->name).'\'s Announcements | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	<?php
	    $institution_type = $owner->institution_type;
	    $institution_type_plural = $owner->institution_type_plural;
	?>
	<div class="ui container fluid school_page">

		@include('partial.'.$institution_type.'_page_header')

	</div>
	<br>

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="/announcements/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My {{ucfirst($institution_type)}} Announcements
				  </a>
				  <a href="/announcements/user/{{ Auth::user()->username }}" class="active item">
				    Announcements Posted By Me
				  </a>
				</div>
				
			</div>
			<div class="eight wide column announcement_threads">
				@if(empty($announcements->all()))
					<div class="ui centered dividing header">
						<div class="content">
						  Announcements
						</div>
						<div class="sub header">0 in total</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No announcements to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
				
					<div class="ui fluid card">
						<div id="cover_wrapper" class="image">
							@if($announcements->first()->$institution_type->cover && !empty($announcements->first()->$institution_type->cover->url))
							<img id="cover" src="{{ $announcements->first()->$institution_type->cover->url }}">
							@else
							<img id="cover" src="/media/img/{{$institution_type}}_cover.jpg">
							@endif
						</div>
					</div>

					<div class="ui centered block header">
						@if(Auth::user()->picture && !empty(Auth::user()->picture->url))
						<img class="ui avatar image" src="{{ Auth::user()->picture->url }}">
						@else
						<img class="ui avatar image" src="/media/img/user.jpg">
						@endif
						<div class="content">
						  Announcements Posted By Me
						</div>
						<div class="sub header"><em>visible only to {{$institution_type}} members</em></div>
					</div>

					@foreach($announcements as $announcement_thread)
					
						@include('partial.announcement_threadV')
						
					@endforeach
					{{ $announcements->links() }}
				@endif
					
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection