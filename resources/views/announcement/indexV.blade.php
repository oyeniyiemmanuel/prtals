@extends('layout.master')

@section('title', 'Announcements | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'Announcements'
			,'trend' => 'All'
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">

				<div class="ui dividing header">
					Announcements
				</div>
				<div class="ui mini vertical pointing menu">
				  <a href="/announcements/school/{{ Auth::user()->school->slug }}" class="item">
				    My School
				  </a>
				  <a href="/announcements/user/{{ Auth::user()->username }}" class="item">
				    Posted By Me
				  </a>
				</div>
				
			</div>
			<div class="eight wide column announcement_threads">
				@if(empty($announcements))
					<div class="ui centered dividing header">
						<div class="content">
						  Announcements
						</div>
						<div class="sub header">0 in total</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No announcement to display
						  <div class="sub header"></div>
						</div>
					</div>
				@endif
				
					<div class="ui centered block header">
						<div class="content">
						  All Announcements
						</div>
					</div>
					<div class="ui items">
					@foreach($announcements as $announcement_thread)
					  @include('partial.announcement_threadV')
					@endforeach
					{{ $announcements->links() }}
					</div>
				
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection