@extends('layout.master')

@section('title', 'Create Announcement | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	<div class="ui container fluid school_page">
	<?php
	    $institution_type = Auth::user()->institution_type;
	    $institution_type_plural = Auth::user()->institution_type_plural;
	?>
		@include('partial.'.$institution_type.'_page_header')

	</div>
	<br>

	<div class="ui container welcome">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="/announcements/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My {{ucfirst($institution_type)}} Announcements
				  </a>
				  <a href="/announcements/user/{{ Auth::user()->username }}" class="item">
				    Announcements Posted By Me
				  </a>
				</div>
			</div>

			<div class="eight wide column">
				@include('errors.form_valid')
				<div class="ui secondary raised segment">
					<form id="login" class="ui large form" method="POST" action="{{ url('/announcements') }}">
			          {{ csrf_field() }}
			          <div class="ui stacked segment">
						<div class="field">
							<label>Title</label>
							<input type="text" name="title" placeholder="Announcement title.." required>
						</div>

						<div class="field">
							<label>Message</label>
							<textarea rows="6" name="message" placeholder="Announcement body.." required></textarea>
						</div>

			            <button type="submit" class="ui mini teal submit button">Submit Announcement</button>
			          </div>
			        </form>
		        </div>
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')
				
				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection