@extends('layout.master')

@section('title', ucwords($announcement_thread->title).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">
	<?php
	    $institution_type = Auth::user()->institution_type;
	    $institution_type_plural = Auth::user()->institution_type_plural;
	?>
		@include('partial.'.$institution_type.'_page_header')

	</div>
	<br>

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="/announcements/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My {{ucfirst($institution_type)}} Announcements
				  </a>
				  <a href="/announcements/user/{{ Auth::user()->username }}" class="item">
				   Announcements Posted By Me
				  </a>
				</div>
				
			</div>
			<div class="eight wide column announcement_threads">
				@if(empty($announcement_thread->all()))
					<div class="ui centered dividing header">
						<div class="content">
						  Announcements
						</div>
						<div class="sub header">0 in total</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No announcements to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
				
					<div class="ui fluid card">
						<div id="cover_wrapper" class="image">
							@if($announcement_thread->$institution_type->cover && !empty($announcement_thread->$institution_type->cover->url))
							<img id="cover" src="{{ $announcement_thread->$institution_type->cover->url }}">
							@else
							<img id="cover" src="/media/img/{{$institution_type}}_cover.jpg">
							@endif
						</div>
					</div>

					
					@include('partial.announcement_threadV')

				@endif
					
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection