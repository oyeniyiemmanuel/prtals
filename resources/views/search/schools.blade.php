@extends('layout.master')

@section('title', 'Search Results | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => 'Search'
			,'trend' => ''
		])

	<div class="ui center aligned container">
    	<div class="ui tiny menu">
    		<a class="item" href="{{ url('/search') }}/post/{{ $query }}">Posts</a>
    		<a class="item" href="{{ url('/search') }}/user/{{ $query }}">Students</a>
            <a class="item" href="{{ url('/search') }}/university/{{ $query }}">Universities</a>
    		<a class="active item" href="{{ url('/search') }}/school/{{ $query }}">High Schools</a>
    		<a class="item" href="{{ url('/search') }}/news/{{ $query }}">News</a>
    		<a class="item" href="{{ url('/search') }}/event/{{ $query }}">Events</a>
    	</div>
    </div>
    <br>

    <div class="ui container user_search_results search_results">
    	<div class="ui stackable grid">
    		<div class="one wide column"></div>
    		<div class="ten wide column">
                <a href="{{ url('/schools') }}">
                    <i class="left arrow icon"></i>
                    <b>Go to all schools</b>
                </a>
    			<form class="ui big form" method="GET" action="{{ url('/search') }}/school">
                    {{ csrf_field() }}
                    <div class="field">
                        <div class="ui big action left icon input">
                            <i class="search icon"></i>
                            <input class="set_cursor" type="search" name="query" placeholder="Search High Schools.." value="{{ $query }}" autofocus>
                        </div>
                    </div>
                </form>
                <br>

    			<div class="school_threads">
					<div class="ui segment">
                    @if(!empty($results->all()))
                        @foreach($results->sortBy('name') as $school)
                        <div class="ui grid user_search_result">
                            <div class="four wide column">
                                <div href="/schools/{{ $school->slug }}" class="ui card">
                                    <div class="image">
                                    @if($school->logo && !empty($school->logo->url))
                                      <img alt="{{ $school->name }} {{ env('APP_URL') }}" src="{{ $school->logo->url }}">
                                    @else
                                      <img alt="{{ $school->name }} {{ env('APP_URL') }}" src="/media/img/school_logo.jpg">
                                    @endif
                                    </div>
                                </div>
                            </div>
                            <div class="twelve wide column">
                                <div class="ui card">
                                    <div class="content">
                                        <div class="header">{{ ucwords(strtolower($school->name) ) }}</div>
                                        <div class="meta">
                                            {{ $school->address }}
                                            <span><em><b>{{ $school->state }}</b></em></span>
                                        </div>
                                    </div>
                                    <div class="extra content">
                                        <div class="ui two buttons">
                                            <a href="{{ url('/schools') }}/{{ $school->slug }}" class="ui basic grey button">View page</a>
                                            <a href="{{ url('/schools') }}/{{ $school->slug }}/members" class="ui basic teal button">Members</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        {{ $results->links() }}
                    @else
                        <div class="ui block header">
							<i class="info icon"></i>
							<div class="content">
							  No search results
							  <div class="sub header"></div>
							</div>
						</div>
                    @endif
                    </div>
				</div>
    		</div>
    		<div class="six wide column"></div>
    	</div>
    </div>	
@endsection

@section('footer')
	@parent
@endsection