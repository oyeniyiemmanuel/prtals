@extends('layout.master')

@section('title', 'Search Results | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => 'Search'
			,'trend' => ''
		])

	<div class="ui center aligned container">
    	<div class="ui tiny menu">
    		<a class="item" href="{{ url('/search') }}/post/{{ $query }}">Posts</a>
    		<a class="item" href="{{ url('/search') }}/user/{{ $query }}">Students</a>
    		<a class="item" href="{{ url('/search') }}/university/{{ $query }}">Universities</a>
    		<a class="item" href="{{ url('/search') }}/school/{{ $query }}">High Schools</a>
    		<a class="item" href="{{ url('/search') }}/news/{{ $query }}">News</a>
    		<a class="active item" href="{{ url('/search') }}/event/{{ $query }}">Events</a>
    	</div>
    </div>
    <br>

    <div class="ui container search_results">
    	<div class="ui stackable grid">
    		<div class="one wide column"></div>
    		<div class="ten wide column">
    			<a href="{{ url('/events') }}">
                    <i class="left arrow icon"></i>
                    <b>Go to all events</b>
                </a>
    			<form class="ui big form" method="GET" action="{{ url('/search') }}/event">
                    {{ csrf_field() }}
                    <div class="field">
                    	<div class="ui big action left icon input">
                            <i class="search icon"></i>
                        	<input class="set_cursor" type="search" name="query" placeholder="Search events.." value="{{ $query }}" autofocus>
                        </div>
                    </div>
                </form>
                <br>

    			<div class="event_threads">
					@if(empty($results->all()))
						<div class="ui block header">
							<i class="info icon"></i>
							<div class="content">
							  No search results
							  <div class="sub header"></div>
							</div>
						</div>
					@else
						
						<div class="ui items">
						@foreach($results as $event_thread)
						
							@include('partial.event_threadV')
						
						@endforeach

						{{ $results->links() }}
						</div>
					@endif
				</div>
    		</div>
    		<div class="six wide column"></div>
    	</div>
    </div>	
@endsection

@section('footer')
	@parent
@endsection