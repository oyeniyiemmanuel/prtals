@extends('layout.master')

@section('title', 'Search Results | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => 'Search'
			,'trend' => ''
		])

	<div class="ui center aligned container">
    	<div class="ui tiny menu">
    		<a class="item" href="{{ url('/search') }}/post/{{ $query }}">Posts</a>
    		<a class="active item" href="{{ url('/search') }}/user/{{ $query }}">Students</a>
    		<a class="item" href="{{ url('/search') }}/university/{{ $query }}">Universities</a>
    		<a class="item" href="{{ url('/search') }}/school/{{ $query }}">High Schools</a>
    		<a class="item" href="{{ url('/search') }}/news/{{ $query }}">News</a>
    		<a class="item" href="{{ url('/search') }}/event/{{ $query }}">Events</a>
    	</div>
    </div>
    <br>

    <div class="ui container user_search_results search_results">
    	<div class="ui stackable grid">
    		<div class="one wide column"></div>
    		<div class="ten wide column">
    			<form class="ui big form" method="GET" action="{{ url('/search') }}/user">
                    {{ csrf_field() }}
                    <div class="field">
                    	<div class="ui big action left icon input">
                            <i class="search icon"></i>
	                        <input class="set_cursor" type="search" name="query" placeholder="Search school.." value="{{ $query }}" autofocus>
	                    </div>
                    </div>
                </form>
                <br>

    			<div class="user_threads messages_threads">
					@if(!empty($results->all()))
						@foreach($results as $member)
						<div class="ui grid user_search_result">
							<div class="four wide column">
								<div href="/profile/{{ $member->username }}" class="ui card">
									<div class="image">
									@if($member->picture && !empty($member->picture->url))
									  <img alt="{{ $member->name }} {{ env('APP_URL') }}" src="{{ $member->picture->url }}">
									@else
									  <img alt="{{ $member->name }} {{ env('APP_URL') }}" src="/media/img/user.jpg">
									@endif
									</div>
								</div>
							</div>
							<div class="twelve wide column">
								<div class="ui card">
									<div class="content">
										<div class="header">{{ ucwords($member->name ) }}</div>
										<div class="meta">
											{{ ucfirst($member->roles->first()->name) }}
											@if($member->hasRole('school-admin') || $member->hasRole('university-admin'))
											<span>, <em>Admin</em></span>
											@endif
										</div>
										<div class="description">
											<?php
											    $institution_type = $member->institution_type;
											    $institution_type_plural = $member->institution_type_plural;
											?>
											@if($member->$institution_type->logo && !empty($member->$institution_type->logo->url))
									      	<img alt="{{ $member->$institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" id="logo" src="{{ $member->$institution_type->logo->url }}">
									      	@else
									        <img alt="{{ $member->$institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/{{$institution_type}}_logo.jpg">
									        @endif

											{{ ucwords($member->$institution_type->name) }}
										</div>
									</div>
									<div class="extra content">
										<div class="ui two buttons">
											<a href="{{ url('/profile') }}/{{ $member->username }}" class="ui basic grey button">Profile</a>
											@unless(Auth::guest())
											<a class="ui basic teal button write_user">Message</a>
											@endunless
										</div>
										<div class="ui show_message_form">
											<br>
										      <form class="ui fluid form new_message" method="POST" action="{{ url('/messages') }}">
										      	{{ csrf_field() }}
										        <div class="field">
										          <textarea rows=2 name="message" required></textarea>
										        </div>
										        <input type="hidden" name="recipients" value="{{ $member->id }}">
										        <button class="ui mini submit teal button">
										        	<i class="send outline icon"></i>
										        	Send Message
										        </button>
										      </form>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
						{{ $results->links() }}
					@else
						<div class="ui block header">
							<i class="info icon"></i>
							<div class="content">
							  No search results
							  <div class="sub header"></div>
							</div>
						</div>
					@endif
					<br>
				</div>
    		</div>
    		<div class="six wide column"></div>
    	</div>
    </div>	
@endsection

@section('footer')
	@parent
@endsection