@extends('layout.master')

@section('title', 'Choose University | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="four wide column"></div>
			<div class="eight wide column">
				<div class="ui secondary segment">
					<h1 class="ui centered dividing header">
						<div class="content">
							Enter Your University Details
							<div class="sub header">Fill all the form fields before submitting</div>
						</div>
					</h1>

					@include('errors.form_valid')

					<form id="login" class="ui large form" method="POST" action="{{ url('/new_unversity_request') }}">
			          {{ csrf_field() }}
			          <div class="ui stacked segment">
			            <div class="field">
			              <div class="ui left icon input">
			                <i class="university icon"></i>
			                <input type="text" name="name" placeholder="Full University Name" required>
			              </div>
			            </div>

			            <div class="field">
						  <select name="state" class="ui floating dropdown" required>
						      <option value=""> Select state </option>
						    @foreach($states as $state)
						      <option value="{{ strtolower($state) }}">{{ $state }}</option>
						    @endforeach
						  </select>
						</div>

			            <div class="field">
			              <div class="ui left icon input">
			                <i class="compass icon"></i>
			                <textarea rows="2" name="address" placeholder="   University Address" required></textarea>
			              </div>
			            </div>

			            <div class="field">
			              <div class="ui left icon input">
			                <i class="user icon"></i>
			                <input type="text" name="reporter_name" placeholder="Your Name" required>
			              </div>
			            </div>

			            <div class="field">
			              <div class="ui left icon input">
			                <i class="tablet icon"></i>
			                <input type="text" name="reporter_phone" placeholder="Your Phone No." required>
			              </div>
			            </div>

			            <input type="hidden" name="reporter_role" value="student">

			            <button type="submit" class="ui fluid large teal submit button">Submit</button>
			          </div>

			          <div class="ui error message"></div>

			        </form>

				</div>
			</div>
			<div class="four wide column"></div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection