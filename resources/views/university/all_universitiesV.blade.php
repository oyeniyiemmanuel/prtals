@extends('layout.master')

@section('title', ' Universities | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
    
    @include('partial.page_header', [
            'title' => 'Universities'
            ,'trend' => $filtered_state
        ])

    <div class="ui container fluid">
            <div class=" ui stackable grid">
                <div class="one wide column"></div>
                <div class="two wide column">
                    <br>
                    <a href="{{ url('/universities') }}">
                        <div class="ui fluid basic mini button">
                            All Universities
                        </div>
                    </a>
                    <br>
                    <form class="ui form" method="POST" action="{{ url('/universities_by_state') }}">
                        {{ csrf_field() }}
                      <div class="field">
                          <select onchange="this.form.submit()" name="state" class="ui fluid floating dropdown mini button">
                                <option value=""> By State </option>
                            @foreach($states as $state)
                              <option value="{{ strtolower($state) }}">{{ $state }}</option>
                            @endforeach
                          </select>
                      </div>
                    </form>
                </div>

                <div class="twelve wide column school_threads">
                    <br>
                    <form class="ui big form" method="GET" action="{{ url('/search') }}/universities_only">
                        {{ csrf_field() }}
                        <div class="field">
                            <div class="ui big action left icon input">
                                <i class="search icon"></i>
                                <input class="set_cursor" type="search" name="query" placeholder="Search universities.." value="<?php if(isset($query)&&($query!=null)){ echo $query; } ?>" autofocus>
                            </div>
                        </div>
                    </form>
                    <div class="ui segment">
                    @if(!empty($universities->all()))
                        <div class="ui four doubling cards">
                        @foreach($universities->sortBy('name') as $university)
                          <a href="/universities/{{ $university->slug }}" class="card">
                            <div class="image">
                            @if($university->logo && !empty($university->logo->url))
                              <img alt="{{ $university->name }} {{ env('APP_URL') }}" src="{{ $university->logo->url }}">
                            @else
                              <img alt="{{ $university->name }} {{ env('APP_URL') }}" src="/media/img/university_logo.jpg">
                            @endif
                            </div>

                            <div class="content">
                                <div class="header" href="#">{{ ucwords(strtolower($university->name)) }}</div>
                                <div class="meta">
                                  <span><em>{{ $university->address }}, {{ ucwords($university->state) }}</em></span>
                                </div>
                            </div>
                          </a>
                        @endforeach
                        </div>
                    @else
                        <div class="ui block header">
                            <i class="info icon"></i>
                            <div class="content">
                              The filter returned zero result 
                              <div class="sub header"></div>
                            </div>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="one wide column"></div>
            </div>
            
            <div class="ui dividing header"></div>
          
    </div>

    <div class="ui container">
        <br>
        <div class="ui raised segment">
            <h3 class="ui header">If you're certain your university isn't in our records, send us a request to <a href="{{ url('/new_university_request') }}"><button type="submit" class="ui large teal button">Add Your University</button></a></h3>

        </div>
    </div>	
@endsection

@section('footer')
	@parent
@endsection