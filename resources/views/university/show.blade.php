@extends('layout.master')

@section('title', ucwords($university->name).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">
		
		@include('partial.university_page_header')
		
		<div class="ui stackable grid mobile_margin_1em">
			<div class="one wide column no_padding"></div>
			<div class="ten wide column">
				
				<div class="ui fluid card raised segment">
				@if(Auth::user())
				  @if(Auth::user()->university  && Auth::user()->university->id == $university->id)
					  @if(Auth::user()->hasRole('authority') || Auth::user()->hasRole('university-admin'))
					  <a class="ui grey right corner label university_cover_modal_button">
				        <i class="pencil icon"></i>
				      </a>
				      @endif
			      @endif
			    @endif
			      @include('modal.university_cover_modal')
			      <div id="cover_wrapper">
			      	@if($university->cover && !empty($university->cover->url))
			      	<img alt="{{ $university->name }} {{ env('APP_URL') }}" id="cover" src="{{ $university->cover->url }}">
			      	@else
			        <img alt="{{ $university->name }} {{ env('APP_URL') }}" id="cover" src="/media/img/university_cover.jpg">
			        @endif
			      </div>
			      	<div class="university_logo_cover">
			    		<div class="ui card university_logo">
					      <div class="image">
					      @if(Auth::user())
					      	@if(Auth::user()->university  && Auth::user()->university->id == $university->id)
						      	@if(Auth::user()->hasRole('authority') || Auth::user()->hasRole('university-admin'))
						      	<a class="ui grey mini right corner label university_logo_modal_button">
							        <i class="pencil icon"></i>
							    </a>
							    @endif
						    @endif
						  @endif
						    @if($university->logo && !empty($university->logo->url))
					      	<img alt="{{ $university->name }} {{ env('APP_URL') }}" id="logo" src="{{ $university->logo->url }}">
					      	@else
					        <img alt="{{ $university->name }} {{ env('APP_URL') }}" src="/media/img/university_logo.jpg">
					        @endif
					      </div>
					      @include('modal.university_logo_modal')
					    </div>
				    </div>
			    </div>
			</div>

			<div class="five wide column">
				<div class="ui stackable grid">
		        	<div class="twelve wide column">
		        		<div class="">
		        			@if(Auth::user())
		        				@if(Auth::user()->university  && Auth::user()->university->id == $university->id)
				        			@if(Auth::user()->hasRole('authority') || Auth::user()->hasRole('university-admin'))
				        			<a href="/universities/details/{{ $university->slug }}/edit" class="ui right floated teal small label">
								        <i class="pencil icon"></i> edit details
								    </a>
								    @endif
							    @endif
						    @endif
		        			<div class="ui list">
							  <div class="item">
							    <i class="calendar icon"></i>
							    <div class="content">
							      founded <strong><?php echo $university->founded? 'on  '.Carbon\Carbon::parse($university->founded)->format('F d Y'): ': <em>null</em>'; ?></strong>
							    </div>
							  </div>

							  <div class="item">
							    <i class="marker icon"></i>
							    <div class="content">
							    <?php echo $university->address? $university->address.','.ucfirst($university->state): 'Location: <em>null</em>'; ?>
							    </div>
							  </div>

							  <div class="item">
							    <i class="users icon"></i>
							    <div class="content">
							      registered undergraduates on prtals <a>{{ count($university->undergraduates->where('user_id', '!=', null)) }}</a>
							    </div>
							  </div>

							  <div class="item">
							    <i class="hourglass end icon"></i>
							    <div class="content">
							      {{ substr($university->history, 0, 70) }}...<a class="university_history_modal_button">read all</a>
							    </div>
							    @include('modal.university_history_modal')
							  </div>

							  <div class="item">
							    <i class="trophy icon"></i>
							    <div class="content">
							      {{ substr($university->accomplishments, 0, 70) }}...<a class="university_accomplishments_modal_button">read all</a>
							    </div>
							    @include('modal.university_accomplishments_modal')
							  </div>

							  <div class="item">
							    <i class="money icon"></i>
							    <div class="content">
							      {{ substr($university->tuition, 0, 70) }}...<a class="university_tuition_modal_button">read all</a>
							    </div>
							    @include('modal.university_tuition_modal')
							  </div>
							</div>
		        		</div>
		        	</div>

		        	<div class="twelve wide column" style="padding-top: 0;">
		        		<div class="ui fluid card">
		        			@if(!empty($university->users->where('verified', '=', 1)->all()))
							<div class="content">
							  <div class="center aligned">Admin</div>
							  <div class="description center aligned">
							  	@foreach($university->users->all() as $user)
							  		@if($user->hasRole('university-admin'))
							  		<a href="/profile/{{ $user->username }}">
								  	<span data-tooltip="{{ $user->name }}">
								  		@if(!empty($user->picture->url))
								  		<img alt="{{ $user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $user->picture->url }}">
								  		@else
								  		<img alt="{{ $user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
								  		@endif
								  	</span>
								  	</a>
								  	@endif
								@endforeach
							  </div>
							</div>
							@else
								<div class="content">
								  <div class="description center aligned">
								    <div class="ui block header">
										<i class="info icon"></i>
										<div class="content">
											No Admin yet
										</div>
									</div>
								  </div>
								</div>
		        			@endif
		        		</div>
						<div class="ui fluid card">
						@if(!empty($university->users->where('verified', '=', 1)->all()))
							<div class="content">
							  <div class="center aligned">Members</div>
							  <div class="description center aligned">
							  	@foreach($university->users->take(6) as $user)
							  		@if(!empty($user->roles()->first()))
							  		<a href="/profile/{{ $user->username }}">
								  	<span data-tooltip="{{ $user->name }}">
								  		@if(!empty($user->picture->url))
								  		<img alt="{{ $user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $user->picture->url }}">
								  		@else
								  		<img alt="{{ $user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
								  		@endif
								  	</span>
								  	</a>
								  	@endif
								@endforeach
							  </div>
							</div>
							@if(Auth::guest() || Auth::user()->verified == 0)
							<div class="ui mini bottom attached basic button buttons" style="padding: 0;">
								<a href="/universities/{{ $university->slug }}/members" class="ui basic button">
									<i class="list icon"></i>
							  		View all Members
								</a>
								@if(Auth::guest())
								<a href="{{ url('/signup') }}" class="ui basic button">
									<i class="add user icon"></i>
							  		Become a member
								</a>
								@endif
								@if(Auth::user() && Auth::user()->verified == 0)
								<a href="{{ url('/choose_student_type') }}" class="ui basic button">
									<i class="add user icon"></i>
							  		Become a member
								</a>
								@endif
							</div>
							@endif

							@if((Auth::user() && Auth::user()->verified == 1) )
							<a href="/universities/{{ $university->slug }}/members" class="ui mini bottom attached basic button">
									<i class="list icon"></i>
							  		View all Members
							</a>
							@endif

						@else
							<div class="content">
							  <div class="description center aligned">
							    <div class="ui block header">
									<i class="info icon"></i>
									<div class="content">
									  No member yet
									  <div class="sub header"></div>
									</div>
								</div>
							  </div>
							</div>

							<a href="{{ url('/signup') }}" class="ui mini bottom attached basic button">
								<i class="add user icon"></i>
							  	Become the first member
							</a>
						@endif
						</div>
		        	</div>
		        </div>
			</div>
			<div class="one wide column"></div>
		</div>
					
	    <div class="">
	    	<div class="ui stackable grid">
	    		<div class="one wide column"></div>
	    		<div class="five wide column">
	    			<div class="ui raised teal segment" style="background: #FCFCFC">
		    			<?php
		    				$news = App\News::where('university_id', '=', $university->id)->latest()->get();
		    			?>
		    			@if(empty($news->all()))
		    				@include('bots.news', [
								'bot_status' => true
								,'bot_main_word' => ''
								,'bot_main_phrase' => 'Share News About Your School'
								,'bot_main_sub_phrase' => ''
								,'bot_catch_phrase' => 'Sign in to write news about your school'
							])
			    			<div class="ui block header">
								<i class="info icon"></i>
								<div class="content">
								  No News 
								  <div class="sub header"></div>
								</div>
							</div>
					    @else
					    	@include('bots.news', [
								'bot_status' => true
								,'bot_main_word' => ''
								,'bot_main_phrase' => 'Share News About Your School'
								,'bot_main_sub_phrase' => ''
								,'bot_catch_phrase' => 'Sign in to write news about your school'
							])

							<a href="{{ url('/universities') }}/{{ $university->slug }}/news">
							  	See all news of this school
							  	<i class="right arrow icon"></i>
							  </a>
						    @foreach($news->take(1) as $news_thread)
								<div class="ui items">
								  @include('partial.news_threadV')
								</div>
							@endforeach
						@endif

		    			<?php
		    				$events = App\Event::where('university_id', '=', $university->id)->latest()->get();
		    			?>
		    			@if(empty($events->all()))
		    				<div class="ui dividing header">
								<div class="content">
								  Events
								</div>
								<div class="sub header">0 in total</div>
						    </div>
			    			<div class="ui block header">
								<i class="info icon"></i>
								<div class="content">
								  No Events 
								  <div class="sub header"></div>
								</div>
							</div>
					    @else
			    			<div class="ui dividing header">
								<div class="content">
								  Events
								  <a href="{{ url('/universities') }}/{{ $university->slug }}/events" class="ui right floated mini label">
								  	view all
								  </a>
								</div>
						    </div>
						    @foreach($events->take(1) as $event_thread)

								  @include('partial.event_threadV')

							@endforeach
						@endif
						
						@if(Auth::user()  && Auth::user()->university && Auth::user()->university->id == $university->id)
			    			<?php
			    				$announcements = App\Announcement::where('university_id', '=', $university->id)->latest()->get();
			    			?>
			    			@if(empty($announcements->all()))
			    				<div class="ui dividing header">
									<div class="content">
									  Announcements
									</div>
									<div class="sub header">0 in total</div>
							    </div>
				    			<div class="ui block header">
									<i class="info icon"></i>
									<div class="content">
									  No Announcements 
									  <div class="sub header"></div>
									</div>
								</div>
						    @else
				    			<div class="ui dividing header">
									<div class="content">
									  Announcements
									  <a href="{{ url('/announcements') }}/university/{{ Auth::user()->university->slug }}" class="ui right floated mini label">
									  	view all
									  </a>
									</div>
							    </div>
							    @foreach($announcements->take(1) as $announcement_thread)
									<div class="ui items">
									  @include('partial.announcement_threadV')
									</div>
								@endforeach
							@endif
						@endif
				    
	    			</div>
	    		</div>

	    		<div class="five wide column post_threads">
				    @if(empty($university->posts->all()))
				    	@include('bots.post',  [
							'bot_status' => true
							,'bot_main_word' => ''
							,'bot_main_phrase' => 'Write New Post'
							,'bot_main_sub_phrase' => ''
							,'bot_catch_phrase' => 'Sign in to create new post'
						])
						<div class="ui block header">
							<i class="info icon"></i>
							<div class="content">
							  No posts to display
							  <div class="sub header"></div>
							</div>
						</div>
					@else
						@include('bots.post',  [
							'bot_status' => true
							,'bot_main_word' => ''
							,'bot_main_phrase' => 'Write New Post'
							,'bot_main_sub_phrase' => ''
							,'bot_catch_phrase' => 'Sign in to create new post'
						])
						<a href="{{ url('/universities') }}/{{ $university->slug }}/posts">
						  	See all posts of this school
						  	<i class="right arrow icon"></i>
						  </a>

					    @foreach($university->posts->take(5) as $post)
							<div class="ui fluid card post_thread">
							  @include('partial.half_post_threadV')
							</div>
						@endforeach
					@endif
	    		</div>

	    		<div class="four wide column">
	    			@include('partial.aside_schools_list')
	    		</div>

	    		<div class="one wide column"></div>
	    	</div>
	    </div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection