@extends('layout.master')

@section('title', 'Add New University | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="four wide column"></div>
			<div class="eight wide column">
				<div class="ui secondary segment">
					<h1 class="ui centered dividing header">
						<div class="content">
							Admin Add University
							<div class="sub header">Fill all the form fields before submitting</div>
						</div>
					</h1>

					@include('errors.form_valid')

					<form id="login" class="ui large form" method="POST" action="{{ url('/add_new_university') }}">
			          {{ csrf_field() }}
			          <div class="ui stacked segment">
			            <div class="field">
			              <div class="ui left icon input">
			                <i class="university icon"></i>
			                <input type="text" name="name" placeholder="Full University Name" required>
			              </div>
			            </div>

			            <div class="field">
			              <div class="ui left icon input">
			                <textarea rows="2" name="address" placeholder="   University Address" required></textarea>
			              </div>
			            </div>

			            <div class="field">
						  <select name="state" class="ui floating dropdown" required>
						      <option value=""> Select state </option>
						    @foreach($states as $state)
						      <option value="{{ strtolower($state) }}">{{ $state }}</option>
						    @endforeach
						  </select>
						</div>

						<!--<div class="ui secondary segment">
							<div class="field">
								<label> The university first user's verification no. and role<br> <em>It is recommended to input the user's university registration number. If it is unknown, input <strong>"university-admin"</strong></em></label>
				              <div class="ui left icon input">
				                <i class="user icon"></i>
				                <input type="text" name="reg_no" placeholder="university-admin" required>
				              </div>
				            </div>

				            <div class="field">
							  <select name="role" class="ui floating dropdown" required>
							      <option value=""> Select user's role </option>
							      <option value="student">Student</option>
							      <option value="teacher">Teacher</option>
							      <option value="authority">Authority</option>
							  </select>
							</div>
						</div>-->

			            <button type="submit" class="ui fluid large teal submit button">Submit</button>
			          </div>

			          <div class="ui error message"></div>

			        </form>

				</div>
			</div>
			<div class="four wide column"></div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection