@extends('layout.master')

@section('title', 'Edit University Details | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">

		@include('partial.university_page_header')

		<div class="ui stackable grid">
			<div class="three wide column"></div>
			<div class="ten wide column">
				<div class="ui raised inverted segment" style="background: #023B42;">
					<h1 class="ui centered header">
						@if($university->logo && !empty($university->logo->url))
				      	<img alt="{{ $university->name }} {{ env('APP_URL') }}" class="ui mini image" id="logo" src="{{ $university->logo->url }}">
				      	@else
				        <img alt="{{ $university->name }} {{ env('APP_URL') }}" class="ui mini image" src="/media/img/university_logo.jpg">
				        @endif
						<div class="content">
							Edit University Details
						</div>
					</h1>
				</div>
					@include('errors.form_valid')
					
					<form class="ui small form" method="POST" action="">
			          {{ csrf_field() }}

			          <div class="ui secondary raised segment">
				          <h4 class="ui header">Basic details</h4>
				          <div class="two fields">
				            <div class="eight wide field">
				            	<label>University name</label>
				              <div class="ui left icon input">
				                <i class="user icon"></i>
				                <input type="text" name="name" placeholder="university name" value="{{ $university->name }}" required>
				              </div>
				            </div>

				            <div class="eight wide field">
				            	<label>founded</label>
				            	<div class="ui left icon input">
									<i class="calendar icon"></i>
									<input type="date" name="founded" placeholder="yyyy-mm-dd"  value="<?php echo $university->founded? $university->founded: ''; ?>" max="{{ date('Y-m-d') }}" required>
								</div>
				            </div>
				          </div>
							
						  <div class="two fields">
						  	<div class="eight wide field">
				            	<label>state</label>
								<select name="state" class="ui fluid floating dropdown" required>
								    <option value=""> state </option>
								@foreach($states as $state)
								 	<option <?php echo ($university) && ($university->state == strtolower($state))? ' selected': ''; ?> value="{{ strtolower($state) }}">{{ $state }}</option>
								@endforeach
								</select>
				            </div>

				           	<div class="eight wide field">
				           		<label>Estimated number of students</label>
								<div class="ui left icon input">
									<i class="bar chart icon"></i>
									<input type="text" name="no_of_pupils" placeholder="e.g. 22204" value="<?php echo $university->no_of_pupils? $university->no_of_pupils: ''; ?>" required>
								</div>
				            </div>
						  </div>
					  </div>

					  <div class="ui secondary raised segment">
						  <h4 class="ui header">Extra details</h4>
						  <div  class="two fields">
						  	<div class="eight wide field">
						  		<label>Address</label>
						  		<textarea rows="6" name="address" required><?php echo $university->address? $university->address: ''; ?></textarea>
						  	</div>
						  	<div class="eight wide field">
						  		<label>History</label>
						  		<textarea rows="6" name="history" required><?php echo $university->history? $university->history: ''; ?></textarea>
						  	</div>
						  </div>

						  <div  class="two fields">
						  	<div class="eight wide field">
						  		<label>Tuition</label>
						  		<textarea rows="6" name="tuition" required><?php echo $university->tuition? $university->tuition: ''; ?></textarea>
						  	</div>
						  	<div class="eight wide field">
						  		<label>Accomplishments</label>
						  		<textarea rows="6" name="accomplishments" required><?php echo $university->accomplishments? $university->accomplishments: ''; ?></textarea>
						  	</div>
						  </div>
						</div>
			          
			          <button type="submit" class="ui fluid teal submit button">Update</button>
			          
			        </form>
					
					<br>
					<a class="ui mini label" href="{{ url('/universities') }}/{{ $university->slug }}"><i class="arrow left icon"></i><strong>Back To University Page </strong></a>

			</div>
			<div class="three wide column"></div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection