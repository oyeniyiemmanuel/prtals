@extends('layout.master')

@section('title', 'News: '.ucwords($university->name).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">

		@include('partial.university_page_header')

	</div>
	<br>

	@include('partial.university_news')
	
@endsection

@section('footer')
	@parent
@endsection