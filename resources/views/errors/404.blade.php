@extends('layout.master')

@section('title', '404 error | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid homepage-crumbs">
		<div class="ui stackable grid">
			<div class="two wide column"></div>
			<div class="twelve wide column">
				<div class="ui block centered header">
					<i class="info icon"></i>
					<div class="content">
					  <h1 style="font-family: 'lato-thin';">Ooops!! We couldn't find this page, please check the url and try again</h1>
					  @unless (env('APP_DEBUG') == false)
					  <div class="sub header">
					  	{!! nl2br(e($exception)) !!}
					  </div>
					  @endunless
					</div>
				</div>
			</div>
			<div class="two wide column"></div>
		</div>
	</div>

@endsection

@section('footer')
	@parent
@endsection