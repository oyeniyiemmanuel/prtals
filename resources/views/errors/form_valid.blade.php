
@if ($errors->any())
<div class="ui mini black message">
  <i class="close icon"></i>
  <div class="header">
    Could not process your request!
  </div>
  <ul class="list">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif