
@extends('layout.master')

@section('title', ' Forgot Password | Prtals')

@section('navigation_bar')
    @parent
@endsection

@section('content')
    
    @include('partial.page_header', [
            'title' => 'Forgot Password'
            ,'trend' => ''
        ])

    <div class="ui container fluid">
            <div class=" ui stackable grid">
                <div class="five wide column"></div>
                <div class="six wide column">
                    <br>
                    <div class="ui secondary segment">
                        <div id="signin_error_messages">
                            @include('errors.form_valid')
                        </div>
                        @if (session('status'))
                            <div class="ui mini black message">
                              <i class="close icon"></i>
                              <div class="header">
                                {{ session('status') }}
                              </div>
                            </div>
                        @endif
                        <form id="" class="ui large form" method="POST" action="{{ url('/password/email') }}">
                          {{ csrf_field() }}
                          <div class="ui stacked segment">
                            <div class="field">
                              <div class="ui left icon input">
                                <i class="at icon"></i>
                                <input type="text" name="email" placeholder="Enter Your Email Address" value="{{ old('email') }}" required autofocus>
                              </div>
                            </div>
                            <button type="submit" class="ui fluid large teal submit button">Send Me a Password Reset Link</button>
                          </div>
                        </form>

                        <div class="ui message">
                           <a href="{{ url('/signin') }}" class="ui teal mini label">Sign In</a>&nbsp;&nbsp; 
                           <a href="{{ url('/signup') }}" class="ui teal mini label">Sign Up</a>
                        </div>
                    </div>
                </div>
                <div class="five wide column"></div>
            </div>          
    </div>  
@endsection

@section('footer')
    @parent
@endsection
