@extends('layout.master')

@section('title', ' Reset Password | Prtals')

@section('navigation_bar')
    @parent
@endsection

@section('content')
    
    @include('partial.page_header', [
            'title' => 'Enter Your New Password'
            ,'trend' => ''
        ])

    <div class="ui container fluid">
            <div class=" ui stackable grid">
                <div class="five wide column"></div>
                <div class="six wide column">
                    <br>
                    <div class="ui secondary segment">
                        <div id="signin_error_messages">
                            @include('errors.form_valid')
                        </div>
                        <form id="" class="ui large form" method="POST" action="{{ url('/password/reset') }}">
                          {{ csrf_field() }}
                          
                          <input type="hidden" name="token" value="{{ $token }}">

                          <div class="ui stacked segment">
                            <div class="field">
                              <div class="ui left icon input">
                                <i class="at icon"></i>
                                <input type="text" name="email" placeholder="Enter Your Email Address" value="{{ old('email') }}">
                              </div>
                            </div>
                            <div class="field">
                              <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password" placeholder="Enter password">
                              </div>
                            </div>
                            <div class="field">
                              <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password_confirmation" placeholder="Re-enter Password">
                              </div>
                            </div>
                            <button type="submit" class="ui fluid large teal submit button">Reset Password</button>
                          </div>
                        </form>
                    </div>
                </div>
                <div class="five wide column"></div>
            </div>          
    </div>  
@endsection

@section('footer')
    @parent
@endsection
