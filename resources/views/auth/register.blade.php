@extends('layout.master')

@section('title', ' Sign Up | Prtals')

@section('navigation_bar')
    @parent
@endsection

@section('content')
    
    @include('partial.page_header', [
            'title' => 'Prtals Sign Up'
            ,'trend' => ''
        ])

    <div class="ui container fluid">
            <div class=" ui stackable grid">
                <div class="five wide column" style="padding: 0 !important"></div>
                <div class="six wide column">
                    <br>
                    <div class="ui secondary segment">
                        <div id="signup_error_messages">
                            @include('errors.form_valid')
                        </div>
                        <form id="signup" class="ui large form"  method="POST" action="{{ url('/signup') }}">
                            {{ csrf_field() }}
                            <div class="ui stacked segment">
                                <div class="field">
                                    <div class="ui left icon input">
                                        <i class="user icon"></i>
                                        <input type="text" name="name" placeholder="Full name.." value="{{ old('name') }}" required>
                                    </div>
                                </div>
                                <div class="field {{ $errors->has('email') ? 'error' : '' }}">
                                    <div class="ui left icon input">
                                        <i class="at icon"></i>
                                        <input type="text" name="email" placeholder="E-mail address" value="{{ old('email') }}" required>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="ui left icon input">
                                        <i class="lock icon"></i>
                                        <input type="password" name="password" placeholder="Password" value="{{ old('password') }}" required>
                                    </div>
                                </div>
                                <div class="field {{ $errors->has('password') ? 'error' : '' }}">
                                    <div class="ui left icon input">
                                        <i class="lock icon"></i>
                                        <input type="password" name="password_confirmation" placeholder="confirm Password" value="{{ old('password_confirmation') }}" required>
                                    </div>
                                </div>
                                <button type="submit" class="ui fluid large teal submit button">Sign up</button>
                            </div>

                            <div class="ui error message"></div>

                        </form>
                        
                        <div class="ui message">
                          Got an account? &nbsp;&nbsp; <a href="{{ url('/signin') }}" class="ui teal mini label">Sign In</a>
                        </div>
                    </div>
                </div>
                <div class="five wide column"></div>
            </div>
            
            <div class="ui dividing header"></div>
          
    </div>

    <div class="ui container">
        <br>
        <div class="ui raised segment">
            <h3 class="ui header">If you're certain your school isn't in our records, send us a request to <a href="{{ url('/new_school_request') }}"><button type="submit" class="ui large teal button">Add Your School</button></a></h3>

        </div>
    </div>  
@endsection

@section('footer')
    @parent
@endsection