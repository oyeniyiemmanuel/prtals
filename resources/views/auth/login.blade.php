@extends('layout.master')

@section('title', ' Sign In | Prtals')

@section('navigation_bar')
    @parent
@endsection

@section('content')
    
    @include('partial.page_header', [
            'title' => 'Sign In'
            ,'trend' => ''
        ])

    <div class="ui container fluid">
            <div class=" ui stackable grid">
                <div class="five wide column" style="padding: 0 !important"></div>
                <div class="six wide column">
                    <br>
                    <div class="ui secondary segment">
                        <div id="signin_error_messages">
                            @include('errors.form_valid')
                        </div>
                        <form id="signin" class="ui large form" method="POST" action="{{ url('/signin') }}">
                          {{ csrf_field() }}
                          <div class="ui stacked segment">
                            <div class="field {{ $errors->has('email') ? 'error' : '' }}">
                              <div class="ui left icon input">
                                <i class="user icon"></i>
                                <input type="text" name="email" placeholder="E-mail address" value="{{ old('email') }}">
                              </div>
                            </div>
                            <div class="field {{ $errors->has('password') ? 'error' : '' }}">
                              <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password" placeholder="Password">
                              </div>
                            </div>
                            <div class="inline field">
                              <div class="ui toggle checkbox">
                                <input type="checkbox" tabindex="0" class="hidden" name="remember">
                                <label>Remember Me</label>
                              </div>
                            </div>
                            <button type="submit" class="ui fluid large teal submit button">Proceed</button>
                          </div>
                        </form>

                        <div class="ui message">
                            <a href="{{ url('/password/reset') }}">Forgot Password?</a><br>
                          don't have an account? &nbsp;&nbsp; <a href="{{ url('/signup') }}" class="ui teal mini label">Sign Up</a>
                        </div>
                    </div>
                </div>
                <div class="five wide column"></div>
            </div>
            
            <div class="ui dividing header"></div>
          
    </div>

    <div class="ui container">
        <br>
        <div class="ui raised segment">
            <h3 class="ui header">If you're certain your school isn't in our records, send us a request to <a href="{{ url('/new_school_request') }}"><button type="submit" class="ui large teal button">Add Your School</button></a></h3>

        </div>
    </div>  
@endsection

@section('footer')
    @parent
@endsection