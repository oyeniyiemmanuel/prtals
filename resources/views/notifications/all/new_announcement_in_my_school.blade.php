<div class="event">
	<?php
	    $institution_type = Auth::user()->institution_type;
	    $institution_type_plural = Auth::user()->institution_type_plural;
	?>
	<div class="label">
	  <img alt="<?php $school = Auth::user()->$institution_type; echo $school->name;?> {{ env('APP_URL') }}" src="<?php $school = Auth::user()->$institution_type; echo $school->logo? $school->logo->url: '/media/img/school_logo.jpg'; ?>">
	</div>
	<div class="content">
	  <div class="summary">
		<a href="/announcements/{{ $notification->data['announcement']['slug'] }}">
			<strong>Announcement: </strong>
			{{ substr($notification->data['announcement']['title'], 0, 30) }}.. 
			by 
			{{ $notification->data['owner']['name'] }}
			<div class="date">
			{{ $notification->created_at->diffForHumans() }}
			</div>
		</a>
	  </div>
	</div>
</div>