
<div class="event">
	<div class="label">
	  <img alt="<?php $user = App\User::where('id', $notification->data['liker']['id'])->get()->first(); echo $user->name;?> {{ env('APP_URL') }}" src="<?php $user = App\User::where('id', $notification->data['liker']['id'])->get()->first(); echo $user->picture? $user->picture->url: '/media/img/user.jpg'; ?>">
	</div>
	<div class="content">
	  <div class="summary">
		<a href="/posts/{{ $notification->data['post']['slug'] }}">
			{{ $notification->data['liker']['name'] }}
			<strong>liked your post</strong>
			{{ substr($notification->data['post']['title'], 0, 20) }}..
			<div class="date">
			{{ $notification->created_at->diffForHumans() }}
			</div>
		</a>
	  </div>
	</div>
</div>