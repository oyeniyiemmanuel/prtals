@extends('layout.master')

@section('title', 'Notifications | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => 'Notifications'
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="two wide column"></div>
			<div class="ten wide column all_notifications">
				<div class="ui feed">
				@if( count( Auth::user()->notifications) == 0 )
	              <div class="ui block header">
						<i class="alarm outline mute icon"></i>
						<div class="content">
						  No notifications
						  <div class="sub header"></div>
						</div>
					</div>
	            @else
	            	<form method="POST" action="{{ url('/clear_notifications') }}">
	            		{{ csrf_field() }}
		            	<button class="ui mini teal button right floated">
		            		<i class="trash icon"></i> Clear All
		            	</button>
	            	</form>
	              @foreach(Auth::user()->notifications as $notification)
	              
	                @include('notifications.all.'.snake_case(class_basename($notification->type)))
	              
	              @endforeach
	            @endif
				</div>
			</div>
			<div class="two wide column">
				
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection