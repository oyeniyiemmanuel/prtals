
<a href="/posts/{{ $notification->data['post']['slug'] }}" class="event item">
	<div class="content">
	  <div class="summary">
	  <i class="talk outline olive icon"></i>
			{{ $notification->data['commenter']['name'] }}
			<strong> commented on</strong>
			{{ substr($notification->data['post']['title'], 0, 10) }}..
			<div class="date">
			{{ $notification->created_at->diffForHumans() }}
			</div>
	  </div>
	</div>
</a>