
<a  href="/news/{{ $notification->data['news']['slug'] }}" class="event item">
	<div class="content">
	  <div class="summary">
	  <i class="newspaper icon"></i>
			<strong> News Alert: </strong>
			{{ substr($notification->data['news']['message'], 0, 30) }}..
			<div class="date">
			{{ $notification->created_at->diffForHumans() }}
			</div>
	  </div>
	</div>
</a>