
<a href="/announcements/{{ $notification->data['announcement']['slug'] }}" class="event item">
	<div class="content">
	  <div class="summary">
	  <i class="bullhorn outline up green icon"></i>
			<strong> Announcement: </strong>
			{{ substr($notification->data['announcement']['message'], 0, 26) }}..
			<div class="date">
			{{ $notification->created_at->diffForHumans() }}
			</div>
	  </div>
	</div>
</a>