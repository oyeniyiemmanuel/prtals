
<a  href="/posts/{{ $notification->data['post']['slug'] }}" class="event item">
	<div class="content">
	  <div class="summary">
	  <i class="thumbs outline up blue icon"></i>
			{{ $notification->data['liker']['name'] }}
			<strong> liked your post</strong>
			{{ substr($notification->data['post']['title'], 0, 10) }}..
			<div class="date">
			{{ $notification->created_at->diffForHumans() }}
			</div>
	  </div>
	</div>
</a>