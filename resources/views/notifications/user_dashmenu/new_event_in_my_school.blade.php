
<a  href="/events/{{ $notification->data['event']['slug'] }}" class="event item">
	<div class="content">
	  <div class="summary">
	  <i class="ticket up icon"></i>
			<strong> Event Alert: </strong>
			{{ substr($notification->data['event']['message'], 0, 30) }}..
			<div class="date">
			{{ $notification->created_at->diffForHumans() }}
			</div>
	  </div>
	</div>
</a>