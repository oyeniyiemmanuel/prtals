@extends('layout.master')

@section('title', 'Events | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'Events'
			,'trend' => 'All'
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="/events" class="active item">
				    All Events
				  </a>
				  @unless(Auth::guest() || Auth::user()->verified == 0)
				  <?php
					    $institution_type = Auth::user()->institution_type;
					    $institution_type_plural = Auth::user()->institution_type_plural;
					?>
				  <a href="/events/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My {{$institution_type}} Events
				  </a>
				  <a href="/events/user/{{ Auth::user()->username }}" class="item">
				    Events Posted By Me
				  </a>
				  @endunless
				</div>
				
			</div>
			<div class="eight wide column event_threads">
				@if(empty($events->all()))
					<div class="ui centered block header">
						<div class="content">
						  All Events
						</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No event to display
						  <div class="sub header"></div>
						</div>
					</div>
				@endif
					<div class="ui items">
					@foreach($events as $event_thread)
					  @include('partial.event_threadV')
					@endforeach
					{{ $events->links() }}
					</div>
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection