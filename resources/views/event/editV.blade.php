@extends('layout.master')

@section('title', 'Edit Event | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => 'Edit: '.$event->title
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container welcome">
		<div class="ui stackable grid">
			<div class="three wide column">

				<?php
				    $institution_type = Auth::user()->institution_type;
				    $institution_type_plural = Auth::user()->institution_type_plural;
				?>
				<div class="ui mini vertical pointing menu">
				  <a href="/events" class="item">
				    All Events
				  </a>
				  <a href="/events/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My {{ucfirst($institution_type)}} Events
				  </a>
				  <a href="/events/user/{{ Auth::user()->username }}" class="item">
				    Posted By Me
				  </a>
				</div>
				
			</div>
			<div class="eight wide column">
				<div class="ui two top attached steps">
				  <a class="active step">
				    <i class=" grey pencil icon"></i>
				    <div class="content">
				      <div class="title" style="color: brown">Step 1</div>
				      <div class="description">Enter event details</div>
				    </div>
				  </a>
				  <a href="/events/{{ $event->slug }}/add_photos" class=" step">
				    <i class="grey picture icon"></i>
				    <div class="content">
				      <div class="title" style="color: brown">Step 2</div>
				      <div class="description">Add Images (<em>optional</em>)</div>
				    </div>
				  </a>
				</div>
				<div class="ui attached segment">
					@include('errors.form_valid')
					<form id="login" class="ui large form" method="POST" action="/events/{{ $event->slug }}">
			          {{ csrf_field() }}
					<div class="field">
						<label>Title</label>
						<input type="text" name="title" placeholder="Event title.." value="{{ $event->title }}" required>
					</div>

					<div class="field">
						<label>Message</label>
						<textarea rows="6" name="message" placeholder="Event body.." required>{{ $event->message }}</textarea>
					</div>
					<input type="hidden" name="event_id" value="{{ $event->id }}">

		            <button type="submit" class="ui mini teal submit button">Update Event</button>

			          <div class="ui error message"></div>

			        </form>
				</div>
				
			</div>
			<div class="five wide column"></div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection