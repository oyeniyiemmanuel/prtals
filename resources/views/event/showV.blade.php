@extends('layout.master')

@section('title', ucwords($event_thread->title).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => ucwords($event_thread->title)
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="/events" class="item">
				    All Events
				  </a>
				  @unless(Auth::guest() || Auth::user()->verified == 0)
				  <?php
					    $institution_type = Auth::user()->institution_type;
					    $institution_type_plural = Auth::user()->institution_type_plural;
					?>
				  <a href="/events/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My {{$institution_type}} Events
				  </a>
				  <a href="/events/user/{{ Auth::user()->username }}" class="item">
				    Events Posted By Me
				  </a>
				  @endunless
				</div>
				
			</div>
			<div class="eight wide column event_threads">
				@if(empty($event_thread->all()))
					<div class="ui centered dividing header">
						<div class="content">
						  Events
						</div>
						<div class="sub header">0 in total</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No events to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
					<?php
				    $event_institution_type = $event_thread->institution_type;
				    $event_institution_type_plural = $event_thread->institution_type_plural;
				?>
					<div class="ui fluid card">
						<div id="cover_wrapper" class="image">
							@if($event_thread->$event_institution_type->cover && !empty($event_thread->$event_institution_type->cover->url))
							<img alt="{{ $event_thread->$event_institution_type->name }} {{ env('APP_URL') }}" id="cover" src="{{ $event_thread->$event_institution_type->cover->url }}">
							@else
							<img alt="{{ $event_thread->$event_institution_type->name }} {{ env('APP_URL') }}" id="cover" src="/media/img/{{$event_institution_type}}_cover.jpg">
							@endif
						</div>
					</div>

					
					@include('partial.event_threadV')

				@endif
					
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection