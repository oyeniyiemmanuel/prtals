@extends('layout.master')

@section('title', 'Events: '.ucwords( $events[0]->school->name ).' Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => ucwords($focus_school->name).' Events'
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				
				<div class="ui dividing header">
					Events
				</div>
				<div class="ui mini vertical pointing menu">
				  <a href="/events" class="item">
				    All
				  </a>
				  <a href="/events/school/{{ Auth::user()->school->slug }}" class="active item">
				    School Events
				  </a>
				  <a href="/events/user/{{ Auth::user()->username }}" class="item">
				    Posted By Me
				  </a>
				</div>
				
			</div>
			<div class="eight wide column event_threads">
				@if(empty($events->all()))
					<div class="ui centered dividing header">
						<div class="content">
						  Events
						</div>
						<div class="sub header">0 in total</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No events to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
				
					<div class="ui fluid card">
						<div id="cover_wrapper" class="image">
							@if($events->first()->school->cover && !empty($events->first()->school->cover->url))
							<img alt="{{ $events->first()->school->name }} {{ env('APP_URL') }}" id="cover" src="{{ $events->first()->school->cover->url }}">
							@else
							<img alt="{{ $events->first()->school->name }} {{ env('APP_URL') }}" id="cover" src="/media/img/school_cover.jpg">
							@endif
						</div>
					</div>

					<div class="ui centered block header">
						@if($events->first()->school->logo && !empty($events->first()->school->logo->url))
						<img alt="{{ $events->first()->school->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $events->first()->school->logo->url }}">
						@else
						<img alt="{{ $events->first()->school->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/school_logo.jpg">
						@endif
						<div class="content">
						  {{ ucwords($events->first()->school->name) }} Events
						</div>
					</div>

					@foreach($events as $event_thread)
					
						@include('partial.event_threadV')

					@endforeach

					{{ $events->links() }}
				@endif
					
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection