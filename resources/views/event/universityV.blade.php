@extends('layout.master')

@section('title', 'Events: '.ucwords( $focus_university->name ).' Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => ucwords($focus_university->name).' Events'
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				
				<div class="ui mini vertical pointing menu">
				  <a href="/events" class="item">
				    All Events
				  </a>
				  @unless(Auth::guest())
				  <a href="/events/university/{{ Auth::user()->university->slug }}" class="active item">
				    My University Events
				  </a>
				  <a href="/events/user/{{ Auth::user()->username }}" class="item">
				    Events Posted By Me
				  </a>
				  @endunless
				</div>
				
			</div>
			<div class="eight wide column event_threads">
				@if(empty($events->all()))
					<div class="ui centered dividing header">
						<div class="content">
						  Events
						</div>
						<div class="sub header">0 in total</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No events to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
				
					<div class="ui fluid card">
						<div id="cover_wrapper" class="image">
							@if($events->first()->university->cover && !empty($events->first()->university->cover->url))
							<img alt="{{ $events->first()->university->name }} {{ env('APP_URL') }}" id="cover" src="{{ $events->first()->university->cover->url }}">
							@else
							<img alt="{{ $events->first()->university->name }} {{ env('APP_URL') }}" id="cover" src="/media/img/school_cover.jpg">
							@endif
						</div>
					</div>

					<div class="ui centered block header">
						@if($events->first()->university->logo && !empty($events->first()->university->logo->url))
						<img alt="{{ $events->first()->university->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $events->first()->university->logo->url }}">
						@else
						<img alt="{{ $events->first()->university->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/school_logo.jpg">
						@endif
						<div class="content">
						  {{ ucwords($events->first()->university->name) }} Events
						</div>
					</div>

					@foreach($events as $event_thread)
					
						@include('partial.event_threadV')

					@endforeach

					{{ $events->links() }}
				@endif
					
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection