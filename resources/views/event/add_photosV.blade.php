@extends('layout.master')

@section('title', 'Add Photos | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'Add Photos: '.ucwords($event->title)
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container welcome">
		<div class="ui stackable grid">
			<div class="three wide column">

				<?php
				    $institution_type = Auth::user()->institution_type;
				    $institution_type_plural = Auth::user()->institution_type_plural;
				?>
				<div class="ui mini vertical pointing menu">
				  <a href="/events" class="item">
				    All Events
				  </a>
				  <a href="/events/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My {{$institution_type}} Events
				  </a>
				  <a href="/events/user/{{ Auth::user()->username }}" class="item">
				    Events Posted By Me
				  </a>
				</div>

			</div>
			<div class="eight wide column">
				<div class="ui two top attached steps">
				  <a href="/events/{{ $event->slug }}/edit" class="completed step">
				    <i class="brown pencil icon"></i>
				    <div class="content">
				      <div class="title" style="color: brown">Step 1</div>
				      <div class="description">Event details completed</div>
				    </div>
				  </a>
				  <a class="active step">
				    <i class="grey picture icon"></i>
				    <div class="content">
				      <div class="title" style="color: brown">Step 2</div>
				      <div class="description">Add Images (<em>optional</em>)</div>
				    </div>
				  </a>
				</div>
				<div class="ui attached segment">
					<div class="ui block header">
						<i class="tasks icon"></i>
						<div class="content">
						  Add Photos To The Event: {{ ucwords($event->title) }}
						  <div class="sub header"></div>
						</div>
					</div>
					<div class="extra images event_pictures">
					@if($event->pictures && !empty($event->pictures->all()))
						@foreach($event->pictures as $picture)
						<a><img alt="{{ $event->title }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $picture->url }}"></a>
						@endforeach	
					@endif
					</div>
					@include('errors.form_valid')
					<form id="addEventPhotos" class="dropzone" method="POST" action="/events/{{ $event->slug }}/add_photos">
			          {{ csrf_field() }}
                    <div class="dz-message">
                        Drag pictures here or click to upload <br>
                        <span>( <em>Maximum files = 4</em> )</span>
                    </div>
                    <input type="hidden" name="event_id" value="{{ $event->id }}">
			        </form>
					<br>
			        <a href="/events/{{ $event->slug }}"><button class="ui mini teal submit button">Skip This Step</button></a>
				</div>
				
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')
				
				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	<script type="text/javascript">
		// initialize dropzone
		Dropzone.options.addEventPhotos = {
		    paramName: 'file',
		    maxFilesize: 2,
		    maxFiles: 4,
		    acceptedFiles: '.jpg, ,.jpeg, .png, .gif, .bmp',
		    init: function() {
		        this.on("queuecomplete", function(file) {
		            window.location = "/events/<?php echo $event->slug ?>";
		        });
		    }
		};
	</script>
	
@endsection

@section('footer')
	@parent
@endsection