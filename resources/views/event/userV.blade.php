@extends('layout.master')

@section('title', ucwords($owner->name).'\'s Events | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'Events By '.ucwords($owner->name)
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				
				<?php
				    $institution_type = $owner->institution_type;
				    $institution_type_plural = $owner->institution_type_plural;
				?>
				<div class="ui mini vertical pointing menu">
				  <a href="/events" class="item">
				    All Events
				  </a>
				  <a href="/events/user/{{ $owner->username }}" class="active item">
				    Events Posted By {{ucwords($owner->name)}}
				  </a>
				</div>
				
			</div>
			<div class="eight wide column event_threads">
				@if(empty($events->all()))
					<div class="ui centered dividing header">
						<div class="content">
						  Events
						</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  {{ucwords($owner->name)}} is yet to post a school event
						  <div class="sub header"></div>
						</div>
					</div>
				@else

					<div class="ui centered block header">
						@if(Auth::user()->picture && !empty(Auth::user()->picture->url))
						<img alt="{{ Auth::user()->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ Auth::user()->picture->url }}">
						@else
						<img alt="{{ Auth::user()->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
						@endif
						<div class="content">
						  Events Posted By {{ ucwords($owner->name) }}
						</div>
					</div>

					@foreach($events as $event_thread)
					
						@include('partial.event_threadV')
						
					@endforeach
					{{ $events->links() }}
				@endif
					
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection