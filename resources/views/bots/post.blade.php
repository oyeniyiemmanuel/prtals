@unless(Auth::guest() || Auth::user()->verified == 0)
	@if($bot_status == true)
	<div class="ui centered block grey header fluid button raised segment new_post_modal_button">
		<div class="content">
			<i class="pencil grey icon"></i>
		  {{$bot_main_phrase}}
		</div>
		@if($bot_main_sub_phrase != '')
		<div class="sub header">
			{{$bot_main_sub_phrase}}
		</div>
		@endif
	</div>
	@endif
@endunless

@if(Auth::user() && Auth::user()->verified == 0)
<div class="ui centered block header">
	<div class="content">
	  Sorry, you can't start posting right away until you're attached to a school
	</div>
	<div class="sub header">
		If your school or institution isn't in our records and you have 
		<a class=" activating element"><em>sent us a request</em></a>
		<div class="ui flowing popup top left transition hidden">
		  <a href="{{ url('/new_school_request') }}" class="ui mini basic fluid button">High School</a>
		  <br>
		  <a href="{{ url('/new_university_request') }}" class="ui mini black fluid button">Higher Institution</a>
		</div>
		 to add it, pls be patient as your school will be added within 24 hours
	</div>
</div>
@endif

@if(Auth::guest())
<a href="{{ url('/posts/create') }}" class="ui centered block header fluid button raised segment">
	<div class="content">
	  {{ $bot_catch_phrase }}
	</div>
</a>
@endif

@include('modal.new_post')