@extends('layout.master')

@section('title', ucwords($user->name).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
    
    @include('partial.page_header', [
            'title' => $user->username
            ,'trend' => ''
        ])

    @include('partial.user_dashmenu')
    
    <div class="ui container fluid">
    @include('partial.user_profile', [
        'user_role' => $user->roles()->first()->name
    ])
    
    </div>

@endsection

@section('footer')
	@parent
@endsection