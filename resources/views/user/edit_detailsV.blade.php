@extends('layout.master')

@section('title', 'Edit Your Profile | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

    @include('partial.page_header', [
            'title' => 'Edit Profile'
            ,'trend' => ''
        ])

    @include('partial.user_dashmenu')
    
    <div class="ui container fluid">
    @include('partial.user_edit_details', [
        'user_role' => $user->roles()->first()->name
    ])
    </div>

@endsection

@section('footer')
	@parent
@endsection