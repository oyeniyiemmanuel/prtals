<div class="ui secondary segment">
	<div class="ui header">{{ ucwords($user->school->name) }} Teachers and their duties</div>
	<table class="ui teal table">
		<thead>
		<tr>
		  <th class="center aligned">department</th>
		  <th class="center aligned">details</th>
		</tr>
		</thead>
		<tbody>
		@if($user->school && $user->school->departments)
		  @foreach($user->school->departments as $department)
		    <tr>
		      <td>
		        <h3 class="ui center aligned header"> {{ ucfirst($department->name) }} </h3>
		      </td>
		      <td>
		      	<table class="ui grey compact table duties_table">
			      	<tbody>
			      	@foreach($department->subjects as $subject)
			      		<tr>
		      				<td class="collapsing">
		      					{{ ucfirst($subject->name) }}
		      				</td>
		      				<td class="collapsing">
		      					<?php
		      						$x = App\Duty::where([
				      								['subject_id', $subject->id]
				      								,['school_id', $user->school_id]
				      								,['department_id', $department->id]
				      							])->get();
		      						foreach($x as $i){?>
										<table class="ui compact table duties_table">
			      							<tbody>
												<tr>
		      										<td class="collapsing">
													<a href="/profile/{{ $i->teacher->user->username }}" class="ui small label basic button">
						      							<i class="user icon"></i>
						      							 {{ucwords($i->teacher->user->name)}}
					      							</a>
					      							</td>
					      							<td class="collapsing">
								      					<?php $x = App\Grade::findOrFail($i->grade_id); echo strtoupper($x->name) ?> 
								      					<?php $x = App\Wing::findOrFail($i->wing_id); echo ucfirst($x->name) ?>
								      				</td>
								      				<td class="collapsing">
								      					<?php 
								      						$j = App\Student::where([
								      								['grade_id', $i->grade_id]
								      								,['school_id', $i->school_id]
								      								,['department_id', $i->department_id]
								      								,['wing_id', $i->wing_id]
								      							])->get();

								      						echo count($j); 
								      					?> students
								      				</td>
					      						</tr>
					      					</tbody>
					      				</table>
		      							<?php
		      						}
		      					?>
		      				</td>
		      			</tr>
			      	@endforeach
			      	</tbody>
			    </table>
		      </td>
		    </tr>
		   @endforeach

		@else
			<tr>
		      <td>
		        <em>no subject has been registered in your school either by student or teacher</em>
		      </td>
		      <td><em>null</em></td>
		    </tr>
		 @endif
		</tbody>
	</table>
</div>