@extends('layout.master')

@section('title', 'Add New School Member | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

    <div class="ui container fluid school_page">

        @include('partial.school_page_header')
        
        <div class="ui stackable grid">
            <div class="two wide column"></div>
            <div class="four wide column">
                <div class="ui raised segment">
                    <h2 class="ui centered header">Student</h2>
                    <form class="ui large form" method="POST" action="{{ url('/add_new_member') }}">
                        {{ csrf_field() }}
                        <div class="ui stacked segment">
                            <div class="field">
                              <div class="ui left icon input">
                                <i class="key icon"></i>
                                <input type="text" name="reg_no" placeholder="enter student reg. no." required>
                              </div>
                            </div>
                            <input type="hidden" name="school_id" value="{{ Auth::user()->school->id }}">
                            <input type="hidden" name="role" value="student">
                            <button type="submit" class="ui fluid teal submit button">Add</button>
                        </div>
                    </form>
                </div>
            </div>
            
            @if(Auth::user()->hasRole('authority'))
            <div class="four wide column">
                <div class="ui raised segment">
                    <h2 class="ui centered header">Teacher</h2>
                    <form class="ui large form" method="POST" action="{{ url('/add_new_member') }}">
                        {{ csrf_field() }}
                        <div class="ui stacked segment">
                            <div class="field">
                              <div class="ui left icon input">
                                <i class="key icon"></i>
                                <input type="text" name="reg_no" placeholder="enter teacher reg. no." required>
                              </div>
                            </div>
                            <input type="hidden" name="school_id" value="{{ Auth::user()->school->id }}">
                            <input type="hidden" name="role" value="teacher">
                            <button type="submit" class="ui fluid teal submit button">Add</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="four wide column">
                <div class="ui raised segment">
                    <h2 class="ui centered header">Authority</h2>
                    <form class="ui large form" method="POST" action="{{ url('/add_new_member') }}">
                        {{ csrf_field() }}
                        <div class="ui stacked segment">
                            <div class="field">
                              <div class="ui left icon input">
                                <i class="key icon"></i>
                                <input type="text" name="reg_no" placeholder="enter authority reg. no." required>
                              </div>
                            </div>
                            <input type="hidden" name="school_id" value="{{ Auth::user()->school->id }}">
                            <input type="hidden" name="role" value="authority">
                            <button type="submit" class="ui fluid teal submit button">Add</button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            
            <div class="two wide column"></div>
        </div>
    </div>

@endsection

@section('footer')
	@parent
@endsection