@extends('layout.master')

@section('title', 'Dashboard | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'Dashboard'
			,'trend' =>  Auth::user()->roles->first()->name
		])

	@include('partial.user_dashmenu')

    @include('partial.user_index')

@endsection

@section('footer')
	@parent
@endsection