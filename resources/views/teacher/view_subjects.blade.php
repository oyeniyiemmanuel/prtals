@extends('layout.master')

@section('title', 'Teacher Manage Subjects | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.user_dashmenu')

    <div class="ui container">
    <h2 class="ui center aligned header">
    	Manage subjects you teach at {{ ucfirst(Auth::user()->school->name) }}
    	<div class="sub header">
    		<a href="{{ url('/profile') }}"><i class="arrow left icon"></i><strong>go to profile </strong></a>
    	</div>
    </h2>
    	<div class="ui stackable grid">
    		<div class="three wide column">
				<div class="ui small mobile_hide vertical menu">
					<div class="active item ui teal header">
						<div class="content">
						  My Subjects
						</div>
					</div>
					@if(Auth::user()->teacher && !empty(Auth::user()->teacher->subjects->all()))
					  @foreach(Auth::user()->teacher->subjects->sortBy('name') as $subject)
					  <a class="item" href="">
					    {{ ucfirst($subject->name) }}
					  </a>
					  @endforeach
					@else
					  <a class="item">
					    <em>no results</em>
					  </a>
					@endif
				</div>

				<div class="ui small mobile_hide vertical menu">
					<div class="active item ui teal header">
						<div class="content">
						  Grades
						</div>
					</div>
					@if(Auth::user()->teacher && !empty(Auth::user()->teacher->grades->all()))
					  @foreach(Auth::user()->teacher->grades->sortBy('name') as $teacher_grade)
					  <a class="item" href="">
					    {{ strtoupper($teacher_grade->name) }}
					  </a>
					  @endforeach
					@else
					  <a class="item">
					    <em>no results</em>
					  </a>
					@endif
				</div>
    		</div>
    		<div class="ten wide column">
    			

    			@include('teacher.subjects_profile_view', ['user_role' => 'teacher'])

    			
    		</div>
    		<div class="three wide column">
    			<h4 class="ui dividing header">Add a new subject here</h4>
    			<form class="ui small form" method="POST" action="{{ url('/teacher/add_subject') }}">
    				{{ csrf_field() }}
		           <div class="ui stacked segment">
		           <div class=" field">
						<select name="subject" class="ui fluid floating dropdown" required>
						<option value="">Select Subject</option>
						@foreach($subject_categories as $category)
						<option <?php echo (Auth::user()->teacher) && (Auth::user()->teacher->best_subject == $category)? ' selected': ''; ?> value="{{ $category }}">{{ $category }}</option>
						@endforeach
						</select>
					</div>
					<div class="fields">
			           	<div class="ten wide field">
							<select name="grade" class="ui fluid floating dropdown" required>
								@foreach($grades as $grade)
								<option <?php echo (Auth::user()->teacher) && (Auth::user()->grade) && (Auth::user()->grade->name == $grade)? ' selected': ''; ?> value="{{ $grade }}">{{ strtoupper($grade) }}</option>
								@endforeach
							</select>
						</div>
						<div class="six wide field">
							<select name="wing" class="ui fluid floating dropdown" required>
								@foreach($alphabets as $alphabet)
								<option value="{{ $alphabet }}">{{ strtoupper($alphabet) }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class=" field">
						<select name="department" class="ui fluid floating dropdown" required>
							<option value="">Department</option>
							@foreach($departments as $department)
							<option <?php echo (Auth::user()->teacher) && (Auth::user()->department) && (Auth::user()->department->name == $department)? ' selected': ''; ?> value="{{ $department }}">{{ strtoupper($department) }}</option>
							@endforeach
						</select>
					</div>
		           </div>

		           <button type="submit" class="ui fluid mini teal submit button">Add</button>
    			</form>
    		</div>
    	</div>
    </div>	
@endsection

@section('footer')
	@parent
@endsection