<div class="ui raised segment">
	<table class="ui teal table">
		<thead>
		<tr>
		  <th class="center aligned">Subjects</th>
		  <th class="center aligned">Details</th>
		</tr>
		</thead>
		<tbody>
		@if($user->teacher && !empty($user->teacher->duties->all()))
		  @foreach($user->teacher->subjects->sortBy('name') as $subject)
		    <tr>
		      <td>
		        <h3 class="ui center aligned header">{{ ucfirst($subject->name) }}</h3>
		      </td>
		      <td>
		      <table class="ui grey compact table duties_table">
			      	<tbody>
		      		@foreach($user->teacher->duties as $duty)
		      		
				      	@if($subject->id == $duty->subject_id)
				      			<tr>
				      				<td>
				      				<a href="" class="ui teal mini button ribbon label">
				      						<i class="flag icon"></i>
				      						<?php $x = App\Department::findOrFail($duty->department_id); echo $x->name ?>
				      					</a>
				      				</td>
				      				<td class="collapsing">
				      					<strong><?php $x = App\Grade::findOrFail($duty->grade_id); echo strtoupper($x->name) ?>
				      					<?php $x = App\Wing::findOrFail($duty->wing_id); echo ucfirst($x->name) ?></strong>
				      				</td>
				      				<td class="collapsing">
				      					<span class="ui mini tag label button">
				      					<i class="student icon"></i>
				      					<?php 
				      						$x = App\Student::where([
				      								['grade_id', $duty->grade_id]
				      								,['school_id', $duty->school_id]
				      								,['department_id', $duty->department_id]
				      								,['wing_id', $duty->wing_id]
				      							])->get();

				      						echo count($x); 
				      					?> students
				      					</span>
				      				</td>
				      				@if(Auth::user() == $user)
				      				<td class="collapsing">
				      					<form method="POST" action="/teacher/remove_duty">
				      						{{ csrf_field() }}
					      					<input type="hidden" name="duty_id" value="{{ $duty->id }}">
					      					<button class="ui basic mini button remove_duty">
					      						<i class="trash icon"></i>
					      						remove
					      					</button>
				      					</form>
				      				</td>
				      				@endif
				      			</tr>
				      	@endif
		      		@endforeach
		      		</tbody>
			    </table>
		      </td>
		    </tr>
		   @endforeach

		@else
			<tr>
		      <td>
		        <i class="circle info icon"></i> Your subjects list is empty
		      </td>
		      <td><em>not set</em></td>
		    </tr>
		@endif
		</tbody>
	</table>
</div>
