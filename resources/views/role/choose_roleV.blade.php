@extends('layout.master')

@section('title', 'Choose Role | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => 'Enter Your School Registration Number'
			,'trend' => ''
		])

    <div class="ui container">
    	<div class="ui stackable grid">
    		<div class="four wide column"></div>
    		<div class="eight wide column">
    			<br>
    			<div class="ui secondary raised segment">
    				<h3 class="ui header">
					{{ ucwords(Auth::user()->school->name) }} <br> <em>{{ Auth::user()->school->address }}, {{ ucfirst(Auth::user()->school->state) }} </em>&nbsp; <a href="{{ url('/choose_school') }}"><button class="ui teal button">Not My School</button></a>
					</h3>
					<hr>
					@if(!empty(Auth::user()->school->users->where('verified', '=', 1)->all()))
						@foreach(Auth::user()->school->users->where('verified', '=', 1) as $user)
							@if($user->hasRole('school-admin'))
								<h4>
									Your School Admin is: &nbsp; &nbsp; &nbsp; 
									<a href="{{ url('/profile') }}/{{ $user->username }}">@if($user->picture && !empty($user->picture->url))
									<img class="ui avatar image" src="{{ $user->picture->url }}">
									@endif
									{{ ucwords($user->name) }} <em>@if($user->student && !empty($user->student->phone)){({{ $user->student->phone }})}@endif</em>
									</a>
								</h4>
								<p><em>
									<b>NOTE:</b> YOUR REGISTRATION NUMBER WILL BE REJECTED UNLESS YOU HAVE BEEN ADDED BY YOUR SCHOOL ADMIN.
									<br>
									If you are having a difficulty in contacting your school-admin, call - <b>{{ env('APP_CONTACT') }}</b>
								</em></p>
							@endif
						@endforeach
					@else
						<h4>
							<em>You seem to be the first user to register in your school, this will automatically make you the <b>school admin</b></em>
						</h4>
						<p>select your role and type "school-admin" in the registration field below</p>
					@endif
					
					<form class="ui form" method="POST" action="{{ url('/attach_role') }}">
					    {{ csrf_field() }}
					  <div class="field">
					    <select name="role" class="ui floating dropdown" required>
					      <option value="">Select Your Role</option>
					      <option value="student">Student</option>
					      <option value="teacher">Teacher</option>
					      <option value="authority">School Authority</option>
					    </select>
					  </div>

					  <div class="field">
						<div class="ui left icon input">
							<i class="key icon"></i>
							<input type="text" name="reg_no" placeholder="Your Registration No." required>
						</div>
					  </div>

					  <input type="hidden" name="school_id" value="{{ Auth::user()->school->id }}">

					  <button type="submit" class="ui fluid large teal submit button">Proceed</button>
					</form>
    			</div>
    			<br>
    		</div>
    		<div class="four wide column"></div>
    	</div>
    </div>	
@endsection

@section('footer')
	@parent
@endsection