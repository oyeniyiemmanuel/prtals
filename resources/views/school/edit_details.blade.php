@extends('layout.master')

@section('title', 'Edit School Details | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">

		@include('partial.school_page_header')

		<div class="ui stackable grid">
			<div class="three wide column"></div>
			<div class="ten wide column">
				<div class="ui raised inverted segment" style="background: #023B42;">
					<h1 class="ui centered header">
						@if($school->logo && !empty($school->logo->url))
				      	<img alt="{{ $school->name }} {{ env('APP_URL') }}" class="ui mini image" id="logo" src="{{ $school->logo->url }}">
				      	@else
				        <img alt="{{ $school->name }} {{ env('APP_URL') }}" class="ui mini image" src="/media/img/school_logo.jpg">
				        @endif
						<div class="content">
							Edit School Details
						</div>
					</h1>
				</div>

					@include('errors.form_valid')
					
					<form class="ui small form" method="POST" action="">
			          {{ csrf_field() }}

			          <div class="ui secondary raised segment">
				          <h4 class="ui header">Basic details</h4>
				          <div class="two fields">
				            <div class="eight wide field">
				            	<label>school name</label>
				              <div class="ui left icon input">
				                <i class="user icon"></i>
				                <input type="text" name="name" placeholder="school name" value="{{ $school->name }}" required>
				              </div>
				            </div>

				            <div class="eight wide field">
				            	<label>founded</label>
				            	<div class="ui left icon input">
									<i class="calendar icon"></i>
									<input type="date" name="founded" placeholder="yyyy-mm-dd"  value="<?php echo $school->founded? $school->founded: ''; ?>" max="{{ date('Y-m-d') }}" required>
								</div>
				            </div>
				          </div>
							
						  <div class="two fields">
						  	<div class="eight wide field">
				            	<label>state</label>
								<select name="state" class="ui fluid floating dropdown" required>
								    <option value=""> state </option>
								@foreach($states as $state)
								 	<option <?php echo ($school) && ($school->state == strtolower($state))? ' selected': ''; ?> value="{{ strtolower($state) }}">{{ $state }}</option>
								@endforeach
								</select>
				            </div>

				           	<div class="eight wide field">
				           		<label>uniform details</label>
								<div class="ui left icon input">
									<i class="shirt icon"></i>
									<input type="text" name="uniform" placeholder="uniform" value="<?php echo $school->uniform? $school->uniform: ''; ?>" required>
								</div>
				            </div>
						  </div>
					  </div>

					  <div class="ui secondary raised segment">
						  <h4 class="ui header">Statistics</h4>
						  <div class="ui secondary segment">
						  	<div class="four fields">
						  		<div class="four wide field">
						  			<label>No. of pupils</label>
									<div class="ui left icon input">
										<i class="bar chart icon"></i>
										<input type="text" name="no_of_pupils" placeholder="204" value="<?php echo $school->no_of_pupils? $school->no_of_pupils: ''; ?>" required>
									</div>
						  		</div>
						  		<div class="four wide field">
						  			<label>No. of teachers</label>
									<div class="ui left icon input">
										<i class="bar chart icon"></i>
										<input type="text" name="no_of_teachers" placeholder="20" value="<?php echo $school->no_of_teachers? $school->no_of_teachers: ''; ?>" required>
									</div>
						  		</div>
						  		<div class="four wide field">
						  			<label>No. of staffs</label>
									<div class="ui left icon input">
										<i class="bar chart icon"></i>
										<input type="text" name="no_of_other_staffs" placeholder="14" value="<?php echo $school->no_of_other_staffs? $school->no_of_other_staffs: ''; ?>" required>
									</div>
						  		</div>
						  		<div class="four wide field">
						  			<label>No. of grades</label>
									<div class="ui left icon input">
										<i class="bar chart icon"></i>
										<input type="text" name="no_of_classes" placeholder="24" value="<?php echo $school->no_of_classes? $school->no_of_classes: ''; ?>" required>
									</div>
						  		</div>
						  	</div>
						  </div>
					  </div>

					  <div class="ui secondary raised segment">
						  <h4 class="ui header">Extra details</h4>
						  <div  class="two fields">
						  	<div class="eight wide field">
						  		<label>Address</label>
						  		<textarea rows="6" name="address" required><?php echo $school->address? $school->address: ''; ?></textarea>
						  	</div>
						  	<div class="eight wide field">
						  		<label>History</label>
						  		<textarea rows="6" name="history" required><?php echo $school->history? $school->history: ''; ?></textarea>
						  	</div>
						  </div>

						  <div  class="two fields">
						  	<div class="eight wide field">
						  		<label>Tuition</label>
						  		<textarea rows="6" name="tuition" required><?php echo $school->tuition? $school->tuition: ''; ?></textarea>
						  	</div>
						  	<div class="eight wide field">
						  		<label>Rule</label>
						  		<textarea rows="6" name="rules" required><?php echo $school->rules? $school->rules: ''; ?></textarea>
						  	</div>
						  </div>

						  <div  class="two fields">
						  	<div class="eight wide field">
						  		<label>Anthem</label>
						  		<textarea rows="6" name="anthem" required><?php echo $school->anthem? $school->anthem: ''; ?></textarea>
						  	</div>
						  	<div class="eight wide field">
						  		<label>Accomplishments</label>
						  		<textarea rows="6" name="accomplishments" required><?php echo $school->accomplishments? $school->accomplishments: ''; ?></textarea>
						  	</div>
						  </div>
					  </div>

			          
			          <button type="submit" class="ui fluid teal submit button">Update</button>
			          
			        </form>
					
					<br>
					<a class="ui mini label" href="{{ url('/schools') }}/{{ $school->slug }}"><i class="arrow left icon"></i><strong>Back To School Page </strong></a>

			</div>
			<div class="three wide column"></div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection