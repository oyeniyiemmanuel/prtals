@extends('layout.master')

@section('title', 'Choose School | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
    
    @include('partial.page_header', [
            'title' => 'Choose Your High School'
            ,'trend' => ''
        ])
    @if(1>2)
    <div class="ui container">
        <div class="ui stackable grid">
            <div class="four wide column no_padding"></div>
            <div class="four wide column no_padding"></div>
            <div class="four wide column">
                <br>
                <a href="{{ url('/choose_student_type') }}">
                    <button class="ui black button">
                        <i class="left arrow icon"></i>
                        Back to select student type
                    </button>
                </a>
            </div>
            <div class="four wide column no_padding"></div>
        </div>
    </div>
    <div class="ui container">
            <div class="ui stackable grid">
                <div class="six wide column no_padding"></div>
                <div class="ten wide column no_padding">
                    @if( isset($filtered_state) )

                        @if(!count($schools)== 0)
                        <h1>There are {{ count($schools) }} school(s) in {{ $filtered_state }}</h1>
                        @else
                        <h1>No Registered school from {{ $filtered_state }}</h1>
                        @endif
                        
                    @else
                        <h1></h1>
                    @endif

                </div>
            </div>
        
            <div class=" ui stackable grid">

                <div class="six wide column">
                    <div class="ui grid">
                        <div class="eight wide column">
                            <a href="{{ url('/choose_school') }}">
                                <div class="ui fluid basic button">
                                    All Schools
                                </div>
                            </a>
                        </div>
                        <div class="eight wide column">
                            <form class="ui form" method="POST" action="{{ url('/choose_school_by_state') }}">
                                {{ csrf_field() }}
                              <div class="field">
                                  <select onchange="this.form.submit()" name="state" class="ui fluid floating dropdown">
                                        <option value=""> By State </option>
                                    @foreach($states as $state)
                                      <option value="{{ strtolower($state) }}">{{ $state }}</option>
                                    @endforeach
                                  </select>
                              </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="ten wide column">
                    <form class="ui form" method="POST" action="{{ url('/attach_school') }}">
                        {{ csrf_field() }}
                        <div class="ui fluid selection floating dropdown labeled icon button">
                            <input type="hidden" name="school_name">
                            <i class="university icon"></i>
                            <span class="text">Choose your school here</span>
                            <div class="menu">
                              <div class="ui icon search input">
                                <i class="search icon"></i>
                                <input type="text" placeholder="Search schools...">
                              </div>
                              <div class="divider"></div>
                              <div class="header">
                                <i class="university icon"></i>
                                @if(isset($filtered_state))
                                    Schools In {{ $filtered_state }}
                                @else
                                    All schools
                                @endif
                              </div>
                              <div class="scrolling menu">
                                @foreach($schools as $school)
                                <div class="item" data-value="{{ $school->id }}">
                                  <div class="ui blue empty circular label"></div>
                                  {{ $school->name }}, <em>{{ $school->address }}</em> <strong>{{ucfirst($school->state)}}</strong>
                                </div>
                                @endforeach 
                              </div>
                            </div>
                        </div>

                        <br>
                        <input type="hidden" name="role" value="student">
                        <input type="hidden" name="reg_no" value="school-admin">
                        <button type="submit" class="ui fluid large teal submit button">Proceed</button>
                    </form>
                </div>
            </div>
            
            <div class="ui stackable grid">
                <div class="four wide column">
                    <div class="ui secondary segment">
                        <h3 class="ui header">
                            If you're certain your school isn't in our records 
                            <a href="{{ url('/new_school_request') }}">
                                <button type="submit" class="ui large secondary button">
                                    <i class="plus icon"></i>
                                    Add Your School
                                </button>
                            </a>
                        </h3>
                    </div>
                </div>
                <div class="four wide column"></div>
                <div class="four wide column"></div>
                <div class="four wide column"></div>
            </div>
    </div>
    @endif

    <div class="ui container">
        <div class="ui stackable grid">
            <div class="four wide column"></div>
            <div class="eight wide column"> 
                <a href="{{ url('/choose_student_type') }}">
                    <i class="arrow left icon"></i>
                    <strong>back to select student type </strong>
                </a>               
                <div class="ui secondary segment">
                    <form class="ui form" method="POST" action="{{ url('/attach_school') }}">
                        {{ csrf_field() }}

                        <select name="school_name" class="" required>
                            <option value=""> Choose your School </option>

                            @foreach($schools as $school)
                            <option value="{{ $school->id }}"> {{ ucwords($school->name) }}, <em>{{ ucfirst($school->state) }}</em> </option>
                            @endforeach
                        </select>
                        <br>

                        <input type="hidden" name="role" value="student">
                        <input type="hidden" name="reg_no" value="school-admin">
                        <button type="submit" class="ui fluid large teal submit button">Proceed</button>
                    </form>
                </div>

                <div class="ui centered block header">
                    <div class="content">
                      Can't Find Your School?
                    </div>
                    <div class="sub header">
                        <a class=" activating element"><em>Send us a request</em></a>
                        <div class="ui flowing popup top left transition hidden">
                          <a href="{{ url('/new_school_request') }}" class="ui mini basic fluid button">High School</a>
                          <br>
                          <a href="{{ url('/new_university_request') }}" class="ui mini black fluid button">Higher Institution</a>
                        </div>
                         and it will be added within 24 hours. <br>
                         Alternatively, you can call <b>+234 806 268 0206</b> and it will be added immediately.
                    </div>
                </div>
            </div>
            <div class="four wide column"></div>
        </div>
    </div>	
@endsection

@section('footer')
	@parent
@endsection