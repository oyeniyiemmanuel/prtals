@extends('layout.master')

@section('title', 'Posts By Me| Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">

		@include('partial.school_page_header')

	</div>
	<br>

	@include('partial.user_school_posts')
	
@endsection

@section('footer')
	@parent
@endsection