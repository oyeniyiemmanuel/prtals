@extends('layout.master')

@section('title', 'News By Me| Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">

		@include('partial.school_page_header')

	</div>
	<br>

	@include('partial.user_school_news')
	
@endsection

@section('footer')
	@parent
@endsection