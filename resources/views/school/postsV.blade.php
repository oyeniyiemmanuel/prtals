@extends('layout.master')

@section('title', 'Posts: '.ucwords($school->name).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">

		@include('partial.school_page_header')

	</div>
	<br>

	@include('partial.school_posts')
	
@endsection

@section('footer')
	@parent
@endsection