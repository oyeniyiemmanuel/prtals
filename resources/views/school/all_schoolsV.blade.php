@extends('layout.master')

@section('title', ' Schools | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
    
    @include('partial.page_header', [
            'title' => 'Schools'
            ,'trend' => $filtered_state
        ])

    <div class="ui container fluid">
            <div class=" ui stackable grid">
                <div class="one wide column"></div>
                <div class="two wide column">
                    <br>
                    <a href="{{ url('/schools') }}">
                        <div class="ui fluid basic mini button">
                            All Schools
                        </div>
                    </a>
                    <br>
                    <form class="ui form" method="POST" action="{{ url('/schools_by_state') }}">
                        {{ csrf_field() }}
                      <div class="field">
                          <select onchange="this.form.submit()" name="state" class="ui fluid floating dropdown mini button">
                                <option value=""> By State </option>
                            @foreach($states as $state)
                              <option value="{{ strtolower($state) }}">{{ $state }}</option>
                            @endforeach
                          </select>
                      </div>
                    </form>
                </div>

                <div class="twelve wide column school_threads">
                    <br>
                    <form class="ui big form" method="GET" action="{{ url('/search') }}/schools_only">
                        {{ csrf_field() }}
                        <div class="field">
                            <div class="ui big action left icon input">
                                <i class="search icon"></i>
                                <input class="set_cursor" type="search" name="query" placeholder="Search schools.." value="<?php if(isset($query)&&($query!=null)){ echo $query; } ?>" autofocus>
                            </div>
                        </div>
                    </form>
                    <div class="ui segment">
                    @if(!empty($schools->all()))
                        <div class="ui four doubling cards">
                        @foreach($schools->sortBy('name') as $school)
                          <a href="/schools/{{ $school->slug }}" class="card">
                            <div class="image">
                            @if($school->logo && !empty($school->logo->url))
                              <img alt="{{ $school->name }} {{ env('APP_URL') }}" src="{{ $school->logo->url }}">
                            @else
                              <img alt="{{ $school->name }} {{ env('APP_URL') }}" src="/media/img/school_logo.jpg">
                            @endif
                            </div>

                            <div class="content">
                                <div class="header" href="#">{{ ucwords(strtolower($school->name)) }}</div>
                                <div class="meta">
                                  <span><em>{{ $school->address }}, {{ ucwords($school->state) }}</em></span>
                                </div>
                            </div>
                          </a>
                        @endforeach
                        </div>
                    @else
                        <div class="ui block header">
                            <i class="info icon"></i>
                            <div class="content">
                              The filter returned zero result 
                              <div class="sub header"></div>
                            </div>
                        </div>
                    @endif
                    </div>
                </div>
                <div class="one wide column"></div>
            </div>
            
            <div class="ui dividing header"></div>
          
    </div>

    <div class="ui container">
        <br>
        <div class="ui raised segment">
            <h3 class="ui header">If you're certain your school isn't in our records, send us a request to <a href="{{ url('/new_school_request') }}"><button type="submit" class="ui large teal button">Add Your School</button></a></h3>

        </div>
    </div>	
@endsection

@section('footer')
	@parent
@endsection