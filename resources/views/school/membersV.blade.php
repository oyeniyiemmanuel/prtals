@extends('layout.master')

@section('title', ucwords($school->name).' Members| Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">

		@include('partial.school_page_header')

	</div>
	<br>
	<div class="ui container">
		<div class="ui stackable grid">
			<div class="sixteen wide column">
				<div class="ui segment">
					
					@if(!empty($members))
						<h3 class="ui centered block header">
							<div class="content">
								@if($members[0]->school->logo && !empty($members[0]->school->logo->url))
								<img alt="{{ $members[0]->school->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $members[0]->school->logo->url }}">
								@else
								<i class="university icon"></i>
								@endif
								{{ ucwords($members[0]->school->name) }} Members
								<div class="sub header"></div>
							</div>
						</h3>

						<div class="ui six doubling cards">
						@foreach($members as $member)
						  <a href="/profile/{{ $member->username }}" class="card">
						    <div class="image">
						    @if($member->picture && !empty($member->picture->url))
						      <img alt="{{ $member->name }} {{ env('APP_URL') }}" src="{{ $member->picture->url }}">
						    @else
						      <img alt="{{ $member->name }} {{ env('APP_URL') }}" src="/media/img/user.jpg">
						    @endif
						    </div>

							<div class="content">
								<div class="header" href="#">{{ $member->name }}</div>
								<div class="meta">
								  <span>{{ $member->roles->first()->name }}</span>
								  @if($member->hasRole('school-admin'))
								  <span><em>admin</em></span>
								  @endif
								</div>
							</div>
						  </a>
						@endforeach
						</div>
					@else
						<div class="ui block header">
							<i class="info icon"></i>
							<div class="content">
							  School has no member yet 
							  <div class="sub header"></div>
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>	
	</div>
	
@endsection

@section('footer')
	@parent
@endsection