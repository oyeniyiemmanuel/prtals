@extends('layout.master')

@section('title', ucwords($school->name).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container fluid school_page">
		
		@include('partial.school_page_header')
		
		<div class="ui stackable grid mobile_margin_1em">
			<div class="one wide column no_padding"></div>
			<div class="ten wide column">
				
				<div class="ui fluid card raised segment">
				@if(Auth::user())
				  @if(Auth::user()->school  && Auth::user()->school->id == $school->id)
					  @if(Auth::user()->hasRole('authority') || Auth::user()->hasRole('school-admin'))
					  <a class="ui grey right corner label school_cover_modal_button">
				        <i class="pencil icon"></i>
				      </a>
				      @endif
			      @endif
			    @endif
			      @include('modal.school_cover_modal')
			      <div id="cover_wrapper">
			      	@if($school->cover && !empty($school->cover->url))
			      	<img alt="{{ $school->name }} {{ env('APP_URL') }}" id="cover" src="{{ $school->cover->url }}">
			      	@else
			        <img alt="{{ $school->name }} {{ env('APP_URL') }}" id="cover" src="/media/img/school_cover.jpg">
			        @endif
			      </div>
			      	<div class="school_logo_cover">
			    		<div class="ui card school_logo">
					      <div class="image">
					      @if(Auth::user())
					      	@if(Auth::user()->school  && Auth::user()->school->id == $school->id)
						      	@if(Auth::user()->hasRole('authority') || Auth::user()->hasRole('school-admin'))
						      	<a class="ui grey mini right corner label school_logo_modal_button">
							        <i class="pencil icon"></i>
							    </a>
							    @endif
						    @endif
						  @endif
						    @if($school->logo && !empty($school->logo->url))
					      	<img alt="{{ $school->name }} {{ env('APP_URL') }}" id="logo" src="{{ $school->logo->url }}">
					      	@else
					        <img alt="{{ $school->name }} {{ env('APP_URL') }}" src="/media/img/school_logo.jpg">
					        @endif
					      </div>
					      @include('modal.school_logo_modal')
					    </div>
				    </div>
			    </div>
			</div>

			<div class="five wide column">
				<div class="ui stackable grid">
		        	<div class="twelve wide column">
		        		<div class="">
		        			@if(Auth::user())
		        				@if(Auth::user()->school  && Auth::user()->school->id == $school->id)
				        			@if(Auth::user()->hasRole('authority') || Auth::user()->hasRole('school-admin'))
				        			<a href="/schools/details/{{ $school->slug }}/edit" class="ui right floated teal small label">
								        <i class="pencil icon"></i> edit details
								    </a>
								    @endif
							    @endif
						    @endif
		        			<div class="ui list">
							  <div class="item">
							    <i class="calendar icon"></i>
							    <div class="content">
							      founded <strong><?php echo $school->founded? 'on  '.Carbon\Carbon::parse($school->founded)->format('F d Y'): ': <em>null</em>'; ?></strong>
							    </div>
							  </div>

							  <div class="item">
							    <i class="marker icon"></i>
							    <div class="content">
							    <?php echo $school->address? $school->address.','.ucfirst($school->state): 'Location: <em>null</em>'; ?>
							    </div>
							  </div>

							  <div class="item">
							    <i class="users icon"></i>
							    <div class="content">
							      registered students on prtals <a>{{ count($school->students->where('user_id', '!=', null)) }}</a>
							    </div>
							  </div>

							  <div class="item">
							    <i class="hourglass end icon"></i>
							    <div class="content">
							      {{ substr($school->history, 0, 70) }}...<a class="school_history_modal_button">read all</a>
							    </div>
							    @include('modal.school_history_modal')
							  </div>

							  <div class="item">
							    <i class="trophy icon"></i>
							    <div class="content">
							      {{ substr($school->accomplishments, 0, 70) }}...<a class="school_accomplishments_modal_button">read all</a>
							    </div>
							    @include('modal.school_accomplishments_modal')
							  </div>

							  <div class="item">
							    <i class="flag icon"></i>
							    <div class="content">
							      {{ substr($school->anthem, 0, 70) }}...<a class="school_anthem_modal_button">read all</a>
							    </div>
							    @include('modal.school_anthem_modal')
							  </div>

							  <div class="item">
							    <i class="money icon"></i>
							    <div class="content">
							      {{ substr($school->tuition, 0, 70) }}...<a class="school_tuition_modal_button">read all</a>
							    </div>
							    @include('modal.school_tuition_modal')
							  </div>
							</div>
		        		</div>
		        	</div>

		        	<div class="twelve wide column" style="padding-top: 0;">
		        		<div class="ui fluid card">
		        			@if(!empty($school->users->where('verified', '=', 1)->all()))
							<div class="content">
							  <div class="center aligned">Admin</div>
							  <div class="description center aligned">
							  	@foreach($school->users->all() as $user)
							  		@if($user->hasRole('school-admin'))
							  		<a href="/profile/{{ $user->username }}">
								  	<span data-tooltip="{{ $user->name }}">
								  		@if(!empty($user->picture->url))
								  		<img alt="{{ $user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $user->picture->url }}">
								  		@else
								  		<img alt="{{ $user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
								  		@endif
								  	</span>
								  	</a>
								  	@endif
								@endforeach
							  </div>
							</div>
							@else
								<div class="content">
								  <div class="description center aligned">
								    <div class="ui block header">
										<i class="info icon"></i>
										<div class="content">
											No Admin yet
										</div>
									</div>
								  </div>
								</div>
		        			@endif
		        		</div>
						<div class="ui fluid card">
						@if(!empty($school->users->where('verified', '=', 1)->all()))
							<div class="content">
							  <div class="center aligned">Members</div>
							  <div class="description center aligned">
							  	@foreach($school->users->take(6) as $user)
							  		@if(!empty($user->roles()->first()))
							  		<a href="/profile/{{ $user->username }}">
								  	<span data-tooltip="{{ $user->name }}">
								  		@if(!empty($user->picture->url))
								  		<img alt="{{ $user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $user->picture->url }}">
								  		@else
								  		<img alt="{{ $user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
								  		@endif
								  	</span>
								  	</a>
								  	@endif
								@endforeach
							  </div>
							</div>
							@if(Auth::guest() || Auth::user()->verified == 0)
							<div class="ui mini bottom attached basic button buttons" style="padding: 0;">
								<a href="/schools/{{ $school->slug }}/members" class="ui basic button">
									<i class="list icon"></i>
							  		View all Members
								</a>
								@if(Auth::guest())
								<a href="{{ url('/signup') }}" class="ui basic button">
									<i class="add user icon"></i>
							  		Become a member
								</a>
								@endif
								@if(Auth::user() && Auth::user()->verified == 0)
								<a href="{{ url('/choose_student_type') }}" class="ui basic button">
									<i class="add user icon"></i>
							  		Become a member
								</a>
								@endif
							</div>
							@endif

							@if((Auth::user() && Auth::user()->verified == 1) )
							<a href="/schools/{{ $school->slug }}/members" class="ui mini bottom attached basic button">
									<i class="list icon"></i>
							  		View all Members
							</a>
							@endif

						@else
							<div class="content">
							  <div class="description center aligned">
							    <div class="ui block header">
									<i class="info icon"></i>
									<div class="content">
									  No member yet
									  <div class="sub header"></div>
									</div>
								</div>
							  </div>
							</div>

							<a href="{{ url('/signup') }}" class="ui mini bottom attached basic button">
								<i class="add user icon"></i>
							  	Become the first member
							</a>
						@endif
						</div>
		        	</div>
		        </div>
			</div>
			<div class="one wide column"></div>
		</div>
					
	    <div class="">
	    	<div class="ui stackable grid">
	    		<div class="one wide column"></div>
	    		<div class="five wide column">
	    			<div class="ui raised segment" style="background: #FCFCFC">
		    			<?php
		    				$news = App\News::where('school_id', '=', $school->id)->latest()->get();
		    			?>
		    			@if(empty($news->all()))
		    				@include('bots.news', [
								'bot_status' => true
								,'bot_main_word' => ''
								,'bot_main_phrase' => 'Share News About Your School'
								,'bot_main_sub_phrase' => ''
								,'bot_catch_phrase' => 'Sign in to write news about your school'
							])
			    			<div class="ui block header">
								<i class="info icon"></i>
								<div class="content">
								  No News 
								  <div class="sub header"></div>
								</div>
							</div>
					    @else
			    			@include('bots.news', [
								'bot_status' => true
								,'bot_main_word' => ''
								,'bot_main_phrase' => 'Share News About Your School'
								,'bot_main_sub_phrase' => ''
								,'bot_catch_phrase' => 'Sign in to write news about your school'
							])

							<a href="{{ url('/schools') }}/{{ $school->slug }}/news">
							  	See all news of this school
							  	<i class="right arrow icon"></i>
							  </a>
						    @foreach($news->take(1) as $news_thread)
								<div class="ui items">
								  @include('partial.news_threadV')
								</div>
							@endforeach
						@endif

		    			<?php
		    				$events = App\Event::where('school_id', '=', $school->id)->latest()->get();
		    			?>
		    			@if(empty($events->all()))
		    				<div class="ui dividing header">
								<div class="content">
								  Events
								</div>
								<div class="sub header">0 in total</div>
						    </div>
			    			<div class="ui block header">
								<i class="info icon"></i>
								<div class="content">
								  No Events 
								  <div class="sub header"></div>
								</div>
							</div>
					    @else
			    			<div class="ui dividing header">
								<div class="content">
								  Events
								  <a href="{{ url('/schools') }}/{{ $school->slug }}/events" class="ui right floated mini label">
								  	view all
								  </a>
								</div>
						    </div>
						    @foreach($events->take(1) as $event_thread)

								  @include('partial.event_threadV')

							@endforeach
						@endif
						
						@if(Auth::user()  && Auth::user()->school && Auth::user()->school->id == $school->id)
			    			<?php
			    				$announcements = App\Announcement::where('school_id', '=', $school->id)->latest()->get();
			    			?>
			    			@if(empty($announcements->all()))
			    				<div class="ui dividing header">
									<div class="content">
									  Announcements
									</div>
									<div class="sub header">0 in total</div>
							    </div>
				    			<div class="ui block header">
									<i class="info icon"></i>
									<div class="content">
									  No Announcements 
									  <div class="sub header"></div>
									</div>
								</div>
						    @else
				    			<div class="ui dividing header">
									<div class="content">
									  Announcements
									  <a href="{{ url('/announcements') }}/school/{{ Auth::user()->school->slug }}" class="ui right floated mini label">
									  	view all
									  </a>
									</div>
							    </div>
							    @foreach($announcements->take(1) as $announcement_thread)
									<div class="ui items">
									  @include('partial.announcement_threadV')
									</div>
								@endforeach
							@endif
						@endif
				    
	    			</div>
	    		</div>

	    		<div class="five wide column post_threads">
				    @if(empty($school->posts->all()))
				    	@include('bots.post',  [
							'bot_status' => true
							,'bot_main_word' => ''
							,'bot_main_phrase' => 'Write New Post'
							,'bot_main_sub_phrase' => ''
							,'bot_catch_phrase' => 'Sign in to create new post'
						])

						<div class="ui block header">
							<i class="info icon"></i>
							<div class="content">
							  No posts to display
							  <div class="sub header"></div>
							</div>
						</div>
					@else
						@include('bots.post',  [
							'bot_status' => true
							,'bot_main_word' => ''
							,'bot_main_phrase' => 'Write New Post'
							,'bot_main_sub_phrase' => ''
							,'bot_catch_phrase' => 'Sign in to create new post'
						])
						<a href="{{ url('/schools') }}/{{ $school->slug }}/posts">
						  	See all posts of this school
						  	<i class="right arrow icon"></i>
						  </a>
					    @foreach($school->posts->take(5) as $post)
							<div class="ui fluid card post_thread">
							  @include('partial.half_post_threadV')
							</div>
						@endforeach
					@endif
	    		</div>

	    		<div class="four wide column">
	    			@include('partial.aside_schools_list')
	    		</div>

	    		<div class="one wide column"></div>
	    	</div>
	    </div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection