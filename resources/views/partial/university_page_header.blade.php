<div class="school_header">
	<div class="ui stackable grid">
		<div class="two wide column no_padding"></div>
		<a href="{{ url('/universities') }}/{{ $university->slug }}" class="eight wide column school_link">
			@if($university->logo && !empty($university->logo->url))
	      	<div class="ui mobile_hide"><img alt="{{ $university->name }} {{ env('APP_URL') }}" class="ui tiny left floated bordered circular image" id="logo" src="{{ $university->logo->url }}"></div>
	      	@else
	        <div class="ui mobile_hide"><img alt="{{ $university->name }} {{ env('APP_URL') }}" class="ui tiny left floated bordered circular image" src="/media/img/university_logo.jpg"></div>
	        @endif

	        <h1 class="name">{{ ucwords($university->name) }}</h1>
	        <p class="address"><em>{{ ucwords($university->address) }}</em>, <b>{{ ucfirst($university->state) }}</b></p>
		</a>
		<div class="six wide column no_mobile_side_padding">
        	<div class="ui inverted segment mini five statistics no_mobile_side_padding" style="background: transparent;">
        		<div class="inverted orange statistic">
				  <div class="value">
				    {{ $university->views( $university->slug ) }}
				  </div>
				  <div class="label">
				    Views
				  </div>
				</div>

        		<a href="{{ url('/universities') }}/{{ $university->slug }}/posts" class="inverted olive statistic">
				  <div class="value">
				    <?php
	    				echo count(App\Post::where('university_id', '=', $university->id)->get()->all());
	    			?>
				  </div>
				  <div class="label">
				    Posts
				  </div>
				</a>

        		<a href="{{ url('/universities') }}/{{ $university->slug }}/news" class="inverted violet statistic">
				  <div class="value">
				    <?php
	    				echo count(App\News::where('university_id', '=', $university->id)->get()->all());
	    			?>
				  </div>
				  <div class="label">
				    News
				  </div>
				</a>

        		<div class="inverted grey statistic">
				  <div class="value">
				    <?php
	    				echo count(App\Announcement::where('university_id', '=', $university->id)->get()->all());
	    			?>
				  </div>
				  <div class="label">
				    Announcements
				  </div>
				</div>

        		<a href="{{ url('/universities') }}/{{ $university->slug }}/events" class="inverted teal statistic">
				  <div class="value">
				    <?php
	    				echo count(App\Event::where('university_id', '=', $university->id)->get()->all());
	    			?>
				  </div>
				  <div class="label">
				    Events
				  </div>
				</a>
        	</div>
		</div>
	</div>
</div>

<div class="ui tiny menu enlarge_mobile_icons" style="margin-top: 0;">
	<span data-tooltip="University"><a href="{{ url('/universities') }}/{{ $university->slug }}" class="item">
		<i class="university grey icon"></i>
		<span class="ui mobile_hide">University Home</span>
	</a></span>
	<span data-tooltip="University Posts"><a href="{{ url('/universities') }}/{{ $university->slug }}/posts" class="item">
		<i class="book grey icon"></i>
		<span class="ui mobile_hide">Posts</span>
	</a></span>
	<span data-tooltip="University Events"><a href="{{ url('/universities') }}/{{ $university->slug }}/events" class="item">
		<i class="ticket grey icon"></i>
		<span class="ui mobile_hide">Events</span>
	</a></span>
	<span data-tooltip="University News"><a href="{{ url('/universities') }}/{{ $university->slug }}/news" class="item">
		<i class="newspaper grey icon"></i>
		<span class="ui mobile_hide">News</span>
	</a></span>
	<span data-tooltip="University Announcements"><a href="{{ url('/universities') }}/{{ $university->slug }}/announcements" class="item">
		<i class="bullhorn grey icon"></i>
		<span class="ui mobile_hide">Announcements</span>
	</a></span>
	<span data-tooltip="University Members"><a href="{{ url('/universities') }}/{{ $university->slug }}/members" class="item">
		<i class="users grey icon"></i>
		<span class="ui mobile_hide">Members</span>
	</a></span>
</div>