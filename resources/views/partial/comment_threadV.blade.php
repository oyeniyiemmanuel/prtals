
<div class="label">
      <img alt="<?php $user = App\User::where('id', $comment->user['id'])->get()->first(); echo $user->name;?> {{ env('APP_URL') }}" src="<?php $user = App\User::where('id', $comment->user['id'])->get()->first(); echo $user->picture? $user->picture->url: '/media/img/user.jpg'; ?>">
    </div>
    <div class="content">
      <div class="summary">
        <a href="/profile/{{ $comment->user['username'] }}">{{ $comment->user['name'] }}</a> 
        <div class="date">
          {{ $comment->created_at->diffForHumans() }}
        </div>
      </div>
      <div class="extra text">
        {{ $comment->message }}
      </div>
      @unless(Auth::guest() || Auth::user()->verified == 0)
      <div class="meta">
        <input type="hidden" name="post_id" value="{{ $post->id }}">
        <input type="hidden" name="comment_id" value="{{ $comment->id }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    
        @if(count($comment->likes) > 0)
            
            <a class="like like_comment">
                @if($comment->isLiked)
                <i class="like large icon" ></i>
                @else
                <i class="empty heart large icon" ></i>
                @endif
               {{ count($comment->likes) }} like(s)
            </a>
        @else
        <a class="like like_comment">
            <i class="empty heart large icon" ></i>
        </a>
        @endif

        @if(Auth::user() && Auth::user()->id == $comment->user_id)
        <a class="trash">
          <i class="trash large delete_comment icon"></i>
        </a>
        @endif
      </div>
      @endunless
    </div>