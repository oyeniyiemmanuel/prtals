@unless(Auth::guest())
<?php
    $institution_type = Auth::user()->institution_type;
    $institution_type_plural = Auth::user()->institution_type_plural;
?>

<div class="ui container fluid">
    <div class="ui stackable grid">
        <div class="one wide column"></div>
        <div class="eleven wide column">

            <div class="ui inverted segment">
                <div class="ui four tiny statistics">
                  <a href="/posts/user/{{ Auth::user()->username }}" class="ui olive inverted statistic">
                    <div class="label mobile_hide">
                    Your  posts
                    </div>
                    <div class="value">
                      <i class="book icon"></i> 
                      <?php
                        echo count(App\Post::where('user_id', '=', Auth::id())->get()->all());
                      ?>
                    </div>
                  </a>
                  <a href="/news/user/{{ Auth::user()->username }}" class="ui red inverted statistic">
                    <div class="label mobile_hide">
                    Your  news
                    </div>
                    <div class="value">
                      <i class="newspaper icon"></i> 
                      <?php
                        echo count(App\News::where('user_id', '=', Auth::id())->get()->all());
                      ?>
                    </div>
                  </a>
                  <a href="/events/user/{{ Auth::user()->username }}" class="ui olive inverted statistic">
                    <div class="label mobile_hide">
                    Your  events
                    </div>
                    <div class="value">
                      <i class="ticket icon"></i> 
                      <?php
                        echo count(App\Event::where('user_id', '=', Auth::id())->get()->all());
                      ?>
                    </div>
                  </a>

                  <a href="/announcements/user/{{ Auth::user()->username }}" class="ui teal inverted statistic">
                    <div class="label mobile_hide">
                    Your  announcements
                    </div>
                    <div class="value">
                      <i class="bullhorn icon"></i> 
                      <?php
                        echo count(App\Announcement::where('user_id', '=', Auth::id())->get()->all());
                      ?>
                    </div>
                  </a>
                </div>
            </div>

            <div class="ui stackable grid">
                <div class="eight wide column">
                    
                    <div class="ui secondary segment">
                    <?php
                        $announcements = App\Announcement::where($institution_type.'_id', '=', Auth::user()->$institution_type->id)->latest()->get();
                    ?>
                    @if(empty($announcements->all()))
                        <div class="ui dividing header">
                            <div class="content">
                              {{ ucwords(Auth::user()->$institution_type->name) }} Announcements
                            </div>
                            <div class="sub header">0 in total</div>
                        </div>
                        <div class="ui block header">
                            <i class="info icon"></i>
                            <div class="content">
                              No Announcements 
                              <div class="sub header"></div>
                            </div>
                        </div>
                    @else
                        <div class="ui dividing header">
                            <div class="content">
                              {{ ucwords(Auth::user()->$institution_type->name) }} Announcements
                              <a href="/announcements/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="ui right floated mini label">
                                view all
                              </a>
                            </div>
                        </div>
                        @foreach($announcements->take(1) as $announcement_thread)
                            <div class="ui items">
                              @include('partial.announcement_threadV')
                            </div>
                        @endforeach
                    @endif
                    </div>
                    
                    <div class="ui secondary segment">
                    <?php
                        $news = App\News::latest()->get();
                    ?>
                    @if(empty($news->all()))
                        <div class="ui dividing header">
                            <div class="content">
                             General News
                            </div>
                            <div class="sub header">0 in total</div>
                        </div>
                        <div class="ui block header">
                            <i class="info icon"></i>
                            <div class="content">
                              No News 
                              <div class="sub header"></div>
                            </div>
                        </div>
                    @else
                        @include('bots.news',  [
                          'bot_status' => true
                          ,'bot_main_word' => ''
                          ,'bot_main_phrase' => 'Share News About Your School'
                          ,'bot_main_sub_phrase' => ''
                          ,'bot_catch_phrase' => 'Sign in to write news about your school'
                        ])
                        
                        @foreach($news->take(1) as $news_thread)
                            <div class="ui items">
                              @include('partial.news_threadV')
                            </div>
                        @endforeach
                    @endif
                    </div>
                    
                    <div class="ui segment">
                    <?php
                        $events = App\Event::latest()->get();
                    ?>
                    @if(empty($events->all()))
                        <div class="ui dividing header">
                            <div class="content">
                              General Events
                            </div>
                            <div class="sub header">0 in total</div>
                        </div>
                        <div class="ui block header">
                            <i class="info icon"></i>
                            <div class="content">
                              No Events 
                              <div class="sub header"></div>
                            </div>
                        </div>
                    @else
                        <div class="ui dividing header">
                            <div class="content">
                              General Events
                              <a href="/events" class="ui right floated mini label">
                                view all
                              </a>
                            </div>
                        </div>
                        @foreach($events->take(1) as $event_thread)

                              @include('partial.event_threadV')

                        @endforeach
                    @endif
                    </div>
                </div>

                <div class="eight wide column post_threads">
                <?php
                    $posts = App\Post::latest()->paginate(10);
                ?>
                    @if(empty($posts))

                        <div class="ui block header">
                            <i class="info icon"></i>
                            <div class="content">
                              No posts to display
                              <div class="sub header"></div>
                            </div>
                        </div>
                    @else
                        @include('bots.post', [
                          'bot_status' => true
                          ,'bot_main_word' => ''
                          ,'bot_main_phrase' => 'Write New Post'
                          ,'bot_main_sub_phrase' => ''
                          ,'bot_catch_phrase' => 'Sign in to create new post'
                        ])

                        @foreach($posts as $post)
                            <div class="ui fluid card post_thread">
                              @include('partial.half_post_threadV')
                            </div>
                        @endforeach
                        {{ $posts->links() }}
                    @endif
                </div>
            </div>
        </div>

        <div class="three wide column">
            @include('partial.aside_school_members_list')

            @include('partial.aside_users_list')

            @include('partial.aside_schools_list')
        </div>

        <div class="one wide column"></div>
    </div>
</div>
@endunless