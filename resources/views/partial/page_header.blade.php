<div class="ui center aligned container fluid page_header">
	<div class="ui stackable grid">
		<div class="one wide column no_padding"></div>
		<div class="fourteen wide column">
			<h1 class="title">{{ $title }} <span class="trend"><em>{{ $trend }}</em></span></h1>
		</div>
		<div class="one wide column no_padding"></div>
	</div>
</div>