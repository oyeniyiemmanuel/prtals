<div class="ui dividing header">
	Events
</div>
<div class="ui mini vertical pointing menu">
  <a href="/events" class="item">
    All
  </a>
  @unless(Auth::guest())
  <a href="/events/school/{{ Auth::user()->school->slug }}" class="item">
    My School
  </a>
  <a href="/events/user/{{ Auth::user()->username }}" class="item">
    Posted By Me
  </a>
  @endunless
</div>