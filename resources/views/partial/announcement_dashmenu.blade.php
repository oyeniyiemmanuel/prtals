<div class="ui dividing header">
	Announcements
</div>
<div class="ui mini vertical pointing menu">
  <a href="/announcements/create" class="item">
    Write
  </a>
  <a href="/announcements/school/{{ Auth::user()->school->slug }}" class="item">
    My School
  </a>
  <a href="/announcements/user/{{ Auth::user()->username }}" class="item">
    Posted By Me
  </a>
  <a href="{{ url('/announcements/create') }}" class="item">
    Create New
  </a>
</div>