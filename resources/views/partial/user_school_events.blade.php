<div class="ui container">
	<div class="ui stackable grid">
		<div class="three wide column">
			
			<div class="ui mini mobile_hide vertical pointing menu">
			  <a class="item" href="{{ url('/schools') }}/{{ Auth::user()->school->slug }}/events">
			    <div class="ui mini grey label">
			      <?php
			        echo count(App\event::where('school_id', '=', Auth::user()->school->id)->get());
			      ?>
			    </div>
			    School
			  </a>
			  <a class="active item" href="{{ url('/schools') }}/{{ Auth::user()->school->slug }}/events/mine">
			    <div class="ui mini grey label">
			      <?php
			        echo count(App\event::where('user_id', '=', Auth::id())->get());
			      ?>
			    </div>
			    My Events	
			  </a>
			  <div class="item">
			    <div class="ui icon input">
			      <input type="text" placeholder="Search events...">
			      <i class="search icon"></i>
			    </div>
			  </div>
			</div>
			
		</div>
		<div class="eight wide column event_threads">
			@if(empty($events))
				<div class="ui block centered header">
					<i class="book icon"></i>
					<div class="content">
					  {{ ucwords(Auth::user()->name) }}'s events
					</div>
				</div>

				<div class="ui block header">
					<i class="info icon"></i>
					<div class="content">
					  No events to display
					  <div class="sub header"></div>
					</div>
				</div>
			@else
				<div class="ui block centered header">
					<i class="book icon"></i>
					<div class="content">
					  {{ ucwords(Auth::user()->name) }}'s events
					  <div class="sub header"></div>
					</div>
				</div>
				@foreach($events as $event_thread)
					<div class="ui fluid card event_thread">
					  @include('partial.event_threadV')
					</div>
				@endforeach
				{{ $events->links() }}
			@endif
		</div>
		<div class="five wide column">
			@include('partial.aside_schools_list')

			@include('partial.aside_users_list')
		</div>
	</div>
</div>