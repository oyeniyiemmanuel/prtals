<div class="ui container">
	<div class="ui stackable grid">
		<div class="three wide column">
			
			<div class="ui mini mobile_hide vertical pointing menu">
			  <a class="item" href="{{ url('/universities') }}/{{ Auth::user()->university->slug }}/news">
			    <div class="ui mini grey label">
			      <?php
			        echo count(App\News::where('university_id', '=', Auth::user()->university->id)->get());
			      ?>
			    </div>
			    University
			  </a>
			  <a class="active item" href="{{ url('/universities') }}/{{ Auth::user()->university->slug }}/news/mine">
			    <div class="ui mini grey label">
			      <?php
			        echo count(App\News::where('user_id', '=', Auth::id())->get());
			      ?>
			    </div>
			    My News
			  </a>
			</div>
			
		</div>
		<div class="eight wide column news_threads">
			@if(empty($news->all()))
				<div class="ui block centered header">
					<i class="book icon"></i>
					<div class="content">
					  {{ ucwords(Auth::user()->name) }}'s news
					</div>
				</div>

				<div class="ui block header">
					<i class="info icon"></i>
					<div class="content">
					  No news to display
					  <div class="sub header"></div>
					</div>
				</div>
			@else
				<div class="ui block centered header">
					<i class="book icon"></i>
					<div class="content">
					  {{ ucwords(Auth::user()->name) }}'s news
					  <div class="sub header"></div>
					</div>
				</div>
				@foreach($news as $news_thread)
					<div class="ui fluid card new_thread">
					  @include('partial.news_threadV')
					</div>
				@endforeach
				{{ $news->links() }}
			@endif
		</div>
		<div class="five wide column">
			@include('partial.aside_schools_list')

			@include('partial.aside_users_list')
		</div>
	</div>
</div>