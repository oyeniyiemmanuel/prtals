
<div class="ui mini vertical pointing menu">
  <a class="item" href="{{ url('/posts') }}">
    All Posts
  </a>
  @unless(Auth::guest() || Auth::user()->verified == 0)
  <?php
      $institution_type = Auth::user()->institution_type;
      $institution_type_plural = Auth::user()->institution_type_plural;
  ?>
  <a class="item" href="{{ url('/posts/'.$institution_type.'/'.Auth::user()->$institution_type->slug) }}">
    <div class="ui mini label">
      <?php
        echo count(App\Post::where($institution_type.'_id', '=', Auth::user()->$institution_type->id)->get());
      ?>
    </div>
    My {{$institution_type}} Posts
  </a>
  <a class="item" href="{{ url('/posts/user/'.Auth::user()->username) }}">
    <div class="ui mini label">
      <?php
        echo count(App\Post::where('user_id', '=', Auth::id())->get());
      ?>
    </div>
    My Posts
  </a>
  @endunless
</div>

<div class="ui mini mobile_hide vertical menu mobile_hide">
    @if($post_categories)
      @foreach($post_categories as $category)
      <a class="item" href="{{ url('/posts/category/'.$category['category']) }}">
        <i class="tag grey icon"></i>
        {{ $category['category'] }}
      </a>
      @endforeach
    @endif
</div>

<div class="ui mini mobile_hide vertical menu mobile_hide">
    @if(Auth::guest())
      @if($subject_categories)
          @foreach($subject_categories as $category)
          <a class="item" href="{{ url('/posts/subject/'.$category) }}">
            <i class="circle {{ $category }} icon"></i>
            {{ $category }}
          </a>
          @endforeach
      @endif
      @if($field_categories)
          @foreach($field_categories as $category)
          <a class="item" href="{{ url('/posts/field/'.$category) }}">
            <i class="circle {{ $category }} icon"></i>
            {{ $category }}
          </a>
          @endforeach
      @endif
    @endif

    @if(Auth::user())
        @if(Auth::user()->school_id != null)
          @if($subject_categories)
              @foreach($subject_categories as $category)
              <a class="item" href="{{ url('/posts/subject/'.$category) }}">
                <i class="circle {{ $category }} icon"></i>
                {{ $category }}
              </a>
              @endforeach
          @endif
        @endif

        @if(Auth::user()->university_id != null)
          @if($field_categories)
        	  @foreach($field_categories as $category)
        	  <a class="item" href="{{ url('/posts/field/'.$category) }}">
                <i class="circle {{ $category }} icon"></i>
        	    {{ $category }}
        	  </a>
        	  @endforeach
          @endif
        @endif
    @endif
</div>