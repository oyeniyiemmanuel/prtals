<div class="ui container">
	<div class="ui stackable grid">
		<div class="three wide column">
			
			<div class="ui mini mobile_hide vertical pointing menu">
			  <a href="/posts/create" class="item">
			    Write
			  </a>
			  <a class="item" href="{{ url('/schools') }}/{{ Auth::user()->school->slug }}/posts">
			    <div class="ui mini grey label">
			      <?php
			        echo count(App\Post::where('school_id', '=', Auth::user()->school->id)->get());
			      ?>
			    </div>
			    School
			  </a>
			  <a class="active item" href="{{ url('/schools') }}/{{ Auth::user()->school->slug }}/posts/mine">
			    <div class="ui mini grey label">
			      <?php
			        echo count(App\Post::where('user_id', '=', Auth::id())->get());
			      ?>
			    </div>
			    My Posts
			  </a>
			</div>
			
		</div>
		<div class="eight wide column post_threads">
			@if(empty($posts->all()))
				<div class="ui block centered header">
					<i class="book icon"></i>
					<div class="content">
					  {{ ucwords(Auth::user()->name) }}'s Posts
					</div>
				</div>

				<div class="ui block header">
					<i class="info icon"></i>
					<div class="content">
					  No posts to display
					  <div class="sub header"></div>
					</div>
				</div>
			@else
				<div class="ui block centered header">
					<i class="book icon"></i>
					<div class="content">
					  {{ ucwords(Auth::user()->name) }}'s Posts
					  <div class="sub header"></div>
					</div>
				</div>
				@foreach($posts as $post)
					<div class="ui fluid card post_thread">
					  @include('partial.half_post_threadV')
					</div>
				@endforeach
				{{ $posts->links() }}
			@endif
		</div>
		<div class="five wide column">
			@include('partial.aside_schools_list')

			@include('partial.aside_users_list')
		</div>
	</div>
</div>