<?php
	if(Auth::user() && Auth::user()->verified == 1) {
		$role = Auth::user()->roles->first()->name;
		$model = "App\\".ucfirst($role);

		$aside_users = $model::where([
					['user_id', '!=', null]
					,['user_id', '!=', Auth::id()]
					])->inRandomOrder()->get();
	} else {
		$aside_users = App\Undergraduate::where([
					['user_id', '!=', null]
					])->inRandomOrder()->get();
	}
	
?>
<div class="ui list">

	<div class="ui icon dividing header">
		<div class="content">
			<div class="sub header">
				@if(Auth::user() && Auth::user()->verified == 1)
					@if($role == 'authority')
					Other school authorities 
					@else
					{{ ucfirst($role) }}s you may know
					@endif
				@else
					Students you may know
				@endif
			</div>
		</div>
	</div>
	
@if(!empty($aside_users->all()))
	@foreach($aside_users->take(10) as $suggested_user)
	<?php
	    $institution_type = $suggested_user->user->institution_type;
	    $institution_type_plural = $suggested_user->user->institution_type_plural;
	?>
	  <div class="item">
	    <img alt="{{ $suggested_user->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="<?php echo $suggested_user->user->picture? $suggested_user->user->picture->url: '/media/img/user.jpg'; ?>">
	    <div class="content">
	      <a href="/profile/{{ $suggested_user->user->username }}" class="">{{ ucwords($suggested_user->user->name) }}</a>
	      <div class="description"><i class="university icon"></i>
	      @if($suggested_user->school_id != null)
	      	<a href="/{{$institution_type_plural}}/<?php 
	      			$suggested_user_school = App\School::findOrFail($suggested_user->school_id);
		      		echo ucwords($suggested_user_school->slug);
		      	 ?>">
		      	<em>
		      	<?php
		      		if( strlen($suggested_user_school->name) < 30 ) {
		      			echo ucwords(strtolower($suggested_user_school->name));
		      		} else {
		      			echo ucwords(strtolower(substr($suggested_user_school->name, 0, 30)));
		      		}
		      	 ?> 
		      	</em>
	      	</a>
	      @endif
	      @if($suggested_user->university_id != null)
	      	<a href="/{{$institution_type_plural}}/<?php
	      			$suggested_user_university = App\University::findOrFail($suggested_user->university_id);
		      		echo ucwords($suggested_user_university->slug);
		      	 ?>">
		      	<em>
		      	<?php
		      		if( strlen($suggested_user_university->name) < 30 ) {
		      			echo ucwords(strtolower($suggested_user_university->name));
		      		} else {
		      			echo ucwords(strtolower(substr($suggested_user_university->name, 0, 27))).'...';
		      		}
		      	 ?> 
		      	</em>
	      	</a>
	      @endif
	      </div>
	    </div>
	  </div>
	@endforeach
@else
	<div class="ui block header" style="margin: 0;">
		<i class="info icon"></i>
		<div class="content">
		  No suggestions to display
		  <div class="sub header"></div>
		</div>
	</div>
@endif
</div>