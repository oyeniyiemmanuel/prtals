
<div class="infinite-scroll">
    <div class="ui stackable grid">
        <div class="sixteen wide column">
            <div class="homepage-grid post_threads">        
                        
                <?php
                    $posts = App\Post::latest()->paginate(16);
                ?>
                @if(empty($posts->all()))
                    <div class="ui block header">
                        <i class="info icon"></i>
                        <div class="content">
                          No Posts 
                          <div class="sub header"></div>
                        </div>
                    </div>
                @else
                        @foreach($posts->take(16) as $post)
                        <?php
                            $post_institution_type = $post->institution_type;
                            $post_institution_type_plural = $post->institution_type_plural;
                        ?>
                        
                        <div class="grid-item">
                        <div class="ui centered card green">
                        <div class="content">
                          <div class="meta">
                            <span class="right floated time" style="font-size: 0.7em; font-weight: bold;">{{ $post->created_at->diffForHumans() }}</span>
                            
                          </div>
                          <a href="/profile/{{ $post->user['username'] }}" data-tooltip="{{ $post->user['name'] }}" class="left floated author">
                            @if( $post->user->picture && !empty($post->user->picture->url) )
                            <img alt="{{ $post->user->name }} {{ env('APP_URL') }}" alt="{{ $post->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $post->user->picture->url }}">
                            @else
                            <img alt="{{ $post->user->name }} {{ env('APP_URL') }}" alt="{{ $post->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
                            @endif
                            
                             <span href="/profile/{{ $post->user['username'] }}"><?php $name = explode(' ', $post->user['name']); echo end($name); ?> </span>
                          </a>
                        </div>
                        @if($post_institution_type == 'school')
                          <a href="{{ env('APP_URL') }}/posts/{{ $post->slug }}" class="image">
                            <img alt="" src="/media/img/subjects/{{ $post->subject->name }}.jpg">
                          </a>
                        @endif
                        @if($post_institution_type == 'university')
                          <a href="{{ env('APP_URL') }}/posts/{{ $post->slug }}" class="image">
                            <img alt="" src="/media/img/subjects/{{ $post->field->name }}.jpg">
                          </a>
                        @endif
                          <div class="content">
                            <a href="/posts/{{ $post->slug }}" class=""><h3 style="letter-spacing: 0px;"><?php echo ucwords($post->title) ?></h3></a>
                            <div class="meta">
                              @if($post->$post_institution_type->logo && !empty($post->$post_institution_type->logo->url))
                              <a href="{{ env('APP_URL') }}/{{$post_institution_type_plural}}/{{ $post->$post_institution_type->slug }}"><img alt="{{ $post->$post_institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $post->$post_institution_type->logo->url }}"></a>
                              @else
                              <a href="{{ env('APP_URL') }}/{{$post_institution_type_plural}}/{{ $post->$post_institution_type->slug }}"><img alt="{{ $post->$post_institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/{{$post_institution_type}}_logo.jpg"></a>
                              @endif
                              <a href="{{ env('APP_URL') }}/{{$post_institution_type_plural}}/{{ $post->$post_institution_type->slug }}" class="date">{{ ucwords($post->$post_institution_type->name) }}, {{ ucfirst($post->$post_institution_type->state) }}</a>
                            </div>
                            <div class="description">
                              @if(strlen($post->message) < 150)
                                {!! nl2br(e($post->message)) !!}
                              @else
                                {!! nl2br(e(substr($post->message, 0, 150))) !!}...
                                <a href="{{ url('/posts') }}/{{$post->slug}}" >continue <i class="ui angle double right icon"></i></a>
                              @endif
                            </div>
                            @if($post_institution_type == 'school')
                            <a href="{{ url('/posts/subject') }}/{{ $post->subject->name }}" class="ui tiny basic ribbon label">
                                <span class="{{ $post->subject->name }}">{{ ucfirst($post->subject->name) }}</span> &nbsp;
                                <span class="{{ $post->subject->name }}"><em>{{ ucwords($post->category) }} </em></span>
                            </a>
                            @endif
                            @if($post_institution_type == 'university')
                            <a href="{{ url('/posts/field') }}/{{ $post->field->name }}" class="ui tiny basic ribbon label">
                                <span class="{{ $post->field->name }}">{{ ucfirst($post->field->name) }}</span> &nbsp;
                                <span class="{{ $post->field->name }}"><em>{{ ucwords($post->category) }} </em></span>
                            </a>
                            @endif
                          </div>
                          <div class="extra content">
                            <span class=" rates_and_comments">
                              @if(count($post->likes) > 0) 
                              <a  style="color: grey; font-size: 0.8em;" class="rates_modal_button"> {{ count($post->likes) }} rate(s) </a>
                              @else
                              <a style="color: grey; font-size: 0.8em;">not rated yet</a>
                              @endif

                              @include('modal.rates') |

                              @if(count($post->comments) > 0)
                              <a  style="color: grey; font-size: 0.8em;" class="comments_modal_button" > {{ count($post->comments) }} comment(s) </a> 
                              @else
                              <a style="color: grey; font-size: 0.8em;">no comments</a>
                              @endif

                              @include('modal.comments')
                             
                            </span>
                          </div>
                        </div>
                        </div>
                        @endforeach
                        
                    <br>

                @endif
            </div>
            
        </div>
    </div>
</div>
{{ $posts->links() }}

 
