<?php
    $news_institution_type = $news_thread->institution_type;
    $news_institution_type_plural = $news_thread->institution_type_plural;
?>
<div class="ui secondary segment">
	<div class="ui items">

	  <div class="item news_thread">
	    <div class="content">
	      <div class="header">{{ ucwords($news_thread->title) }}</div>
	      <div class="meta">
	      	<a href="/{{$news_institution_type_plural}}/{{ $news_thread->$news_institution_type->slug }}" class="">
	        	@if($news_thread->$news_institution_type->logo && !empty($news_thread->$news_institution_type->logo->url))
				<img alt="{{ $news_thread->$news_institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $news_thread->$news_institution_type->logo->url }}">
				@else
				<img alt="{{ $news_thread->$news_institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/{{$news_institution_type}}_logo.jpg">
				@endif 
	        	{{ ucwords($news_thread->$news_institution_type->name) }}, {{ $news_thread->$news_institution_type->state }}
	        </a>
	        <span class="right floated stay">{{ $news_thread->created_at->diffForHumans() }}</span>
	      </div>
	      <div class="description">
	        <p>
	        	{!! nl2br(e($news_thread->message)) !!}
	        </p>
	      </div>
	      <div class="extra">
	      	<span class="ui right floated">
	      		<em>reported by</em>
				<a href="/profile/{{ $news_thread->user->username }}" class="">
		        	<?php $name = explode(' ', $news_thread->user->name); echo ucfirst(end($name)); ?>
		        </a>	      		
	      	</span>

	        @if(Auth::id() == $news_thread->user_id)
	        <a href="/news/{{ $news_thread->slug }}/edit">edit</a>
	        <a class="delete_news">delete</a>
	        <input type="hidden" name="news_id" value="{{ $news_thread->id }}">
	        {{ csrf_field() }}
	        @endif
	      </div>
	    </div>
	  </div>
	</div>
</div>
