@if(Session::has('flash_msg'))
<div class="">

    <script type="text/javascript">
		
		swal({
			title: "<?php echo session('flash_msg.title') ?>",
			text: "<?php echo session('flash_msg.message') ?>",
			type: "{{ session('flash_msg.level') }}",
			timer: 1500,
			showConfirmButton: false,
			html: true
		});

	</script>
	{{ session::forget('flash_msg') }}
</div>
@endif

@if(Session::has('flash_msg_overlay'))
<div class="">

    <script type="text/javascript">
		
		swal({
			title: "<?php echo session('flash_msg_overlay.title') ?>",
			text: "<?php echo session('flash_msg_overlay.message') ?>",
			type: "{{ session('flash_msg_overlay.level') }}",
			confirmButtonText: 'ok',
			confirmButtonColor: 'teal',
			html: true
		});

	</script>
	{{ session::forget('flash_msg_overlay') }}
</div>
@endif