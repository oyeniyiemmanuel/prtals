<div class="ui dividing header">
	News
</div>
<div class="ui mini vertical pointing menu">
  <a href="{{ url('/news') }}" class="item">
    All
  </a>
  @unless(Auth::guest())
  <a href="/news/school/{{ Auth::user()->school->slug }}" class="item">
    My School
  </a>
  <a href="/news/user/{{ Auth::user()->username }}" class="item">
    Posted By Me
  </a>
  @endunless
</div>