	<div class="ui feed">
		<div class="event announcement_thread">
			<div class="label">
			  <i class="bullhorn icon"></i>
			</div>
			<div class="content">
			  <div class="summary">
			    {{ ucwords($announcement_thread->title) }}
			    <div class="date">
			      {{ $announcement_thread->created_at->diffForHumans() }}
			    </div>
			  </div>
			  <div class="extra text">
			  	{!! nl2br(e($announcement_thread->message)) !!}
			  </div>
			  <div class="meta">
			  	<span>
			  		<em>reported by</em>
			        <a href="/profile/{{ $announcement_thread->user->username }}" class="">
			        	<strong>
			        	<?php $name = explode(' ', $announcement_thread->user->name); echo ucfirst(end($name)); ?>
			        	</strong>
			        </a>
			    </span>

		        @if(Auth::id() == $announcement_thread->user_id)
		        <a href="/announcements/{{ $announcement_thread->slug }}/edit">edit</a>
		        <a class="delete_announcement">delete</a>
		        <input type="hidden" name="announcement_id" value="{{ $announcement_thread->id }}">
		        {{ csrf_field() }}
		        @endif
			  </div>
			</div>
		</div>
    </div>
