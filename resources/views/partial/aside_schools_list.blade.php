<?php
    if(Auth::user() && Auth::user()->verified == 1) {
        $institution_type = Auth::user()->institution_type;
        $institution_type_plural = Auth::user()->institution_type_plural;
        $model = "App\\".ucfirst($institution_type);
        $aside_schools = $model::where([
                                ['id', '!=', Auth::user()->$institution_type->id]
                                ])->inRandomOrder()->get();

    } else {
        $aside_schools = App\University::inRandomOrder()->get();
        $institution_type = 'university';
        $institution_type_plural = 'universities';
    }
  
?>
<div class="ui list">
  <div class="ui icon dividing header">
    <div class="content">
      <div class="sub header">
        {{ucfirst($institution_type_plural)}} you may know 
      </div>
    </div>
  </div>
@if(!empty($aside_schools->all()))
  @foreach($aside_schools->take(10) as $school)
    <div class="item">
      <img alt="{{ $school->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/{{$institution_type}}_logo.jpg">
      <div class="content">
        <a href="/{{$institution_type_plural}}/{{ $school->slug }}" class="">
          @if(strlen($school->name) < 30)
          <strong>{{ ucwords(strtolower($school->name)) }}</strong>
          @else
          <strong>{{ ucwords(strtolower(substr($school->name, 0, 27))) }}...</strong>
          @endif
        </a>
        <div class="description"><i class="marker icon"></i><em>{{ ucfirst(strtolower($school->state)) }}</em> </div>
      </div>
    </div>
  @endforeach
@else
  <div class="ui block header"  style="margin: 0;">
    <i class="info icon"></i>
    <div class="content">
      No suggestions to display
      <div class="sub header"></div>
    </div>
  </div>
@endif
</div>