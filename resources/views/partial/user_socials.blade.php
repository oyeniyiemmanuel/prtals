@if($user->$user_role)
	<span><i class="google plus big icon"></i> {{ $user->$user_role->g_plus }}</span> &nbsp; &nbsp;
	<span><i class="facebook square big icon"></i> {{ $user->$user_role->facebook }}</span> &nbsp; &nbsp;
	<span><i class="twitter big icon"></i> {{ $user->$user_role->twitter }}</span>
@else
	<span><i class="google plus big icon"></i> <em>Not Set</em></span> &nbsp; &nbsp;
	<span><i class="facebook square big icon"></i> <em>Not Set</em></span> &nbsp; &nbsp;
	<span><i class="twitter big icon"></i> <em>Not Set</em></span>
@endif
	
	@if(Auth::user() == $user)
	<div class="ui mini button right floated edit_trigger">
		<i class="write square icon"></i>Edit Socials
	</div>

	<form class="ui form add_socials" method="POST">
	<hr>
		{{ csrf_field() }}
	      <div class="ui left icon input">
	        <i class="google plus icon"></i>
	        <input type="text" name="g_plus" value="">
	      </div><br><br>
	      <div class="ui left icon input">
	        <i class="facebook square icon"></i>
	        <input type="text" name="facebook" value="">
	      </div><br><br>
	      <div class="ui left icon input">
	        <i class="twitter icon"></i>
	        <input type="text" name="twitter" value="">
	      </div><br><br>
	      <input type="hidden" name="user_id" value="{{ Auth::id() }}">
		<button class="ui mini submit teal button">
			Update
		</button>
	</form>
	@endif