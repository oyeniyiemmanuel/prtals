<?php
    $institution_type = $user->institution_type;
    $institution_type_plural = $user->institution_type_plural;
?>
<div class="ui stackable grid">
    <div class="one wide column"></div>

    <div class="four wide column user_profile">
    	<div class="ui centered special cards">
		  <div class="ui fluid card">
		    <div class="blurring dimmable image">
		      <div class="ui dimmer">
		        <div class="content">
		          <div class="center">
		          @if(Auth::user() == $user)
		          <form id="profile_pic_form" enctype="multipart/form-data">
		          	{{ csrf_field() }}
		          	<div class="ui mini icon input">
		          		<input id="choose_file" class="ui inverted buton" type="file" name="image">
		          	</div>
		          </form>
		          @else
				  <button class="ui inverted teal basic button">
				  	<i class="zoom icon"></i>
				  </button>
		          @endif
		          </div>
		        </div>
		      </div>
		      @if( $user->picture && !empty($user->picture->url))
		      <img alt="{{ $user->name }} {{ env('APP_URL') }}" src="{{ $user->picture->url }}">
		      @else
		      <img alt="{{ $user->name }} {{ env('APP_URL') }}" src="/media/img/user.jpg">
		      @endif
		    </div>
		    <div class="content">
		      <div class="right floated meta">
		        <span class="date">Joined {{ $user->created_at->diffForHumans() }}</span>
		      </div>
		      <a class="header">{{ $user->username }}</a>
		    </div>
		    <div class="extra content">
		      <a href="/{{$institution_type_plural}}/{{ $user->$institution_type->slug }}">
			      <p>
				        @if($user->$institution_type->logo && !empty($user->$institution_type->logo->url))
				      	<img alt="{{ $user->$institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" id="logo" src="{{ $user->$institution_type->logo->url }}">
				      	@else
				        <img alt="{{ $user->$institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/{{$institution_type}}_logo.jpg">
				        @endif
						<b>
				        {{ ucwords($user->$institution_type->name) }}
				        </b>
			      </p>
		      </a>
		      <div class="ui divider"></div>
		      <p class="">
		        <i class="male icon"></i>
		        @foreach($user->roles()->pluck('name') as $role)
		        {{ $role }}
		        @endforeach
		      </p>
		      <p>
		        <i class="at icon"></i>
		        {{ $user->email }}
		      </p>
		      @if($user->hasRole('student'))
		      <p>
		        <i class="object group icon"></i>
		        <?php echo $user->grade? strtoupper($user->grade->name).' '.strtoupper($user->student->wing->name): '<em>not set</em>'; ?>
		      </p>
		      <p>
		        <i class="tag icon"></i>
		        <?php echo $user->department? ucfirst($user->department->name): '<em>not set</em>'; ?>
		      </p>
		      @endif
		      <p class="">
		        <i class="heterosexual icon"></i>
		        <?php echo $user->$user_role->gender? $user->$user_role->gender: '<em>not set</em>'; ?>
		      </p>
		      @if($user->hasRole('student'))
		      <p>
		        <i class="circle {{ <?php echo $user->$user_role->best_subject? $user->$user_role->best_subject: ''; ?> }} icon"></i>
		        <?php echo $user->$user_role->best_subject? 'Key subject is '.ucfirst($user->$user_role->best_subject): '<em>not set</em>'; ?>
		      </p>
		      @endif
		      <p>
		        <i class="phone icon"></i>
		        <?php echo $user->$user_role->phone? $user->$user_role->phone: '<em>not set</em>'; ?>
		      </p>
		      <p>
		        <i class="birthday icon"></i>
		        <?php echo $user->$user_role->d_o_b? Carbon\Carbon::parse($user->$user_role->d_o_b)->format('d F'): '<em>not set</em>'; ?>
		      </p>
		      <p>
		        <i class="marker icon"></i>
		        <?php echo $user->$user_role->residential_state? 'Lives in '.ucfirst($user->$user_role->residential_state): '<em>not set</em>'; ?>
		      </p>
		    </div>
		    @if(Auth::user() == $user)
		    <a href="/profile/details/{{ $user->username }}/edit" class="ui bottom attached button">
		      <i class="write square icon"></i>
		      Edit Profile
		    </a>
		    @endif
		  </div>
		</div>
    </div>

    <div class="ten wide column">
    	<div class="ui secondary segment user_bio">
        	@include('partial.user_bio')
		</div>

		<div class="ui secondary segment user_socials">
			@include('partial.user_socials')
		</div>
		
			@if(1>2)
				 @if($user->hasRole('student'))

				 	@include('student.subjects_profile_view')

				 @elseif($user->hasRole('teacher'))
				 	@if(Auth::user() == $user)
				 	<a href="{{ url('/teacher/manage_subjects') }}" class="ui mini teal button pointing below label">
						manage subjects
					</a>
					@endif

					@include('teacher.subjects_profile_view')

				 @endif
			@endif
		
		<div class="ui stackable grid">
			<div class="nine wide column">
				<div class="post_threads">
				@if(count($user->posts) == 0)
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No posts to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
					@foreach($user->posts()->latest()->get() as $post)
						<div class="ui fluid card post_thread">
						  @include('partial.half_post_threadV')
						</div>
					@endforeach
				@endif
				</div>
			</div>
			<div class="seven wide column">
				<div class="ui secondary segment">
					<?php
						$institution_type = $user->institution_type;
					    $institution_type_plural = $user->institution_type_plural;
						$role = $user->roles->first()->name;
						$model = "App\\".ucfirst($role);

						$aside_members = $model::where([
									[$institution_type.'_id', '=', $user->$institution_type->id]
									,['user_id', '!=', null]
									,['user_id', '!=', $user->id]
									])->inRandomOrder()->get();
					?>

					<div class="ui segment">
						<div class="ui dividing header">
							<div class="content">
								<div class="sub header">
								Other {{ ucwords($user->$institution_type->name) }} 

								@if($role == 'authority')
								authorities 
								@else
								{{ ucfirst($role) }}s 
								@endif

								</div>
							</div>
						</div>
						@if(!empty($aside_members->all()))
							@foreach($aside_members->take(12) as $member)
								<a data-tooltip="{{ $member->user->name }}" href="/profile/{{ $member->user->username }}">
									@if($member->user->picture && !empty($member->user->picture->url))
									<img alt="{{ $member->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $member->user->picture->url }}">
									@else
									<img alt="{{ $member->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
									@endif
								</a>
							@endforeach
						@else
						    <div class="ui block header">
								<i class="info icon"></i>
								<div class="content">
									None
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>		
    </div>

    <div class="one wide column"></div>
</div>