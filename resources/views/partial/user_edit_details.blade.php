<?php
    $institution_type = $user->institution_type;
    $institution_type_plural = $user->institution_type_plural;
?>
<div class="ui stackable grid">
    <div class="one wide column"></div>

    <div class="four wide column mobile_hide user_profile">
    	<div class="ui centered special cards">
		  <div class="ui fluid card">
		    <div class="blurring dimmable image">
		      <div class="ui dimmer">
		        <div class="content">
		          <div class="center">
		          <form id="profile_pic_form" enctype="multipart/form-data">
		          	{{ csrf_field() }}
		          	<div class="ui mini icon input">
		          		<input id="choose_file" class="ui inverted buton" type="file" name="image">
		          	</div>
		          </form>
		          </div>
		        </div>
		      </div>
		      @if( Auth::user()->picture && !empty(Auth::user()->picture->url))
		      <img alt="{{ Auth::user()->name }} {{ env('APP_URL') }}" src="{{ Auth::user()->picture->url }}">
		      @else
		      <img alt="{{ Auth::user()->name }} {{ env('APP_URL') }}" src="/media/img/user.jpg">
		      @endif
		    </div>
		    <div class="content">
		      <div class="right floated meta">
		        <span class="date">Joined {{ Auth::user()->created_at->diffForHumans() }}</span>
		      </div>
		      <a class="header">{{ Auth::user()->username }}</a>
		    </div>
		    <div class="extra content">
		      <a href="/{{$institution_type_plural}}/{{ $user->$institution_type->slug }}">
			      <p>
				        @if($user->$institution_type->logo && !empty($user->$institution_type->logo->url))
				      	<img alt="{{ $user->$institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" id="logo" src="{{ $user->$institution_type->logo->url }}">
				      	@else
				        <img alt="{{ $user->$institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/{{ $institution_type }}_logo.jpg">
				        @endif
						<b>
				        {{ ucwords($user->$institution_type->name) }}
				        </b>
			      </p>
		      </a>
		      <div class="ui divider"></div>
		      <p class="">
		        <i class="male icon"></i>
		        @foreach(Auth::user()->roles()->pluck('name') as $role)
		        {{ $role }}
		        @endforeach
		      </p>
		      <p>
		        <i class="at icon"></i>
		        {{ Auth::user()->email }}
		      </p>
		      @if(Auth::user()->hasRole('student'))
		      <p>
		        <i class="object group icon"></i>
		        <?php echo Auth::user()->grade? strtoupper(Auth::user()->grade->name).' '.strtoupper(Auth::user()->student->wing->name): '<em>not set</em>'; ?>
		      </p>
		      <p>
		        <i class="tag icon"></i>
		        <?php echo Auth::user()->department? ucfirst(Auth::user()->department->name): '<em>not set</em>'; ?>
		      </p>
		      @endif
		      <p class="">
		        <i class="heterosexual icon"></i>
		        <?php echo Auth::user()->$user_role->gender? Auth::user()->$user_role->gender: '<em>not set</em>'; ?>
		      </p>
		      @if(Auth::user()->hasRole('student'))
		      <p>
		        <i class="circle thin icon"></i>
		        <?php echo Auth::user()->$user_role->best_subject? 'Key subject is '.ucfirst(Auth::user()->$user_role->best_subject): '<em>not set</em>'; ?>
		      </p>
		      @endif
		      <p>
		        <i class="phone icon"></i>
		        <?php echo Auth::user()->$user_role->phone? Auth::user()->$user_role->phone: '<em>not set</em>'; ?>
		      </p>
		      <p>
		        <i class="birthday icon"></i>
		        <?php echo Auth::user()->$user_role->d_o_b? Carbon\Carbon::parse(Auth::user()->$user_role->d_o_b)->format('d F'): '<em>not set</em>'; ?>
		      </p>
		      <p>
		        <i class="marker icon"></i>
		        <?php echo Auth::user()->$user_role->residential_state? 'Lives in '.ucfirst(Auth::user()->$user_role->residential_state): '<em>not set</em>'; ?>
		      </p>
		    </div>
		  </div>
		</div>
    </div>

    <div class="ten wide column edit_user_details">
    	<div class="ui secondary segment">
    		<a href="{{ url('/profile') }}"><i class="arrow left icon"></i><strong>back to profile </strong></a>

			<form class="ui small form" method="POST" action="/profile/details/{{ Auth::user()->username }}/edit">
	          {{ csrf_field() }}
	          <h4 class="ui dividing header">Personal details</h4>
	          <div class="two fields">
	            <div class="eight wide field">
	              <div class="ui left icon input">
	                <i class="user icon"></i>
	                <input type="text" name="name" placeholder="Full name" value="{{ $user->name }}" required>
	              </div>
	            </div>
	            <div class="eight wide field">
	              <div class="ui left icon input">
	                <i class="hand peace icon"></i>
	                <input id="username_field" type="text" name="username" placeholder="username" value="{{ $user->username }}" required>
	              </div>
	            </div>
	           </div>

	           <div class="four fields">
				<div class="four wide field">
					<select name="gender" class="ui fluid floating dropdown" required>
						<option value="">Gender</option>
						<option<?php echo (Auth::user()->$user_role) && (Auth::user()->$user_role->gender == 'male')? ' selected': ''; ?> value="male">Male</option>
						<option<?php echo (Auth::user()->$user_role) && (Auth::user()->$user_role->gender == 'female')? ' selected': ''; ?> value="female">Female</option>
					</select>
				</div>
				<div class="six wide field">
	              <div class="ui left icon input">
	                <i class="phone icon"></i>
	                <input type="text" name="phone" placeholder="Phone" value="<?php echo Auth::user()->$user_role? Auth::user()->$user_role->phone: ''; ?>" required>
	              </div>
	            </div>
	            <div class="six wide field">
					<select name="residential_state" class="ui fluid floating dropdown" required>
					    <option value=""> where do you stay? </option>
					@foreach($states as $state)
					 	<option <?php echo (Auth::user()->$user_role) && (Auth::user()->$user_role->residential_state == strtolower($state))? ' selected': ''; ?> value="{{ strtolower($state) }}">{{ $state }}</option>
					@endforeach
					</select>
	            </div>
	           </div>

	           <div class="three fields">
	           	<div class="five wide field">
	           		<label>State of origin</label>
					<select name="state_of_origin" class="ui fluid floating dropdown" required>
					    <option value=""> Your State </option>
					@foreach($states as $state)
					 	<option<?php echo (Auth::user()->$user_role) && (Auth::user()->$user_role->state_of_origin == strtolower($state))? ' selected': ''; ?> value="{{ strtolower($state) }}">{{ $state }}</option>
					@endforeach
					</select>
	            </div>
				<div class="five wide field">
					<label>Birthday</label>
					<div class="ui left icon input">
						<i class="birthday icon"></i>
						<input type="date" name="date_of_birth" placeholder="YYYY-MM-DD e.g 2003-03-30" required="required" value="<?php echo Auth::user()->$user_role? Auth::user()->$user_role->d_o_b: ''; ?>" max="" >
					</div>
	            </div>
				<div class="six wide field">
					<label>Local Gov</label>
					<div class="ui left icon input">
						<i class="map pin icon"></i>
						<input type="text" name="local_gov" placeholder="local govenment" required="required" value="<?php echo Auth::user()->$user_role? Auth::user()->$user_role->local_gov: ''; ?>">
					</div>
	            </div>
	           </div>
				
			@if(Auth::user()->hasRole('student'))
	           <h4 class="ui dividing header">Education</h4>
	           <div class="three fields">
	           	<div class="five wide field fields">
	           		<div class="ten wide field">
	           			<select name="grade" class="ui fluid floating dropdown" required>
							<option value="">Grade</option>
							@foreach($grades as $grade)
							<option <?php echo (Auth::user()->$user_role) && (Auth::user()->grade) && (Auth::user()->grade->name == $grade)? ' selected': ''; ?> value="{{ $grade }}">{{ strtoupper($grade) }}</option>
							@endforeach
						</select>
	           		</div>
	           		<div class="six wide field">
	           			<select name="wing" class="ui fluid floating dropdown" required>
							@foreach($alphabets as $alphabet)
							<option<?php echo (Auth::user()->$user_role) && (Auth::user()->student->wing) && (Auth::user()->student->wing->name == $alphabet)? ' selected': ''; ?> value="{{ $alphabet }}">{{ strtoupper($alphabet) }}</option>
							@endforeach
						</select>
	           		</div>
					
				</div>
				<div class="five wide field">
					<select name="department" class="ui fluid floating dropdown" required>
						<option value="">Department</option>
						@foreach($departments as $department)
						<option <?php echo (Auth::user()->$user_role) && (Auth::user()->department) && (Auth::user()->department->name == $department)? ' selected': ''; ?> value="{{ $department }}">{{ strtoupper($department) }}</option>
						@endforeach
					</select>
				</div>
				<div class="six wide field">
					<select name="best_subject" class="ui fluid floating dropdown" required>
					<option value="">Your Best Subject</option>
					@foreach($subject_categories as $category)
					<option <?php echo (Auth::user()->$user_role) && (Auth::user()->$user_role->best_subject == $category)? ' selected': ''; ?> value="{{ $category }}">{{ $category }}</option>
					@endforeach
					</select>
				</div>
	           </div>
	        @endif

	            <button type="submit" class="ui mini teal submit button">Update</button>
	          
	        </form>
    	</div>
    </div>

    <div class="one wide column"></div>
</div>