<?php
            $events = App\Event::latest()->get();
        ?>
        @if(empty($events->all()))
            <a href="/events"><em>School Events</em></a>

            <div class="ui block header">
                <i class="info icon"></i>
                <div class="content">
                  No Events 
                  <div class="sub header"></div>
                </div>
            </div>
        @else
            @foreach($events->take(3) as $event_thread)
                <a href="/events"><em>School Events</em></a>

                <div class="ui fluid card">
                  <a href="{{ env('APP_URL') }}/schools/{{ $event_thread->school->slug }}" class="image">
                    @if($event_thread->school->cover && !empty($event_thread->school->cover->url))
                    <img alt="{{ $event_thread->school->name }} {{ env('APP_URL') }}" src="{{ $event_thread->school->cover->url }}">
                    @else
                    <img alt="{{ $event_thread->school->name }} {{ env('APP_URL') }}" src="/media/img/school_cover.jpg">
                    @endif
                  </a>
                  <div class="content">
                    <a href="/events/{{ $event_thread->slug }}" class="header">{{ ucwords($event_thread->title) }}</a>
                    <div class="meta">
                      @if($event_thread->school->logo && !empty($event_thread->school->logo->url))
                      <a href="{{ env('APP_URL') }}/schools/{{ $event_thread->school->slug }}"><img alt="{{ $event_thread->school->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $event_thread->school->logo->url }}"></a>
                      @else
                      <a href="{{ env('APP_URL') }}/schools/{{ $event_thread->school->slug }}"><img alt="{{ $event_thread->school->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/school_logo.jpg"></a>
                      @endif
                      <a href="{{ env('APP_URL') }}/schools/{{ $event_thread->school->slug }}" class="date">{{ ucwords($event_thread->school->name) }}, {{ $event_thread->school->state }}</a>
                    </div>
                    <div class="description">
                      {{ $event_thread->message }}
                    </div>
                    <div class="extra images event_pictures">
                    @if($event_thread->pictures && !empty($event_thread->pictures->all()))
                        @foreach($event_thread->pictures as $picture)
                        <a><img alt="{{ $event_thread->title }} {{ env('APP_URL') }}" class="ui tiny image" src="{{ $picture->url }}"></a>
                        @endforeach
                    @else
                      <a><img alt="{{ $event_thread->title }} {{ env('APP_URL') }}" class="ui tiny image" src="/media/img/1.jpg"></a>       
                    @endif
                    </div>
                  </div>
                  <div class="extra content">
                    <span>
                      {{ $event_thread->created_at->diffForHumans() }}
                    </span>
                    <a href="/profile/{{ $event_thread->user->username }}" class="ui right floated">
                        @if($event_thread->user->picture && !empty($event_thread->user->picture->url))
                        <img alt="{{ $event_thread->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $event_thread->user->picture->url }}">
                        @else
                        <img alt="{{ $event_thread->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
                        @endif
                        <span class="">
                            <?php $name = explode(' ', $event_thread->user->name); echo ucfirst(end($name)); ?>
                        </span>                
                    </a>
                  </div>
                </div>

            @endforeach
        @endif