<div class="school_header">
	<div class="ui stackable grid">
		<div class="two wide column no_padding"></div>
		<a href="{{ url('/schools') }}/{{ $school->slug }}" class="eight wide column school_link">
			@if($school->logo && !empty($school->logo->url))
	      	<div class="ui mobile_hide"><img alt="{{ $school->name }} {{ env('APP_URL') }}" class="ui tiny left floated bordered circular image" id="logo" src="{{ $school->logo->url }}"></div>
	      	@else
	        <div class="ui mobile_hide"><img alt="{{ $school->name }} {{ env('APP_URL') }}" class="ui tiny left floated bordered circular image" src="/media/img/school_logo.jpg"></div>
	        @endif

	        <h1 class="name">{{ ucwords($school->name) }}</h1>
	        <p class="address"><em>{{ ucwords($school->address) }}</em>, <b>{{ ucfirst($school->state) }}</b></p>
		</a>
		<div class="six wide column no_mobile_side_padding">
        	<div class="ui inverted segment mini five statistics no_mobile_side_padding" style="background: transparent;">
        		<div class="inverted orange statistic">
				  <div class="value">
				    {{ $school->views( $school->slug ) }}
				  </div>
				  <div class="label">
				    Views
				  </div>
				</div>

        		<a href="{{ url('/schools') }}/{{ $school->slug }}/posts" class="inverted olive statistic">
				  <div class="value">
				    <?php
	    				echo count(App\Post::where('school_id', '=', $school->id)->get()->all());
	    			?>
				  </div>
				  <div class="label">
				    Posts
				  </div>
				</a>

        		<a href="{{ url('/schools') }}/{{ $school->slug }}/news" class="inverted violet statistic">
				  <div class="value">
				    <?php
	    				echo count(App\News::where('school_id', '=', $school->id)->get()->all());
	    			?>
				  </div>
				  <div class="label">
				    News
				  </div>
				</a>

        		<div class="inverted grey statistic">
				  <div class="value">
				    <?php
	    				echo count(App\Announcement::where('school_id', '=', $school->id)->get()->all());
	    			?>
				  </div>
				  <div class="label">
				    Announcements
				  </div>
				</div>

        		<a href="{{ url('/schools') }}/{{ $school->slug }}/events" class="inverted teal statistic">
				  <div class="value">
				    <?php
	    				echo count(App\Event::where('school_id', '=', $school->id)->get()->all());
	    			?>
				  </div>
				  <div class="label">
				    Events
				  </div>
				</a>
        	</div>
		</div>
	</div>
</div>

<div class="ui tiny menu enlarge_mobile_icons" style="margin-top: 0;">
	<span data-tooltip="School"><a href="{{ url('/schools') }}/{{ $school->slug }}" class="item">
		<i class="university grey icon"></i>
		<span class="ui mobile_hide">School Home</span>
	</a></span>
	<span data-tooltip="School Posts"><a href="{{ url('/schools') }}/{{ $school->slug }}/posts" class="item">
		<i class="book grey icon"></i>
		<span class="ui mobile_hide">Posts</span>
	</a></span>
	<span data-tooltip="School Events"><a href="{{ url('/schools') }}/{{ $school->slug }}/events" class="item">
		<i class="ticket grey icon"></i>
		<span class="ui mobile_hide">Events</span>
	</a></span>
	<span data-tooltip="School News"><a href="{{ url('/schools') }}/{{ $school->slug }}/news" class="item">
		<i class="newspaper grey icon"></i>
		<span class="ui mobile_hide">News</span>
	</a></span>
	<span data-tooltip="School Members"><a href="{{ url('/schools') }}/{{ $school->slug }}/members" class="item">
		<i class="users grey icon"></i>
		<span class="ui mobile_hide">Members</span>
	</a></span>
</div>