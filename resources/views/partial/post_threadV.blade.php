<?php
    $post_institution_type = $post->institution_type;
    $post_institution_type_plural = $post->institution_type_plural;
?>
<div class="content" style="margin-bottom: 2px;">
	<div class="meta">
	  <span class="right floated time" style="font-size: 0.7em; font-weight: bold;">{{ $post->created_at->diffForHumans() }}</span>
	  
	</div>
	<a href="/profile/{{ $post->user['username'] }}" data-tooltip="{{ $post->user['name'] }}" class="left floated author">
	  @if( $post->user->picture && !empty($post->user->picture->url) )
	  <img alt="{{ $post->user->name }} {{ env('APP_URL') }}" alt="{{ $post->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $post->user->picture->url }}">
	  @else
	  <img alt="{{ $post->user->name }} {{ env('APP_URL') }}" alt="{{ $post->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
	  @endif
	  
	   <span href="/profile/{{ $post->user['username'] }}"><?php $name = explode(' ', $post->user['name']); echo end($name); ?> </span>
	</a>
</div>
@if($post_institution_type == 'school')
  <a href="{{ env('APP_URL') }}/posts/{{ $post->slug }}" class="image">
    <img alt="" src="/media/img/subjects/{{ $post->subject->name }}.jpg">
  </a>
@endif
@if($post_institution_type == 'university')
  <a href="{{ env('APP_URL') }}/posts/{{ $post->slug }}" class="image">
    <img alt="" src="/media/img/subjects/{{ $post->field->name }}.jpg">
  </a>
@endif
<div class="content">
	<a href="/posts/{{ $post->slug }}" class=""><h3 style="letter-spacing: 0px; font-family: 'lato-light'; text-align: center;"><?php echo ucwords($post->title) ?></h3></a>
	<span class="category">
	  	<a href="{{ url('/posts') }}/category/{{ $post->category }}" class=""><em>{{ ucfirst($post->category) }}</em></a>
	  </span>
	<div class="description">
	  <p>
	  	{!! nl2br(e($post->message)) !!}
	  </p>
	</div>

	@if($post->school_id != null)
	<a style="margin-bottom: 2.8%;" href="{{ url('/posts/subject/'.$post->subject['name']) }}" class="ui mini basic ribbon label"><span class="{{ $post->subject['name'] }}">{{ ucfirst($post->subject['name']) }}</span></a>
	@endif
	@if($post->university_id != null)
	<a style="margin-bottom: 2.8%;" href="{{ url('/posts/field/'.$post->field['name']) }}" class="ui mini basic ribbon label"><span class="{{ $post->field['name'] }}">{{ ucfirst($post->field['name']) }}</span></a>
	@endif

	<span class="right floated rates_and_comments">
		&nbsp; &nbsp; &nbsp; &nbsp;
	  @if(count($post->likes) > 0) 
	  <a class="rates_modal_button"> {{ count($post->likes) }} like(s) </a>
	  @else
	  <a style="color: grey">no likes yet</a>
	  @endif

	  @include('modal.rates') |

	  @if(count($post->comments) > 0)
	  <a class="comments_modal_button" > {{ count($post->comments) }} comment(s) </a> 
	  @else
	  <a style="color: grey">no comments</a>
	  @endif

	  @include('modal.comments')
	  &nbsp;
	</span>
	<br>
</div>
<div class="" style="background: #fff;">
	
	
	@unless(Auth::guest() || Auth::user()->verified == 0)
	<div class="ui icon basic buttons right floated check">
		<input type="hidden" name="post_id" value="{{ $post->id }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
		@if($post->isLiked)
		<div class="ui button post_btn rate_post" data-tooltip="unlike" data-variation="mini"><i class=" like red icon"></i> <span class="word">unlike</span></div>
		@else
		<div class="ui button post_btn rate_post" data-tooltip="like" data-variation="mini"><i class=" empty heart icon"></i> <span class="word">like</span></div>
		@endif
		<div class="ui button post_btn add_comment" data-tooltip="comment" data-variation="mini"><i class=" comment grey outline icon"></i> <span class="word">comment</span></div>
		<div class="ui button post_share_btn dropdown">
			<i class=" share alternate icon"></i> <span class="word">share</span>
			<div class="menu">
            	<a href="https://plus.google.com/share?url={{ urlencode(env('APP_URL').'/posts/'.$post->slug) }}" target="_blank" class="ui button item"><i class="google plus icon"></i></a>
            	@if($post->school_id != null)
            	<a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(env('APP_URL').'/posts/'.$post->slug) }}&picture=https://www.prtals.com/media/img/subjects/{{ $post->subject->name }}.jpg&title={{ $post->title }}&description={{$post->message}}" target="_blank" class="ui button item"><i class="facebook f icon"></i></a>
            	@endif
            	@if($post->university_id != null)
            	<a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(env('APP_URL').'/posts/'.$post->slug) }}&picture=https://www.prtals.com/media/img/subjects/{{ $post->field->name }}.jpg&title={{ $post->title }}&description={{$post->message}}&quote={{$post->user->name}}" target="_blank" class="ui button item"><i class="facebook f icon"></i></a>
            	@endif
            	<a href="https://twitter.com/intent/tweet?url={{ urlencode(env('APP_URL').'/posts/'.$post->slug) }}" target="_blank" class="ui button item"><i class="twitter icon"></i></a>
			</div>
		</div>
		@if( 1> 2 && Auth::user() && Auth::user()->id == $post->user['id'])
		<a href="{{ url('/posts') }}/{{ $post->slug }}/edit" class="ui button post_btn mobile_hide" data-tooltip="edit" data-variation="mini"><i class=" write grey icon"></i> <span class="word">edit</span></a>
		<div class="ui button post_btn delete_post mobile_hide" data-tooltip="delete" data-variation="mini"><i class=" trash grey icon"></i> <span class="word">remove</span></div>
		@endif
	</div>
	@endunless

	<div class="ui comments post_comments">
	  <div class="comment" style="padding: 2%;">
	    <a class="avatar">
	      @if( Auth::user() && Auth::user()->picture && !empty(Auth::user()->picture->url) )
		  <img alt="<?php echo Auth::user()? Auth::user()->name: ''; ?> {{ env('APP_URL') }}" src="{{ Auth::user()->picture->url }}">
		  @else
		  <img alt="<?php echo Auth::user()? Auth::user()->name: ''; ?> {{ env('APP_URL') }}" src="/media/img/user.jpg">
		  @endif
	    </a>
	    <div class="content">
	    @unless(Auth::guest())
	      <a class="author">{{ Auth::user()->name }}</a>
	      <form class="ui form add_comment" method="POST">
	      	{{ csrf_field() }}
	        <div class="field">
	          <textarea rows="2" name="message" required></textarea>
	        </div>
	        <input type="hidden" name="post_id" value="{{ $post->id }}">
	        <button class="ui mini submit teal button">
	        	Add Comment
	        </button>
	      </form>
	    @endunless
	    </div>
	  </div>
	</div>
</div>