<?php
            $news = App\News::latest()->get();
        ?>
        @if(empty($news->all()))
            <a href="/news"><em>School News</em></a>

            <div class="ui block header">
                <i class="info icon"></i>
                <div class="content">
                  No News 
                  <div class="sub header"></div>
                </div>
            </div>
        @else
            @foreach($news->take(3) as $news_thread)
                <a href="/news"><em>School News</em></a>

                <div class="ui fluid card">
                  <a href="{{ env('APP_URL') }}/schools/{{ $news_thread->school->slug }}" class="image">
                    @if($news_thread->school->cover && !empty($news_thread->school->cover->url))
                    <img alt="{{ $news_thread->title }} {{ env('APP_URL') }}" src="{{ $news_thread->school->cover->url }}">
                    @else
                    <img alt="{{ $news_thread->title }} {{ env('APP_URL') }}" src="/media/img/school_cover.jpg">
                    @endif
                  </a>
                  <div class="content">
                    <a href="/news/{{ $news_thread->slug }}" class="header">{{ ucwords($news_thread->title) }}</a>
                    <div class="meta">
                      @if($news_thread->school->logo && !empty($news_thread->school->logo->url))
                      <a href="{{ env('APP_URL') }}/schools/{{ $news_thread->school->slug }}"><img alt="{{ $news_thread->school->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $news_thread->school->logo->url }}"></a>
                      @else
                      <a href="{{ env('APP_URL') }}/schools/{{ $news_thread->school->slug }}"><img alt="{{ $news_thread->school->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/school_logo.jpg"></a>
                      @endif
                      <a href="{{ env('APP_URL') }}/schools/{{ $news_thread->school->slug }}" class="date">{{ ucwords($news_thread->school->name) }}, {{ $news_thread->school->state }}</a>
                    </div>
                    <div class="description">
                      {{ $news_thread->message }}
                    </div>
                  </div>
                  <div class="extra content">
                    <span>
                      {{ $news_thread->created_at->diffForHumans() }}
                    </span>
                    <a href="/profile/{{ $news_thread->user->username }}" class="ui right floated">
                        @if($news_thread->user->picture && !empty($news_thread->user->picture->url))
                        <img alt="{{ $news_thread->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $news_thread->user->picture->url }}">
                        @else
                        <img alt="{{ $news_thread->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
                        @endif
                        <span class="">
                            <?php $name = explode(' ', $news_thread->user->name); echo ucfirst(end($name)); ?>
                        </span>                
                    </a>
                  </div>
                </div>
            @endforeach
        @endif