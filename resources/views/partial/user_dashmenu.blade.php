@unless(Auth::guest() || Auth::user()->verified == 0)
<?php
    $institution_type = Auth::user()->institution_type;
    $institution_type_plural = Auth::user()->institution_type_plural;
?>
<div class="ui container fluid dashmenu">
    <div class="ui tiny menu">        
        <div class="ui pointing dropdown item">
            @if(Auth::user()->$institution_type->logo && !empty(Auth::user()->$institution_type->logo->url))
            <img alt="{{ Auth::user()->$institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ Auth::user()->$institution_type->logo->url }}">
            @else
            <i class="university icon"></i>
            @endif
            <span class="ui mobile_hide">{{ ucwords(Auth::user()->$institution_type->name) }}</span>
            <i class="dropdown icon"></i>
            <div class="menu">
                <a href="/{{ $institution_type_plural }}/{{ Auth::user()->$institution_type->slug }}" class="item">School Page</a>
                <a href="/{{ $institution_type_plural }}/{{ Auth::user()->$institution_type->slug }}/members" class="item">Members</a>
                <a href="/{{ $institution_type_plural }}/{{ Auth::user()->$institution_type->slug }}/posts" class="item">Posts</a>
                <a href="/{{ $institution_type_plural }}/{{ Auth::user()->$institution_type->slug }}/news" class="item">News</a>
                <a href="/{{ $institution_type_plural }}/{{ Auth::user()->$institution_type->slug }}/events" class="item">Events</a>
                <a href="/announcements" class="item">Announcements</a>
            </div>
        </div>
        
        <div class="ui pointing dropdown item">
            <i class="write grey icon"></i>
            <span class="ui mobile_hide">Write</span>
            <i class="dropdown icon"></i>
          <div class="menu">
              <a href="{{ url('/posts/create') }}" class="item">
                Post
              </a>
              <a href="{{ url('/news/create') }}" class="item">
                News
              </a>
              <a  href="{{ url('/announcements/create') }}" class="item">
                Announcement
              </a>
              <a  href="{{ url('/events/create') }}" class="item">
                Event
              </a>
          </div>
        </div>
        
        @if(Auth::user()->hasRole('prtals-admin'))
        <a href="{{ url('/add_new_school') }}" class="item">
            <i class="plus grey icon"></i>
            <i class="university grey icon"></i>
        </a>
        <a href="{{ url('/add_new_subject') }}" class="item">
            <i class="plus grey icon"></i>
            <i class="book grey icon"></i>
        </a>
        @endif

        <div class="ui pointing right dropdown item mark_as_read all_notifications" data-token="{{ csrf_token() }}" id="{{ Auth::id() }}">
            <i class="alarm outline grey icon"></i> <i class="dropdown icon"></i>
            @if( !Auth::user()->unreadNotifications->isEmpty() )
            <div class="left pointing ui teal label">{{count( Auth::user()->unreadNotifications)}}</div>
            @endif

            <div class="menu ui feed">
            @if( count( Auth::user()->notifications) == 0 )
              <a class="item">
                <i class="alarm mute outline icon"></i>
                no notifications
              </a>
            @else
                <a href="{{ url('/notifications') }}" class="item">
                    <i class="list icon"></i>
                    View All
                </a>
              @foreach(Auth::user()->notifications->take(10) as $notification)
              
                @include('notifications.user_dashmenu.'.snake_case(class_basename($notification->type)))
              
              @endforeach
            @endif
            </div>
        </div>
      
    </div>
</div>
@endunless

@if(Auth::user() && Auth::user()->verified == 0)
<div class="ui container fluid dashmenu">
    <div class="ui tiny menu">
        <div class="" style="font-size: .85714286rem; letter-spacing: 1px; padding: 0.5em; margin: 0 auto; font-weight: bold; text-transform: capitalize;">
            We noticed you are not attached to any school, please complete your registration to be able to fully use prtals features &nbsp;
            <a href="{{ url('/choose_student_type') }}"><div class="ui mini black button"> Proceed here </div></a>
        </div>
    </div>
</div>
@endif

@if(Auth::guest())
<div class="ui container fluid dashmenu">
    <div class="ui tiny menu">
        <div class="" style="font-size: .85714286rem; letter-spacing: 1px; padding: 0.5em; margin: 0 auto; font-weight: bold; text-transform: capitalize;">
            You're two steps away from sharing what you know with others &nbsp;
            <a href="{{ url('/signup') }}"><div class="ui mini black button"> Start here </div></a>
        </div>
    </div>
</div>
@endif
<br>