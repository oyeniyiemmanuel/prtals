@unless(Auth::guest())
<?php
	$institution_type = Auth::user()->institution_type;
    $institution_type_plural = Auth::user()->institution_type_plural;
	$role = Auth::user()->roles->first()->name;
	$model = "App\\".ucfirst($role);

	$aside_members = $model::where([
				[$institution_type.'_id', '=', Auth::user()->$institution_type->id]
				,['user_id', '!=', null]
				])->inRandomOrder()->get();
?>
<div class="ui raised secondary segment">
	<div class="ui dividing header">
		<div class="content">
			<div class="sub header">
			{{ ucwords(Auth::user()->$institution_type->name) }} 

			@if($role == 'authority')
			authorities 
			@else
			{{ ucfirst($role) }}s 
			@endif

			</div>
		</div>
	</div>
	@foreach($aside_members->take(12) as $member)
		<a data-tooltip="{{ $member->user->name }}" href="/profile/{{ $member->user->username }}">
			@if($member->user->picture && !empty($member->user->picture->url))
			<img alt="{{ $member->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $member->user->picture->url }}">
			@else
			<img alt="{{ $member->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
			@endif
		</a>
	@endforeach
</div>
@endunless