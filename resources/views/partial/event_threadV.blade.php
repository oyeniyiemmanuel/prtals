<?php
    $event_institution_type = $event_thread->institution_type;
    $event_institution_type_plural = $event_thread->institution_type_plural;
?>
<div class="ui feed">
  <div class="event event_thread">
    <div class="label">
    	@if($event_thread->$event_institution_type->logo && !empty($event_thread->$event_institution_type->logo->url))
		<a href="/{{$event_institution_type_plural}}/{{ $event_thread->$event_institution_type->slug }}"><img alt="{{ $event_thread->$event_institution_type->name }} {{ env('APP_URL') }}" src="{{ $event_thread->$event_institution_type->logo->url }}"></a>
		@else
		<a href="/{{$event_institution_type_plural}}/{{ $event_thread->$event_institution_type->slug }}"><img alt="{{ $event_thread->$event_institution_type->name }} {{ env('APP_URL') }}" src="/media/img/{{$event_institution_type}}_logo.jpg"></a>
		@endif
      
    </div>
    <div class="content">
      <div class="date">
        <a href="/{{$event_institution_type_plural}}/{{ $event_thread->$event_institution_type->slug }}" style="color: grey;">{{ ucwords($event_thread->$event_institution_type->name) }}</a>
      </div>
      <div class="summary">
         <a href="/events/{{ $event_thread->slug }}">{{ $event_thread->title }}</a>
      </div>
      <div class="extra images event_pictures">
      	@if($event_thread->pictures && !empty($event_thread->pictures->all()))
			@foreach($event_thread->pictures as $picture)
			<a><img alt="{{ $event_thread->title }} {{ env('APP_URL') }}" src="{{ $picture->url }}"></a>
			@endforeach
		@else
		  <a><img alt="{{ $event_thread->title }} {{ env('APP_URL') }}" src="/media/img/1.jpg"></a>		
        @endif
      </div>
      <div class="description" style="color: grey;">
        {!! nl2br(e($event_thread->message)) !!}
      </div>
      <div class="meta">
	  	<span>
	  		<em>created by</em>
	        <a href="/profile/{{ $event_thread->user->username }}" class="">
	        	<strong>
	        	<?php $name = explode(' ', $event_thread->user->name); echo ucfirst(end($name)); ?>
	        	</strong>
	        </a>
	    </span>

        @if(Auth::id() == $event_thread->user_id)
        <a href="/events/{{ $event_thread->slug }}/edit">edit</a>
        <a class="delete_event">delete</a>
        <input type="hidden" name="event_id" value="{{ $event_thread->id }}">
        {{ csrf_field() }}
        @endif
	  </div>
    </div>
  </div>
</div>