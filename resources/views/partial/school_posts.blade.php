<div class="ui container">
	<div class="ui stackable grid">
		<div class="three wide column">
			
			<div class="ui mini mobile_hide vertical pointing menu">
			  <a href="/posts/create" class="item">
			    Write Post
			  </a>
			  @unless(Auth::guest() || Auth::user()->verified == 0)
			  <?php
			      $institution_type = Auth::user()->institution_type;
			      $institution_type_plural = Auth::user()->institution_type_plural;
			  ?>
			  <a class="item" href="{{ url('/posts/'.$institution_type.'/'.Auth::user()->$institution_type->slug) }}">
			    <div class="ui mini label">
			      <?php
			        echo count(App\Post::where($institution_type.'_id', '=', Auth::user()->$institution_type->id)->get());
			      ?>
			    </div>
			    My {{$institution_type}} Posts
			  </a>
			  <a class="item" href="{{ url('/posts/user/'.Auth::user()->username) }}">
			    <div class="ui mini label">
			      <?php
			        echo count(App\Post::where('user_id', '=', Auth::id())->get());
			      ?>
			    </div>
			    My Posts
			  </a>
			  @endunless
			</div>
			
		</div>
		<div class="eight wide column post_threads">
			@if(empty($posts->all()))
				<div class="ui block centered header">
					<i class="book icon"></i>
					<div class="content">
					  {{ ucwords($school->name) }} Posts
					</div>
				</div>

				@include('bots.post', [
						'bot_status' => true
						,'bot_main_word' => ''
						,'bot_main_phrase' => 'Write New Post'
						,'bot_main_sub_phrase' => ''
						,'bot_catch_phrase' => 'Sign in to create new post'
					])

				<div class="ui block header">
					<i class="info icon"></i>
					<div class="content">
					  No posts to display
					  <div class="sub header"></div>
					</div>
				</div>
			@else
				<div class="ui block centered header">
					<i class="book icon"></i>
					<div class="content">
					  {{ ucwords($school->name) }} Posts
					  <div class="sub header"></div>
					</div>
				</div>

				@include('bots.post', [
						'bot_status' => true
						,'bot_main_word' => ''
						,'bot_main_phrase' => 'Write New Post'
						,'bot_main_sub_phrase' => ''
						,'bot_catch_phrase' => 'Sign in to create new post'
					])
					
				@foreach($posts as $post)
					<div class="ui fluid card post_thread">
					  @include('partial.half_post_threadV')
					</div>
				@endforeach
				{{ $posts->links() }}
			@endif
		</div>
		<div class="five wide column">
			@include('partial.aside_schools_list')

			@include('partial.aside_users_list')
		</div>
	</div>
</div>