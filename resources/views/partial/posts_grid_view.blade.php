<?php
    $posts = App\Post::whereNotIn('id', $loaded_posts)->get();
?>
@if(empty($posts->all()))
    <div class="ui block header">
        <i class="info icon"></i>
        <div class="content">
          No Posts 
          <div class="sub header"></div>
        </div>
    </div>
@else
    @foreach($posts->take(1) as $post)

    <?php
        array_push($loaded_posts, $post->id);
    ?>
    <a href="/posts/category/{{ $post->category }}"><em>{{ ucwords($post->category) }}s </em></a>

    <div class="ui fluid card">
      <a href="{{ env('APP_URL') }}/schools/{{ $post->school->slug }}" class="image">
        @if($post->school->cover && !empty($post->school->cover->url))
        <img alt="{{ $post->title }} {{ env('APP_URL') }}" src="{{ $post->school->cover->url }}">
        @else
        <img alt="{{ $post->title }} {{ env('APP_URL') }}" src="/media/img/school_cover.jpg">
        @endif
      </a>
      <div class="content">
        <a href="/posts/{{ $post->slug }}" class="header">{{ ucwords($post->title) }}</a>
        <div class="meta">
          @if($post->school->logo && !empty($post->school->logo->url))
          <a href="{{ env('APP_URL') }}/schools/{{ $post->school->slug }}"><img alt="{{ $post->school->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $post->school->logo->url }}"></a>
          @else
          <a href="{{ env('APP_URL') }}/schools/{{ $post->school->slug }}"><img alt="{{ $post->school->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/school_logo.jpg"></a>
          @endif
          <a href="{{ env('APP_URL') }}/schools/{{ $post->school->slug }}" class="date">{{ ucwords($post->school->name) }}, {{ $post->school->state }}</a>
        </div>
        <div class="description">
          {{ $post->message }}
        </div>
      </div>
      <div class="extra content">
        <span>
          {{ $post->created_at->diffForHumans() }}
        </span>
        <a href="/profile/{{ $post->user->username }}" class="ui right floated">
            @if($post->user->picture && !empty($post->user->picture->url))
            <img alt="{{ $post->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $post->user->picture->url }}">
            @else
            <img alt="{{ $post->user->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
            @endif
            <span class="">
                <?php $name = explode(' ', $post->user->name); echo ucfirst(end($name)); ?>
            </span>                
        </a>
      </div>
    </div>
    @endforeach
@endif