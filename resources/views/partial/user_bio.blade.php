<div class="ui vertical mini menu right floated">
  <a href="{{ url('/posts/user') }}/{{$user->username}}" class="item">
    Posts 
    <div class="ui mini label">{{ count($user->posts) }}</div>
  </a>
  <a href="{{ url('/news/user') }}/{{$user->username}}" class="item">
    News
    <div class="ui mini label">{{ count($user->news) }}</div>
  </a>
  <a href="{{ url('/events/user') }}/{{$user->username}}" class="item">
    Events
    <div class="ui mini label">{{ count($user->events) }}</div>
  </a>
</div>

<div class="ui header">{{ ucwords($user->name) }}</div>
<p>
	@if($user->$user_role && !empty($user->$user_role->bio))
		{{ $user->$user_role->bio }}
	@else
		Bio is empty...
	@endif 

	@if(Auth::user() == $user)
	<div class="ui mini button edit_trigger">
		<i class="write square icon"></i>Edit Bio
	</div>
	<form class="ui form add_bio" method="POST">
	<hr>
		{{ csrf_field() }}
		<div class="field">
		  <textarea rows="2" name="bio" placeholder="enter bio" required></textarea>
		</div>
		<button class="ui mini submit teal button">
			Update
		</button>
	</form>
	@endif
</p>