@extends('layout.master')

@section('title', 'Edit Post | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'Edit Post: '.ucwords($post->title)
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container welcome">
		<div class="ui stackable grid">
			<div class="three wide column">
				@include('partial.post_dashmenu')
			</div>

			<div class="eight wide column">
				@include('errors.form_valid')
				<form id="login" class="ui large form" method="POST" action="{{ url('/posts') }}/{{ $post->slug }}/update">
		          {{ csrf_field() }}
		          <div class="ui stacked segment">
					<div class="field">
						<label>Title</label>
						<input type="text" name="title" placeholder="What is the title of your post?" required value="{{ $post->title }}">
					</div>

					<div class="field">
						<label>Message</label>
						<textarea rows="6" name="message" placeholder="Enter your message" required>{{ $post->message }}</textarea>
					</div>
					@if(Auth::user()->roles()->first()->name == 'student')
					<select name="subject_name" class="ui mini button floating dropdown" required>
				      <option value="">Select Subject</option>
				      @foreach($subject_categories as $subject)
				      <option <?php echo $post->subject->name == $subject ? 'selected' : '' ; ?> value="{{ $subject }}">{{ ucfirst($subject) }}</option>
				      @endforeach
				    </select>
				    @endif

					@if(Auth::user()->roles()->first()->name == 'undergraduate')
					<select name="field_name" class="ui mini button floating dropdown" required>
				      <option value="">Select Field</option>
				      @foreach($field_categories as $field)
				      <option <?php echo $post->field->name == $field ? 'selected' : '' ; ?> value="{{ $field }}">{{ ucfirst($field) }}</option>
				      @endforeach
				    </select>
				    @endif

				
					<select name="category" class="ui mini button floating dropdown" required>
				      <option value="">Category</option>
				      @foreach($post_categories as $category)
				      <option <?php echo $post->category == $category['category'] ? 'selected' : '' ; ?> value="{{ $category['category'] }}">{{ ucfirst($category['category']) }}</option>
				      @endforeach
				    </select>

				    <input type="hidden" name="school_id" value="{{ Auth::user()->school_id }}">

		            <button type="submit" class="ui mini teal submit button">Submit</button>
		          </div>
		        </form>
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')
				
				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection