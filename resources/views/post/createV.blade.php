@extends('layout.master')

@section('title', 'Create Post | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'Write New Post'
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container welcome">
		<div class="ui stackable grid">
			<div class="three wide column">
				@include('partial.post_dashmenu')
			</div>

			<div class="eight wide column">
				@include('errors.form_valid')
				<form id="login" class="ui large form" method="POST" action="{{ url('/posts') }}">
		          {{ csrf_field() }}
		          <div class="ui stacked secondary segment">
					<div class="field">
						<label>Title</label>
						<input type="text" name="title" placeholder="What is the title of your post?" required>
					</div>

					<div class="field">
						<label>Message</label>
						<textarea rows="6" name="message" placeholder="Enter your message" required></textarea>
					</div>
					@if(Auth::user()->roles()->first()->name == 'student')
					<select name="subject_name" class="ui mini button floating dropdown" required>
				      <option value="">Select Subject</option>
				      @foreach($subject_categories as $subject)
				      <option value="{{ $subject }}">{{ ucfirst($subject) }}</option>
				      @endforeach
				    </select>
				    @endif

				    @if(Auth::user()->roles()->first()->name == 'undergraduate')
					<select name="field_name" class="ui mini button floating dropdown" required>
				      <option value="">Select Field</option>
				      @foreach($field_categories as $field)
				      <option value="{{ $field }}">{{ ucfirst($field) }}</option>
				      @endforeach
				    </select>
				    @endif
				
					<select name="category" class="ui mini button floating dropdown" required>
				      <option value="">Select Category</option>
				      <option value="assignment">Assignment</option>
				      <option value="suggestion">suggestion</option>
				      <option value="fact">fact</option>
				      <option value="question">question</option>
				      <option value="hint">hint</option>
				      <option value="trivia">trivia</option>
				      <option value="poem">poem</option>
				      <option value="thought">thought</option>
				      <option value="discovery">discovery</option>
				    </select>

		            <button type="submit" class="ui mini teal submit button">Submit</button>
		          </div>
		        </form>
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')
				
				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection