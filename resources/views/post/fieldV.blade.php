@extends('layout.master')

@section('title', ucwords($field).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => ucwords($field)
			,'trend' => 'Discipline'
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				@include('partial.post_dashmenu')
				
			</div>
			<div class="eight wide column post_threads">
				@if(empty($posts->all()))
					
					@include('bots.post', [
						'bot_status' => Auth::user() && Auth::user()->verified == 1 && Auth::user()->hasRole('undergraduate')? true: false
						,'bot_main_word' => $field
						,'bot_main_phrase' => 'Write New '.ucwords($field).' Post'
						,'bot_main_sub_phrase' => 'Be the first to create a discussion in this field'
						,'bot_catch_phrase' => 'Sign in to create new post'
					])

					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No posts to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else

					@include('bots.post', [
						'bot_status' => Auth::user() && Auth::user()->verified == 1 && Auth::user()->hasRole('undergraduate')? true: false
						,'bot_main_word' => $field
						,'bot_main_phrase' => 'Write New '.ucwords($field).' Post'
						,'bot_main_sub_phrase' => 'Join other like minds in this discipline'
						,'bot_catch_phrase' => 'Sign in to create new post'
					])

					@foreach($posts as $post)
						<div class="ui fluid card post_thread">
						  @include('partial.half_post_threadV')
						</div>
					@endforeach
					{{ $posts->links() }}
				@endif
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection