@extends('layout.master')

@section('title', ucwords($post->title).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => ucwords($post->title)
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				@include('partial.post_dashmenu')

			</div>
			<div class="eight wide column post_threads">
				<div class="ui fluid card post_thread">
				  @include('partial.post_threadV')
				</div>
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')
				
				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection