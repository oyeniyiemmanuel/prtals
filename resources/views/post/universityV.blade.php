@extends('layout.master')

@section('title', 'Posts: '.ucwords( $focus_university->name ).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => ucwords( $focus_university->name ). ' Posts'
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				@include('partial.post_dashmenu')
				
			</div>
			<div class="eight wide column post_threads">
				@if(empty($posts))
					<div class="ui block centered header">
						<i class="book icon"></i>
						<div class="content">
						  Posts
						</div>
					</div>

					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No posts to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
					<div class="ui block centered header">
						<i class="book icon"></i>
						<div class="content">
						  Posts
						  <div class="sub header"></div>
						</div>
					</div>
					@foreach($posts as $post)
						<div class="ui fluid card post_thread">
						  @include('partial.half_post_threadV')
						</div>
					@endforeach
					{{ $posts->links() }}
				@endif
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection