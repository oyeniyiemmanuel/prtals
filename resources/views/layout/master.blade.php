<!DOCTYPE html>
<html lang="en">
    <head>
        <title>@yield('title')</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="Prtals provides a platform for students to work together, educate, inform & brush on their best subjects, interact on an inter-school level, and help each other solve complex problems">
        <meta name="keywords" content="prtals, prtals.com, www.prtals.com">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="/media/img/favicon.ico" />
        <link rel="stylesheet" href="{{ elixir('/load/prtals-main.css') }}">
        <script type="text/javascript" src="/js/dropzone.js"></script>
        <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "Organization",
              "name": "prtals",
              "alternateName": "prtals NG",
              "url": "https://www.prtals.com",
              "logo": "https://www.prtals.com/media/img/logo.png",
              "sameAs": [
                "http://www.twitter.com/prtalsng",
                "http://instagram.com/prtalsng"
              ]
            }
        </script>
    </head>
    <body>
    @if(env('APP_ENV') != 'local')
        @include('google.analyticstracking')
    @endif

        @section('navigation_bar')
        
            @show

        <div class="ui stick mobile_hide" style="background: #ffffff;">
            
            <div class="ui container fluid">
                <div class="ui small fixed top main menu">
                    <a class="item logo_name" href="{{ url('/') }}"> 
                        <img class="ui tiny image" alt="prtals logo {{ env('APP_URL') }}" src="{{ url('/media/img/logo.png') }}">
                    </a>

                    <div class="item">
                        <form class="ui form" method="GET" action="{{ url('/search') }}">
                            {{ csrf_field() }}
                            <div class="ui big action left icon input">
                                <i class="search icon"></i>
                                <input class="no_border" type="search" name="query" placeholder="Search..">
                            </div>
                        </form>
                    </div>

                    <div class="right menu">
                        <a class="link item" href="{{ url('/posts') }}">Posts</a>
                        <a class="link item" href="{{ url('/news') }}">News</a>
                        <a class="link item" href="{{ url('/events') }}">Events</a>
                        <a class="link item" href="{{ url('/universities') }}">Universities</a>                    
                        <a class="link item" href="{{ url('/schools') }}">High Schools</a>                    
                        @if (Auth::user())
                        <a class="link item" href="{{ url('/announcements') }}">Announcements</a>
                        @endif
                        
                        @if (Auth::user())
                        <div class="ui pointing dropdown link item">
                          @if(Auth::user()->picture && !empty(Auth::user()->picture->url))
                          <img alt="{{ Auth::user()->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ Auth::user()->picture->url }}">
                          @else
                          <img alt="{{ Auth::user()->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
                          @endif
                          <?php $name = explode(' ', Auth::user()->name); echo ucfirst(end($name)); ?> <i class="dropdown icon"></i>
                          <div class="menu">
                            <a class="item" href="{{ url('/dashboard') }}"><i class="dashboard grey icon"></i> Dashboard</a>
                            @if(Auth::user()->school)
                            <a class="item" href="{{ url('/schools') }}/{{ Auth::user()->school->slug }}"><i class="university grey icon"></i> My School</a>
                            @endif
                            <a class="item" href="{{ url('/profile') }}"><i class="smile grey icon"></i> Profile</a>
                            <a class="item" href="{{ url('/messages') }}"><i class="envelope grey icon"></i> Messages</a>
                            <a class="item" href="{{ url('/logout') }}"><i class="sign out grey icon"></i> Logout</a>
                          </div>
                        </div>
                        @else
                            <a href="{{ url('/signin') }}" class="item link" style="color: teal;">Sign In</a>
                            <div class="item">
                                <a href="{{ url('/signup') }}" class="ui massive teal button">Join Now</a>
                            </div>
                        @endif
                    </div>
                </div>

                <div style="margin-top: 78px;">
                   
                </div>

                @if(1 > 2)
                <div class="ui inverted teal tiny attached menu" style="margin-top: 78px;">
                    @foreach($subject_random->take(10) as $category)
                    <a href="{{ url('/') }}/posts/subject/{{ $category }}" class="link item">
                        {{ ucwords(str_replace('-', ' ', $category)) }}
                    </a>
                    @endforeach
                    <a href="{{ url('/subjects') }}" class="link right item"><div class="ui mini inverted black tag label">&nbsp; View All Subjects</div></a>
                </div>
            
                <div class="ui inverted teal tiny attached menu" style="background: #61848A;">
                    @foreach($post_random->take(12) as $category)
                    @if($category['category'] == 'discovery')
                    <a href="{{ url('/') }}/posts/category/{{ $category['category'] }}" class="link item">{{ ucfirst('discoveries') }}</a>
                    @else
                    <a href="{{ url('/') }}/posts/category/{{ $category['category'] }}" class="link item">{{ ucfirst($category['category']) }}s</a>
                    @endif
                    @endforeach
                    <a href="{{ url('/categories') }}" class="link right item">
                        <div class="ui mini inverted black tag label">&nbsp; View All Categories</div>
                    </a>
                </div>
            
                <div class="ui inverted teal tiny attached menu" style="background: #023B42;">
                    @foreach($field_random->take(9) as $field)
                    <a href="{{ url('/') }}/posts/field/{{ $field }}" class="link item">
                        {{ ucwords(str_replace('-', ' ', $field)) }}
                    </a>
                    @endforeach
                    <a href="{{ url('/fields') }}" class="link right item">
                        <div class="ui mini inverted black tag label">&nbsp; View All Disciplines</div>
                    </a>
                </div>
                @endif
            </div>
        </div>
        
        <div class="ui vertical inverted sidebar menu mobile_menu">
            <a class="toc item">
              <i class="close red icon"></i> close
            </a>
            <div class="link item">
                <form class="ui form" method="GET" action="{{ url('/search') }}">
                    {{ csrf_field() }}
                    <div class="ui big action left icon input">
                        <i class="search icon"></i>
                        <input type="search" name="query" placeholder="Search..">
                    </div>
                </form>
            </div>
            
            <a href="{{ url('/') }}" class="item">
                <i class="home teal icon"></i> Home
            </a>
            <a href="{{ url('/posts') }}" class="item">
                <i class="book teal icon"></i> Posts
            </a>
            <a href="{{ url('/news') }}" class="item">
                <i class="newspaper teal icon"></i> News
            </a>
            <a href="{{ url('/events') }}" class="item">
                <i class="ticket teal icon"></i> Events
            </a>
            <a href="{{ url('/universities') }}" class="item">
                <i class="university teal icon"></i> Universities
            </a>
            <a href="{{ url('/schools') }}" class="item">
                <i class="university teal icon"></i>High Schools
            </a>
            <a href="{{ url('/subjects') }}" class="item">
                <i class="circle thin icon"></i> Subjects
            </a>
            <a href="{{ url('/categories') }}" class="item">
                <i class="tag icon"></i> Categories
            </a>
            <a href="{{ url('/fields') }}" class="item">
                <i class="object group icon"></i> Disciplines
            </a>
        </div>
        
        <div id="context" class="pusher">
              <div class="ui large secondary inverted pointing menu mobile_menu mobile_only" style="background: #0E4D4E;">
                <a class="toc item" style=" padding-right: 0 !important;">
                  <i class="sidebar teal big icon" style="margin-right: 0 !important;"></i>
                </a>

                @if (Auth::guest())
                <div class="right item" style="padding-left: 0 !important; color: grey;">
                    |&nbsp;
                    <a href="{{ url('/posts') }}" class="">
                        <i class="book big teal icon"></i> 
                    </a> |&nbsp; 
                    <a href="{{ url('/news') }}" class="">
                        <i class="newspaper big teal icon"></i> 
                    </a> |&nbsp; 
                    <a href="{{ url('/events') }}" class="">
                        <i class="ticket big teal icon"></i> 
                    </a> |&nbsp; 
                    <a href="{{ url('/universities') }}" class="">
                        <i class="university big teal icon"></i> 
                    </a> |&nbsp; 
                    <a href="{{ url('/signin') }}" class="">
                        Sign In
                    </a>
                    &nbsp;&nbsp;&nbsp;
                    <a href="{{ url('/signup') }}" class="ui mini teal label button">Join Now</a>
                </div>
                @else
                <div class="right item">
                    <a href="{{ url('/posts') }}" class="">
                        <i class="book big teal icon"></i> 
                    </a> |&nbsp; 
                    <a href="{{ url('/news') }}" class="">
                        <i class="newspaper big teal icon"></i> 
                    </a> |&nbsp; 
                    <a href="{{ url('/events') }}" class="">
                        <i class="ticket big teal icon"></i> 
                    </a> |&nbsp; 
                    <a href="{{ url('/universities') }}" class="">
                        <i class="university big teal icon"></i> 
                    </a> |&nbsp;
                </div>
                <div class="ui pointing dropdown link right item">
                    @if(Auth::user()->picture && !empty(Auth::user()->picture->url))
                    <img alt="{{ Auth::user()->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ Auth::user()->picture->url }}">
                    @else
                    <img alt="{{ Auth::user()->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/user.jpg">
                    @endif
                  <?php $name = explode(' ', Auth::user()->name); echo ucfirst(end($name)); ?> <i class="dropdown icon"></i>
                  <div class="menu">
                    <a class="item" href="{{ url('/dashboard') }}"><i class="dashboard grey icon"></i> Dashboard</a>
                    @if(Auth::user()->school)
                    <a class="item" href="{{ url('/schools') }}/{{ Auth::user()->school->slug }}"><i class="university grey icon"></i> My School</a>
                    @endif
                    <a class="item" href="{{ url('/profile') }}"><i class="smile grey icon"></i> Profile</a>
                    <a class="item" href="{{ url('/messages') }}"><i class="envelope grey icon"></i> Messages</a>
                    <a class="item" href="{{ url('/logout') }}"><i class="sign out grey icon"></i> Logout</a>
                  </div>
                </div>
                @endif
                    
                
              </div>
            <div class="content_wrapper">
                @yield('content')
            </div>
            @section('footer')
            <div class="ui vertical footer segment">
                <div class="ui container fluid">
                    @if(1>2)
                        <span class=""> <a href="">Privacy & Terms</a></span>
                        &nbsp;&nbsp;&nbsp;
                    @endif
                    &copy; prtals 2017
                </div>
              </div>
          </div>
        <script src="{{ elixir('/load/prtals-main.js') }}"></script>

        @include('modal.login')
        @include('modal.register')

        <!-- Display flash message  -->
        @include('partial.flash_message')

    </body>
</html>
