@extends('layout.master')

@section('title', 'Disciplines| Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'Higher Institution Disciplines'
			,'trend' => ''
		])

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="sixteen wide column">
				<br>
				<div class="ui six doubling cards">
				@foreach($field_random as $field)
				  <a href="/posts/field/{{ $field }}" class="card">
				    <div class="image">
				      <img alt="{{ $field }} {{ env('APP_URL') }}" src="/media/img/subjects/{{ $field }}.jpg">
				    </div>

					<div class="content">
						<div class="header">{{ ucwords(str_replace('-', ' ', $field)) }}</div>
						<div class="meta">
						<?php
							$field = App\Field::where('name', '=', $field)->get()->first();

							$number = $field->posts()->count();
						?>
						  <span>{{ $number }} discussions</span>
						  @if($number != 0)
						  <div class="ui mini black button">Join Discussion</div>
						  @else
						  <div class="ui mini black button">Start Discussion</div>
						  @endif
						</div>
					</div>
				  </a>
				@endforeach
				</div>
			</div>
		</div>	
	</div>
	
@endsection

@section('footer')
	@parent
@endsection