@extends('layout.master')

@section('title', 'Add New University Field | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="four wide column"></div>
			<div class="eight wide column">
				<div class="ui secondary segment">
					<h1 class="ui centered dividing header">
						<div class="content">
							Admin Add New University Field
						</div>
					</h1>

					@include('errors.form_valid')

					<form id="login" class="ui large form" method="POST" action="{{ url('/add_new_field') }}">
			          {{ csrf_field() }}
			            <div class="field">
			              <div class="ui left icon input">
			                <i class="circle icon"></i>
			                <input type="text" name="field" placeholder="Enter New field" required>
			              </div>
			            </div>

			          <button type="submit" class="ui fluid large teal submit button">Submit</button>
			        </form>

				</div>
			</div>
			<div class="four wide column"></div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection