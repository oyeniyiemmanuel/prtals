@extends('layout.master')

@section('title', ' Categories| Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'All Categories'
			,'trend' => ''
		])

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="sixteen wide column">
				<br>
				<div class="ui six doubling cards">
				@foreach($post_random as $category)
				  <a href="/posts/category/{{ $category['category'] }}" class="card">
				  	@if( 1>2 )
				    <div class="image">
				      <img alt="{{ $category['category'] }} {{ env('APP_URL') }}" src="/media/img/categories/{{ $category['category'] }}.jpg">
				    </div>
					@endif
					<div class="content">
						<div class="header">{{ ucwords($category['category']) }}</div>
						<div class="meta">
						<?php
							$number = App\Post::where('category', '=', $category['category'])->count();
						?>
						  <span>{{ $number }} discussions</span>
						  @if($number != 0)
						  <div class="ui mini black button">Join Discussion</div>
						  @else
						  <div class="ui mini black button">Start Discussion</div>
						  @endif
						</div>
					</div>
				  </a>
				@endforeach
				</div>
			</div>
		</div>	
	</div>
	
@endsection

@section('footer')
	@parent
@endsection