@extends('layout.master')

@section('title', 'New Message | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
		@include('partial.page_header', [
			'title' => 'New Message Thread'
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="{{ url('/messages/create') }}" class="active item">
				    New Thread
				  </a>
				  <a href="{{ url('/messages') }}" class="item">
				    Inbox
				    @if(Auth::user()->newThreadsCount() > 0)
				    <div class="ui mini teal label">{{ Auth::user()->newThreadsCount() }}</div>
				    @endif
				  </a>
				</div>
				
			</div>
			<div class="eight wide column messages_threads">	

				<div class="ui comments">
				@foreach($users as $user)
				  <div class="comment">
				    <a href="{{ url('/profile') }}/{{ $user->username }}" class="avatar">
				      	@if($user->picture && !empty($user->picture->url))
				    	<img alt="{{ $user->name }} {{ env('APP_URL') }}" src="{{ $user->picture->url }}">
				    	@else
				    	<img alt="{{ $user->name }} {{ env('APP_URL') }}" src="/media/img/user.jpg">
				    	@endif
				    </a>
				    <div class="content">
				      <span class="author">{{ ucwords($user->name) }}</span>
				      <div class="metadata">
				        <span class="date">{{ ucwords($user->roles->pluck('name')->first()) }}</span>
				      </div>
				      <div class="text">
				      	<?php
						    $institution_type = $user->institution_type;
						    $institution_type_plural = $user->institution_type_plural;
						?>
				        <a href="{{ url('/'.$institution_type_plural.'') }}/{{ $user->$institution_type->slug }}">
				        {{ ucwords($user->$institution_type->name) }}</a>  
				        <em>{{ $user->$institution_type->address }}</em>, 
				        <b>{{ ucfirst($user->$institution_type->state) }}</b>
				      </div>
				      <div class="actions">
				        <a class="ui basic label button write_user">Write</a>
				        <div class="ui show_message_form">
						    <div class="content">
						      <form class="ui fluid form new_message" method="POST" action="{{ url('/messages') }}">
						      	{{ csrf_field() }}
						        <div class="field">
						          <textarea rows="5" name="message" required></textarea>
						        </div>
						        <input type="hidden" name="recipients" value="{{ $user->id }}">
						        <button class="ui mini submit teal button">
						        	<i class="send outline icon"></i>
						        	Send Message
						        </button>
						      </form>
						    </div>
						</div>
				      </div>
				    </div>
				  </div>
				  @endforeach
				</div>			
			</div>

			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection