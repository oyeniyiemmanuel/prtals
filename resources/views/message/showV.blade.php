@extends('layout.master')

@section('title', 'Message | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	<?php 
		$other_person = $thread->participants()->where('user_id', '!=', Auth::id())->get()->first();
		$recipient = $other_person ? $other_person->user : $thread->participants()->get()->first()->user
	?>
		@include('partial.page_header', [
			'title' => $recipient->username
			,'trend' => 'Private Message'
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="{{ url('/messages/create') }}" class="item">
				    New Thread
				  </a>
				  <a href="{{ url('/messages') }}" class="item">
				    Inbox
				    @if(Auth::user()->newThreadsCount() > 0)
				    <div class="ui mini teal label">{{ Auth::user()->newThreadsCount() }}</div>
				    @endif
				  </a>
				</div>
				
			</div>
			<div class="eight wide column messages_threads">
			<div class="private_thread">

				<div class="ui comments">
				@foreach($thread->messages as $message)
				  <div class="comment">
				    <a href="{{ url('/profile') }}/{{ $message->user->username }}" class="avatar">
				    	@if($message->user->picture && !empty($message->user->picture->url))
				    	<img alt="{{ $message->user->name }} {{ env('APP_URL') }}" src="{{ $message->user->picture->url }}">
				    	@else
				    	<img alt="{{ $message->user->name }} {{ env('APP_URL') }}" src="/media/img/user.jpg">
				    	@endif
				    </a>
				    <div class="content">
				      <div class="metadata">
				        <span class="date">{{ $message->created_at->diffForHumans() }}</span>
				      </div>
				      <div class="text">{{ $message->body }}</div>
				    </div>
				  </div>
				@endforeach
				</div>

				<div class="ui comments">
				  <div class="comment">
				    <div class="content">
				      <form class="ui fluid form new_message" method="POST" action="{{ url('/messages') }}/{{ $thread->id }}/add">
				      	{{ csrf_field() }}
				        <div class="field">
				          <textarea rows="2" name="message" required></textarea>
				        </div>
				        <input type="hidden" name="recipients" value="{{ $recipient->id }}">
				        <button class="ui mini submit teal button">
				        	<i class="send outline icon"></i>
				        	Send
				        </button>
				      </form>
				    </div>
				  </div>
				</div>
				
			</div>
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection