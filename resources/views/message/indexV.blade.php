@extends('layout.master')

@section('title', 'Messages | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
		@include('partial.page_header', [
			'title' => 'Message Threads'
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="{{ url('/messages/create') }}" class="item">
				    New Thread
				  </a>
				  <a href="" class="active item">
				    Inbox 
				    @if(Auth::user()->newThreadsCount() > 0)
				    <div class="ui mini teal label">{{ Auth::user()->newThreadsCount() }}</div>
				    @endif
				  </a>
				</div>
				
			</div>
			<div class="eight wide column messages_threads">
				
				<div class="ui comments">
				@if($threads->count() > 0)
					@foreach($threads as $thread)
					<?php $read_thread = $thread->isUnread(Auth::user()->id) ? 'raised teal' : ''; ?>
					<?php 
						$latest_sender = $thread->latestMessage->user;
						//$other_person = $thread->participants()->where('user_id', '!=', Auth::id())->get()->first();

						//$recipient = $other_person ? $other_person->user : $thread->participants()->get()->first()->user;
					?>
					<a href="{{ url('/messages') }}/{{ $thread->id }}">
						<div class="ui {{ $read_thread }} secondary segment">
							<div class="comment">
								<div class="avatar">
									@if($latest_sender->picture && !empty($latest_sender->picture->url))
								 	<img alt="{{ $latest_sender->name }} {{ env('APP_URL') }}" src="{{ $latest_sender->picture->url }}">
								 	@else
								 	<img alt="{{ $latest_sender->name }} {{ env('APP_URL') }}" src="/media/img/user.jpg">
								 	@endif
								</div>
								<div class="content">
								  <span class="author">{{ ucwords($thread->participantsString()) }}</span>
								  <div class="metadata">
								    <div class="date">{{ $thread->updated_at->diffForHumans() }}</div>
								  </div>
								  <div class="text">
								    <p>{{ $thread->latestMessage->body }}</p>
								  </div>
								</div>
							</div>
						</div>
					</a>
					<span class="ui mini pointing above label button delete_thread">
						<input type="hidden" name="thread_id" value="{{ $thread->id }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<i class="trash icon"></i> Delete thread
					</span>
					<br>
					<br>
					@endforeach
				@else
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No Message threads
						  <div class="sub header"></div>
						</div>
					</div>
				@endif
				</div>
				
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection