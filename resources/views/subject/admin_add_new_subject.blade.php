@extends('layout.master')

@section('title', 'Add New Subject | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="four wide column"></div>
			<div class="eight wide column">
				<div class="ui secondary segment">
					<h1 class="ui centered dividing header">
						<div class="content">
							Admin Add New Subject
							<div class="sub header">Pls check the departments carefully if it is valid to the corresponding subject before submitting. Every school's <em><strong>subject-teacher-student</strong></em> structure and relationship is dependent on this action</div>
						</div>
					</h1>

					@include('errors.form_valid')

					<form id="login" class="ui large form" method="POST" action="{{ url('/add_new_subject') }}">
			          {{ csrf_field() }}
			          <div class="two fields">
			            <div class="field">
			              <select name="department" class="ui fluid floating dropdown" required>
								<option value="">Select Department</option>
								@foreach($departments as $department)
								<option value="{{ $department }}">{{ strtoupper($department) }}</option>
								@endforeach
								<option value="art_and_commercial">SENIOR HIGH ( Art & Commercial Only )</option>
								<option value="all_senior_high">All SENIOR HIGH ( Science, Art & Commercial )</option>
								<option value="general">GENERAL (Junior High & Senior High)</option>
							</select>
			            </div>
			            <div class="field">
			              <div class="ui left icon input">
			                <i class="circle icon"></i>
			                <input type="text" name="subject" placeholder="Enter New Subject" required>
			              </div>
			            </div>
			          </div>

			          <button type="submit" class="ui fluid large teal submit button">Submit</button>

			        </form>

				</div>
			</div>
			<div class="four wide column"></div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection