@extends('layout.master')

@section('title', ' Subjects| Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'All Subjects'
			,'trend' => ''
		])

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="sixteen wide column">
				<br>
				<div class="ui six doubling cards">
				@foreach($subject_random as $subject)
				  <a href="/posts/subject/{{ $subject }}" class="card">
				    <div class="image">
				      <img alt="{{ $subject }} {{ env('APP_URL') }}" src="/media/img/subjects/{{ $subject }}.jpg">
				    </div>

					<div class="content">
						<div class="header">{{ ucwords(str_replace('-', ' ', $subject)) }}</div>
						<div class="meta">
						<?php
							$subject = App\Subject::where('name', '=', $subject)->get()->first();

							$number = $subject->posts()->count();
						?>
						  <span>{{ $number }} discussions</span>
						  @if($number != 0)
						  <div class="ui mini black button">Join Discussion</div>
						  @else
						  <div class="ui mini black button">Start Discussion</div>
						  @endif
						</div>
					</div>
				  </a>
				@endforeach
				</div>
			</div>
		</div>	
	</div>
	
@endsection

@section('footer')
	@parent
@endsection