<div class="ui university_tuition_modal small modal">
  <div class="content">
  <h3 class="ui dividing centered header">{{ ucwords($university->name) }} tuition</h3>
    <div class="ui middle aligned center aligned stackable grid">
      <div class="four wide column"></div>
      <div class="eight wide column">
        {{ $university->tuition }}

      </div>
      <div class="four wide column"></div>
    </div>
  </div>
  <div class="actions">
    <div class="ui deny"><i class="large remove icon circular"></i></div>
  </div>
</div>