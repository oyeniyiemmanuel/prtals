
<div class="ui comments_modal small modal">
  <div class="content">
  <h3 class="ui dividing centered header">Comments</h3>
    <div class="ui middle aligned center aligned stackable grid">
      <div class="four wide column"></div>
      <div class="eight wide column">
        <div class="ui feed comment_threads">
            @foreach($post->comments as $comment)
            <div class="event">
              @include('partial.comment_threadV')
            </div>
            @endforeach
        </div>

      </div>
      <div class="four wide column"></div>
    </div>
  </div>
  <div class="actions">
    <div class="ui deny"><i class="large remove icon circular"></i></div>
  </div>
</div>
