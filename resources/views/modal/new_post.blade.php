@unless(Auth::guest() || Auth::user()->verified == 0)
<div class="ui new_post_modal basic small modal">
  <div class=" content">
    <form id="login" class="ui large form" method="POST" action="{{ url('/posts') }}">
          {{ csrf_field() }}
          <div class="ui stacked secondary segment">
            @if($bot_main_word != '')
            <div class="ui header">
                <img alt="{{ $bot_main_word }}" class="ui tiny left floated image" src="{{ url('/media/img/subjects') }}/{{$bot_main_word}}.jpg">
                <h3  style="margin: 0 !important;">{{ucwords($bot_main_word)}}</h3> 
                <div class="sub header">
                    <?php
                        if(Auth::user()->hasRole('student')) {
                            $subject = App\Subject::where('name', '=', $bot_main_word)->get()->first();
                            if($subject != null){
                                $discussion_count = $subject->posts()->count();
                            } else {
                                $discussion_count = null;
                            }
                        }
                        if(Auth::user()->hasRole('undergraduate')) {
                            $field = App\Field::where('name', '=', $bot_main_word)->get()->first();
                            if($field != null){
                                $discussion_count = $field->posts()->count();
                            } else {
                                $discussion_count = null;
                            }
                        }
                    ?>
                    <?php echo $discussion_count != null? 'This currently has '.$discussion_count.' ongoing discussions': ' ' ; ?>
                </div>               
            </div>
            @endif
            <div class="field">
              <label>Title</label>
              <input type="text" name="title" placeholder="What is the title of your {{ucwords($bot_main_word)}} post?" required>
            </div>

            <div class="field">
              <label>Message</label>
              <textarea rows="6" name="message" placeholder="Enter your message" required></textarea>
            </div>

            @if($bot_main_word == '')
                @if(Auth::user()->roles()->first()->name == 'student')
                    <select name="subject_name" class="ui mini button floating dropdown" required>
                        <option value="">Select Subject</option>
                        @foreach($subject_categories as $subject)
                        <option value="{{ $subject }}">{{ ucfirst($subject) }}</option>
                        @endforeach
                    </select>
                @endif

                @if(Auth::user()->roles()->first()->name == 'undergraduate')
                    <select name="field_name" class="ui mini button floating dropdown" required>
                        <option value="">Select Field</option>
                        @foreach($field_categories as $field)
                        <option value="{{ $field }}">{{ ucfirst($field) }}</option>
                        @endforeach
                    </select>
                @endif

            @else
                @if(Auth::user()->roles()->first()->name == 'student')
                <input type="hidden" name="subject_name" value="{{ $bot_main_word }}">
                @endif
                @if(Auth::user()->roles()->first()->name == 'undergraduate')
                <input type="hidden" name="field_name" value="{{ $bot_main_word }}">
                @endif
            @endif
          
            <select name="category" class="ui mini button floating dropdown" required>
                <option value="">Select Category</option>
                <option value="assignment">Assignment</option>
                <option value="suggestion">suggestion</option>
                <option value="fact">fact</option>
                <option value="question">question</option>
                <option value="hint">hint</option>
                <option value="trivia">trivia</option>
                <option value="poem">poem</option>
                <option value="thought">thought</option>
                <option value="discovery">discovery</option>
              </select>

              <button type="submit" class="ui mini teal submit button">Post</button>
        </div>
    </form>
  </div>
</div>
@endunless