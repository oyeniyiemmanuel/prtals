<div class="ui centered school_logo_modal small basic modal">
  <div class="header">
    <div class="ui centered header">
      <form class="ui centered form" id="school_logo_pic_form" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="ui mini icon input">
          <input id="choose_logo" class="ui inverted buton" type="file" name="image">
          <input type="hidden" name="slug" value="{{ $school->slug }}">
          <span class="actions">
            <span class="ui mini inverted white button deny"><i class="remove icon circular"></i></span>
          </span>
        </div>
      </form>
    </div>
  </div>
</div>