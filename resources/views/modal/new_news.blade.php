@unless(Auth::guest() || Auth::user()->verified == 0)
<?php
    $institution_type = Auth::user()->institution_type;
    $institution_type_plural = Auth::user()->institution_type_plural;
    $school = Auth::user()->$institution_type;
    $news_count =$school->news()->count();
?>
<div class="ui new_news_modal basic small modal">
  <div class=" content">
    <form class="ui large form" method="POST" action="{{ url('/news') }}">
          {{ csrf_field() }}
          <div class="ui stacked secondary segment">
            <div class="ui header">
                @if($school->logo && !empty($school->logo->url))
                <img alt="{{ $school->name }} {{ env('APP_URL') }}" class="ui left floated avatar image" src="{{ $school->logo->url }}">
                @else
                <img alt="{{ $school->name }} {{ env('APP_URL') }}" class="ui left floated avatar image" src="/media/img/{{$institution_type}}_logo.jpg">
                @endif
                <h3  style="margin: 0 !important;">{{ucwords($school->name)}}</h3> 
                <div class="sub header">
                   currently has {{$news_count}} news shared
                </div>               
            </div>
            <div class="field">
                <label>Title</label>
                <input type="text" name="title" placeholder="News title.." required>
            </div>

            <div class="field">
                <label>Message</label>
                <textarea rows="6" name="message" placeholder="News body.." required></textarea>
            </div>

            <button type="submit" class="ui mini teal submit button">Share News</button>
        </div>
    </form>
  </div>
</div>
@endunless