
<div class="ui form_cover register_modal small basic modal">
	<div class="content">
		<div class="ui middle aligned center aligned stackable grid">
			<div class="two wide column"></div>
			<div class="twelve wide column">
				<div class="ui secondary segment">
					<h2 class="ui dividing header">
						Fill in your details
					</h2>
					<div id="signup_error_messages"></div>
					<form id="signup" class="ui large form">
						{{ csrf_field() }}
						<div class="ui stacked segment">
							<div class="field">
								<div class="ui left icon input">
									<i class="user icon"></i>
									<input type="text" name="name" placeholder="Full name.." required>
								</div>
							</div>
							<div class="field">
								<div class="ui left icon input">
									<i class="at icon"></i>
									<input type="text" name="email" placeholder="E-mail address" required>
								</div>
							</div>
							<div class="field">
								<div class="ui left icon input">
									<i class="lock icon"></i>
									<input type="password" name="password" placeholder="Password" required>
								</div>
							</div>
							<div class="field">
								<div class="ui left icon input">
									<i class="lock icon"></i>
									<input type="password" name="password_confirmation" placeholder="confirm Password" required>
								</div>
							</div>
							<button type="submit" class="ui fluid large teal submit button">Sign up</button>
						</div>

						<div class="ui error message"></div>

					</form>
					
					<div class="ui message">
		              Got an account? &nbsp;&nbsp; <button href="" class="ui teal mini label login_modal_button">Sign In</button>
		            </div>
					<div class="actions">
						<div class="ui black deny button">Close</div>
					</div>
				</div>
			</div>
			<div class="two wide column"></div>
		</div>
	</div>
</div>