<div class="ui rates_modal small modal">
  <div class="content">
  <h3 class="ui dividing centered header">Rates</h3>
    <div class="ui middle aligned center aligned stackable grid">
      <div class="four wide column"></div>
      <div class="eight wide column">
        <div class="ui feed">
          @foreach($post->likes as $rate)
          <div class="event">
            <div class="label">
              <img alt="<?php $user = App\User::where('id', $rate['id'])->get()->first(); echo $user->name;?> {{ env('APP_URL') }}" src="<?php $user = App\User::where('id', $rate['id'])->get()->first(); echo $user->picture? $user->picture->url: '/media/img/user.jpg'; ?>">
            </div>
            <div class="content">
              <div class="summary">
                @if($user->school_id != null)
                <a href="/profile/{{ $rate['username'] }}">{{ $rate['name'] }}</a> from <a href="/schools/<?php echo App\School::findOrFail($rate['school_id'])->slug ?>"> <?php echo App\School::findOrFail($rate['school_id'])->name ?></a>.
                @endif

                @if($user->university_id != null)
                <a href="/profile/{{ $rate['username'] }}">{{ $rate['name'] }}</a> from <a href="/universities/<?php echo App\University::findOrFail($rate['university_id'])->slug ?>"> <?php echo App\University::findOrFail($rate['university_id'])->name ?></a>.
                @endif
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      <div class="four wide column"></div>
    </div>
  </div>
  <div class="actions">
    <div class="ui deny"><i class="large remove icon circular"></i></div>
  </div>
</div>