

<div class="ui form_cove login_modal small basic modal">
  <div class="content">
    <div class="ui middle aligned center aligned stackable grid">
      <div class="two wide column"></div>
      <div class="twelve wide column">
        <div class="ui secondary segment">
            <h2 class="ui dividing header">
               Prtals Sign In
            </h2>
            <div id="signin_error_messages"></div>
            <form id="signin" class="ui large form" method="POST" action="{{ url('/signin') }}">
              {{ csrf_field() }}
              <div class="ui stacked segment">
                <div class="field">
                  <div class="ui left icon input">
                    <i class="user icon"></i>
                    <input type="text" name="email" placeholder="E-mail address">
                  </div>
                </div>
                <div class="field">
                  <div class="ui left icon input">
                    <i class="lock icon"></i>
                    <input type="password" name="password" placeholder="Password">
                  </div>
                </div>
                <div class="inline field">
                  <div class="ui toggle checkbox">
                    <input type="checkbox" tabindex="0" class="hidden" name="remember">
                    <label>Remember Me</label>
                  </div>
                </div>
                <button type="submit" class="ui fluid large teal submit button">Proceed</button>
              </div>
            </form>

            <div class="ui message">
                <a href="{{ url('/password/reset') }}">Forgot Password?</a><br>
              don't have an account? &nbsp;&nbsp; <button href="" class="ui teal mini label register_modal_button">Sign Up</button>
            </div>
            <div class="actions">
                <div class="ui black deny button">Close</div>
            </div>
        </div>
        
      </div>
      <div class="two wide column"></div>
    </div>
  </div>
</div>