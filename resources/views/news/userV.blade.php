@extends('layout.master')

@section('title', ucwords($owner->name).'\'s News | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => 'News By '.ucwords($owner->name)
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				<?php
				    $institution_type = $owner->institution_type;
				    $institution_type_plural = $owner->institution_type_plural;
				?>
				<div class="ui mini vertical pointing menu">
				  <a href="{{ url('/news') }}" class="item">
				    All News
				  </a>
				  <a href="/news/user/{{ $owner->username }}" class="active item">
				    News Posted By {{ucwords($owner->name)}}
				  </a>
				</div>
				
			</div>
			<div class="eight wide column news_threads">
				@if(empty($news->all()))
					@include('bots.news', [
						'bot_status' => true
						,'bot_main_word' => ''
						,'bot_main_phrase' => 'Share News About Your School'
						,'bot_main_sub_phrase' => ''
						,'bot_catch_phrase' => 'Sign in to write news about your school'
					])

					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  {{ucwords($owner->name)}} has not posted any news
						  <div class="sub header"></div>
						</div>
					</div>
				@else
					@include('bots.news', [
						'bot_status' => true
						,'bot_main_word' => ''
						,'bot_main_phrase' => 'Share News About Your School'
						,'bot_main_sub_phrase' => ''
						,'bot_catch_phrase' => 'Sign in to write news about your school'
					])

					@foreach($news as $news_thread)
					
						@include('partial.news_threadV')
						
					@endforeach
					{{ $news->links() }}
				@endif
					
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection