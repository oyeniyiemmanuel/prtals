@extends('layout.master')

@section('title', 'Edit News | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => 'Edit News'
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container welcome">
		<div class="ui stackable grid">
			<div class="three wide column">
				<?php
				    $institution_type = Auth::user()->institution_type;
				    $institution_type_plural = Auth::user()->institution_type_plural;
				?>
				<div class="ui dividing header">
					News
				</div>
				<div class="ui mini vertical pointing menu">
				  <a href="{{ url('/news') }}" class="item">
				    All
				  </a>
				  <a href="/news/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My School
				  </a>
				  <a href="/news/user/{{ Auth::user()->username }}" class="item">
				    Posted By Me
				  </a>
				  <a href="/news/{{ $news->slug }}" class="active item">
				    <em>{{ ucwords(substr($news->title, 0, 10 )) }}...</em>
				  </a>
				</div>
			</div>
			<div class="eight wide column">
				<div class="ui dividing header">
					Edit News: <em>{{ ucwords($news->title) }}</em>
				</div>
				@include('errors.form_valid')
				<form id="login" class="ui large form" method="POST" action="/news/{{ $news->slug }}">
		          {{ csrf_field() }}
		          <div class="ui stacked segment">
					<div class="field">
						<label>Title</label>
						<input type="text" name="title" placeholder="News title.." value="{{ $news->title }}" required>
					</div>

					<div class="field">
						<label>Message</label>
						<textarea rows="6" name="message" placeholder="News body.." required>{{ $news->message }}</textarea>
					</div>

					<input type="hidden" name="news_id" value="{{ $news->id }}">

		            <button type="submit" class="ui mini teal submit button">Update</button>
		          </div>

		          <div class="ui error message"></div>

		        </form>
			</div>
			<div class="five wide column"></div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection