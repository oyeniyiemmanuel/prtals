@extends('layout.master')

@section('title', 'News: '.ucwords( $focus_university->name ).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => ucwords($focus_university->name).' News'
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				
				<div class="ui mini vertical pointing menu">
				  <a href="{{ url('/news') }}" class="item">
				    All News
				  </a>
				  @unless(Auth::guest())
				  <a href="/news/university/{{ Auth::user()->university->slug }}" class="active item">
				    My University News
				  </a>
				  <a href="/news/user/{{ Auth::user()->username }}" class="item">
				    News Posted By Me
				  </a>
				  @endunless
				</div>
				
			</div>
			<div class="eight wide column news_threads">
				@if(empty($news->all()))
					<div class="ui centered dividing header">
						<div class="content">
						  News
						</div>
						<div class="sub header">0 in total</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No news to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
				
					<div class="ui fluid card">
						<div id="cover_wrapper" class="image">
							@if($news->first()->university->cover && !empty($news->first()->university->cover->url))
							<img alt="{{ $news->first()->university->name }} {{ env('APP_URL') }}" id="cover" src="{{ $news->first()->university->cover->url }}">
							@else
							<img alt="{{ $news->first()->university->name }} {{ env('APP_URL') }}" id="cover" src="/media/img/university_cover.jpg">
							@endif
						</div>
					</div>

					<div class="ui centered block header">
						@if($news->first()->university->logo && !empty($news->first()->university->logo->url))
						<img alt="{{ $news->first()->university->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $news->first()->university->logo->url }}">
						@else
						<img alt="{{ $news->first()->university->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/university_logo.jpg">
						@endif
						<div class="content">
						  {{ ucwords($news->first()->university->name) }} News
						</div>
					</div>

					@foreach($news as $news_thread)
					
						@include('partial.news_threadV')

					@endforeach
					{{ $news->links() }}
				@endif
					
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection