@extends('layout.master')

@section('title', ucwords($news_thread->title).' | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => ucwords($news_thread->title)
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">
				<div class="ui mini vertical pointing menu">
				  <a href="{{ url('/news') }}" class="item">
				    All News
				  </a>
				  @unless(Auth::guest() || Auth::user()->verified == 0)
				  <?php
					    $institution_type = Auth::user()->institution_type;
					    $institution_type_plural = Auth::user()->institution_type_plural;
					?>
				  <a href="/news/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My {{ucwords($institution_type)}} News
				  </a>
				  <a href="/news/user/{{ Auth::user()->username }}" class="item">
				   News Posted By Me
				  </a>
				  @endunless
				</div>
				
			</div>
			<div class="eight wide column news_threads">
				@if(empty($news_thread->all()))
					<div class="ui centered dividing header">
						<div class="content">
						  News
						</div>
						<div class="sub header">0 in total</div>
					</div>
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No news to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
					<?php
					    $news_institution_type = $news_thread->institution_type;
					    $news_institution_type_plural = $news_thread->institution_type_plural;
					?>
					<div class="ui fluid card">
						<div id="cover_wrapper" class="image">
							@if($news_thread->$news_institution_type->cover && !empty($news_thread->$institution_type->cover->url))
							<img alt="{{ $news_thread->$news_institution_type->name }} {{ env('APP_URL') }}" id="cover" src="{{ $news_thread->$news_institution_type->cover->url }}">
							@else
							<img alt="{{ $news_thread->$news_institution_type->name }} {{ env('APP_URL') }}" id="cover" src="/media/img/{{$news_institution_type}}_cover.jpg">
							@endif
						</div>
					</div>

					
					@include('partial.news_threadV')

				@endif
					
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection