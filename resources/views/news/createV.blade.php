@extends('layout.master')

@section('title', 'Create News | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	@include('partial.page_header', [
			'title' => 'Write News'
			,'trend' => ''
		])

	@include('partial.user_dashmenu')

	<div class="ui container welcome">
		<div class="ui stackable grid">
			<div class="three wide column">
				<?php
				    $institution_type = Auth::user()->institution_type;
				    $institution_type_plural = Auth::user()->institution_type_plural;
				?>
				<div class="ui dividing header">
					News
				</div>
				<div class="ui mini vertical pointing menu">
				  <a href="/news/create" class="active item">
				    Write
				  </a>
				  <a href="{{ url('/news') }}" class="item">
				    All
				  </a>
				  <a href="/news/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My School
				  </a>
				  <a href="/news/user/{{ Auth::user()->username }}" class="item">
				    Posted By Me
				  </a>
				</div>
			</div>

			<div class="eight wide column">
				@include('errors.form_valid')
				<form id="login" class="ui large form" method="POST" action="{{ url('/news') }}">
		          {{ csrf_field() }}
		          <div class="ui stacked secondary segment">
					<div class="field">
						<label>Title</label>
						<input type="text" name="title" placeholder="News title.." required>
					</div>

					<div class="field">
						<label>Message</label>
						<textarea rows="6" name="message" placeholder="News body.." required></textarea>
					</div>

		            <button type="submit" class="ui mini teal submit button">Share News</button>
		          </div>
		        </form>
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')
				
				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection