@extends('layout.master')

@section('title', 'News | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	
	@include('partial.page_header', [
			'title' => 'News'
			,'trend' => 'All'
		])

	@include('partial.user_dashmenu')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="three wide column">

				<div class="ui mini vertical pointing menu">
				  <a href="{{ url('/news') }}" class="active item">
				    All news
				  </a>
				  @unless(Auth::guest() || Auth::user()->verified == 0)
				  <?php
					    $institution_type = Auth::user()->institution_type;
					    $institution_type_plural = Auth::user()->institution_type_plural;
					?>
				  <a href="/news/{{$institution_type}}/{{ Auth::user()->$institution_type->slug }}" class="item">
				    My {{$institution_type}} News
				  </a>
				  <a href="/news/user/{{ Auth::user()->username }}" class="item">
				    Posted By Me
				  </a>
				  @endunless
				</div>
				
			</div>
			<div class="eight wide column news_threads">
				@if(empty($news->all()))
					@include('bots.news', [
						'bot_status' => true
						,'bot_main_word' => ''
						,'bot_main_phrase' => 'Share News About Your School'
						,'bot_main_sub_phrase' => ''
						,'bot_catch_phrase' => 'Sign in to write news about your school'
					])

					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No news to display
						  <div class="sub header"></div>
						</div>
					</div>
				@else
				
					@include('bots.news',  [
						'bot_status' => true
						,'bot_main_word' => ''
						,'bot_main_phrase' => 'Share News About Your School'
						,'bot_main_sub_phrase' => ''
						,'bot_catch_phrase' => 'Sign in to write news about your school'
					])

					<div class="ui items">
					@foreach($news as $news_thread)
					  @include('partial.news_threadV')
					@endforeach
					{{ $news->links() }}
					</div>
				@endif
			</div>
			<div class="five wide column">
				@include('partial.aside_schools_list')

				@include('partial.aside_users_list')
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection