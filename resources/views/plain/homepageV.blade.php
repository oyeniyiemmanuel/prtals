@extends('layout.master')

@section('title', 'Prtals: Students And Schools At Their Best')

@section('navigation_bar')
	@parent
@endsection

@section('content')
	@if (1 > 2)
		<div class="welcome_header">
	        <div class="ui stackable grid">
	        	<div class="one wide column"></div>
	        	@if(Auth::guest())
	        	<div class="ten wide column">
	        		<div>
			            <h1>Students And Schools At Their Best</h1>
			        </div>
	        	</div>
	        	<div class="four wide column">
	        		<div class="ui secondary segment">
		                <div id="signin_error_messages">
		                	@include('errors.form_valid')
		                </div>
		                <form id="signin" class="ui small fluid form" method="POST" action="{{ url('/signin') }}">
		                  {{ csrf_field() }}
		                    <div class="field">
		                      <div class="ui left icon input">
		                        <i class="user icon"></i>
		                        <input type="text" name="email" placeholder="E-mail address">
		                      </div>
		                    </div>
		                    <div class="field">
		                      <div class="ui left icon input">
		                        <i class="lock icon"></i>
		                        <input type="password" name="password" placeholder="Password">
		                      </div>
		                    </div>
		                    <div class="inline field">
		                      <div class="ui toggle checkbox">
		                        <input type="checkbox" tabindex="0" class="hidden" name="remember">
		                        <label>Remember Me</label>
		                      </div>
		                    </div>
		                    <button type="submit" class="ui fluid large teal submit button">Sign In</button>
		                </form>

		                <div class="ui message">
		                	<a href="{{ url('/password/reset') }}">Forgot Password?</a><br>
		                  don't have an account? <button href="" class="ui teal mini label register_modal_button">Sign Up</button>
		                </div>
		            </div>
	        	</div>
	        	@else
	        	<div class="sixteen wide column">
	        		<div>
			            <h1>Students And Schools At Their Best</h1>
			        </div>
	        	</div>
	        	@endif
	        	<div class="one wide column"></div>
	        </div>

	        <div class="ui container fluid home_statistics no_side_margin">
		    	<div class="ui three small inverted statistics">
				  <div class="statistic">
				    <div class="label">
				      <i class="book icon"></i>
				    </div>
				    <div class="label">
				      Posts
				    </div>
				    <div class="value">
				      <?php
				      	echo App\Post::get()->count();
				      ?>
				    </div>
				  </div>
				  <div class="statistic">
				    <div class="label">
				      <i class="university icon"></i>
				    </div>
				    <div class="label">
				      Schools
				    </div>
				    <div class="value">
				      <?php
				      	echo App\School::get()->count();
				      ?>
				    </div>
				  </div>
				  <div class="statistic">
				    <div class="label">
				      <i class="student icon"></i>
				    </div>
				    <div class="label">
				      Students
				    </div>
				    <div class="value">
				      <?php
				      	echo App\Student::where('user_id', '!=', null)->count();
				      ?>
				    </div>
				  </div>
				</div>
		    </div>			
	    </div>
	
	
	<div class="ui container fluid features featured-schools">
		<div class="ui stackable grid">
			<div class="one wide column"></div>
			<div class="fourteen wide column">
				<?php
					$schools = App\School::inRandomOrder()->get();
				?>
				<div class="ui six doubling cards">
					@foreach($schools->take(6) as $school)
                      <a href="/schools/{{ $school->slug }}" class="card">
                        <div class="image">
                        @if($school->logo && !empty($school->logo->url))
                          <img alt="{{ $school->name }} {{ env('APP_URL') }}" src="{{ $school->logo->url }}">
                        @else
                          <img alt="{{ $school->name }} {{ env('APP_URL') }}" src="/media/img/school_logo.jpg">
                        @endif
                        </div>

                        <div class="content">
                            <div class="header" href="#">{{ ucwords(strtolower($school->name)) }}</div>
                            <div class="meta">
                              <span><em>{{ $school->address }}, {{ ucwords($school->state) }}</em></span>
                            </div>
                        </div>
                      </a>
                    @endforeach
				</div>

				<div class="ui centered card">
					<a href="{{ url('/schools') }}" class="ui teal label">See all {{ count($schools) }} Schools</a>
				</div>
				
			</div>
			<div class="one wide column"></div>
		</div>
	</div>
	
	
	<div class="ui container fluid features some-features mobile_hide no_side_margin">
		<div class="ui stackable grid">
			<div class="four wide column">
				<div class="feature">
					<img alt="prtals prtals.com" class="ui small centered image" alt="prtals feature" src="{{ url('/media/img/discuss.png') }}">
					<p class="description">
						Discuss topics from subjects like mathematics, literature etc. to specials like programming, peoms, trivia and so on. 
					</p>
				</div>
			</div>
			<div class="four wide column">
				<div class="feature">
					<img alt="prtals prtals.com" class="ui small centered image" alt="prtals feature" src="{{ url('/media/img/question.png') }}">
					<p class="description">
						Challenge others with questions, or post assignments and let other students help you
					</p>
				</div>
			</div>
			<div class="four wide column">
				<div class="feature">
					<img alt="prtals prtals.com" class="ui small centered image" alt="prtals feature" src="{{ url('/media/img/newspaper.png') }}">
					<p class="description">
						Pass important news about your school across to other schools 
					</p>
				</div>
			</div>
			<div class="four wide column">
				<div class="feature">
					<img alt="prtals prtals.com" class="ui small centered image" alt="prtals feature" src="{{ url('/media/img/debate.png') }}">
					<p class="description">
						post and share pictures about your school events like debates, quizzes etc.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="ui container fluid features news">
		<div class="ui stackable grid">
			<div class="one wide column"></div>
			<div class="seven wide column">
				<h1 class="ui mobile_hide">News</h1>
				<?php
					$news = App\News::latest()->get();
				?>
				@if(empty($news->all()))
				<div class="ui block header">
					<i class="info icon"></i>
					<div class="content">
					  No news to display
					  <div class="sub header"></div>
					</div>
				</div>
				@endif
				@foreach($news->take(2) as $news_thread)

					@include('partial.news_threadV')

				@endforeach
			</div>
			<div class="seven wide column">
				<h1 class="ui mobile_hide">Events</h1>
				<?php
					$events = App\Event::latest()->get();
				?>
				@if(empty($events->all()))
				<div class="ui block header">
					<i class="info icon"></i>
					<div class="content">
					  No event to display
					  <div class="sub header"></div>
					</div>
				</div>
				@endif
				@foreach($events->take(2) as $event_thread)

					@include('partial.event_threadV')

				@endforeach
			</div>
			<div class="one wide column"></div>
		</div>
	</div>
	@endif
	
	<div class="ui center aligned container fluid home_page_header">
		<div class="ui stackable grid">
			<div class="one wide column no_padding"></div>
			<div class="fourteen wide column">
				<h1 class="title">share what you know in your discipline of study</h1>
				<h2 class="trend">teach others, learn from them</h2>
			</div>
			<div class="one wide column no_padding"></div>
		</div>
	</div>

	<div class="ui container fluid homepage-crumbs no_side_margin" style="padding: 0 1em !important ">
		<div class="ui stackable grid">
			<div class="one wide column"></div>
			<div class="fourteen wide column">
				<br>
				<div class="ui six doubling cards">
				@foreach($field_random->take(6) as $field)
				  <a href="/posts/field/{{ $field }}" class="card">
				    <div class="image">
				      <img alt="{{ $field }} {{ env('APP_URL') }}" src="/media/img/subjects/{{ $field }}.jpg">
				    </div>

					<div class="content">
						<div class="header">{{ ucwords(str_replace('-', ' ', $field)) }}</div>
						<div class="meta">
						<?php
							$field = App\Field::where('name', '=', $field)->get()->first();

							$number = $field->posts()->count();
						?>
						  <span>{{ $number }} discussions</span>
						  @if($number != 0)
						  <div class="ui mini black button">Join Discussion</div>
						  @else
						  <div class="ui mini black button">Start Discussion</div>
						  @endif
						</div>
					</div>
				  </a>
				@endforeach
				</div>
				<div class="ui centered header"><a href="{{ url('/fields') }}" class="ui tiny basic label">VIEW ALL DISCIPLINES</a></div>
			</div>
			<div class="one wide column no_padding"></div>
			<div class="one wide column no_padding"></div>
			<div class="fourteen wide column no_padding">
				<br>
				<div class="ui six doubling cards">
				@foreach($subject_random->take(6) as $subject)
				  <a href="/posts/subject/{{ $subject }}" class="card">
				    <div class="image">
				      <img alt="{{ $subject }} {{ env('APP_URL') }}" src="/media/img/subjects/{{ $subject }}.jpg">
				    </div>

					<div class="content">
						<div class="header">{{ ucwords(str_replace('-', ' ', $subject)) }}</div>
						<div class="meta">
						<?php
							$subject = App\Subject::where('name', '=', $subject)->get()->first();

							$number = $subject->posts()->count();
						?>
						  <span>{{ $number }} discussions</span>
						  @if($number != 0)
						  <div class="ui mini black button">Join Discussion</div>
						  @else
						  <div class="ui mini black button">Start Discussion</div>
						  @endif
						</div>
					</div>
				  </a>
				@endforeach
				</div>
				<div class="ui centered header"><a href="{{ url('/subjects') }}" class="ui tiny basic label">VIEW ALL SUBJECTS</a></div>
			</div>
			<div class="one wide column no_padding"></div>
		</div>
		<br><br>
	</div>

	<div class="ui container fluid features news homepage-crumbs no_side_margin" style="padding:1em !important; padding-bottom: 2% !important; ">
		<div class="ui stackable grid">
			<div class="one wide column no_padding"></div>
			<div class="six wide column">
				<div class="ui segment">
					<?php
						$news = App\News::inRandomOrder()->get();
					?>
					@if(empty($news->all()))
					<div class="ui block header">
						<i class="info icon"></i>
						<div class="content">
						  No news to display
						  <div class="sub header"></div>
						</div>
					</div>
					@endif

					<a href="{{ url('/news') }}">
	                    <b>Check All News</b>
	                    <i class="right arrow icon"></i>
	                </a>
					@foreach($news->take(2) as $news_thread)

						<?php
						    $news_institution_type = $news_thread->institution_type;
						    $news_institution_type_plural = $news_thread->institution_type_plural;
						?>
						<div class="ui secondary segment">
							<div class="ui items">

							  <div class="item news_thread">
							    <div class="content">
							      <div class="header">{{ ucwords(strtolower($news_thread->title)) }}</div>
							      <div class="meta">
							      	<a href="/{{$news_institution_type_plural}}/{{ $news_thread->$news_institution_type->slug }}" class="">
							        	@if($news_thread->$news_institution_type->logo && !empty($news_thread->$news_institution_type->logo->url))
										<img alt="{{ $news_thread->$news_institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" src="{{ $news_thread->$news_institution_type->logo->url }}">
										@else
										<img alt="{{ $news_thread->$news_institution_type->name }} {{ env('APP_URL') }}" class="ui avatar image" src="/media/img/{{$news_institution_type}}_logo.jpg">
										@endif 
							        	{{ ucwords($news_thread->$news_institution_type->name) }}, {{ $news_thread->$news_institution_type->state }}
							        </a>
							        <span class="right floated stay">{{ $news_thread->created_at->diffForHumans() }}</span>
							      </div>
							      <div class="description">
							      	@if(strlen($news_thread->message) < 100)
							        <p>
							        	{{ $news_thread->message }}
							        </p>
							        @else
							        <p>
							        	{{ substr($news_thread->message, 0, 100) }}...
							        </p>
							        <a href="{{ url('/news') }}/{{$news_thread->slug}}" class="ui tiny basic label button">read more <i class="ui angle double right icon"></i></a>
							        @endif
							      </div>
							      <div class="extra">
							      	<span class="ui right floated">
							      		<em>reported by</em>
										<a href="/profile/{{ $news_thread->user->username }}" class="">
								        	<?php $name = explode(' ', $news_thread->user->name); echo ucfirst(end($name)); ?>
								        </a>	      		
							      	</span>

							        @if(Auth::id() == $news_thread->user_id)
							        <a href="/news/{{ $news_thread->slug }}/edit">edit</a>
							        <a class="delete_news">delete</a>
							        <input type="hidden" name="news_id" value="{{ $news_thread->id }}">
							        {{ csrf_field() }}
							        @endif
							      </div>
							    </div>
							  </div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
			<div class="four wide column">
				<div class="ui segment">
					@include('partial.aside_schools_list')
				</div>
			</div>
			<div class="four wide column">
				<div class="ui segment">
					@include('partial.aside_users_list')
				</div>
			</div>
			<div class="one wide column"></div>
		</div>
	</div>

	<div class="ui container fluid no_side_margin" style="background: #023B42; padding-right: 1em !important; padding-left: 1em !important; padding-bottom: 2% !important;">
		<div class="ui stackable grid">
			<div class="two wide column no_padding"></div>
			<div class="twelve wide column">
				<div class="ui raised segment">
					<form class="ui big form" method="GET" action="{{ url('/search') }}/university">
						{{ csrf_field() }}
						<div class="field">
							<div class="ui left icon input">
								<i class="search icon"></i>
								<input type="search" name="query" placeholder="Find Your School">
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="two wide column"></div>
		</div>
	</div>

	<div class="ui container fluid features homepage-crumbs no_side_margin">
		@if(1>2)
		<div class="ui stackable grid ">
			<div class="four wide column">
				<div class="ui fluid card">
				  <div class="content">
				    <h4 class="ui sub header">hey there, you wouldn't be here if you're not smart</h4>
				  </div>
				  <div class="extra content">
				    <button class="ui teal button">Share your knowledge with others</button>
				  </div>
				</div>
			</div>
			<div class="four wide column">
				<div class="ui fluid card">
				  <div class="content">
				    <h4 class="ui sub header">Prtals community is growing</h4>
				  </div>
				  <div class="extra content">
				    <button class="ui teal button">Be the first to register in your school</button>
				  </div>
				  <div class="extra content">
				      This makes you the school-admin
				  </div>
				</div>
			</div>
			<div class="four wide column"></div>
			<div class="four wide column"></div>
		</div>
		@endif
		
		@include('partial.homepage_posts')
	</div>
	
@endsection

@section('footer')
	@parent
@endsection