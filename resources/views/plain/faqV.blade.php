@extends('layout.master')

@section('title', 'Guide | Prtals')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<div class="ui container">
		<div class="ui stackable grid">
			<div class="ten wide column">
				<div class="ui huge list">
					<div class="item">
						<i class="help icon"></i>
						<div class="content">
							<a class="header">"Verification Failed" when i input my registration number</a>
							<div class="description">
								Make sure your registration number is correct. Select your proper role.
								Also check if another "role-mate" from your school hasn't verified with your registration number. you can confirm via the error messages displayed.
							</div>
						</div>
					</div>

					<div class="item">
						<i class="help icon"></i>
						<div class="content">
							<a class="header">Question</a>
							<div class="description">
								Answer
							</div>
						</div>
					</div>

					<div class="item">
						<i class="help icon"></i>
						<div class="content">
							<a class="header">Question</a>
							<div class="description">
								Answer
							</div>
						</div>
					</div>

					<div class="item">
						<i class="help icon"></i>
						<div class="content">
							<a class="header">Question</a>
							<div class="description">
								Answer
							</div>
						</div>
					</div>

					<div class="item">
						<i class="help icon"></i>
						<div class="content">
							<a class="header">Question</a>
							<div class="description">
								Answer
							</div>
						</div>
					</div>

					<div class="item">
						<i class="help icon"></i>
						<div class="content">
							<a class="header">Question</a>
							<div class="description">
								Answer
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="six wide column"></div>
		</div>
	</div>

@endsection

@section('footer')
	@parent
@endsection