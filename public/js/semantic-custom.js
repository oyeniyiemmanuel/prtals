  $(document)
    .ready(function() {
      // fix menu when passed
      $('.masthead')
        .visibility({
          once: false,
          onBottomPassed: function() {
            $('.fixed.menu').transition('fade in');
          },
          onBottomPassedReverse: function() {
            $('.fixed.menu').transition('fade out');
          }
        });
      // create sidebar and attach to menu open
      $('.ui.sidebar').sidebar('attach events', '.toc.item');

        //dropdown
        $('.ui.dropdown')
          .dropdown({
            on: 'hover'
          });

        $(".post_threads").on("mouseover", ".ui.dropdown.post_share_btn", function(){
            $('.ui.dropdown')
              .dropdown({
                on: 'hover'
              });
        });

        // new post modal
        $('.new_post_modal.modal')
          .modal('attach events', '.new_post_modal_button', 'show');

        // new news modal
        $('.new_news_modal.modal')
          .modal('attach events', '.new_news_modal_button', 'show');

        // login modal
        $('.login_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.login_modal_button', 'show');

        // register modal
        $('.register_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.register_modal_button', 'show');

        // school_cover modal
        $('.school_cover_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.school_cover_modal_button', 'show');

        // school_logo modal
        $('.school_logo_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.school_logo_modal_button', 'show');

        // university_cover modal
        $('.university_cover_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.university_cover_modal_button', 'show');

        // university_logo modal
        $('.university_logo_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.university_logo_modal_button', 'show');

        // school_anthem modal
        $('.school_anthem_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.school_anthem_modal_button', 'show');

        // school_history modal
        $('.school_history_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.school_history_modal_button', 'show');

        // school_tuition modal
        $('.school_tuition_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.school_tuition_modal_button', 'show');

        // school_accomplishments modal
        $('.school_accomplishments_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.school_accomplishments_modal_button', 'show');

        // university_history modal
        $('.university_history_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.university_history_modal_button', 'show');

        // university_tuition modal
        $('.university_tuition_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.university_tuition_modal_button', 'show');

        // university_accomplishments modal
        $('.university_accomplishments_modal.modal')
          .modal({
            inverted: true
          })
          .modal('setting', 'transition', 'vertical flip')
          .modal('attach events', '.university_accomplishments_modal_button', 'show');

        // comments modal
        $('.post_threads').on('click', '.comments_modal_button', function(){
            $(this).next()
            .modal({
              detachable: false
            })
            .modal('show');
        });

        // rates modal
        $('.post_threads').on('click', '.rates_modal_button', function(){
            $(this).next()
            .modal({
              detachable: false
            })
            .modal('show');
        });

        // navbar sticky
        $('.ui.sticky')
          .sticky({
            pushing: true,
            context: '#context',
            observeChanges: true,
            //offset       : 1,
            //bottomOffset : 50,
            onStick: function() {
                $('.ui.small.attached.menu')
                .show();
            },
            /*onUnstick: function() {
                $('.ui.small.attached.menu')
                .transition()
                .transition('fade down');
            }*/
          });

        // close Message
        $('.message .close')
          .on('click', function() {
            $(this)
              .closest('.message')
              .transition('fade')
            ;
          });

        // checkbox
        $('.ui.checkbox').checkbox();

        // user_index dashmenu popup
        $('.menu .browse')
          .popup({
            inline     : true,
            hoverable  : true,
            position   : 'bottom left',
            delay: {
              show: 100,
              hide: 800
            }
          });

        // toggle add comment textarea
        $('.post_threads').on('click', '.add_comment', function () {
          $(this).parent().parent().find('.ui.post_comments')
          .transition('vertical flip', '500ms')
          .transition('clear queue');
        });

        // toggle send message textarea
        $('.messages_threads').on('click', '.write_user', function () {
          $(this).parent().parent().find('.ui.show_message_form')
          .transition('vertical flip', '500ms')
          .transition('clear queue');
        });
        // toggle user edit bio textarea
        $('.user_bio').on('click', '.edit_trigger', function () {
          $(this).parent().find('.add_bio')
          .transition('vertical flip', '200ms')
          .transition('clear queue');
        });
        // toggle user edit socials textarea
        $('.user_socials').on('click', '.edit_trigger', function () {
          $(this).parent().find('.add_socials')
          .transition('vertical flip', '200ms')
          .transition('clear queue');
        });

        // post actions popup
        $('.post_btn')
        .popup({
          on: 'hover'
        });
        // for share btn
        $('.post_share_btn')
        .popup({
          transition: 'horizontal flip'
        })
        .popup({
          on: 'hover'
        });

        $('.activating.element')
          .popup(
            {
              inline     : true,
              hoverable  : true,
              position   : 'bottom left',
              delay: {
                show: 100,
                hide: 800
              }
            }
            );

        // profile image "edit photo" on hover
        $('.special.cards .image').dimmer({
          on: 'hover'
        });
        
    });