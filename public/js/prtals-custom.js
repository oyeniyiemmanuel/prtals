jQuery(document).ready(function(){ 
    // initialize photoswipe
    $('.event_pictures').photoSwipe();

    $('#choose_file').inputFileText({ 
        text: 'change pic' 
        ,buttonClass: 'ui mini inverted teal basic button' 
    });

    $('#choose_cover').inputFileText({ 
        text: 'change cover' 
        ,buttonClass: 'ui mini inverted teal basic button' 
    });

    $('#choose_logo').inputFileText({ 
        text: 'change logo' 
        ,buttonClass: 'ui mini inverted teal basic button' 
    });

    $('.private_thread').scrollTop($('.private_thread').height());

    // register
    $('.form_cover').on('submit', 'form', function(){
        // Getting the link id
        var form_id = $(this).attr('id');
        var name = $(this).find("input[name='name']").val();
        var email = $(this).find("input[name='email']").val();
        var password = $(this).find("input[name='password']").val();
        var password_confirmation = $(this).find("input[name='password_confirmation']").val();
        var  href = '/' + form_id;
        var _token = $("input[name='_token']").val();

        //preloader
        $('#' + form_id + '_error_messages').html('<div class="ui"><div class="ui active inverted dimmer"><div class="ui text loader">Verifying..</div></div></div>');

        $.ajax({
            type: "POST",
            data: $(this).serializeArray(),
            url: href,
            dataType: 'json',
            cache: false,
            success: function (data) {  
                //console.log('success');
            },
            error: function(data){
                // Displaying if there are any errors
                var errors = data.responseJSON;
                //console.log('server errors', errors);
                if(errors == null){
                    window.location = '/dashboard';
                }

                $.each( errors , function( key, value ) {
                    $('#' + form_id + '_error_messages').html('');
                    $('#' + form_id + '_error_messages').html('<div class="ui message"><p><i class="warning sign icon"></i>' + value + '</p></div');
                });
                
            }
        });
        return false;
    });

    // remove teacher duty
    $('.duties_table').on('click', '.remove_duty', function(){

        var duty_row = $(this).parent().parent().parent().parent().parent().parent().parent();
        var duty_id = $(this).parent().find("input[name='duty_id']").val();
        var _token = $(this).parent().find("input[name='_token']").val();
        var href = '/teacher/remove_duty';

        swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              showCancelButton: true,
              confirmButtonColor: 'teal',
              cancelButtonColor: 'grey',
              confirmButtonText: 'Yes, delete it',
              showLoaderOnConfirm: true,
              closeOnConfirm: true
            },function () {

                $.ajax({
                    type: "POST",
                    data: {
                        duty_id: duty_id
                        ,_token: _token
                    },
                    url: href,
                    dataType: 'html',
                    cache: false,
                    success: function (data) {  
                        location.reload();
                    },
                    error: function(jqXHR, text, error){
                        // Displaying if there are any errors
                        console.log(error);           
                    }
                });
            });
        

        return false;
    });

    // delete post
    $('.post_threads').on('click', '.delete_post', function(){

        var post_thread = $(this).parent().parent().parent();
        var post_id = $(this).parent().find("input[name='post_id']").val();
        var _token = $(this).parent().find("input[name='_token']").val();
        var href = '/posts/' + post_id;

        swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              showCancelButton: true,
              confirmButtonColor: 'teal',
              cancelButtonColor: 'grey',
              confirmButtonText: 'Yes, delete it',
              showLoaderOnConfirm: true,
              closeOnConfirm: true
            },function () {

                $.ajax({
                    type: "DELETE",
                    data: {
                        post_id: post_id
                        ,_token: _token
                        ,_method: 'delete'
                    },
                    url: href,
                    dataType: 'html',
                    cache: false,
                    success: function (data) {  
                        post_thread.slideUp(100);
                    },
                    error: function(jqXHR, text, error){
                        // Displaying if there are any errors
                        console.log(error);           
                    }
                });
            });
        

        return false;
    });

    // delete message_thread
    $('.messages_threads').on('click', '.delete_thread', function(){

        var delete_button = $(this);
        var br1 = $(this).next();
        var br2 = $(this).next().next();
        var thread = $(this).prev();
        var thread_id = $(this).find("input[name='thread_id']").val();
        var _token = $(this).find("input[name='_token']").val();
        var href = '/messages/' + thread_id;

        swal({
              title: 'Are you sure?',
              text: "Messages will re-appear if the recipient keeps sending on this thread",
              showCancelButton: true,
              confirmButtonColor: 'teal',
              cancelButtonColor: 'grey',
              confirmButtonText: 'Yes, delete anyway',
              showLoaderOnConfirm: true,
              closeOnConfirm: true
            },function () {

                $.ajax({
                    type: "DELETE",
                    data: {
                        thread_id: thread_id
                        ,_token: _token
                        ,_method: 'delete'
                    },
                    url: href,
                    dataType: 'html',
                    cache: false,
                    success: function (data) {  
                        thread.slideUp(100);
                        delete_button.slideUp(100);
                        br1.slideUp(100);
                        br2.slideUp(100);
                        console.log(data);
                    },
                    error: function(jqXHR, text, error){
                        // Displaying if there are any errors
                        console.log(error);           
                    }
                });
            });
        

        return false;
    });

    // delete news
    $('.news_thread').on('click', '.delete_news', function(){

        var news_thread = $(this).parent().parent().parent().parent().parent();
        var news_id = $(this).parent().find("input[name='news_id']").val();
        var _token = $(this).parent().find("input[name='_token']").val();
        var href = '/news/' + news_id;

        swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              showCancelButton: true,
              confirmButtonColor: 'teal',
              cancelButtonColor: 'grey',
              confirmButtonText: 'Yes, delete it',
              showLoaderOnConfirm: true,
              closeOnConfirm: true
            },function () {

                $.ajax({
                    type: "DELETE",
                    data: {
                        news_id: news_id
                        ,_token: _token
                        ,_method: 'delete'
                    },
                    url: href,
                    dataType: 'html',
                    cache: false,
                    success: function (data) {  
                        news_thread.slideUp(100);
                    },
                    error: function(jqXHR, text, error){
                        // Displaying if there are any errors
                        console.log(error);           
                    }
                });
            });
        

        return false;
    });

    // delete announcement
    $('.announcement_thread').on('click', '.delete_announcement', function(){

        var announcement_thread = $(this).parent().parent().parent().parent();
        var announcement_id = $(this).parent().find("input[name='announcement_id']").val();
        var _token = $(this).parent().find("input[name='_token']").val();
        var href = '/announcements/' + announcement_id;

        swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              showCancelButton: true,
              confirmButtonColor: 'teal',
              cancelButtonColor: 'grey',
              confirmButtonText: 'Yes, delete it',
              showLoaderOnConfirm: true,
              closeOnConfirm: true
            },function () {

                $.ajax({
                    type: "DELETE",
                    data: {
                        announcement_id: announcement_id
                        ,_token: _token
                        ,_method: 'delete'
                    },
                    url: href,
                    dataType: 'html',
                    cache: false,
                    success: function (data) {  
                        announcement_thread.slideUp(100);
                    },
                    error: function(jqXHR, text, error){
                        // Displaying if there are any errors
                        console.log(error);           
                    }
                });
            });
        

        return false;
    });

    // delete event
    $('.event_thread').on('click', '.delete_event', function(){

        var event_thread = $(this).parent().parent().parent().parent();
        var event_id = $(this).parent().find("input[name='event_id']").val();
        var _token = $(this).parent().find("input[name='_token']").val();
        var href = '/events/' + event_id;

        swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              showCancelButton: true,
              confirmButtonColor: 'teal',
              cancelButtonColor: 'grey',
              confirmButtonText: 'Yes, delete it',
              showLoaderOnConfirm: true,
              closeOnConfirm: false
            },function () {

                $.ajax({
                    type: "DELETE",
                    data: {
                        event_id: event_id
                        ,_token: _token
                        ,_method: 'delete'
                    },
                    url: href,
                    dataType: 'html',
                    cache: false,
                    success: function (data) {  
                        event_thread.slideUp(100);
                        swal({
                            title: 'Event deleted',
                            confirmButtonColor: 'teal',
                            timer: 1000
                        });
                    },
                    error: function(jqXHR, text, error){
                        // Displaying if there are any errors
                        console.log(error);           
                    }
                });
            });
        

        return false;
    });
    
    // check username
    $('.edit_user_details').on('keyup', '#username_field', function(){
        // Getting the link id
        var input = $(this);
        var username = $(this).val();
        var href = '/profile/username/' + username + '/check';
        var _token = $(this).parent().parent().parent().parent().find("input[name='_token']").val();
        var submit_button = $(this).parent().parent().parent().parent().find("button[type='submit']");

        $.ajax({
            type: "GET",
            data: {
                username: username
                ,_token: _token
                ,_method: 'delete'
            },
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {  
                //console.log(data);

                if(data == 'you'){
                    $(input).prev().attr('class', 'hand peace icon');
                    $(input).parent().parent().attr('data-tooltip', 'Your current username');
                    $(submit_button).attr('class', 'ui mini teal submit button');
                }else if(data == 'taken'){
                    $(input).prev().attr('class', 'warning sign icon');
                    $(input).parent().parent().attr('data-tooltip', 'taken');
                    $(submit_button).attr('class', 'ui mini disabled button');
                    //console.log(submit_button);
                }else{
                    $(input).prev().attr('class', 'checkmark icon');
                    $(input).parent().parent().attr('data-tooltip', 'available');
                    $(submit_button).attr('class', 'ui mini teal submit button');
                }
                
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);           
            }
        });

        return false;
    });

    // rate post
    $('.post_threads').on('click', '.rate_post', function(){
        // Getting the link id
        var post_thread = $(this).parent().parent().parent();
        var post_id = $(this).parent().find("input[name='post_id']").val();
        var _token = $(this).parent().find("input[name='_token']").val();
        var href = '/posts/like/' + post_id;

        $.ajax({
            type: "GET",
            data: {
                post_id: post_id
                ,_token: _token
                ,_method: 'delete'
            },
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {  

                $(post_thread).html(data);
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);           
            }
        });

        return false;
    });

    // add socials
    $('.user_socials').on('submit', '.add_socials', function(){
        // Getting the link id
        var user_socials = $(this).parent();
        var user_id = $(this).find("input[name='user_id']").val();
        var href = '/profile/socials/'+ user_id +'/edit';

        //preloader
        $(this).prev().html('updating socials..');

        $.ajax({
            type: "POST",
            data: $(this).serializeArray(),
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {  

                $(user_socials).html(data);
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);           
            }
        });

        return false;
    });

    // add bio
    $('.user_bio').on('submit', '.add_bio', function(){
        // Getting the link id
        var user_bio = $(this).parent();
        var user_id = $(this).find("input[name='user_id']").val();
        var href = '/profile/bio/'+ user_id +'/edit';

        //preloader
        $(this).prev().html('updating bio..');

        $.ajax({
            type: "POST",
            data: $(this).serializeArray(),
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {  

                $(user_bio).html(data);
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);           
            }
        });

        return false;
    });

    // add comment
    $('.post_threads').on('submit', '.add_comment', function(){
        // Getting the link id
        var post_thread = $(this).parent().parent().parent().parent().parent();
        var href = '/comments';

        //preloader
        $(this).parent().html('adding comment..');

        $.ajax({
            type: "POST",
            data: $(this).serializeArray(),
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {  

                $(post_thread).html(data);
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);           
            }
        });

        return false;
    });

    // like comment
    $('.post_threads').on('click', '.like_comment', function(){
        // Getting the link id
        var comment_thread = $(this).parent().parent().parent();
        var post_id = $(this).parent().parent().find("input[name='post_id']").val();
        var comment_id = $(this).parent().parent().find("input[name='comment_id']").val();
        var _token = $(this).parent().parent().find("input[name='_token']").val();
        var href = '/comments/like/' + post_id + '/' + comment_id;

        $.ajax({
            type: "GET",
            data: {
                post_id: post_id
                ,comment_id: comment_id
                ,_token: _token
                ,_method: 'delete'
            },
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {
                comment_thread.html(data);
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);           
            }
        });

        return false;
    });

    // delete comment
    $('.post_threads').on('click', '.delete_comment', function(){
        // Getting the link id
        var comment_thread = $(this).parent().parent().parent().parent();
        var modal = comment_thread.parent().parent().parent().parent().parent();
        var post_thread = modal.parent().parent().parent();
        var comment_id = $(this).parent().parent().find("input[name='comment_id']").val();
        var _token = $(this).parent().parent().find("input[name='_token']").val();
        var href = '/comments/' + comment_id;

        //preloader
        $(this).parent().html('deleting comment..');

        $.ajax({
            type: "DELETE",
            data: {
                comment_id: comment_id
                ,_token: _token
                ,_method: 'delete'
            },
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {

                comment_thread.slideUp(100);

                $(modal).modal('hide');

                post_thread.html(data); 

                
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);           
            }
        });

        return false;
    });

    // mark notifications as read
    $('.dashmenu').on('mouseover', '.mark_as_read', function(){
        // Getting the link id
        var  user_id = $(this).attr('id');
        var  href = '/users/' + user_id + '/notifications';
        var _token = $(this).data("token");

        $.ajax({
            type: "DELETE",
            data: {
                user_id: user_id
                ,_token: _token
                ,_method: 'delete'
            },
            url: href,
            dataType: 'html',
            cache: false,
            success: function (data) {  
                // clear new notification 
                $('#'+user_id).find('.label').detach();
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);          
            }
        });
        return false;
    });

    // upload profile image
    $('.user_profile').on('change', 'input[type=file]', function(){
        // Getting the link id
        var $fileInput = $(this),
                file = this.files[0];
        var  formData = new FormData;
        formData.append('image', file);
        var href = '/profile/image_upload';
        var _token = $(this).parent().parent().find("input[name='_token']").val();
        var display = $(this).parent().parent().parent().parent().parent();

        $(display).html('<div class="ui"><div class="ui active inverted dimmer"><div class="ui text loader">uploading..</div></div></div>');

        $.ajax({
            type: "POST",
            data: formData,
            url: href,
            headers: {
                "X-CSRF-TOKEN": _token
            },
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {  
                // clear new notification 
                window.location = '/profile';
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);          
            }
        });

        return false;
    });

    // upload school cover
    $('.school_cover_modal').on('change', 'input[type=file]', function(){
        // Getting the link id
        var $fileInput = $(this),
                file = this.files[0];
        var  formData = new FormData;
        formData.append('image', file);
        var href = '/school_cover_image';
        var slug = $(this).parent().parent().find("input[name='slug']").val();
        var _token = $(this).parent().parent().find("input[name='_token']").val();
        var modal = $(this).parent().parent().parent().parent().parent();

        $(modal).modal('hide');
        $('#cover').parent().html('<div id="cover" class="ui"><div class="ui active dimmer"><div class="ui text loader">uploading cover..</div></div></div>');

        $.ajax({
            type: "POST",
            data: formData,
            url: href,
            headers: {
                "X-CSRF-TOKEN": _token
            },
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {  
                // clear new notification 
                window.location = '/schools/' + slug;
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);          
            }
        });

        return false;
    });

    // upload university cover
    $('.university_cover_modal').on('change', 'input[type=file]', function(){
        // Getting the link id
        var $fileInput = $(this),
                file = this.files[0];
        var  formData = new FormData;
        formData.append('image', file);
        var href = '/university_cover_image';
        var slug = $(this).parent().parent().find("input[name='slug']").val();
        var _token = $(this).parent().parent().find("input[name='_token']").val();
        var modal = $(this).parent().parent().parent().parent().parent();

        $(modal).modal('hide');
        $('#cover').parent().html('<div id="cover" class="ui"><div class="ui active dimmer"><div class="ui text loader">uploading cover..</div></div></div>');

        $.ajax({
            type: "POST",
            data: formData,
            url: href,
            headers: {
                "X-CSRF-TOKEN": _token
            },
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {  
                // clear new notification 
                window.location = '/universities/' + slug;
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);          
            }
        });

        return false;
    });

    // upload school logo
    $('.school_logo_modal').on('change', 'input[type=file]', function(){
        // Getting the link id
        var $fileInput = $(this),
                file = this.files[0];
        var  formData = new FormData;
        formData.append('image', file);
        var href = '/school_logo';
        var slug = $(this).parent().parent().find("input[name='slug']").val();
        var _token = $(this).parent().parent().find("input[name='_token']").val();
        var modal = $(this).parent().parent().parent().parent().parent();

        $(modal).modal('hide');
        $('#logo').parent().html('<div id="logo" class="ui"><div class="ui active dimmer"><div class="ui text loader">uploading logo..</div></div></div>');

        $.ajax({
            type: "POST",
            data: formData,
            url: href,
            headers: {
                "X-CSRF-TOKEN": _token
            },
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {  
                // clear new notification 
                window.location = '/schools/' + slug;
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);          
            }
        });

        return false;
    });

    // upload university logo
    $('.university_logo_modal').on('change', 'input[type=file]', function(){
        // Getting the link id
        var $fileInput = $(this),
                file = this.files[0];
        var  formData = new FormData;
        formData.append('image', file);
        var href = '/university_logo';
        var slug = $(this).parent().parent().find("input[name='slug']").val();
        var _token = $(this).parent().parent().find("input[name='_token']").val();
        var modal = $(this).parent().parent().parent().parent().parent();

        $(modal).modal('hide');
        $('#logo').parent().html('<div id="logo" class="ui"><div class="ui active dimmer"><div class="ui text loader">uploading logo..</div></div></div>');

        $.ajax({
            type: "POST",
            data: formData,
            url: href,
            headers: {
                "X-CSRF-TOKEN": _token
            },
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {  
                // clear new notification 
                window.location = '/universities/' + slug;
            },
            error: function(jqXHR, text, error){
                // Displaying if there are any errors
                console.log(error);          
            }
        });

        return false;
    });

});

// homepage masonry initialization
var runMasonry = function(){
       $('.ui.stackable.grid .homepage-grid').masonry({
        columnWidth: '.grid-item',
        itemSelector: '.grid-item',
        percentPosition: true
       }); 
}; 

runMasonry();

// initialize infinite ajax scroll for homepage items
if(($('.grid-item').length > 0) && $('.infinite-scroll')[0]){
    var ias = jQuery.ias({
        container: '.infinite-scroll',
        item: '.grid-item',
        pagination: '.pagination',
        next: 'li.active + li a'
    });

    ias.extension(new IASSpinnerExtension({
        html: '<div class="ui active centered inline loader"></div>'
    }));
    //ias.extension(new IASTriggerExtension({offset: 4}));
    ias.extension(new IASNoneLeftExtension({text: "No more posts"}));
    ias.on('loaded', function(data, items){
        $('.ui.stackable.grid .homepage-grid').imagesLoaded( function(){
           $('.ui.stackable.grid .homepage-grid').masonry('reloadItems').masonry({
            columnWidth: '.grid-item',
            itemSelector: '.grid-item',
            percentPosition: true
           }); 
        });
    });
    console.log('grid items');
}


// initialize infinite ajax scroll for posts items
if(($('.post_thread').length > 0) && $('.post_threads')[0]){
    var ias_posts = jQuery.ias({
        container: '.post_threads',
        item: '.post_thread',
        pagination: '.pagination',
        next: 'li.active + li a'
    });

    ias_posts.extension(new IASSpinnerExtension({
        html: '<div class="ui active centered inline loader"></div>'
    }));
    ias_posts.extension(new IASTriggerExtension({offset: 2, text: 'load more posts'}));
    ias_posts.extension(new IASNoneLeftExtension({text: "No more posts"}));
    ias_posts.on('loaded', function(data, items){
        $('.ui.stackable.grid .homepage-grid').imagesLoaded( function(){
           //console.log('loaded'); 
        });
    });
    console.log('post items');
}

// initialize infinite ajax scroll for events items
if(($('.event_thread').length > 0) && $('.event_threads')[0]){
    var ias_events = jQuery.ias({
        container: '.event_threads',
        item: '.event_thread',
        pagination: '.pagination',
        next: 'li.active + li a'
    });

    ias_events.extension(new IASSpinnerExtension({
        html: '<div class="ui active centered inline loader"></div>'
    }));
    ias_events.extension(new IASTriggerExtension({offset: 2, text: 'load more events'}));
    ias_events.extension(new IASNoneLeftExtension({text: "No more events"}));
    ias_events.on('loaded', function(data, items){
        $('.ui.stackable.grid .homepage-grid').imagesLoaded( function(){
           //console.log('loaded'); 
        });
    });
    console.log('event items');
}

// initialize infinite ajax scroll for news items
if(($('.news_thread').length > 0) && $('.news_threads')[0]){
    var ias_news = jQuery.ias({
        container: '.news_threads',
        item: '.news_thread',
        pagination: '.pagination',
        next: 'li.active + li a'
    });

    ias_news.extension(new IASSpinnerExtension({
        html: '<div class="ui active centered inline loader"></div>'
    }));
    ias_news.extension(new IASTriggerExtension({offset: 2, text: 'load more news'}));
    ias_news.extension(new IASNoneLeftExtension({text: "No more news"}));
    ias_news.on('loaded', function(data, items){
        $('.ui.stackable.grid .homepage-grid').imagesLoaded( function(){
           //console.log('loaded'); 
        });
    });
    console.log('news items');
}

// initialize infinite ajax scroll for news items
if(($('.announcement_thread').length > 0) && $('.announcement_threads')[0]){
    var ias_announcement = jQuery.ias({
        container: '.announcement_threads',
        item: '.announcement_thread',
        pagination: '.pagination',
        next: 'li.active + li a'
    });

    ias_announcement.extension(new IASSpinnerExtension({
        html: '<div class="ui active centered inline loader"></div>'
    }));
    ias_announcement.extension(new IASTriggerExtension({offset: 2, text: 'load more announcements'}));
    ias_announcement.extension(new IASNoneLeftExtension({text: "No more announcements"}));
    ias_announcement.on('loaded', function(data, items){
        $('.ui.stackable.grid .homepage-grid').imagesLoaded( function(){
           //console.log('loaded'); 
        });
    });
    console.log('announcements items');
}
// initialize infinite ajax scroll for search result items
if(($('.user_search_results').length > 0) && $('.user_search_result')[0]){
    var ias_search = jQuery.ias({
        container: '.user_search_results',
        item: '.user_search_result',
        pagination: '.pagination',
        next: 'li.active + li a'
    });

    ias_search.extension(new IASSpinnerExtension({
        html: '<div class="ui active centered inline loader"></div>'
    }));
    ias_search.extension(new IASTriggerExtension({offset: 2, text: 'load more results'}));
    ias_search.extension(new IASNoneLeftExtension({text: "No more results"}));
    ias_search.on('loaded', function(data, items){
        $('.ui.stackable.grid .homepage-grid').imagesLoaded( function(){
           //console.log('loaded'); 
        });
    });
    console.log('search results items');
}

//SET CURSOR POSITION
$.fn.setCursorPosition = function(pos) {
  this.each(function(index, elem) {
    if (elem.setSelectionRange) {
      elem.setSelectionRange(pos, pos);
    } else if (elem.createTextRange) {
      var range = elem.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  });
  return this;
};

var searchInput = $('.set_cursor');

var strLength = searchInput.val().length * 2;

searchInput.setCursorPosition(strLength);